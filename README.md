This is a toolkit for the acquisition, processing and analysis of multiple 
streams of remote sensing imagery for the detection of active fire.

## Usage

### ahi.py


**_AHILocation(loc, res)_**: tool for conversion of AHI local coordinates to geographicals
(latitude, longitude) and vice-versa.

**Inputs:**    
*loc* - tuple or list containing coordinates - either latitude, longitude or
AHI line, pixel    
*res* - resolution of the AHI image you require coordinates for/from.

**Attributes:**    
*.line,.pixel* - AHI coordinates of the location    
*.latitude,.longitude* - WGS84 coordinates of the location    
*.time_offset* - time offset of location from UTC (minutes)    

**_AHICapture(time)_**: class object for handling a single instance of capture from the
AHI satellite. Allows access to attributes of each capture along with access to
individual images via the AHIImage class.

**Inputs:**    
*time* - tuple or list containing either 4 (YYYY,JJJ,hh,mm) or 5 (YYYY,MM,DD,hh,mm)
components, depending upon the input information used.

**Attributes:**    
*.dt* - datetime object with capture attributes    
*.year,.month,.day,.jday,.hour,.minute* - exploded form of the datetime object    
*.files_avail* - dict containing relevant file names and availability on the local system

**Properties:**    
*class.bandname* - returns an AHIImage object relevant to the supplied bandname
e.g.: bandx or bandxx will return an AHIImage object for the given band referencing 
the 2km resolution brightness temperature (OBS) version of the AHI observation. 
For bands with BRF products, append 'r' after the band number to access these files,
and for bands with multiple resolutions, append the desired resolution after the 'r'.
For example AHICapture(time).band3r500 will return an AHIImage class object pointing to
the 500m AHI Band 3 product for the time of capture. This method also gives access 
to cloud products by changing the bandname - cldbom will give access to the BOM AHI
cloud product if it exists locally.

**_AHIImage(fpath)_**: object for the handling of data from an AHI image.

**Inputs:**    
*fpath* - path to the relevant file

**Attributes:**    
*.obs_type* - type of observation that the file provides (visible/IR bands can be 
BRDF or scaled radiance, MWIR/TIR are brightness temperature, also handles SOLAR files).    
*.longitude,.latitude* - longitude and latitude of image as provided by the ancillary AHI images    
*.data* - the raw data from the image in numpy array format. In case of solar files,
this is the solar zenith angle.

**Methods:**    
*class.get\_all\_data()* - allows access to all class attributes (data, lat, lon)    
*class.get\_single\_pixel\_value()* - gathers the pixel value from the image from a 
location tuple (similar to AHILocation)    
*class.plot\_fulldisk(cbar=True)* - plots the full disk using matplotlib. Returns the figure
and axis objects for further use.
