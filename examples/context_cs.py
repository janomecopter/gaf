import gaf
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import time
from multiprocessing import Pool
import platform
import cartopy
import csv
from shapely.geometry import Polygon, Point
from pyhdf.SD import SD, SDC
from pyhdf.error import HDF4Error
import pandas as pd
import matplotlib.cm as mpcm
from matplotlib import colors, gridspec

cmo = mpcm.Oranges
cmo.set_bad(color='0.7')
cmrb = mpcm.RdBu_r
cmrb.set_bad(color='0.7')

laads_path = 'https://ladsweb.modaps.eosdis.nasa.gov'

if platform.node() == 'smgs-seo-sfmbox':
    savepath = '/media/sfm-user/Sunflower/P3'
elif platform.node() == 'sfmboxdos':
    savepath = '/mnt/storage/active-fire/P3'
elif platform.node() == 'smgs-seo-bryan':
    savepath = '/home/bryan/data/P3'
leo_path = savepath + '/LEO/'
ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
band7 = True
window = w = 11
chunk_size = cs = 500
array_size = ars = 5500
no_chunks = ars // cs
land = gaf.get_land_mask()
va = gaf.get_viewangle()

flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)

a = gaf.AHICapture((2016, 6, 1, 0))
file_kw = ['B07-w{:02d}-counts_valid'.format(n) for n in np.arange(11) * 2 + 5]
temp = Dataset(os.path.join(savepath, '{0}/{1:03d}/{2:02d}/{0}{1:03d}-{2:02d}00-{3}.nc'.format(
    a.time.year, a.time.jday, a.time.hour, file_kw[0]))).variables['data'][:, :]
# nan out values in counts_valid that aren't land
temp[flt_land] = np.nan
# copy the land mask into a fresh array
cont_lv = np.copy(flt_land)
cont_lv[np.isnan(temp)] = True
cont_lv[land != 0] = False
cont_lv = np.ma.array(cont_lv, mask=va > 80)

cs_areas = {
    'sea': [4400, 4600, 3050, 3250],
    'nwa': [3600, 3800, 2000, 2200],
    'bor': [2600, 2800, 1400, 1600],
    'thl': [1800, 2000, 800, 1000],
    'chn': [1000, 1200, 1600, 1800],
    'jpn': [900, 1100, 2500, 2700],
    'sib': [200, 400, 2000, 2200]
}

cs_jdays = {'sea': [90, 120], 'nwa': [297, 327], 'bor': [45, 75], 'thl': [59, 89], 'chn': [240, 270],
    'jpn': [124, 154], 'sib': [131, 161]}
cs_times = {'sea': '03:50', 'nwa': '05:00', 'bor': '05:40', 'thl': '06:30', 'chn': '05:10',
            'jpn': '03:50', 'sib': '05:00'}
pt_clmask_loc = 'C:/Scratch/cache/P3/PTCH_CLD/'


def ind_picker(lvl):
    top = np.arange(lvl) - lvl // 2 * window - lvl // 2
    bot = np.arange(lvl) + lvl // 2 * window - lvl // 2
    number_sides = lvl - 2
    for i, item in enumerate(np.arange(number_sides) - (number_sides // 2)):
        if not i:
            side = np.array([item * window - lvl // 2, item * window + lvl // 2])
        else:
            side = np.concatenate((side, np.array([item * window - lvl // 2, item * window + lvl // 2])))
    return np.concatenate((top, side, bot)) + (window ** 2 // 2)


def nc_write(fpath, data):
    if not os.path.isdir(os.path.dirname(fpath)):
        os.makedirs(os.path.dirname(fpath))
    r = Dataset(fpath, 'w')
    r.createDimension('x', data.shape[1])
    r.createDimension('y', data.shape[0])
    var = r.createVariable('data', 'f4', ('y', 'x',), zlib=True)
    var[:] = data
    var.comment = 'context statistics for background temperature - {}'.format(os.path.basename(fpath))
    r.instrument = 'AHI-8'
    r.creator_name = 'RMIT Geospatial Science'
    r.creator_email = 'bryan.hally@rmit.edu.au'
    r.close()
    print('{} saved'.format(os.path.basename(fpath)))


def plot_areas():
    ocean = '#4286f4'
    earth = '#db8553'
    cmap_disk = colors.ListedColormap([ocean, earth])
    bounds = [0, 0.5, 1]
    norm = colors.BoundaryNorm(bounds, cmap_disk.N)

    f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))
    for k in cs_areas.keys():
        locs = {
            'nw': [cs_areas[k][i] for i in [0, 2]],
            'ne': [cs_areas[k][i] for i in [0, 3]],
            'se': [cs_areas[k][i] for i in [1, 3]],
            'sw': [cs_areas[k][i] for i in [1, 2]]
        }
        grid = np.empty((5, 2))
        for i, l in enumerate(locs.keys()):
            grid[i] = [getattr(gaf.AHILocation(locs[l]), x) for x in ['longitude', 'latitude']]
        grid[-1] = grid[0]
        ax.plot(grid[:, 0], grid[:, 1], transform=cartopy.crs.PlateCarree(), color='b')
        ax.text((locs['se'][1]-2750)*2000+50000, (-locs['se'][0]+2750)*2000, k, transform=ahi_crs,
                fontsize=14, color='b')
    img_extent = [-5500000, 5500000, -5500000, 5500000]
    ax.imshow(cont_lv, cmap=cmap_disk, norm=norm, origin='upper', extent=img_extent, transform=ahi_crs)
    ax.gridlines(xlocs=np.arange(-180, 195, 15), ylocs=np.arange(-75, 85, 15))
    # f.savefig('/mnt/storage/active-fire/P3/figures/cs_globe.png', format='png', dpi=300)
    return f, ax


def tc_plot_areas():
    a = gaf.AHICapture((2016, 2, 13, 4, 0))
    # construct the three-dimensional array
    image = np.stack((a.band3.data, a.band2.data, a.band1.data), axis=-1)
    image[np.any(image == 1e20, axis=-1)] = [0, 0, 0]
    image = image*6
    image[image > 1] = 1
    image[image < 0] = 0
    f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))
    for k in cs_areas.keys():
        locs = {'nw': [cs_areas[k][i] for i in [0, 2]], 'ne': [cs_areas[k][i] for i in [0, 3]],
                'se': [cs_areas[k][i] for i in [1, 3]], 'sw': [cs_areas[k][i] for i in [1, 2]]}
        grid = np.empty((5, 2))
        for i, l in enumerate(locs.keys()):
            grid[i] = [getattr(gaf.AHILocation(locs[l]), x) for x in ['longitude', 'latitude']]
        grid[-1] = grid[0]
        ax.plot(grid[:, 0], grid[:, 1], transform=cartopy.crs.PlateCarree(), color='r')
    img_extent = [-5500000, 5500000, -5500000, 5500000]
    ax.imshow(image, origin='upper', extent=img_extent, transform=ahi_crs)
    ax.gridlines(xlocs=np.arange(-180, 181, 5), ylocs=np.arange(-75, 85, 5))


# Helper functions for VNP14/MxD14 usage
def get_leo_data():
    csv_list = [f for f in os.listdir(leo_path) if '.csv' in f]
    for item in csv_list:
        if not os.path.isdir(leo_path + item.split('_')[0]):
            os.makedirs(leo_path + item.split('_')[0])
        with open(savepath + '/LEO/' + item) as f:
            a = csv.reader(f)
            frows = [row for row in a]
        for listing in frows[1:]:
            furl = laads_path + listing[1]
            fname = furl.split('/')[-1]
            f_save = leo_path + item.split('_')[0] + '/' + fname
            if not os.path.isfile(f_save):
                gaf.file_downloader(furl, f_save)


def list_leo_data():
    t6 = {}
    for k in cs_areas.keys():
        locs = {'nw': [cs_areas[k][i] for i in [0, 2]], 'ne': [cs_areas[k][i] for i in [0, 3]],
                'se': [cs_areas[k][i] for i in [1, 3]], 'sw': [cs_areas[k][i] for i in [1, 2]]}
        extents = np.empty((4, 2))
        for i, l in enumerate(locs.keys()):
            extents[i] = [getattr(gaf.AHILocation(locs[l]), x) for x in ['longitude', 'latitude']]
        p = Polygon(extents)
        flist = np.array(os.listdir(leo_path + k))
        num_fires = np.empty((len(flist))).astype(np.int)
        for i, f in enumerate(flist):
            if f.split('.')[-1] == 'hdf':
                hdf = SD(os.path.join(leo_path, k, f), SDC.READ)
                try:
                    lats = hdf.select('FP_latitude')[:]
                    lons = hdf.select('FP_longitude')[:]
                except ValueError:
                    num_fires[i] = 0
                    continue
                except HDF4Error:
                    num_fires[i] = 0
                    continue
            elif f.split('.')[-1] == 'nc':
                nc = Dataset(os.path.join(leo_path, k, f))
                try:
                    lats = nc.variables['FP_latitude'][:]
                    lons = nc.variables['FP_longitude'][:]
                except:
                    num_fires[i] = 0
                    continue
            else:
                continue
            points = np.array([Point(p) for p in list(zip(lons, lats))], dtype=np.object)
            num_fires[i] = np.count_nonzero([p.contains(point) for point in points])
        #print('Top fires for {}'.format(k))
        fire_list = np.column_stack((flist[np.argsort(num_fires)][::-1],
                                     np.sort(num_fires)[::-1]))
        #[print(item) for item in fire_list[:6]]
        t6[k] = fire_list[:]
    return t6


def def_poslist(ylen, xlen):
    xr = np.arange(0, xlen, 40)
    yr = np.arange(0, ylen, 40)
    poslist = []
    for x in xr:
        poslist.append([0, x])
    for y in yr:
        poslist.append([y, xlen - 1])
    poslist.append([ylen - 1, xlen - 1])
    for x in xr[::-1]:
        poslist.append([ylen - 1, x])
    for y in yr[::-1][:-1]:
        poslist.append([y, 0])
    poslist = np.array(poslist)
    return poslist


def plot_t6_member(t6, k, n=5, plot=False):
    locs = {'nw': [cs_areas[k][i] for i in [0, 2]], 'ne': [cs_areas[k][i] for i in [0, 3]],
            'se': [cs_areas[k][i] for i in [1, 3]], 'sw': [cs_areas[k][i] for i in [1, 2]]}
    extents = np.empty((4, 2))
    for i, l in enumerate(locs.keys()):
        extents[i] = [getattr(gaf.AHILocation(locs[l]), x) for x in ['longitude', 'latitude']]
    p = Polygon(extents)
    flist = np.array(os.listdir(leo_path + k))
    fname = t6[k][n][0]
    if fname.split('.')[-1] == 'hdf':
        wc = '.'.join(fname.split('.')[1:3])
        hdf = SD(os.path.join(leo_path, k, fname), SDC.READ)
        lats = hdf.select('FP_latitude')[:]
        lons = hdf.select('FP_longitude')[:]
        flist = [f for f in os.listdir(os.path.join(leo_path, k)) if wc in f]
        hdf = SD(os.path.join(leo_path, k, [f for f in flist if 'D03' in f][0]), SDC.READ)
        lon = hdf.select('Longitude')[:]
        lat = hdf.select('Latitude')[:]
        poslist = def_poslist(lon.shape[0], lon.shape[1])
        poly = np.zeros_like(poslist).astype(np.float32)
        for i, item in enumerate(poslist):
            poly[i] = [lon[item[0], item[1]], lat[item[0], item[1]]]
        df = pd.DataFrame(poly, columns=['lon', 'lat'])
    elif fname.split('.')[-1] == 'nc':
        wc = '.'.join(fname.split('.')[1:3])
        flist = [f for f in os.listdir(os.path.join(leo_path, k)) if wc in f]
        nc = Dataset(os.path.join(leo_path, k, fname))
        lats = nc.variables['FP_latitude'][:]
        lons = nc.variables['FP_longitude'][:]
        df = pd.read_pickle(os.path.join(leo_path, k, [f for f in flist if '.df' in f][0]))
        df.lon[df.lon < 0] += 360
    else:
        exit()
    points = np.array([Point(p) for p in list(zip(lons, lats))], dtype=np.object)
    fire_list = [p.contains(point) for point in points]
    valid_points = np.array([[point.x, point.y] for point in points[fire_list]])
    # split f into time components
    yr = int(fname.split('.')[1][1:5])
    jd = int(fname.split('.')[1][-3:])
    h = int(fname.split('.')[2][:2])
    mt = fname.split('.')[2][-2:]
    if int(mt) % 10 < 5:
        m = (np.floor(int(mt)/10)*10 - 10).astype(int)
        if m == -10:
            h -= 1
            m = 50
    else:
        m = (np.floor(int(mt)/10)*10).astype(int)
    # compare with next AHIImage if stationkeeping
    if np.logical_and(h == 2, m == 40):
        m = 50
    a = gaf.AHICapture((yr, jd, h, m))
    print(a.time.dt)
    b7 = a.band7.data
    b7[~cont_lv] = np.nan
    extents = [-5500000, 5500000, -5500000, 5500000]
    if plot:
        fig, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))
        ax.scatter(valid_points[:, 0], valid_points[:, 1], s=4, c='y', alpha=0.8,
                   transform=cartopy.crs.PlateCarree())
        x, y = p.exterior.xy
        ax.plot(x, y, c='r', transform=cartopy.crs.PlateCarree())
        # x, y = leo_poly.exterior.xy
        ax.plot(df['lon'], df['lat'], c='b', ls='--', transform=cartopy.crs.PlateCarree())
        ax.imshow(b7, cmap='gray', origin='upper', extent=extents, transform=ahi_crs)
        # ax.set_extent((556000, 1044000, -3744000, -3257000), crs=ahi_crs)
        ax.set_extent((p.bounds[0] - 1, p.bounds[2] + 1, p.bounds[1] - 1, p.bounds[3] + 1),
                      crs=cartopy.crs.PlateCarree())
        ax.gridlines(xlocs=np.arange(100, 180), ylocs=np.arange(-50, 10))
        ax.set_title('Fires from {}\nTotal fires in area: {}'.format(fname, len(valid_points)))

    x, y, _ = np.split(
        ahi_crs.transform_points(cartopy.crs.PlateCarree(), valid_points[:, 0], valid_points[:, 1]),
        3, axis=1)
    ahi_coords = np.column_stack(((x + 5500000) // 2000, 5500 - ((y + 5500000) // 2000))).astype(int)
    ahi_pixels = np.unique(ahi_coords, axis=0)
    return ax


def produce_stats(k, fname):
    # split f into time components
    yr = int(fname.split('.')[1][1:5])
    jd = int(fname.split('.')[1][-3:])
    h = int(fname.split('.')[2][:2])
    mt = fname.split('.')[2][-2:]
    if int(mt) % 10 < 5:
        m = (np.floor(int(mt) / 10) * 10 - 10).astype(int)
        if m == -10:
            h -= 1
            m = 50
    else:
        m = (np.floor(int(mt) / 10) * 10).astype(int)
    # compare with next AHIImage if stationkeeping
    if np.logical_and(h == 2, m == 40):
        m = 50
    a = gaf.AHICapture((yr, jd, h, m))
    # use cs_bounds to subselect image data
    int_bounds = cs_areas[k]
    ext_bounds = [int_bounds[0] - (w // 2), int_bounds[1] + (w // 2),
                  int_bounds[2] - (w // 2), int_bounds[3] + (w // 2)]
    bt = a.band7.data[ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]
    sub_land = cont_lv[ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]
    bt[~sub_land] = np.nan
    try:
        r = Dataset(os.path.join(gaf.IMAGEDIR_AHI, 'CLD', '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                                 '{}{:02d}{:02d}{:02d}0000-P1S-ABOM_OBS_CLOUDMASK-PRJ_GEOS141_2000-'
                                 'HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month, a.time.day, a.time.hour)))
        cld = (r['cloud_mask'][0, ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]) > 0
        bt[cld] = np.nan
    except:
        print('No cloud data available for 2016{:02d}{:02d}'.format(a.time.month, a.time.day))
        raise
    # pad zeros to get rid of indexing issue
    bt_pad = np.empty((bt.shape[0] + 1, bt.shape[1] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:-1, :-1] = bt

    val_array = np.array([bt_pad[wy:wy - window, wx:wx - window]
                          for wy in np.arange(w) for wx in np.arange(w)])
    val_array = np.moveaxis(val_array, 0, -1)

    vals = {}
    for value in np.arange(5, window + 2, 2):
        # merge lvls 3 and 5
        if value == 5:
            inds = np.concatenate((ind_picker(value - 2), ind_picker(value)))
        else:
            inds = ind_picker(value)
        vals[value] = np.take(val_array, inds, axis=2)
    # separate out grid for analysis
    bt_focus = bt[w // 2:-(w // 2), w // 2:-(w // 2)]
    yl, xl = bt_focus.shape

    value = 5
    counts_valid = np.count_nonzero(~np.isnan(vals[value]), axis=2)
    window_diffs = vals[value] - bt_focus[:, :, np.newaxis]
    window_diffs_abs = np.abs(window_diffs)
    mean_window = np.nanmean(window_diffs, axis=2)
    std_window = np.nanstd(window_diffs, axis=2)
    mad_window = np.nanmean(window_diffs_abs, axis=2)

'''
mdf = df[['YYYYMMDD','HHMM','lat','lon']][df.lon>80]
for item in mdf.values:
    p = Point[item[2], item[3]]
    for k in cs_polys.keys():
        if cs_polys[k].contains(p):
'''


def pluck_stats(k):
    int_bounds = cs_areas[k]
    t_list = [[2016, d, h, m] for d in np.arange(cs_jdays[k][0], cs_jdays[k][1] + 1)
              for h in [int(cs_times[k].split(':')[0])]
              for m in [int(cs_times[k].split(':')[1])]]

    default_array = np.empty((31, 200, 200))
    default_array[:] = np.nan
    default_array = np.ma.array(default_array, mask=np.repeat(
        ~cont_lv[np.newaxis, int_bounds[0]:int_bounds[1], int_bounds[2]:int_bounds[3]], 31, axis=0))

    d_mean = {}
    d_mad = {}
    counts_valid = {}
    bt_stack = np.copy(default_array)
    for value in np.arange(5, window + 2, 2):
        d_mean[value] = np.copy(default_array)
        d_mad[value] = np.copy(default_array)
        counts_valid[value] = np.copy(default_array)

    #btmean_stack = np.empty((31, 200, 200))
    #btmean_stack[:] = np.nan
    #btmad_stack = np.empty_like(btmean_stack)
    #btmean_stack[:] = np.nan
    #btstd_stack = np.empty_like(btmean_stack)
    #btstd_stack[:] = np.nan
    #count_stack = np.empty_like(btmean_stack)
    #count_stack[:] = np.nan
    #bt_stack = np.empty_like(btmean_stack)
    #bt_stack[:] = np.nan
    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        # blow away land

        ext_bounds = [int_bounds[0] - (w // 2), int_bounds[1] + (w // 2),
                      int_bounds[2] - (w // 2), int_bounds[3] + (w // 2)]
        try:
            bt = a.band7.data[ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]
        except AttributeError:
            continue
        sub_land = cont_lv[ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]
        bt[~sub_land] = np.nan
        try:
            r = Dataset(os.path.join(pt_clmask_loc, k,
                                     [f for f in os.listdir(pt_clmask_loc + k) if
                                      '2016{:02d}{:02d}'.format(a.time.month, a.time.day) in f][0]))
        except:
            print('No cloud data for {:03d}-{:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
            raise
        t_cld = r.variables['cloud_mask'][0, :210, :210]
        bt[t_cld == 1] = np.nan

        bt_pad = np.empty((bt.shape[0] + 1, bt.shape[1] + 1,))
        bt_pad[:] = np.nan
        bt_pad[:-1, :-1] = bt

        val_array = np.array([bt_pad[wy:wy - window, wx:wx - window]
                              for wy in np.arange(w) for wx in np.arange(w)])
        val_array = np.moveaxis(val_array, 0, -1)

        vals = {}
        for value in np.arange(5, window + 2, 2):
            # merge lvls 3 and 5
            if value == 5:
                inds = np.concatenate((ind_picker(value - 2), ind_picker(value)))
            else:
                inds = ind_picker(value)
            vals[value] = np.take(val_array, inds, axis=2)
        # separate out grid for analysis
        bt_focus = bt[w // 2:-(w // 2), w // 2:-(w // 2)]
        bt_stack[i] = bt_focus
        #yl, xl = bt_focus.shape

        #value = 5
        for value in np.arange(5, window + 2, 2):
            counts_valid[value][i] = np.count_nonzero(~np.isnan(vals[value]), axis=2)
            counts_valid[value][i][~cont_lv[int_bounds[0]:int_bounds[1], int_bounds[2]:int_bounds[3]]] = np.nan
            window_diffs = vals[value] - bt_focus[:, :, np.newaxis]
            window_diffs_abs = np.abs(window_diffs)
            d_mean[value][i] = np.nanmean(window_diffs, axis=2)
            d_mad[value][i] = np.nanmean(window_diffs_abs, axis=2)
            d_mad[value][i][~cont_lv[int_bounds[0]:int_bounds[1], int_bounds[2]:int_bounds[3]]] = np.nan
        '''
        counts_valid = np.count_nonzero(~np.isnan(vals[value]), axis=2)
        window_diffs = vals[value] - bt_focus[:, :, np.newaxis]
        window_diffs_abs = np.abs(window_diffs)
        mean_window = np.nanmean(window_diffs, axis=2)
        std_window = np.nanstd(window_diffs, axis=2)
        mad_window = np.nanmean(window_diffs_abs, axis=2)
        
        #count_stack[i] = np.count_nonzero(~np.isnan(vals[value]), axis=2)
        #count_stack[i][~cont_lv[int_bounds[0]:int_bounds[1], int_bounds[2]:int_bounds[3]]] = np.nan
        window_diffs = vals[value] - bt_focus[:, :, np.newaxis]
        window_diffs_abs = np.abs(window_diffs)
        btmean_stack[i] = np.nanmean(window_diffs, axis=2)
        #btstd_stack[i] = np.nanstd(window_diffs, axis=2)
        btmad_stack[i] = np.nanmean(window_diffs_abs, axis=2)
        btmad_stack[i][~cont_lv[int_bounds[0]:int_bounds[1], int_bounds[2]:int_bounds[3]]] = np.nan
        #bt_stack[i] = bt_focus
    btmad_stack[np.where(np.all(btmad_stack == 0, axis=(1, 2)))] = np.nan
    return {'bt': bt_stack, 'btmean': btmean_stack, 'btstd': btstd_stack, 'btmad': btmad_stack, 'counts': count_stack}
    '''
    for value in np.arange(5, window + 2, 2):
        d_mad[value][np.where(np.all(d_mad[value] == 0, axis=(1, 2)))] = np.nan
    return {'mean': d_mean, 'mad': d_mad, 'count': counts_valid, 'bt': bt_stack, 'vals5': vals[5]}


def plot_stats(d, k, metric):
    f, ax = plt.subplots(figsize=(9, 7))
    if metric == 'counts':
        im = ax.imshow(np.nanmean(d[metric], axis=0), vmin=0, vmax=24, cmap=cmo)
        plt.colorbar(im, ax=ax)
    elif np.nanmin(d[metric]) >= 0:
        im = ax.imshow(np.nanmean(d[metric], axis=0), vmin=0, vmax=8, cmap=cmo)
        plt.colorbar(im, ax=ax, extend='max')
    else:
        im = ax.imshow(np.nanmean(d[metric], axis=0), vmin=-5, vmax=5, cmap=cmrb)
        plt.colorbar(im, ax=ax, extend='both')
    ax.xaxis.set_ticklabels(np.arange(-25, 200, 25)+cs_areas[k][2])
    ax.yaxis.set_ticklabels(np.arange(-25, 200, 25)+cs_areas[k][0])
    ax.set_title('{} 31 day {}'.format(k, metric))
    return f, ax


def temporal_context(d):
    # y = 72
    # x = 150
    y = 36; x = 177
    y, x = np.random.choice(200, 2)
    t_set = d['bt'][:30]
    xy_vec = d['bt'][:30, y, x]
    p = t_set - xy_vec[:, np.newaxis, np.newaxis]
    rms = np.sqrt(np.nanmean(p ** 2, axis=0))
    ind = np.unravel_index(np.argsort(rms, axis=None), rms.shape)
    print('y: {} x: {}'.format(y, x))
    print('Temp_diff: {:.3f}'.format(
        np.nanmean(d['bt'][-1, ind[0][1:25], ind[1][1:25]]) - d['bt'][-1, ind[0][0], ind[1][0]]))
    print('Context_diff: {:.3f}'.format(d['mean'][5][-1, ind[0][0], ind[1][0]]))
    f = plt.figure(figsize=(14, 5))
    spec = gridspec.GridSpec(1, 5)
    ax0 = f.add_subplot(spec[:2])
    ax0.imshow(np.nanmean(d['bt'], axis=0), cmap='gray', zorder=0)
    ax0.scatter(ind[1][0], ind[0][0], color='r', zorder=2)
    ax0.scatter(ind[1][1:25], ind[0][1:25], color='b', s=6, zorder=1)
    ax0.set_xlim(0, 200)
    ax0.set_ylim(200, 0)
    ax0.set_xticklabels([])
    ax0.set_yticklabels([])
    ax0.text(-12, 10, 'a)', fontsize=14)
    ax1 = f.add_subplot(spec[2:4])
    for i in np.arange(25):
        if not i:
            ax1.plot(np.arange(30), d['bt'][:30, ind[0][i], ind[1][i]], label='central', c='r', zorder=1)
            ax1.scatter(30, d['bt'][-1, ind[0][i], ind[1][i]], c='r', zorder=1)
        else:
            ax1.plot(np.arange(30), d['bt'][:30, ind[0][i], ind[1][i]], label='choice {}'.format(i), c='b', zorder=0,
                     alpha=0.6)
            ax1.scatter(30, d['bt'][-1, ind[0][i], ind[1][i]], c='b', zorder=0, alpha=0.6)
    ax1.set_xlabel('Day')
    ax1.set_ylabel('Brightness temperature (K)')
    ax1.text(-5.5, 317.2, 'b)', fontsize=14)
    bp_vals = d['bt'][-1, ind[0][1:25], ind[1][1:25]]
    bp_vals = bp_vals[~np.isnan(bp_vals)]
    cont_vals = d['vals5'][ind[0][0], ind[1][0]]
    cont_vals = cont_vals[~np.isnan(cont_vals)]
    ax2 = f.add_subplot(spec[4], sharey=ax1)
    ax2.boxplot([bp_vals, cont_vals])
    ax2.set_xticklabels(['selected', 'context'])
    ax2.axhline(d['bt'][-1, ind[0][0], ind[1][0]], c='r')
    ax2.set_ylabel('Brightness temperature (K)')
    ax2.text(0, 317.2, 'c)', fontsize=14)
    f.set_tight_layout(True)
    f.savefig(os.path.join(savepath, 'figures',
                           'spatio_temp_{}_{}.png'.format(y + cs_areas['thl'][0], x + cs_areas['thl'][2])),
              format='png', dpi=300)



'''
def print_mean_figs():
    labels = ['a', 'b', 'c', 'd']
    f, ax = plt.subplots(4, 2, figsize=(6, 8.6))
    for i, k in enumerate(list(cs_areas.keys())[:4]):
        mm = np.nanmean(d_mean[k], axis=0)
        im = ax[i, 0].imshow(mm, vmin=-5, vmax=5, cmap=cmrb)
        cb = plt.colorbar(im, ax=ax[i, 0], extend='both')
        cb.set_label('Temperature difference (K)', fontsize=8)
        cb.ax.tick_params(labelsize=8)
        ax[i, 0].text(-20, 15, '{})'.format(labels[i]), fontsize=10)
        ax[i, 0].xaxis.set_ticks([])
        ax[i, 0].yaxis.set_ticks([])
        ax[i, 1].hist(np.ravel(mm), 100, range=(-10, 10), ec='#2720FF', fc='#8682FE')
        ax[i, 1].set_xlim(-10, 10)
        ax[i, 1].set_ylim(0, 4500)
        #ax[i, 1].set_xlabel('Temperature difference (K)', fontsize=8)
        #ax[i, 1].set_ylabel('Count', fontsize=8)
        ax[i, 1].xaxis.set_tick_params(labelsize=8)
        ax[i, 1].yaxis.set_tick_params(labelsize=8)
    ax[3, 1].set_xlabel('Temperature difference (K)', fontsize=8)
    f.set_tight_layout(True)
    f.savefig(r'C:\Scratch\cache\P3\temp\meanplan1.png', format='png', dpi=300)
    f, ax = plt.subplots(4, 2, figsize=(6, 8.6))
    for i, k in enumerate(list(cs_areas.keys())[4:]):
        mm = np.nanmean(d_mean[k], axis=0)
        im = ax[i, 0].imshow(mm, vmin=-5, vmax=5, cmap=cmrb)
        cb = plt.colorbar(im, ax=ax[i, 0], extend='both')
        cb.set_label('Temperature difference (K)', fontsize=8)
        cb.ax.tick_params(labelsize=8)
        ax[i, 0].text(-20, 15, '{})'.format(labels[i]), fontsize=10)
        ax[i, 0].xaxis.set_ticks([])
        ax[i, 0].yaxis.set_ticks([])
        ax[i, 1].hist(np.ravel(mm), 100, range=(-10, 10), ec='#2720FF', fc='#8682FE')
        ax[i, 1].set_xlim(-10, 10)
        ax[i, 1].set_ylim(0, 4500)
        # ax[i, 1].set_xlabel('Temperature difference (K)', fontsize=8)
        # ax[i, 1].set_ylabel('Count', fontsize=8)
        ax[i, 1].xaxis.set_tick_params(labelsize=8)
        ax[i, 1].yaxis.set_tick_params(labelsize=8)
    im = ax[3, 0].imshow(mm, vmin=-5, vmax=5, cmap=cmrb)
    cb = plt.colorbar(im, ax=ax[3, 0], extend='both')
    ax[3, 0].xaxis.set_ticks([])
    ax[3, 0].yaxis.set_ticks([])
    ax[3, 1].hist(np.ravel(mm), 100, range=(-10, 10), ec='#2720FF', fc='#8682FE')
    ax[3, 1].set_xlim(-10, 10)
    ax[3, 1].xaxis.set_tick_params(labelsize=8)
    ax[3, 1].yaxis.set_tick_params(labelsize=8)
    ax[3, 1].set_xlabel('Temperature difference (K)', fontsize=8)
    f.set_tight_layout(True)
    # ax[2, 1].set_xlabel('Temperature difference (K)', fontsize=8)
    # f.delaxes(ax[3,0])
    # f.delaxes(ax[3,1])
    f.savefig(r'C:\Scratch\cache\P3\temp\meanplan2.png', format='png', dpi=300)


d_mean = {}
d_mad = {}
for k in cs_areas.keys():
    d = pluck_stats(k)
    d_mean[k] = d['btmean']
    d_mad[k] = d['btmad']
print('Ready!')


for k in cs_areas.keys():
    d = pluck_stats(k)
    mm = np.nanmean(d['btmean'], axis=0)
    f, ax = plt.subplots(1, 2)
    im = ax[0].imshow(mm, vmin=-5, vmax=5, cmap=cmrb)
    plt.colorbar(im, ax=ax[0], extend='both')
    ax[0].xaxis.set_ticks([])
    ax[0].yaxis.set_ticks([])
    ax[1].hist(np.ravel(mm), 100, range=(-10, 10), ec='#2720FF', fc='#8682FE')
    f.suptitle(k)


for k in cs_areas.keys():
    d = pluck_stats(k)
    mm = np.nanmean(d['btmean'], axis=0)
    f, ax = plt.subplots(1, 2, figsize=(16, 6))
    im = ax[0].imshow(mm, vmin=-5, vmax=5, cmap=cmrb)
    plt.colorbar(im, ax=ax[0], extend='both')
    ax[0].xaxis.set_ticks([])
    ax[0].yaxis.set_ticks([])
    ax[1].hist(np.ravel(mm), 100, range=(-10, 10), ec='#2720FF', fc='#8682FE')
    f.set_tight_layout(True)
    f.savefig(r'C:\Scratch\cache\P3\temp\{}-meancombo.png'.format(k))
    plt.close(f)
    mm = np.nanmean(d_mad[k],axis=0)
    f,ax = plt.subplots(1, 2, figsize=(16, 6))
    im = ax[0].imshow(mm, vmin=0, vmax=8, cmap=cmo)
    plt.colorbar(im, ax=ax[0], extend='max')
    ax[0].xaxis.set_ticks([])
    ax[0].yaxis.set_ticks([])
    ax[1].hist(np.ravel(mm), 100, range=(0, 10), ec='#2720FF', fc='#8682FE')
    ax[1].set_xlim(0,10)
    f.set_tight_layout(True)
    f.savefig(r'C:\Scratch\cache\P3\temp\{}-madcombo.png'.format(k), format='png', dpi=300)
    plt.close(f)
    
    f, ax = plt.subplots(7, 2, figsize=(6, 9))
    for i,k in enumerate(cs_areas.keys()):
        mm = np.nanmean(d_mean[k], axis=0)
        im = ax[i,0].imshow(mm, vmin=-5, vmax=5, cmap=cmrb)
        plt.colorbar(im, ax=ax[i,0], extend='both')
        ax[i,0].xaxis.set_ticks([])
        ax[i,0].yaxis.set_ticks([])
        ax[i,1].hist(np.ravel(mm), 100, range=(-10, 10), ec='#2720FF', fc='#8682FE')
        ax[i,1].set_xlim(-10,10)
    f.set_tight_layout(True)
    f.savefig(r'C:\Scratch\cache\P3\temp\{}-meanall.png'.format(k), format='png', dpi=300)

'''
def cs_expand_stats():
    d_mean = {}
    d_mad = {}
    d_count = {}
    for k in cs_areas.keys():
        d = pluck_stats(k)
        d_mean[k] = d['mean']
        d_mad[k] = d['mad']
        d_count[k] = d['count']
    return d_mean, d_mad, d_count


'''
        for i, v in enumerate(d_mean[k].keys()):
            mmean = np.nanmean(np.stack([d_mean[k][v] for v in list(d_mean[k].keys())[:i + 1]]), axis=(0, 1))
            mmad = np.nanmean(np.stack([d_mad[k][v] for v in list(d_mad[k].keys())[:i + 1]]), axis=(0, 1))
            print('{} & {} & {:.3f} & {:.3f} & {:.3f} & {:.3f} \\\\'
                  .format(k, v, np.nanmean(mmean), np.nanstd(mmean), np.nanmean(mmad), np.nanstd(mmad)))
    
    
    for k in cs_areas.keys():
        for i, v in enumerate(d_mean[k].keys()):
            mmean = np.nanmean(d_mean[k][v], axis=0)
            mmad = np.nanmean(d_mad[k][v], axis=0)
            print('{} & {} & {:.3f} & {:.3f} & {:.3f} & {:.3f} \\\\'
                  .format(k, v, np.nanmean(mmean), np.nanstd(mmean), np.nanmean(mmad), np.nanstd(mmad)))
                  
    
    np.count_nonzero(np.all(np.isnan(d_count['bor'][5]),axis=0))
    np.count_nonzero(d_count['bor'][5] > 0, axis=0)
'''


def cs_area_stats(d_mean, d_count):
    # d_mean, d_count = cs_expand_stats()
    pc = (np.arange(7) * .1 + 0.15)[::-1]
    max_p = {5: 24, 7: 24, 9: 32, 11: 40}
    print(' & \\textbf{pc} & \\textbf{1.00} & \\textbf{0.99 - 0.75} & \\textbf{0.99 - 0.65} & '
          '\\textbf{0.99 - 0.55} & \\textbf{0.99 - 0.45} & \\textbf{0.99 - 0.35} & \\textbf{0.99 - 0.25} & '
          '\\textbf{0.99 - 0.15} & \\textbf{all} \\\\')
    print('\\hline')
    # f, ax = plt.subplots()
    # cols = ['r', 'g', 'b', 'm', 'c', 'y', '0.5']
    for i, k in enumerate(cs_areas.keys()):
        print('{} & mean (K) & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & '
              '{:.3f} \\\\'.format(k, np.nanmean(d_mean[k][5][np.where(d_count[k][5] == max_p[5])]),
                                   *[np.nanmean(d_mean[k][5][np.where(
                                       np.logical_and(d_count[k][5] > np.floor(max_p[5] * p),
                                                      d_count[k][5] < 24))])for p in pc], np.nanmean(d_mean[k][5])))
        print(' & std (K) & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} \\\\'.format(
            np.nanstd(d_mean[k][5][np.where(d_count[k][5] == max_p[5])]),
            *[np.nanstd(d_mean[k][5][np.where(np.logical_and(d_count[k][5] > np.floor(max_p[5] * p),
                                                             d_count[k][5] < 24))]) for p in pc],
            np.nanstd(d_mean[k][5])))
        count_list = [np.count_nonzero(np.logical_and(d_count[k][5] > np.floor(max_p[5] * p),
                                                      d_count[k][5] != max_p[5])) for p in pc]
        total_count = np.count_nonzero(np.logical_and(~np.isnan(d_count[k][5]), d_count[k][5] != 0))
        full_count = np.count_nonzero(d_count[k][5] == 24)
        non_full_count = total_count - full_count
        print(' & count & {} & {} & {} & {} & {} & {} & {} & {} & {} \\\\'.format(
            full_count, *count_list, total_count))
        print(' & \% avail & {:.1f}\% & {:.1f}\% & {:.1f}\% & {:.1f}\% & {:.1f}\% & {:.1f}\% & {:.1f}\% & {:.1f}\% '
              '& \\\\'.format(100 * full_count / total_count, *[100 * cnt / total_count for cnt in count_list]))
        print('\\hline')
        # ax.plot(pc, [cnt/total_count for cnt in count_list], c=cols[i], label=k)
    # ax.legend()


def find_points_in_poly():
    cs_poly = {}
    for k in cs_areas.keys():
        locs = {'nw': [cs_areas[k][i] for i in [0, 2]], 'ne': [cs_areas[k][i] for i in [0, 3]],
                'se': [cs_areas[k][i] for i in [1, 3]], 'sw': [cs_areas[k][i] for i in [1, 2]]}
        extents = np.empty((4, 2))
        for i, l in enumerate(locs.keys()):
            extents[i] = [getattr(gaf.AHILocation(locs[l]), x) for x in ['longitude', 'latitude']]
        cs_poly[k] = Polygon(extents)
    dirpath = leo_path + 'VNP14IMGML/'
    flist = [f for f in os.listdir(dirpath) if '.gz' not in f]
    for i, f in enumerate(flist):
        df = pd.read_csv(os.path.join(dirpath, f))
        if not i:
            mdf = df[df.lon > 80][['YYYYMMDD', 'HHMM', 'lon', 'lat']]
        else:
            mdf = pd.concat([mdf, df[df.lon > 80][['YYYYMMDD', 'HHMM', 'lon', 'lat']]], ignore_index=True)
    mdf['month'] = (mdf.YYYYMMDD % 10000) // 100
    mdf['day'] = mdf.YYYYMMDD % 100
    mdf['key'] = 'None'
    mdf['point'] = [Point(mdf.lon[i], mdf.lat[i]) for i in np.arange(mdf.shape[0])]
    mdf['jday'] = [gaf.Time((2016, mdf.month[i], mdf.day[i], 0, 0)).jday for i in np.arange(mdf.shape[0])]
    for i in np.arange(mdf.shape[0]):
        for k in cs_poly.keys():
            if cs_poly[k].contains(mdf.point[i]):
                mdf.key[i] = k
    mdf.to_pickle(dirpath + 'VNP14out.df')