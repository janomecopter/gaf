# -*- coding: utf-8 -*-
"""
Created on Thu Aug 10 17:53:06 2017

@author: e76243
"""

import gaf
# import numpy as np
from multiprocessing import Pool, cpu_count
import os


def main():
    flist = []
    for root, dirs, files in os.walk(os.path.join(gaf.IMAGEDIR, 'AHI', 'B07')):
        for filename in files:
            if int(root.split('/')[-1]) >= 100 and root.split('/')[-2] != '2015':
                flist.append(os.path.join(root, filename))

    p = Pool(cpu_count() / 2)
    p.map(process_bat, (i for i in flist))

    gaf.tools.send_complete('process_bat.py')


def process_bat(path):
    gaf.make_medians(path)


if __name__ == '__main__':
    main()
