# -*- coding: utf-8 -*-

import sys, os
sys.path.append(os.path.realpath('..'))
import gaf
import numpy as np
from multiprocessing import Pool, cpu_count


def main():
    array = [[2016, i, j, k] for i in np.arange(367)+1 for j in range(24) for k in np.arange(0, 60, 10)]

    p = Pool(cpu_count() * 2)
    p.map(get_images, (i for i in array))

    gaf.tools.send_complete('file download')


def get_images(time):
    ahi = gaf.AHICapture(time)
    ahi.band7
    ahi.band13


if __name__ == '__main__':
    main()
