import gaf
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import time
from multiprocessing import Pool
import platform
import pandas as pd


if platform.node() == 'smgs-seo-sfmbox':
    savepath = '/media/sfm-user/Sunflower Bucket/P3'
elif platform.node() == 'sfmboxdos':
    savepath = '/mnt/storage/active-fire/P3'
else:
    savepath = r'C:/Scratch/cache/P3'
band7 = True
window = w = 25
chunk_size = cs = 500
array_size = ars = 5500
no_chunks = ars // cs
land = gaf.get_land_mask()
va = gaf.get_viewangle()
flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
# break into 500 pixel subsets by land > 5
blks = np.column_stack(np.where(np.array([np.count_nonzero(flt_land[y - cs:y, x - cs:x]) > 100
                                          for y in np.arange(0, ars, cs) + cs
                                          for x in np.arange(0, ars, cs) + cs]).reshape(no_chunks, -1)))
# ditch antarctica
blks = np.delete(blks, np.where(blks[:, 0] == 10)[0], axis=0)
d_list = [6, 10, 20, 35, 36, 41, 71, 72, 82, 97, 101, 103, 133, 144, 149, 153, 164, 173, 184,
          188, 200, 222, 230, 236, 253, 257, 274, 279, 286, 290, 314, 322, 323, 343, 353, 355]
# t_list = np.array([[2016, d, h, 0] for d in d_list for h in np.arange(0, 24)])
t_list = np.array([[2016, d, 5, 0] for d in d_list[22:23]])
d = [[t, blk] for t in t_list for blk in blks]

file_kw = ['B07-w{:02d}-mean_window'.format(n) for n in np.arange(11) * 2 + 5]


def make_cont_lv():
    a = gaf.AHICapture((2016, 6, 1, 0))
    file_kw = ['B07-w{:02d}-counts_valid'.format(n) for n in np.arange(11) * 2 + 5]
    temp = Dataset(os.path.join(savepath, '{0}/{1:03d}/{2:02d}/{0}{1:03d}-{2:02d}00-{3}.nc'.format(
        a.time.year, a.time.jday, a.time.hour, file_kw[0]))).variables['data'][:, :]
    # nan out values in counts_valid that aren't land
    temp[~flt_land] = np.nan
    # copy the land mask into a fresh array
    cont_lv = np.copy(flt_land)
    cont_lv[np.isnan(temp)] = False
    #cont_lv[land != 0] = False
    cont_lv = np.ma.array(cont_lv, mask=va > 80)
    return cont_lv


def make_proc_list():
    proc_list = []
    for item in d:
        a = gaf.AHICapture(item[0])
        if band7:
            band = a.band7
        else:
            band = a.band13
        y, x = item[1]
        fpref = '{}{:03d}-{:02d}00-B{:02d}-[{},{}]'.format(a.time.year, a.time.jday, a.time.hour, band.band, y, x)
        save_folder = os.path.join(savepath, '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                               '{:02d}'.format(a.time.hour))
        if not os.path.isdir(save_folder):
            os.makedirs(save_folder)
        if len(gaf.listdir_start(save_folder, fpref)) < 492:
            proc_list.append(item)
    return proc_list


def nc_write(fpath, data):
    if not os.path.isdir(os.path.dirname(fpath)):
        os.makedirs(os.path.dirname(fpath))
    r = Dataset(fpath, 'w')
    r.createDimension('x', data.shape[1])
    r.createDimension('y', data.shape[0])
    var = r.createVariable('data', 'f4', ('y', 'x',), zlib=True)
    var[:] = data
    var.comment = 'context statistics for background temperature - {}'.format(os.path.basename(fpath))
    r.instrument = 'AHI-8'
    r.creator_name = 'RMIT Geospatial Science'
    r.creator_email = 'bryan.hally@rmit.edu.au'
    r.close()
    print('{} saved'.format(os.path.basename(fpath)))


def ind_picker(lvl):
    top = np.arange(lvl) - lvl // 2 * window - lvl // 2
    bot = np.arange(lvl) + lvl // 2 * window - lvl // 2
    number_sides = lvl - 2
    for i, item in enumerate(np.arange(number_sides) - (number_sides // 2)):
        if not i:
            side = np.array([item * window - lvl // 2, item * window + lvl // 2])
        else:
            side = np.concatenate((side, np.array([item * window - lvl // 2, item * window + lvl // 2])))
    return np.concatenate((top, side, bot)) + (window ** 2 // 2)


def proc_chunk(data):
    st = time.time()
    a = gaf.AHICapture(data[0])
    #print(a.time.dt)
    # construct window extents from blk
    y, x = data[1]
    if y == 0:
        y_range = [0, cs + w // 2]
    elif y == 10:
        y_range = [ars - cs - (w // 2), ars]
    else:
        y_range = [y * cs - (w // 2), (y + 1) * cs + (w // 2)]
    if x == 0:
        x_range = [0, cs + w // 2]
    elif x == 10:
        x_range = [ars - cs - (w // 2), ars]
    else:
        x_range = [x * cs - (w // 2), (x + 1) * cs + (w // 2)]
    if band7:
        band = a.band7
    else:
        band = a.band13
    fpref = '{}{:03d}-{:02d}00-B{:02d}-[{},{}]'.format(a.time.year, a.time.jday, a.time.hour, band.band, y, x)
    save_folder = os.path.join(savepath, '{}'.format(a.time.year), '{:03d}'.format(a.time.jday), '{:02d}'.format(a.time.hour))
    if not os.path.isdir(save_folder):
        os.makedirs(save_folder)
    #if len(gaf.listdir_start(save_folder, fpref)) > 0:
    #    print('{} already done'.format(fpref))
    #    return 0
    bt = band.data[y_range[0]:y_range[1], x_range[0]:x_range[1]]
    # mask land and va>80
    sub_land = flt_land[y_range[0]:y_range[1], x_range[0]:x_range[1]]
    bt[~sub_land] = np.nan
    # import cloud product
    try:
        r = Dataset(os.path.join(gaf.IMAGEDIR_AHI, 'CLD', '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                                 '{}{:02d}{:02d}{:02d}0000-P1S-ABOM_OBS_CLOUDMASK-PRJ_GEOS141_2000-'
                                 'HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month, a.time.day, a.time.hour)))
        cld = (r['cloud_mask'][0, y_range[0]:y_range[1], x_range[0]:x_range[1]]) > 0
        bt[cld] = np.nan
    except:
        print('No cloud data available for 2016{:02d}{:02d}'.format(a.time.month, a.time.day))
        raise

    # pad zeros to get rid of indexing issue
    bt_pad = np.empty((bt.shape[0] + 1, bt.shape[1] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:-1, :-1] = bt

    val_array = np.array([bt_pad[wy:wy-window, wx:wx-window]
                          for wy in np.arange(w) for wx in np.arange(w)])
    val_array = np.moveaxis(val_array, 0, -1)

    vals = {}
    for value in np.arange(5, window + 2, 2):
        # merge lvls 3 and 5
        if value == 5:
            inds = np.concatenate((ind_picker(value-2), ind_picker(value)))
        else:
            inds = ind_picker(value)
        vals[value] = np.take(val_array, inds, axis=2)
    # separate out grid for analysis
    bt_focus = bt[w // 2:-(w // 2), w // 2:-(w // 2)]
    yl, xl = bt_focus.shape

    for value in np.arange(5, window + 2, 2):
        counts_valid = np.count_nonzero(~np.isnan(vals[value]), axis=2)
        window_diffs = vals[value] - bt_focus[:, :, np.newaxis]
        window_diffs_abs = np.abs(window_diffs)
        mean_window = np.nanmean(window_diffs, axis=2)
        std_window = np.nanstd(window_diffs, axis=2)
        mad_window = np.nanmean(window_diffs_abs, axis=2)

        # try and save this rubbish
        nc_write(os.path.join(save_folder, fpref + '-w{:02d}-counts_valid.nc'.format(value)), counts_valid)
        nc_write(os.path.join(save_folder, fpref + '-w{:02d}-mean_window.nc'.format(value)), mean_window)
        nc_write(os.path.join(save_folder, fpref + '-w{:02d}-std_window.nc'.format(value)), std_window)
        nc_write(os.path.join(save_folder, fpref + '-w{:02d}-mad_window.nc'.format(value)), mad_window)

        if value == 5:
            max_window_items = 24
        else:
            max_window_items = (value // 2) * 8
        for pc in np.arange(7) * 0.10 + 0.15:
            mn = int(pc * max_window_items)
            num_nan = np.sum(np.isnan(window_diffs), axis=2)
            wind_diff_sort = np.sort(window_diffs, axis=2)
            wind_abs_sort = np.sort(window_diffs_abs, axis=2)

            neg_mean = np.mean(wind_diff_sort[:, :, :mn], axis=2)
            best_mean = np.mean(wind_abs_sort[:, :, :mn], axis=2)
            # create X vector for bad_picks slicing
            row_idx = np.repeat(np.arange(yl*xl)[np.newaxis, :], mn).reshape(yl*xl, -1)
            X = -np.arange(mn) + max_window_items - 1
            bad_picks = (np.broadcast_to(X, (yl, xl, mn)) - num_nan[:, :, np.newaxis]).reshape(yl * xl, -1)
            pos_mean = np.nanmean(wind_diff_sort.reshape(yl*xl, -1)[row_idx, bad_picks], axis=1).reshape(yl, -1)
            worst_mean = np.nanmean(wind_abs_sort.reshape(yl*xl, -1)[row_idx, bad_picks], axis=1).reshape(yl, -1)

            nc_write(os.path.join(save_folder, fpref + '-w{:02d}-{:.2f}-neg_mean.nc'.format(value, pc)), neg_mean)
            nc_write(os.path.join(save_folder, fpref + '-w{:02d}-{:.2f}-pos_mean.nc'.format(value, pc)), pos_mean)
            nc_write(os.path.join(save_folder, fpref + '-w{:02d}-{:.2f}-best_mean.nc'.format(value, pc)), best_mean)
            nc_write(os.path.join(save_folder, fpref + '-w{:02d}-{:.2f}-worst_mean.nc'.format(value, pc)), worst_mean)
    # growing window analysis
    for pc in np.arange(7) * 0.10 + 0.15:
        # bad_list = np.array([np.newaxis,np.newaxis])
        # good_list = np.array(())
        bad_list = 0
        good_list = 0
        for lw in np.arange(5, window + 2, 2):
            #  need no. of values less than trigger in each window size
            if lw == 5:
                lw_window_items = 24
            else:
                lw_window_items = (lw // 2) * 8  # number of items in lw iterable window
            tpc = (lw ** 2) - 1  # total pixels inside lw window
            # highest number of pixels to trigger invalid response at lw window
            vpc = np.floor((pc * lw_window_items) - 0.005).astype(np.int8)
            ttc = np.ceil((tpc * pc)-0.005).astype(np.int8)
            lw_diffs = vals[lw] - bt_focus[:, :, np.newaxis]  # diffs for this lw window
            lw_diffs_abs = np.sort(np.abs(lw_diffs))  # abs diffs for lw window

            num_nan = np.sum(np.isnan(lw_diffs), axis=2)  # work out how many nans live in each pixel list
            # create the selection index arrays - firstly propagate the row indices
            row_idx = np.repeat(np.arange(yl*xl)[np.newaxis, :], vpc).reshape(yl*xl, -1)
            # create aranges for the values to be separated from the lw array
            X = -np.arange(vpc) + lw_window_items - 1
            # broadcast to cs * cs * vpc, take away the nan values, reshape to cs**2
            bad_picks = (np.broadcast_to(X, (yl, xl, vpc)) - num_nan[:, :, np.newaxis]).reshape(yl*xl, -1)

            # nans will not be eliminated at this time - we have to try to get rid of them
            if not isinstance(bad_list, np.ndarray):
                bad_list = lw_diffs_abs.reshape(yl * xl, -1)[row_idx, bad_picks]
            else:
                bad_list = np.concatenate((bad_list, lw_diffs_abs.reshape(yl * xl, -1)[row_idx, bad_picks]),
                                          axis=1)
            if not isinstance(good_list, np.ndarray):
                good_list = lw_diffs_abs.reshape(yl * xl, -1)[:, :vpc]
            else:
                good_list = np.concatenate((good_list, lw_diffs_abs.reshape(yl * xl, -1)[:, :vpc]), axis=1)
            bad_list = np.sort(bad_list, axis=1)[:, -(ttc + 1):]
            good_list = np.sort(good_list, axis=1)[:, :(ttc - 1)]

            if lw != 5:
                bad_out = np.sort(bad_list, axis=1)[:, -ttc:]
                good_out = np.sort(good_list, axis=1)[:, :ttc]
                # if there are nans on board, they're shit and need to go
                bad_pix = np.any(np.isnan(bad_out), axis=1).reshape(yl, -1)
                good_pix = np.any(np.isnan(good_out), axis=1).reshape(yl, -1)
                best_mean_sw = np.nanmean(good_out, axis=1).reshape(yl, -1)
                worst_mean_sw = np.nanmean(bad_out, axis=1).reshape(yl, -1)
                # blow away shit pix
                best_mean_sw[good_pix] = np.nan
                worst_mean_sw[bad_pix] = np.nan
                nc_write(os.path.join(save_folder, fpref + '-lw{:02d}-{:.2f}-worst_mean.nc'.format(lw, pc)), worst_mean_sw)
                nc_write(os.path.join(save_folder, fpref + '-lw{:02d}-{:.2f}-best_mean.nc'.format(lw, pc)), best_mean_sw)
    print('{} complete in {:.1f} secs'.format(fpref, time.time() - st))
    return 1


def proc_land_available():
    # to make a determination on the effectiveness of context, we need to know how much data the
    # land in AHI has available to do calculations
    # first, load in a data file and blow away land in the lv mask not used
    #file_kw = ['B07-w{:02d}-counts_valid'.format(n) for n in np.arange(11) * 2 + 5]
    #temp = Dataset(os.path.join(savepath, '2016/{0}/01/2016{0}-0100-{1}.nc'.format('006', file_kw[0]))
    #               ).variables['data'][:, :]
    # nan out values in counts_valid that aren't land
    #temp[~flt_land] = np.nan
    # copy the land mask into a fresh array
    #cont_lv = np.copy(flt_land)
    #cont_lv[np.isnan(temp)] = False
    # make a padded array for selection purposes
    cont_lv = make_cont_lv()
    cont_lv_pad = np.empty((5501, 5501,), dtype=np.bool)
    cont_lv_pad[:] = False
    cont_lv_pad[:-1, :-1] = cont_lv
    # create the val_array on the padded context values
    val_array = np.array([cont_lv_pad[wy:wy - window, wx:wx - window]
                          for wy in np.arange(w) for wx in np.arange(w)])
    # move axes so that the ind_picker works properly
    val_array = np.moveaxis(val_array, 0, -1)
    # construct the index arrays and fill a dict of values
    vals = {}
    for value in np.arange(5, window + 2, 2):
        # merge lvls 3 and 5
        if value == 5:
            inds = np.concatenate((ind_picker(value - 2), ind_picker(value)))
        else:
            inds = ind_picker(value)
        vals[value] = np.take(val_array, inds, axis=2)
    # trim the land_mask for interaction purposes
    lv_focus = cont_lv[w // 2:-(w // 2), w // 2:-(w // 2)]

    for i in np.arange(5, 27, 2):
        if i == 5:
            temp = np.count_nonzero(vals[5], axis=2).astype(np.float)
        else:
            temp = np.sum(np.stack(
                [np.count_nonzero(vals[v], axis=2) for v in np.arange(5, i+2, 2)], axis=-1), axis=2).astype(np.float)
        # eliminate non-land pixels from evaluation
        temp[~lv_focus] = np.nan
        #print('Context window: {:02d}'.format(i))
        total_land = np.count_nonzero(~np.isnan(temp))
        pc_list = (np.arange(7) * 0.1 + 0.15)[::-1]
        if i == 5:
            #print('Total land pixels evaluated: {}'.format(total_land))
            print('\\multirow{2}{*}{\\textbf{Window size}} & \multicolumn{7}{l}{\\textbf{Percentage'
                  ' of context pixels required for assessment}}\\\\\\cline{2-8}')
            print(' & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\'.format(*pc_list))
        dud_list = []
        for pc in pc_list:
            number_valid = (pc*(i**2 - 1))-0.005
            dud_list.append(total_land - np.count_nonzero(temp > number_valid))
        print('${0} \\times {0}$ & {1} & {2} & {3} & {4} & {5} & {6} & {7} \\\\'.format(i, *dud_list))
        print(' & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% \\\\\\hline'.format(
            *[100 * d / total_land for d in dud_list]))

def cloud_stats():
    day_list = [int(f) for f in os.listdir(os.path.join(savepath, '2016')) if 'png' not in f]
    day_list.sort()
    #cld_stats = open('/mnt/storage/active-fire/P3/AHI_cloud_stats2.txt', 'w')
    cont_lv = make_cont_lv()
    slice_pix = np.zeros((10,))
    slice_cld = np.zeros((36, 10))
    clds = np.zeros((len(day_list)))
    for i, day in enumerate(day_list):
        a = gaf.AHICapture((2016, day, 5, 0))
        r = Dataset(os.path.join(gaf.IMAGEDIR_AHI, 'CLD', '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                                 '{}{:02d}{:02d}{:02d}0000-P1S-ABOM_OBS_CLOUDMASK-PRJ_GEOS141_2000-'
                                 'HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month, a.time.day, a.time.hour)))
        cld = ((r['cloud_mask'][0, :, :]) > 0).astype(np.float)
        cld = np.ma.array(cld, mask=~cont_lv)
        clds[i] = np.count_nonzero(cld == 1)
        #if not i:
        #    print('Cloud by slice \\\\')
        #    print('0 - 500 & 500 - 1000 & 1000 - 1500 & 1500 - 2000 & 2000 - 2500 & 2500 - 3000 & 3000 - 3500 &'
        #          '3500 - 4000 & 4000 - 4500 & 4500 - 5000 \\\\')
        for j, n in enumerate(np.arange(500, 5500, 500)):
            sub_cld = cld[n - 500:n, :]
            slice_cld[i, j] = np.count_nonzero(sub_cld == 1)
            if not i:
                slice_pix[j] = np.count_nonzero(sub_cld == 0) + np.count_nonzero(sub_cld == 1)
    u = 100*slice_cld/slice_pix[np.newaxis, :]
    print('Y range & Land pixels & Mean cloud & SD cloud \\\\')
    ranges = ['0 - 500', '500 - 1000', '1000 - 1500', '1500 - 2000', '2000 - 2500', '2500 - 3000', '3000 - 3500',
              '3500 - 4000', '4000 - 4500', '4500 - 5000']
    for i in np.arange(len(ranges)):
        print('{} & {:.0f} & {:.1f}\% & {:.1f}\% \\\\'.format(ranges[i], slice_pix[i], np.mean(u, axis=0)[i],
                                                              np.std(u, axis=0)[i]))

        # print('{} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {}'.format(day, *slice_cld))
        # print(' & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% '
        #      '& {:.2f}\% & {:.2f}\%'.format(*[100 * slice_cld[n]/slice_pix[n] for n in np.arange(len(slice_cld))]))


def breakdown_by_pixels_available():
    cont_lv = make_cont_lv()
    for k in np.arange(5, 27, 2):
        mean_kw = 'B07-w{:02d}-mean_window'.format(k)
        count_kw = 'B07-w{:02d}-counts_valid'.format(k)
        if k == 5:
            max_count = 24
        else:
            max_count = 8 * (k//2)
        window_df = pd.DataFrame(index=d_list, columns=[*['count{:03d}'.format(n) for n in np.arange(max_count) + 1],
                                                        *['mean{:03d}'.format(n) for n in np.arange(max_count) + 1],
                                                        *['std{:03d}'.format(n) for n in np.arange(max_count) + 1]])
        for d in d_list:
            folname = os.path.join(savepath, '2016', '{:03d}'.format(d), '05')
            x_bar = Dataset(os.path.join(folname,
                                         [f for f in os.listdir(folname) if mean_kw in f][0])).variables['data'][:]
            counts = Dataset(os.path.join(folname,
                                          [f for f in os.listdir(folname) if count_kw in f][0])).variables['data'][:]
            counts[~cont_lv] = np.nan
            for n in np.arange(max_count) + 1:
                window_df.loc[d]['count{:03d}'.format(n)] = np.count_nonzero(counts == n)
                window_df.loc[d]['mean{:03d}'.format(n)] = np.nanmean(x_bar[counts == n])
                window_df.loc[d]['std{:03d}'.format(n)] = np.nanstd(x_bar[counts == n])
        window_df.to_pickle(os.path.join(savepath, 'figures', 'count_vs_mean', '{}.pkl'.format(k)))


def plot_pixel_breakdown():
    x_bar = {}
    x_std = {}
    f, axes = plt.subplots(1, 2, figsize=(14, 6))
    text_size = 14
    # f1, ax1 = plt.subplots(figsize=(8, 6))
    axes[0].axhline(0, c='0.4')
    cmap = plt.get_cmap('gnuplot')
    colors = [cmap(i) for i in np.linspace(0, 1, 11)]
    for i, k in enumerate(np.arange(5, 27, 2)):
        if k == 5:
            max_count = 24
        else:
            max_count = 8 * (k // 2)
        window_df = pd.read_pickle(os.path.join(savepath, 'figures', 'count_vs_mean', '{}.pkl'.format(k)))
        # show trend for means by counts_valid
        x_bar[k] = np.zeros(max_count)
        x_std[k] = np.zeros(max_count)
        for j, n in enumerate(np.arange(max_count) + 1):
            x_bar[k][j] = ((window_df['count{:03d}'.format(n)] * window_df['mean{:03d}'.format(n)]).sum() /
                           window_df['count{:03d}'.format(n)].sum())
            x_std[k][j] = ((window_df['count{:03d}'.format(n)] * window_df['std{:03d}'.format(n)]).sum() /
                           window_df['count{:03d}'.format(n)].sum())
        axes[0].plot(100 * (np.arange(max_count) + 1) / max_count, x_bar[k], zorder=11 - i, label='{0} x {0}'.format(k),
                     color=colors[i])
        axes[1].plot(100 * (np.arange(max_count) + 1) / max_count, x_std[k], zorder=11 - i, label='{0} x {0}'.format(k),
                     color=colors[i])
    axes[0].set_ylim(-0.7, 0.7)
    axes[1].set_ylim(0, 5)
    for ax in axes:
        ax.set_xlabel('Percentage of pixels available in outer ring of window', fontsize=text_size)
        ax.set_xticks(np.linspace(0, 100, 11))
        ax.set_xlim(102, 0)
        ax.tick_params(axis='both', which='major', labelsize=text_size)
    for i, item in enumerate(['a', 'b']):
        axes[i].text(100, (axes[i].get_ylim()[1] - 0.06*(axes[i].get_ylim()[1] -
                                                       axes[i].get_ylim()[0])), '{})'.format(item), fontsize=16)
    axes[1].legend(fontsize=text_size, title='Ring size', ncol=2)
    axes[0].set_ylabel('Mean temperature difference', fontsize=text_size)
    axes[1].set_ylabel('Mean standard deviation', fontsize=text_size)
    f.set_tight_layout(True)
    f.show()
    f.savefig(os.path.join(savepath, 'figures', 'count_vs_mean', 'disk_context_breakdown3.png'), format='png', dpi=300)
    # f.show()


def break_by_sensor_angle():
    #cont_lv = make_cont_lv()
    mean_list = {}
    std_list = {}
    for k in np.arange(5, 27, 2):
        mean_kw = 'B07-w{:02d}-mean_window'.format(k)
        sensor_breaks = np.linspace(0, 80, 9).astype(np.int)
        sensor_df = pd.DataFrame(index=d_list,
                                 columns=[*['count_{:02d}'.format(sensor_breaks[n + 1]) for n in np.arange(8)],
                                          *['mean_{:02d}'.format(sensor_breaks[n + 1]) for n in np.arange(8)],
                                          *['std_{:02d}'.format(sensor_breaks[n + 1]) for n in np.arange(8)]])
        for d in d_list:
            folname = os.path.join(savepath, '2016', '{:03d}'.format(d), '05')
            x_bar = Dataset(os.path.join(folname,
                                         [f for f in os.listdir(folname) if mean_kw in f][0])).variables['data'][:]
            for i in np.arange(sensor_breaks.shape[0]-1):
                sensor_df.loc[d]['mean_{:02d}'.format(sensor_breaks[i + 1])] = \
                    np.nanmean(x_bar[np.logical_and(va > sensor_breaks[i], va <= sensor_breaks[i + 1])])
                sensor_df.loc[d]['std_{:02d}'.format(sensor_breaks[i + 1])] = \
                    np.nanstd(x_bar[np.logical_and(va > sensor_breaks[i], va <= sensor_breaks[i + 1])])
                sensor_df.loc[d]['count_{:02d}'.format(sensor_breaks[i + 1])] = \
                    np.count_nonzero(x_bar[np.logical_and.reduce(
                        (~np.isnan(x_bar), va > sensor_breaks[i], va <= sensor_breaks[i + 1]))])

        mean_list[k] = np.zeros(8)
        std_list[k] = np.zeros(8)
        for j in np.arange(sensor_breaks.shape[0]-1):
            mean_list[k][j] = ((sensor_df['count_{:02d}'.format(sensor_breaks[j + 1])] *
                                sensor_df['mean_{:02d}'.format(sensor_breaks[j + 1])]).sum() /
                               sensor_df['count_{:02d}'.format(sensor_breaks[j + 1])].sum())
            std_list[k][j] = ((sensor_df['count_{:02d}'.format(sensor_breaks[j + 1])] *
                               sensor_df['std_{:02d}'.format(sensor_breaks[j + 1])]).sum() /
                              sensor_df['count_{:02d}'.format(sensor_breaks[j + 1])].sum())
    return mean_list, std_list


def full_disk_stats():
    iterables = [d_list, np.arange(11) * 2 + 5]
    mi = pd.MultiIndex.from_product(iterables, names=['day', 'window_size'])
    df = pd.DataFrame(index=mi, columns=['counts', 'mean', 'std', 'mad'])
    for k in iterables[1]:
        mean_kw = 'B07-w{:02d}-mean_window'.format(k)
        mad_kw = 'B07-w{:02d}-mad_window'.format(k)
        for d in iterables[0]:
            folname = os.path.join(savepath, '2016', '{:03d}'.format(d), '05')
            mean_data = Dataset(os.path.join(folname, [f for f in os.listdir(folname) if mean_kw in f][0])).variables[
                            'data'][:]
            x_mean = np.nanmean(mean_data)
            x_cnt = np.count_nonzero(~np.isnan(mean_data))
            x_mad = np.nanmean(
                Dataset(os.path.join(folname, [f for f in os.listdir(folname) if mad_kw in f][0])).variables['data'][:])
            x_std = np.nanstd(mean_data)
            df.loc[(d, k)] = x_cnt, x_mean, x_std, x_mad
    df.to_pickle(os.path.join(savepath, 'figures', 'full_disk', 'full_disk.pkl'))


def use_full_disk_stats():
    # \label{T:mean_SD_FD}
    try:
        df = pd.read_pickle(os.path.join(savepath, 'figures', 'full_disk', 'full_disk.pkl'))
    except:
        print('Run function full_disk_stats() to get dataframe')
        raise
    df['counts'] = df['counts'].astype(int)
    for col in ['mean', 'mad', 'std']:
        df[col] = df[col].astype(float)
    w_sizes = np.arange(5, 27, 2)
    # weighted full disk stats
    means = np.zeros(11)
    stds = np.zeros(11)
    count = np.zeros(11, dtype=np.int)
    gdf = df.groupby(['window_size'])
    for i, k in enumerate(w_sizes):
        ddf = gdf.get_group(k)
        means[i] = (ddf['counts'] * ddf['mean']).sum() / ddf['counts'].sum()
        stds[i] = (ddf['counts'] * ddf['std']).sum() / ddf['counts'].sum()
        count[i] = ddf['counts'].sum().astype(int)
    print('window & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} \\\\'.format(*w_sizes))
    print('mean & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f}'
          ' & {:.3f} & {:.3f} & {:.3f} \\\\'.format(*means))
    print('std & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f}'
          ' & {:.3f} & {:.3f} & {:.3f} \\\\'.format(*stds))
    print('count & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} \\\\'.format(*count))


def proc_expanding_window_analysis(arb_thresh=0):
    cont_lv = make_cont_lv()
    pc = (np.arange(7) * 10 + 15)[::-1]
    mc = {5: 24}
    mc.update({key: 8 * (key // 2) for key in np.arange(7, 27, 2)})
    mi = pd.MultiIndex.from_product([d_list, mc.keys()], names=['day', 'window_size'])
    mdf = pd.DataFrame(index=mi, columns=['count100', *['count{:02d}'.format(n) for n in pc],
                                          'mean100', *['mean{:02d}'.format(n) for n in pc],
                                          'std100', *['std{:02d}'.format(n) for n in pc],
                                          'total_day'])
    # mdf = mdf.astype(dtype={col: np.float for col in mdf.columns if 'count' in col})
    # mdf = mdf.astype(dtype={col: np.float for col in mdf.columns if 'mean' in col})
    # mdf = mdf.astype(dtype={col: np.float for col in mdf.columns if 'std' in col})
    for d in d_list:
        folname = os.path.join(savepath, '2016', '{:03d}'.format(d), '05')
        for win_size in list(mc.keys()):
            mean_kw = 'B07-w{:02d}-mean_window'.format(win_size)
            count_kw = 'B07-w{:02d}-counts_valid'.format(win_size)
            x_bar = Dataset(os.path.join(folname,
                                         [f for f in os.listdir(folname) if mean_kw in f][0])).variables['data'][:]
            counts = Dataset(os.path.join(folname,
                                          [f for f in os.listdir(folname) if count_kw in f][0])).variables['data'][:]
            # blow away non-land
            counts[~cont_lv] = np.nan
            # blow away values covered by cloud
            counts[np.isnan(x_bar)] = np.nan
            # remove masked values in x_bar - see [201,3424]
            counts[np.where(x_bar.mask)] = np.nan
            if win_size == 5:
                mdf.loc[(d, win_size)]['count100'] = np.count_nonzero(counts == mc[win_size])
                mdf.loc[(d, win_size)]['mean100'] = np.nanmean(x_bar[counts == mc[win_size]])
                mdf.loc[(d, win_size)]['std100'] = np.nanstd(x_bar[counts == mc[win_size]])
                mdf.loc[(d, win_size)]['total_day'] = np.count_nonzero(~np.isnan(counts))
                # mdf.loc[(d, win_size)]['count0'] = np.count_nonzero(counts < pc[-1]*mc[win_size])
                wdf = {}
                for n in pc:
                    wdf[n] = pd.DataFrame(columns=['x', 'y', *['wcount{:02d}'.format(key) for key in mc.keys()],
                                                   *['mean{:02d}'.format(key) for key in mc.keys()],
                                                   *['status{:02d}'.format(key) for key in mc.keys()],
                                                   'total_count', 'result_mean'])
                    wdf[n] = wdf[n].astype(dtype={'x': np.int, 'y': np.int, 'total_count': np.int, 'result_mean': np.float})
                    wdf[n] = wdf[n].astype(dtype={'wcount{:02d}'.format(key): np.int for key in mc.keys()})
                    wdf[n] = wdf[n].astype(dtype={'mean{:02d}'.format(key): np.float for key in mc.keys()})
                    wdf[n] = wdf[n].astype(dtype={'status{:02d}'.format(key): np.bool for key in mc.keys()})
                    if arb_thresh:
                        cutoff = np.min((np.floor(mc[win_size] * n / 100).astype(int), arb_thresh))
                    else:
                        cutoff = np.floor(mc[win_size] * n / 100).astype(int)
                    mdf.loc[(d, win_size)]['count{:02d}'.format(n)] = np.count_nonzero(
                        np.logical_and(counts < mc[win_size], counts > cutoff))
                    mdf.loc[(d, win_size)]['mean{:02d}'.format(n)] = np.nanmean(x_bar[np.where(
                        np.logical_and(counts < mc[win_size], counts > cutoff))])
                    mdf.loc[(d, win_size)]['std{:02d}'.format(n)] = np.nanstd(x_bar[np.where(
                        np.logical_and(counts < mc[win_size], counts > cutoff))])
                    dud_values = np.where(np.logical_and(counts >= 0, counts <= cutoff))
                    wdf[n]['y'] = dud_values[0]
                    wdf[n]['x'] = dud_values[1]
                    wdf[n]['wcount{:02d}'.format(win_size)] = counts[dud_values]
                    wdf[n]['total_count'] += counts[dud_values]
                    wdf[n]['mean{:02d}'.format(win_size)] = x_bar[dud_values]
                    wdf[n][[col for col in wdf[n].columns if 'status' in col]] = False
            else:
                for n in pc:
                    # calculate cutoff value
                    # cutoff = np.floor(mc[win_size] * n)
                    status_list = ['status{:02d}'.format(x) for x in np.arange(5, win_size, 2)]
                    # select out the duds from the 5 x 5 window
                    ndf = wdf[n].loc[np.where(~np.any(wdf[n][status_list].values, axis=1))]
                    duds = ndf[['y', 'x']].values
                    # slice the mean and counts
                    ndf['mean{:02d}'.format(win_size)] = x_bar[duds[:, 0], duds[:, 1]]
                    ndf['wcount{:02d}'.format(win_size)] = counts[duds[:, 0], duds[:, 1]]
                    # add to the total count of pixels for cutoffs
                    ndf['total_count'] = np.nansum(ndf[[col for col in ndf.columns if 'wcount' in col]].values, axis=1)
                    # initialize status column as False
                    # if there is no threshold of pixels, drive for the additional pixels from the extra window
                    if not arb_thresh:
                        ndf.loc[ndf['total_count'] >= np.floor(n / 100 * np.sum(
                            [v for key, v in mc.items() if key <= win_size])), 'status{:02d}'.format(win_size)] = True
                    # else we use the arb cutoff
                    else:
                        ndf.loc[ndf['total_count'] >= np.min((arb_thresh, np.floor(n / 100 * np.sum(
                            [v for key, v in mc.items() if key <= win_size])))), 'status{:02d}'.format(win_size)] = True
                    # if we've been successful before we don't want to recalculate, so set the status False again
                    #try:
                    #    ndf.loc[ndf[np.any(ndf[status_list].values, axis=1)], 'status{:02d}'.format(win_size)] = False
                    #except ValueError:
                    #    pass
                    # assign resultant mean to success dataframe
                    sub_frame = ndf[ndf['status{:02d}'.format(win_size)] == True]
                    # sub_frame['result_mean'] = np.nanmean(
                    #    sub_frame[[c for c in wdf[n].columns if 'mean' in c]].values)
                    sub_frame['result_mean'] = np.nansum([sub_frame['mean{:02d}'.format(x)] *
                                                          sub_frame['wcount{:02d}'.format(x)]
                                                          for x in np.arange(5, win_size + 2, 2)],
                                                         axis=0)/sub_frame.total_count
                    ndf.loc[sub_frame.index] = sub_frame
                    wdf[n].loc[ndf.index] = ndf
                    mdf.loc[(d, win_size)]['count{:02d}'.format(n)] = sub_frame.shape[0]
                    mdf.loc[(d, win_size)]['mean{:02d}'.format(n)] = sub_frame['result_mean'].mean()
                    mdf.loc[(d, win_size)]['std{:02d}'.format(n)] = sub_frame['result_mean'].std()
            print('d{:03d}-w{:02d} done'.format(d, win_size))
        for k in wdf.keys():
            wdf[k].to_pickle(os.path.join(savepath, 'figures', 'expanding',
                                          'wdf-{:03d}-{:02d}-{}.pkl'.format(d, k, arb_thresh)))
    mdf.to_pickle(os.path.join(savepath, 'figures', 'expanding', 'mdf-{}.pkl'.format(arb_thresh)))


def construct_exp_window_stats(arb_thresh=0):
    arb_thresh = 0
    mdf = pd.read_pickle(os.path.join(savepath, 'figures', 'expanding', 'mdf-{}.pkl'.format(arb_thresh)))
    rdf = pd.DataFrame(columns=mdf.columns)
    win_list = np.arange(5, 27, 2)
    rdf['window_size'] = win_list
    rdf = rdf.set_index('window_size')
    total_pixels = mdf.xs(5, level=1).total_day.sum()
    rdf.total_day = total_pixels
    for k in win_list:
        cnt_list = [col for col in mdf.columns if 'count' in col]
        if k != 5:
            cnt_list.remove('count100')
        for col in cnt_list:
            rdf.loc[k, col] = mdf.xs(k, level=1)[col].sum()
        mnc_list = [col for col in mdf.columns if 'mean' in col]
        if k != 5:
            mnc_list.remove('mean100')
        for col in mnc_list:
            rdf.loc[k, col] = (mdf.xs(k, level=1)[col] * mdf.xs(k, level=1)['count{}'.format(col[4:])]).sum() / \
                              mdf.xs(k, level=1)['count{}'.format(col[4:])].sum()
        stc_list = [col for col in mdf.columns if 'std' in col]
        if k != 5:
            stc_list.remove('std100')
        for col in stc_list:
            rdf.loc[k, col] = (mdf.xs(k, level=1)[col] * mdf.xs(k, level=1)['count{}'.format(col[3:])]).sum() / \
                              mdf.xs(k, level=1)['count{}'.format(col[3:])].sum()
    pc = (np.arange(7) * 10 + 15)[::-1]
    print(
        '\\textbf{window} & \\textbf{pc} & \\textbf{1.00} & \\textbf{0.99 - 0.75} & \\textbf{0.99 - 0.65} & '
        '\\textbf{0.99 - 0.55} & \\textbf{0.99 - 0.45} & \\textbf{0.99 - 0.35} & \\textbf{0.99 - 0.25} & '
        '\\textbf{0.99 - 0.15} \\\\')
    print('\\hline\\hline')
    for k in win_list:
        if k == 5:
            print('{0} $\\times$ {0}'.format(k) + ' & mean (\SI{}{\kelvin}) ' + '& {:.3f} & {:.3f} & {:.3f} & {:.3f} & '
                                                                                '{:.3f} & {:.3f} & {:.3f} & '
                                                                                '{:.3f} \\\\'.format(
                *rdf.xs(k)[[col for col in rdf.columns if 'mean' in col]].values))
            print(' & std (\SI{}{\kelvin}) ' + '& {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} '
                  '& {:.3f} \\\\'.format(*rdf.xs(k)[[col for col in rdf.columns if 'std' in col]].values))
            print(' & count & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} \\\\'.format(
                  *rdf.xs(k)[[col for col in rdf.columns if 'count' in col]].values))
            percentages = (rdf.xs(k)[[col for col in rdf.columns if 'count' in col]] /
                           rdf.xs(k)['total_day'])
            print(' & \% avail & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & '
                  '{:.2f}\% & {:.2f}\% & {:.2f}\% \\\\'.format(*percentages * 100))
            print('\\hline')
            pc_5x5 = np.array(rdf.xs(k).count100 + rdf.xs(k)[cnt_list].values, dtype=np.float) / rdf.xs(5)['total_day']
            print('\\multicolumn{3}{l||}{Total 5x5 success}' + ' & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & '
                  '{:.2f}\% & {:.2f}\% & {:.2f}\% \\\\'.format(*pc_5x5 * 100))
            print('\\hline')
        else:
            print('{0} $\\times$ {0}'.format(k) + ' & mean (\SI{}{\kelvin}) ' + '& - & {:.3f} & {:.3f} & {:.3f} & '
                                                                                '{:.3f} & {:.3f} & {:.3f} & {:.3f} '
                                                                                '\\\\'.format(*rdf.xs(k)[mnc_list].values))
            print(' & std (\SI{}{\kelvin}) ' + '& - & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & '
                                               '{:.3f} \\\\'.format(*rdf.xs(k)[stc_list].values))
            print(' & count & 0 & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} \\\\'.format(
                  *rdf.xs(k)[cnt_list].values))
            percentages = rdf.xs(k)[cnt_list] / rdf.xs(k)['total_day']
            print(' & \% avail & 0\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% & '
                  '{:.2f}\% & {:.2f}\% & {:.2f}\% \\\\'.format(*percentages * 100))
            print('\\hline')
    total_fails = np.array([rdf.loc[5].total_day - (rdf.loc[5].count100 + rdf[col].sum()) for col in cnt_list])
    print('\\multicolumn{3}{l||}{Total failures}' + ' & {:.0f} & {:.0f} & {:.0f} & {:.0f} & {:.0f} '
                                                    '& {:.0f} & {:.0f} \\\\'.format(*total_fails))
    pc_fails = 100 * total_fails / total_pixels
    print('\\multicolumn{3}{l||}{}' + ' & {:.2f}\% & {:.2f}\% & {:.2f}\% & {:.2f}\% '
                                      '& {:.2f}\% & {:.2f}\% & {:.2f}\% \\\\'.format(*pc_fails))
    return rdf


def plot_window_breaks(rdf):
    pc = (np.arange(7) * 10 + 15)[::-1]
    cnt_list = [col for col in rdf.columns if 'count' in col]
    cnt_list.remove('count100')
    #cnt_list = ['count45', 'count35', 'count25', 'count15'][::-1]
    descent = rdf.loc[np.arange(7, 27, 2), cnt_list].values
    total_fails = np.array([rdf.loc[5].total_day - (rdf.loc[5].count100 + rdf[col].sum()) for col in cnt_list])
    y = np.concatenate((descent, total_fails[np.newaxis, :]), axis=0)
    t = y / y.sum(axis=0)
    f, ax = plt.subplots(figsize=(8, 6))
    cmap = plt.get_cmap('gnuplot')
    width = 7
    colors = [cmap(i) for i in np.linspace(0, 1, t.shape[0])][::-1]
    for i, n in enumerate(np.arange(7, 27, 2)):
        if not i:
            ax.bar(pc, t[i], width, color=colors[i], label=n)
        else:
            ax.bar(pc, t[i], width, bottom=np.sum(t[:i], axis=0), color=colors[i], label=n)
    ax.bar(pc, t[-1], width, bottom=np.sum(t[:10], axis=0), fc='0.8', label='Failed')
    #ax.set_xticks(np.arange(15, 55, 10))
    ax.set_xticks(np.arange(15, 85, 10))
    ax.legend(title='Window size', bbox_to_anchor=(1.04, 1), borderaxespad=0)
    f.subplots_adjust(right=0.8)
    ax.set_xlim(ax.get_xlim()[1], ax.get_xlim()[0])
    ax.set_ylim(1, 0)
    ax.set_yticklabels((0, 20, 40, 60, 80, 100))
    ax.set_xlabel('Percentage of assessable context')
    ax.set_ylabel('Percentage of success per window size')
    f.savefig(os.path.join(savepath, 'figures', 'expanding', 'pc_break.png'), format='png', dpi=300)


def time_of_day():
    cont_lv = make_cont_lv()
    day_path = os.path.join(savepath, '2016', '314')
    folders = os.listdir(day_path)
    win_size = 5
    hours = np.arange(24)
    df = {}
    for h in hours:
        folname = os.path.join(day_path, '{:02d}'.format(h))
        mean_kw = 'B07-w{:02d}-mean_window'.format(win_size)
        count_kw = 'B07-w{:02d}-counts_valid'.format(win_size)
        x_bar = Dataset(os.path.join(folname,
                                     [f for f in os.listdir(folname) if mean_kw in f][0])).variables['data'][:]
        counts = Dataset(os.path.join(folname,
                                      [f for f in os.listdir(folname) if count_kw in f][0])).variables['data'][:]
        # blow away non-land
        counts[~cont_lv] = np.nan
        # blow away values covered by cloud
        counts[np.isnan(x_bar)] = np.nan
        # remove masked values in x_bar - see [201,3424]
        counts[np.where(x_bar.mask)] = np.nan
        pos = np.column_stack(np.where(~np.isnan(counts)))

        df[h] = pd.DataFrame(columns=['x', 'y', 't_off', 'mean','count'])
        df[h]['x'] = pos[:, 1]
        df[h]['y'] = pos[:, 0]
        lons = gaf.get_longitude()
        df[h]['t_off'] = h + lons[pos[:, 1], pos[:, 0]] / 15
        df[h]['mean'] = x_bar[np.where(~np.isnan(counts))]
        df[h]['count'] = counts[np.where(~np.isnan(counts))]


'''
if __name__ == '__main__':
    proc_list = make_proc_list()
    p = Pool(4)
    p.map(proc_chunk, (i for i in proc_list))
    print('Process complete')

    file_folder = os.path.join(savepath, '2016')
    dlist = os.listdir(file_folder)
    tile_list = []
    for d in dlist:
        hour_list = [h for h in os.listdir(os.path.join(file_folder, d)) if not 'sup']
        [tile_list.append(os.path.join(file_folder, d, h)) for h in hour_list
         if len(os.listdir(os.path.join(file_folder, d, h))) == 32472]
    # p = Pool(6)
    # p.map(combine_folder, (i for i in tile_list))
    # print('Combine complete')



def combine_folder(dpath):
    unique_filetypes = [f for f in os.listdir(dpath) if '[1,1]' in f]
    fd_filenames = ['-'.join(f.split('-')[:3] + f.split('-')[4:]) for f in unique_filetypes]
    files_merged = 0
    for item in fd_filenames:
        tiles = [f for f in os.listdir(dpath) if '-'+'-'.join(item.split('-')[3:]) in f]
        if len(tiles) != 66:
            print('Not enough files for {} to be constructed/already constructed'.format(item))
            continue
        data_array = np.empty((ars, ars))
        data_array[:, :] = np.nan
        for tile in tiles:
            y = int(tile.split('-')[3].split(',')[0][1:])
            x = int(tile.split('-')[3].split(',')[1][:-1])

            if y == 0:
                y_range = [w // 2, cs * (y+1)]
            elif y == 10:
                y_range = [y * cs, ars - (w // 2)]
            else:
                y_range = [y * cs, (y + 1) * cs]
            if x == 0:
                x_range = [w // 2, cs * (x+1)]
            elif x == 10:
                x_range = [x * cs, ars - (w // 2)]
            else:
                x_range = [x * cs, (x + 1) * cs]

            r = Dataset(os.path.join(dpath, tile))
            nc_data = r.variables['data'][:, :]
            data_array[y_range[0]:y_range[1], x_range[0]:x_range[1]] = nc_data
            r.close()
        nc_write(os.path.join(dpath, item), data_array)
        files_merged += 1
    if files_merged == 492:
        print('File merge for {} complete'.format('-'.join(unique_filetypes[0].split('-')[:3])))
        files_to_delete = [f for f in os.listdir(dpath) if '[' in f]
        [os.remove(os.path.join(dpath, f)) for f in files_to_delete]
'''