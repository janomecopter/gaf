# -*- coding: utf-8 -*-
"""
Created on Fri Sep 08 16:23:19 2017

@author: e76243
"""

import pandas as pd
import gaf
import os

file_store = os.path.join(gaf.IMAGEDIR,'process')
flist = gaf.listdir_end(file_store,').h5')

ilist = []
for f in flist:
    blkid = f.split('-')[-1].split(')')[0]
    if blkid not in ilist:
        ilist.append('{}'.format(blkid))
ilist = ['-{}'.format(s) for s in ilist]

metadata = {}
metadata['bt'] = 'AHI Band 7 brightness temperature'
metadata['fit'] = 'BAT SVD fitted termperature - 1hr fitting interval'
metadata['cld'] = ' ABOM clear sky probability * 3'
metadata['time_loc'] = 'local solar time of AHI measurement - UTC + longitude * 15 (hours)'
metadata['time_utc'] = 'UTC time of AHI measurement'

for item in ilist:
    flist = gaf.listdir_contains(file_store,item + ').h5')
    for i, f in enumerate(flist):
        df = pd.read_hdf(f)
        if not i:
            plist = [s.split('_')[0][1:] for s in df.columns if 'bt' in s]
            mf = df
        else:
            mf = pd.concat((mf,df))
    for loc in plist:
        col_list = [s for s in mf.columns if loc in s]
        for s in ('time_loc','time_utc'):
            col_list.append(s)
        df = mf[col_list]
        [df.rename(columns={s:s.split('_')[1]},inplace=True) for s in df.columns if loc in s]
        df.reset_index(drop=True,inplace=True)
        l = gaf.AHILocation((int(loc[:4]),int(loc[4:])))
        #metadata = dict([(s,getattr(l,s)) for s in ['line','pixel','latitude','longitude']])
        [metadata.update({'{}'.format(s): getattr(l,s)}) for s in ['line','pixel','latitude','longitude']]
        metadata['utctime_start'] = df.time_utc.values[0]
        metadata['utctime_end'] = df.time_utc.values[-1]
        gaf.h5store(os.path.join(file_store,'pp','l{}.h5'.format(loc)),df,**metadata)