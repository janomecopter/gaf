# -*- coding: utf-8 -*-
"""
Created on Mon Aug 28 12:39:57 2017

@author: e76243
"""

import gaf
from multiprocessing import Pool, cpu_count
import os
import numpy as np
import matplotlib.path as mplPath
import pandas as pd
import datetime

#save_p = '/mnt/storage/Himawari/temp/FFSig'
# vertices is defined by extents of Landsat image
vertices = np.array([[ 127.21126739,  -16.29740049],
                     [ 128.95958049,  -16.66295062],
                     [ 128.57547943,  -18.39527001],
                     [ 126.81048949,  -18.02542991]])

start_time = gaf.Time((2016,219,0,0))
finish_time = gaf.Time((2016,236,6,0))
t_array = pd.date_range(start_time.dt,finish_time.dt,
                        freq='1H').to_pydatetime()

pp = mplPath.Path(vertices)

lon, lat, bt = gaf.mask_AHI_for_plot(gaf.EXAMPLEFILE)
fits = pp.contains_points(np.column_stack((np.ravel(lon),
                                           np.ravel(lat)))).reshape(lon.shape)
locs = np.column_stack((array for array in np.where(fits)))

ll_locs = np.array([[lon[item[0], item[1]], lat[item[0], item[1]]] for item in locs])
s = pd.DataFrame({'line': np.where(fits)[0], 'pixel': np.where(fits)[1],
                  'longitude': ll_locs[:, 0], 'latitude': ll_locs[:, 1]})
s['rnd_lon'] = np.floor(s.longitude*4)/4.
s['rnd_lat'] = np.floor(s.latitude*4)/4.

sg = s.groupby(['rnd_lat', 'rnd_lon'])
keys = list(sg.groups.keys())
keys.sort()
#keys = keys[::-1]

save_path = os.path.join(gaf.IMAGEDIR,'process')
if not os.path.isdir(save_path):
    os.makedirs(save_path)

def rando_funct(key):
    print('{} start...'.format(key))
    loc_df = sg.get_group(key)
    blk_lat = loc_df.rnd_lat[loc_df.index[0]]
    blk_lon = loc_df.rnd_lon[loc_df.index[0]]
    locs = loc_df[['line', 'pixel']].values
    
    bt_df = gaf.block_bt_stack(locs, start_time, finish_time)
    bt_df['time_loc'] = bt_df.index.to_pydatetime() + datetime.timedelta(hours = blk_lon/15)
    print('{} bt stacked'.format(key))
    
    for i,t in enumerate(t_array):
        tt = gaf.Time(t)
        img_times = pd.date_range(t,t + datetime.timedelta(minutes = 1430),
                              freq='10min').to_pydatetime()
        
        print('std_data for blk{} at {} - start'.format(key,t))
        t_df = gaf.produce_std_data(locs[0],t,14)
        r_df = pd.DataFrame({'time_utc': pd.date_range(t + datetime.timedelta(hours=23),periods=6,freq='10min')})
        print('std_data for blk{} at {} - produced'.format(key,t))
        for loc in locs:
            bt_str = 'l{}{}_bt'.format(*loc)
            b_df = bt_df.loc[img_times[0]:img_times[-1]][['time_loc',bt_str]]
            b_df['s_temp'] = (b_df[bt_str] - b_df[bt_str].mean())/b_df[bt_str].std()
            b_df = b_df.rename(columns={bt_str: 'bt'})
            b_df['time'] = bt_df['time_loc'].dt.time
            b_df = b_df.dropna()
            # subselect the training matrix on the off chance there are more 
            # than two nans
            p_df = t_df.loc[b_df.time]            
            #b_df = gaf.make_std_diurnal(loc,t,cloud=True)
            b_df = gaf.SVD_fit(b_df,p_df)[['fit']]
            b_df.rename(columns={'fit':'l{:04d}{:04d}_fit'.format(*loc)},
                        inplace=True)
            r_df = pd.merge(r_df,b_df,how='left',left_on='time_utc',right_index=True)
        #if not i:
        #m_df = r_df
        #else:
        #    m_df = pd.concat((m_df,r_df))
        print('std_data for blk{} at {} - complete'.format(key,t))
        m_df = pd.merge(bt_df,r_df,how='right',left_index=True,right_on='time_utc')
        f_name = '{}{:03d}-{:02d}{:02d}-blk({},{})'.format(tt.year,tt.jday,
                  tt.hour,tt.minute,blk_lat,blk_lon)
        m_df.to_hdf(os.path.join(save_path,f_name + '.h5'),'df')
        m_df.to_csv(os.path.join(save_path,f_name + '.csv'))
        print('{} saved'.format(f_name))

if __name__ == '__main__':
    #array = [[g,h] for g in times for h in blks]
    p = Pool(2)
    p.map(rando_funct,(i for i in keys))
    import message_sender
    message_sender.send_complete('ffsig_1hr_fits')
