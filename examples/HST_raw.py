# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 16:51:21 2018

@author: e76243
"""

import struct
import numpy as np
import matplotlib.pyplot as plt

fn = r"C:\Users\Public\Downloads\HS_H08_20160826_0550_B07_FLDK_R20_S0810.DAT"

f = open(fn, 'rb')

d = {}
hlen = 0
d['b1'] = f.read(282)
hlen += 282
d['b2'] = f.read(50)
hlen += 50
d['b3'] = f.read(127)
hlen += 127
d['b4'] = f.read(139)
hlen += 139
d['b5'] = f.read(147)
hlen += 147
d['b6'] = f.read(259)
hlen += 259
d['b7'] = f.read(47)
hlen += 47
# Nav
f.read(1)
blen, = struct.unpack('<h', f.read(2))
d['b8'] = f.read(blen - 3)
hlen += blen
# Obs time
f.read(1)
blen, = struct.unpack('<h', f.read(2))
d['b9'] = f.read(blen - 3)
hlen += blen
#10 Error information block
f.read(1)
blen, = struct.unpack('<h', f.read(2))
d['b10'] = f.read(blen - 3)
hlen += blen
#11 Spare block
f.read(259)
hlen += 259

ncols = struct.unpack('<h', d['b2'][5:7])[0]
nrows = struct.unpack('<h', d['b2'][7:9])[0]

data = np.array([struct.unpack('<h', f.read(2))[0] for i in range(ncols*nrows)]).reshape(nrows, -1)

f.close()

band = struct.unpack('<h', d['b5'][3:5])[0]
cent_lambda = struct.unpack('d', d['b5'][5:13])[0]/1e6

bits = struct.unpack('<h', d['b5'][13:15])[0]
gain = struct.unpack('d', d['b5'][19:27])[0]
cons = struct.unpack('d', d['b5'][27:35])[0]
if band < 7:
    max_r = (2 ** bits - 1) * gain + cons
    albedo = struct.unpack('d', d['b5'][35:43])[0]
else:
    #max_r = gain + cons
    c0 = struct.unpack('d', d['b5'][35:43])[0]
    c1 = struct.unpack('d', d['b5'][43:51])[0]
    c2 = struct.unpack('d', d['b5'][51:59])[0]
    C_0 = struct.unpack('d', d['b5'][59:67])[0]
    C_1 = struct.unpack('d', d['b5'][67:75])[0]
    C_2 = struct.unpack('d', d['b5'][75:83])[0]
    c = struct.unpack('d', d['b5'][83:91])[0]
    h = struct.unpack('d', d['b5'][91:99])[0]
    k = struct.unpack('d', d['b5'][99:107])[0]
    max_t_cnt = np.nanmin(data[data > 0])
    #max_rad = max_t_cnt * gain + cons
    max_rad = cons

    #b1 = -0.479757
    #b2 = 1.000766
    #b3 = -1.860569e-07
    #v = 2575.767
    #te_max = (h*c*v/k)/(np.log(1+(2*h*c**2*v**3/max_rad)))
    #tb_max = b1 + b2*te_max + b3*te_max**2

    Te_max = h * c / (k * cent_lambda * np.log(1 + (2 * h * c**2 / (cent_lambda**5 * max_rad))))
    Tb_max = c0 + c1 * Te_max + c2 * Te_max**2

print('Band number: {}'.format(band))
print('Centre wavelength: {}'.format(cent_lambda))
print('Bits per pixel: {}'.format(bits))
print('Gain: {}'.format(gain))
print('Constant: {}'.format(cons))
if band < 7:
    print('Maximum radiance: {} W/m**2 sr um'.format(max_r))
    print('Albedo co-efficient: {}'.format(albedo))
    print('Max albedo: {}'.format(max_r * albedo))
else:
    print('Maximum radiance: {} W/m**2 sr um'.format(max_rad))
    print('c0: {}'.format(c0))
    print('c1: {}'.format(c1))
    print('c2: {}'.format(c2))
    print('C0: {}'.format(C_0))
    print('C1: {}'.format(C_1))
    print('C2: {}'.format(C_2))
    print('Max brightness temperature in image: {}'.format(Tb_max))

#im = plt.imshow(data)
#plt.colorbar(im)