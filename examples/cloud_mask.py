import gaf
import datetime
import numpy as np
from scipy.ndimage import convolve, generic_filter
from scipy import LowLevelCallable  # needs scipy>=0.19.0
# import cartopy
import time
from numba import cfunc, carray
from numba.types import intc, intp, float64, voidptr
from numba.types import CPointer
import os  # os does not work after the generic_filter for some reason
import pandas as pd
from multiprocessing import Pool

#  CFuncs adapted from https://ilovesymposia.com/2017/03/12/
#  scipys-new-lowlevelcallable-is-a-game-changer/
@cfunc(intc(CPointer(float64), intp, CPointer(float64), voidptr))
def stdzero(values_ptr, len_values, result, data):
    values = carray(values_ptr, (len_values,), dtype=float64)
    result[0] = np.nanstd(values)
    return 1


@cfunc(intc(CPointer(float64), intp, CPointer(float64), voidptr))
def meanzero(values_ptr, len_values, result, data):
    values = carray(values_ptr, (len_values,), dtype=float64)
    result[0] = np.nanmean(values)
    return 1


# Radiance parameters for band 7 conversion
ahi7TtoI_params = [132850.7746477298, 3698.0788552409813, -6.937464229518511]
ahi3rtoI = 0.0019254997
# ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
verbose = True

test1_ndays = 30
test1_cldfree = 278
test2_cld = 207
all_daytime_angle = 80
test3_day_thresh = 290
test3_day_diff = 6
test3_night_thresh = 285
test3_night_diff = 5
test4_albedo = 0.3
test5_ratio = 100
save_path = '/media/sfm-user/Sunflower Bucket'


def make_cloud(t):
    # pulls the handles for AHI for the selected time
    a = gaf.AHICapture(t)
    start = time.time()
    # check if file exists
    cld_name = [s for s in a.cld_images if 'WU' in s][0]
    dirpath = r'{}/AHI/CLD_WU/{}/{:03d}'.format(save_path, a.time.year, a.time.jday)
    fpath = dirpath + '/' + cld_name
    #if os.path.isfile(fpath):
    #    print('{} already done'.format(cld_name))
    #    return 0
    try:  # check all images are available, otherwise throw error
        a.band3.data
        a.band7.data
        a.band13.data
        a.solar.data
    except AttributeError:
        print('{} files not available'.format(cld_name))
        return 0
    #  Cloud as bitmask - classified by test failed
    cld = np.zeros((5500, 5500)).astype(int)
    # mask the off-disk section of the cloud mask
    dmask = gaf.get_latitude().mask
    cld = np.ma.array(cld, mask=dmask)
    #  Temporal contrast (thermal)
    first_cap = gaf.Time(a.time.dt - datetime.timedelta(days=test1_ndays))
    if first_cap.year == a.time.year:
        jday_list = np.arange(first_cap.jday, a.time.jday)
        date_list = np.column_stack((np.repeat(a.time.year, test1_ndays), jday_list))
    else:
        date_list = np.zeros((test1_ndays, 2)).astype(int)
        for i, item in enumerate(date_list):
            tt = gaf.Time(a.time.dt - datetime.timedelta(days=test1_ndays - i))
            date_list[i] = [tt.year, tt.jday]
    # create an array to stack the temp values
    b13_histdata = np.zeros((test1_ndays, 5500, 5500)).astype(np.float32)
    # create a Capture instance for each time and extract the B13 data
    for i, x in enumerate(date_list):
        temp = gaf.AHICapture((x[0], x[1], a.time.hour, a.time.minute))
        try:
            b13_histdata[i] = temp.band13.data
        except AttributeError:
            pass
        # nan out values below 278K
        b13_histdata[i][b13_histdata[i] < test1_cldfree] = np.nan
    # first test
    cld[np.logical_and(a.band13.data < np.nanmean(b13_histdata, axis=0)
                       - 1.5 * np.nanstd(b13_histdata, axis=0),
                       np.count_nonzero(b13_histdata, axis=0) > 2)] += 1
    # clean-up
    del b13_histdata
    #  TIR BT Threshold
    cld[a.band13.data < test2_cld] += 2
    #  Daytime Check - save day as we'll use it later
    day = (a.solar.data < all_daytime_angle)
    #  Infrared difference (day)
    cld[np.logical_and.reduce((day, a.band7.data < test3_day_thresh,
                               a.band7.data - a.band13.data > test3_day_diff))] += 4
    #  Infrared difference (night)
    cld[np.logical_and.reduce((~day, a.band7.data < test3_night_thresh,
                              a.band7.data - a.band13.data > test3_night_diff))] += 8
    #  Daytime VIS - albedo up to 0.3
    cld[np.logical_and(day, a.band3.data > test4_albedo)] += 16
    #  Daytime VIS to MIR Ratio
    b3rad = a.band3.data / ahi3rtoI
    params = ahi7TtoI_params
    b7rad = params[0] / (np.exp(params[1] / a.band7.data) + params[2])
    cld[np.logical_and(b3rad / test5_ratio > b7rad, day)] += 32

    second_t = time.time()
    if verbose:
        print('First six tests in {:.1f} secs'.format(second_t - start))

    wincld = np.zeros_like(cld).astype(np.uint8)
    vcslp = np.logical_and(a.band7.data > 280, a.band7.data - a.band13.data < 5)
    # Assign masked values to False and reassign to eliminate the mask - this is done
    # to eliminate issues with Band7 saturation in cyclonic storms, which causes masked
    # pixels in these areas
    vcslp[vcslp.mask] = False
    vcslp = vcslp.data
    # grabs the land product from the ahi ancillary data
    lmask = (gaf.ahi.get_land_mask() == 0)
    # blow away water
    vcslp[lmask] = np.nan

    b7_valid = np.copy(a.band7.data)
    # grabs a copy of the MWIR and throws away invalid values
    b7_valid[~vcslp] = np.nan
    b7_valid[~lmask] = np.nan
    b7_valid[dmask] = np.nan

    window_sizes = [5, 7]#, 9, 11, 13, 15, 17, 19, 21]
    cld_added = np.zeros_like(window_sizes)
    pixels_failed = np.zeros_like(window_sizes)

    for i, size in enumerate(window_sizes):
        kernel = np.ones((size, size))
        cent_idx = int(size / 2)
        # eliminates the central pixel from evaluation
        kernel[cent_idx, cent_idx] = 0
        # generates the number of pixels with valid values in the kernel
        counts = convolve(vcslp.astype(int), kernel)
        # calls the ctype functions described above to create the mean and std of B7
        b7mean = generic_filter(b7_valid, LowLevelCallable(meanzero.ctypes), footprint=kernel)
        b7std = generic_filter(b7_valid, LowLevelCallable(stdzero.ctypes), footprint=kernel)
        # apply test
        context = b7mean - 2 * b7std - 10
        # this counts the numbers of pixels that are added to the wincld array, and whether
        # they are added to the mask or not
        cld_add = np.count_nonzero(np.logical_and.reduce((~day, wincld == 0, counts > 9,
                                                          a.band7.data < context, lmask), axis=0))
        pix_none = np.count_nonzero(np.logical_and.reduce((~day, wincld == 0, counts > 9,
                                                           a.band7.data >= context, lmask), axis=0))
        cld_added[i] = cld_add
        pixels_failed[i] = pix_none
        # add values to wincld - pass with no cloud = 1, pass with cloud = 2
        wincld[np.logical_and.reduce((~day, counts > 9, wincld == 0,
                                      a.band7.data >= context, lmask), axis=0)] = 1
        wincld[np.logical_and.reduce((~day, counts > 9, wincld == 0,
                                      a.band7.data < context, lmask), axis=0)] = 2
        if verbose:
            print('Window {} adds {} pixels to cloudmask'.format(size, cld_add))
            print('Window {} has {} pixels passing without cloud'.format(size, pix_none))
            print('Night mask now contains {} cloud pixels and {} other land pixels'
                  .format(np.count_nonzero(wincld == 2), np.count_nonzero(wincld == 1)))
        # reduce_day = day[cent_idx:h - cent_idx, cent_idx:w - cent_idx]
        # reduce_b7 = a.band7.data[cent_idx:h - cent_idx, cent_idx:w - cent_idx]

        # wincld[cent_idx:w - cent_idx, cent_idx:h - cent_idx] = \
        #    np.add(wincld[cent_idx:w - cent_idx, cent_idx:h - cent_idx],
        #           np.logical_and(~reduce_day, reduce_b7 < context),
        #           where=~wincld[cent_idx:w - cent_idx, cent_idx:h - cent_idx])
        # print('Window {} complete'.format(size))
    # assign to cloud if test6 added
    cld[wincld == 2] += 64
    gaf.cloud_mask_write(fpath, cld.astype(np.uint8))
    #columns = ['cld_added', 'pix_failed']
    #df = pd.DataFrame(data=np.column_stack((cld_added, pixels_failed)),
    #                  index=window_sizes, columns=columns)
    #df.to_csv(r'{}/temp/cloud_stats/'.format(save_path) + cld_name.split('.')[0] + '.csv')
    if verbose:
        print('{} - task complete in {:.1f} secs'.format(cld_name, time.time() - start))
    return 0


if __name__ == '__main__':
    # assert os.path.isdir(gaf.IMAGEDIR_AHI)
    d_list = [6, 10, 20, 35, 36, 41, 71, 72, 82, 97, 101, 103, 133, 144, 149, 153, 164, 173, 184,
              188, 200, 222, 230, 236, 253, 257, 274, 279, 286, 290, 314, 322, 323, 343, 353, 355]
    t_list = np.array([[2016, d, h, 0] for d in d_list for h in np.arange(0, 24)])
    tb = np.zeros(len(t_list)).astype(np.bool)
    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        # check if file exists
        cld_name = [s for s in a.cld_images if 'WU' in s][0]
        dirpath = r'{}/AHI/CLD_WU/{}/{:03d}'.format(save_path, a.time.year, a.time.jday)
        fpath = dirpath + '/' + cld_name
        if os.path.isfile(fpath):
            print('{} already done'.format(cld_name))
        else:
            tb[i] = True

    p = Pool(6)
    p.map(make_cloud, (i for i in t_list[tb]))
