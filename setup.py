import sys
from setuptools import setup

if sys.version_info < (3, 6):
    sys.exit('Python 3.6 is the minimum required version')

INSTALL_REQUIRES = [
    'numpy',
    'cartopy',
    'pandas',
    'shapely',
    'matplotlib',
    'netCDF4',
    'requests',
    'scipy>=0.17.0',
    'pyhdf'
]

setup(
    name='gaf',
    version='0.1.0',
    packages=['gaf'],
    url='',
    license='',
    author='Bryan Hally',
    author_email='bryan.hally@gmail.com',
    description='Geostationary Active Fire Toolset',
    py_modules=['gaf'],
    install_requires=INSTALL_REQUIRES
)
