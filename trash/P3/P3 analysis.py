import os
import numpy as np
import matplotlib.pyplot as plt
import cartopy
import gaf
from netCDF4 import Dataset

ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
#file_path = '/media/sfm-user/Sunflower Bucket/P3/2016'
savepath = '/mnt/storage/active-fire/P3/2016'
window = w = 25
chunk_size = cs = 500
array_size = ars = 5500
no_chunks = ars // cs
land = gaf.get_land_mask()
va = gaf.get_viewangle()
lv_mask = ~np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)

days = [f for f in os.listdir(savepath) if 'png' not in f]
# calculate mean of 5w over the year

file_kw = ['B07-w{:02d}-mean_window'.format(n) for n in np.arange(11)*2 + 5]

for kw in file_kw:
    data = np.ma.empty((ars, ars, len(days)))
    data[:] = np.nan

    for i, day in enumerate(days):
        temp = Dataset(savepath + '/{0}/01/2016{0}-0100-{1}.nc'.format(day, kw)
                       ).variables['data'][:, :].astype(np.float32)
        data[:, :, i] = np.ma.array(temp, mask=np.isnan(temp))

    mean_data = np.nanmean(data, axis=2).astype(np.float32)

    print('{} Stats:'.format(kw))
    print('Bottom percentile (2.5%): {:.2f}'.format(np.percentile(mean_data[~mean_data.mask], 2.5)))
    print('Top percentile (97.5%): {:.2f}'.format(np.percentile(mean_data[~mean_data.mask], 97.5)))
    print('Percent <-2K: {:.2f}%'.format(100*np.count_nonzero(mean_data < -2)
                                         / (5500**2-np.ma.count_masked(mean_data))))
    print('Percent >2K: {:.2f}%\n\n'.format(100*np.count_nonzero(mean_data > 2)
                                            / (5500**2-np.ma.count_masked(mean_data))))
    '''
    f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))
    vmin = -5
    vmax = 5
    img_extent = [-5500000, 5500000, -5500000, 5500000]
    ax.imshow(mean_data, cmap='viridis', origin='upper', extent=img_extent, transform=ahi_crs,
              vmin=vmin, vmax=vmax)
    ax.gridlines(xlocs=np.arange(-180, 195, 5), ylocs=np.arange(-80, 85, 5))
    ax.set_extent([143.5, 150.5, -40, -32])
    # create colorbar so we can label it
    sm = plt.cm.ScalarMappable(cmap='viridis', norm=plt.Normalize(vmin, vmax))
    sm._A = []  # not sure what this does, but why break things
    cb = plt.colorbar(sm, ax=ax)
    cb.set_label('Temperature difference (K)')
    ax.set_title('Mean contextual difference @ {}\nAHI images taken @ 0100 UTC'.format(kw.split('-')[1]))
    f.savefig('{}/{}_Vic.png'.format(file_path, kw), format='png')
    plt.close(f)
    '''

def time_composite(fpath):
    file_kw = ['B07-w{:02d}-mean_window'.format(n) for n in np.arange(11) * 2 + 5][0]
    hours = [h for h in os.listdir(fpath) if os.path.isdir(os.path.join(fpath, h))]
    # construct composites every four hours
    comp_h = dict(
        [[i, np.ma.masked_array(np.zeros((5500, 5500)), mask=np.any((va > 80, va.mask)))] for i in np.arange(6) * 4])
    # pull longitudes for time info
    lons = gaf.get_longitude()
    # create time offset
    to = np.floor(lons/15).astype(np.int)
    # wacky shit happens - make -12 equal to 12
    to[to < 0] += 24
    for comp_time in np.arange(6)*4:
        image_key = comp_time - to
        image_key[image_key < 0] += 24
        image_key[image_key > 23] -= 24
        for t in np.unique(image_key)[:-1]:
            data = Dataset([os.path.join(fpath, '{:02d}'.format(t), f) for f in os.listdir(os.path.join(
                fpath, '{:02d}'.format(t))) if file_kw in f][0]).variables['data'][:, :]
            comp_h[comp_time][image_key == t] = data[image_key == t]


def window_staging(day = 6):
    #file_kw = ['B07-w{:02d}-mean_window'.format(n) for n in np.arange(11) * 2 + 5]
    a = gaf.AHICapture((2016, day, 1, 0))
    text_out = open(savepath + '/{0}{1:03d}-{2:02d}00_context_stats.txt'.format(
            a.time.year, a.time.jday, a.time.hour), 'w')
    file_kw = ['B07-w{:02d}-counts_valid'.format(n) for n in np.arange(11) * 2 + 5]
    temp = Dataset(os.path.join(savepath, '{0}/{1:03d}/{2:02d}/{0}{1:03d}-{2:02d}00-{3}.nc'.format(
        a.time.year, a.time.jday, a.time.hour, file_kw[0]))).variables['data'][:, :]
    # nan out values in counts_valid that aren't land
    temp[lv_mask] = np.nan
    # copy the land mask into a fresh array
    cont_lv = np.copy(lv_mask)
    cont_lv[np.isnan(temp)] = True
    counts_valid = {}
    for i, kw in enumerate(file_kw):
        # reassign i to window size
        ii = i * 2 + 5
        temp = Dataset('/mnt/storage/active-fire/P3/{0}/{1:03d}/{2:02d}/{0}{1:03d}-{2:02d}00-'.format(
            a.time.year, a.time.jday, a.time.hour) + kw + '.nc').variables['data'][:, :]
        temp[cont_lv] = np.nan
        counts_valid[ii] = temp
    # pull cloud
    r = Dataset(os.path.join(gaf.IMAGEDIR_AHI, 'CLD', '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                             '{}{:02d}{:02d}{:02d}0000-P1S-ABOM_OBS_CLOUDMASK-PRJ_GEOS141_2000-'
                             'HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month, a.time.day, a.time.hour)))
    cld = ((r['cloud_mask'][0, :, :]) > 0).astype(np.float)
    cld[cont_lv] = np.nan
    for key in counts_valid.keys():
        counts_valid[key][cld == 1] = -1

    total_pixels = np.count_nonzero(~np.isnan(counts_valid[5]))
    cloud_pixels = np.count_nonzero(counts_valid[5] == -1)

    print('Context per image: {0}{1:03d}-{2:02d}00'.format(a.time.year, a.time.jday, a.time.hour), file=text_out)
    print('Cloud affected pixels: {} ({:.2f}%)'.format(cloud_pixels, 100 * cloud_pixels / total_pixels), file=text_out)
    for i in np.arange(5, 27, 2):
        if i == 5:
            temp = counts_valid[i][~np.isnan(counts_valid[i])]
        else:
            temp = np.sum(np.stack([counts_valid[v][~np.isnan(counts_valid[v])] for v in np.arange(5, i + 2, 2)], axis=-1), axis=1)
        for pc in np.arange(7)*0.1+0.15:
            pixels_for_pass = (pc*(i**2 - 1) - 0.005)
            #temp = counts_valid[i][~np.isnan(counts_valid[i])]
            passing_pixel_count = np.count_nonzero(temp > pixels_for_pass)
            print('Pixels with enough values at window size {} @ {}%: {} ({:.2f}%)'.format(
                i, int(pc*100), passing_pixel_count,
                passing_pixel_count * 100 / total_pixels), file=text_out)
        print('\n', file=text_out)

    for pc in np.arange(7) * 0.1 + 0.15:
        for i in np.arange(5, 27, 2):
            pixels_for_pass = (pc * (i ** 2 - 1) - 0.005)
            if i == 5:
                failed_pixels = np.logical_and(counts_valid[5] < pixels_for_pass,
                                               counts_valid[5] != -1)
                orig_fail = np.count_nonzero(failed_pixels)
                print('Failed pixels at window {} @ {}%: {} ({:.2f}%)'.format(
                    i, int(pc*100), orig_fail, orig_fail*100/total_pixels), file=text_out)
            else:
                p = counts_valid[i][np.where(failed_pixels)]
                now_pass = len(p[p > pixels_for_pass])
                print('Fails at {}, passes at {} ({}%): {} ({:.2f}%)'.format(
                    i-2, i, int(pc*100), now_pass, 100 * now_pass/orig_fail), file=text_out)
                failed_pixels = np.logical_and(failed_pixels, counts_valid[i] < pixels_for_pass)
        total_fails = np.count_nonzero(failed_pixels)
        print('Pixels that totally fail: {} ({:.2f}%)'.format(total_fails, 100 * total_fails/orig_fail), file=text_out)
        print('\n', file=text_out)
    text_out.close()


def cloud_stats():
    day_list = [int(f) for f in os.listdir(savepath) if 'png' not in f]
    cld_stats = open('/mnt/storage/active-fire/P3/AHI_cloud_stats.txt', 'w')
    a = gaf.AHICapture((2016, day_list[0], 1, 0))
    temp = Dataset(os.path.join(savepath, '{0}/{1:03d}/{2:02d}/{0}{1:03d}-{2:02d}00-{3}.nc'.format(
        a.time.year, a.time.jday, a.time.hour, file_kw[0]))).variables['data'][:, :]
    # nan out values in counts_valid that aren't land
    temp[lv_mask] = np.nan
    # copy the land mask into a fresh array
    cont_lv = np.copy(lv_mask)
    cont_lv[np.isnan(temp)] = True
    for day in day_list:
        a = gaf.AHICapture((2016, day, 1, 0))
        r = Dataset(os.path.join(gaf.IMAGEDIR_AHI, 'CLD', '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                                 '{}{:02d}{:02d}{:02d}0000-P1S-ABOM_OBS_CLOUDMASK-PRJ_GEOS141_2000-'
                                 'HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month, a.time.day, a.time.hour)))
        cld = ((r['cloud_mask'][0, :, :]) > 0).astype(np.float)
        cld = np.ma.array(cld, mask=cont_lv)
        print('Day number: {}'.format(day), file=cld_stats)
        print('Total cloud: {} ({:.2f}%)'.format(np.count_nonzero(cld == 1), 100 * np.count_nonzero(cld == 1) / (
                    np.count_nonzero(cld == 0) + np.count_nonzero(cld == 1))), file=cld_stats)
        for n in np.arange(500, 5500, 500):
            sub_cld = cld[n - 500:n, :]
            tot_pix = np.count_nonzero(sub_cld == 0) + np.count_nonzero(sub_cld == 1)
            cld_pix = np.count_nonzero(sub_cld == 1)
            print('Slice {}: {} cloud of {} pixels ({:.2f}%)'.format(n, cld_pix, tot_pix, 100 * cld_pix / tot_pix),
                  file=cld_stats)
    cld_stats.close()
