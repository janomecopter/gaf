# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 13:37:11 2018

@author: e76243
"""

from ftplib import FTP
import os
import h5py
import numpy as np
import pandas as pd
import geopandas
from shapely.geometry import Point

# server info - different products can be obtained by changing the dpath variable
server = 'ladsweb.modaps.eosdis.nasa.gov'
user = 'anonymous'
passwd = 'bryan.hally@rmit.edu.au'
dpath = 'allData/5000/VNP03IMGLL/2016'
#local_store = '/active-fire/Other/VIIRS'
local_store = 'C:/scratch/cache'
vnp14 = 'VNP14IMG'
save_path = os.path.join(local_store,'VIIRS')

def def_poslist(ylen,xlen):
    xr = np.arange(0,xlen,40)
    yr = np.arange(0,ylen,40)
    poslist = []
    for x in xr:
        poslist.append([0,x])
    for y in yr:
        poslist.append([y,xlen-1])
    poslist.append([ylen-1,xlen-1])
    for x in xr[::-1]:
        poslist.append([ylen-1,x])
    for y in yr[::-1][:-1]:
        poslist.append([y,0])
    poslist = np.array(poslist)
    return poslist

#for r,d,f in os.walk(os.path.join(local_store,vnp14)):
#    filelist = f
with open('C:/Users/Public/Downloads/vnp14-4.txt','r') as f:
    filelist = f.readlines()
    
dtlist = [[item.split('.')[1][-3:],item.split('.')[2]] for item in filelist]
# create a list of unique julian days, as the server needs to be crawled by date
day_list = list(set([item[0] for item in dtlist]))
day_list.sort()

# create list of coords to select for shapefile
poslist = def_poslist(6464,6400)

# open connection to ftp server
ftp = FTP(server,user=user,passwd=passwd)
# change working directory to the directory in dpath
ftp.cwd(dpath)

for day in day_list:
    # separate out files for the selected day
    tlist = [item for item in dtlist if day in item[0]]
    ftp.cwd(day)
    # grab full list of files on the server for that day
    flist = ftp.nlst()
    for time in tlist:
        # crawl flist for the relevant file
        fname = [item for item in flist if '{}.{}.'.format(day,time[1]) in item][0]
        pkl_file = os.path.join(save_path,'{}_bounds.df'.format('.'.join(fname.split('.')[:-1])))
        csv_file = os.path.join(save_path,'{}_bounds.csv'.format('.'.join(fname.split('.')[:-1])))
        shp_file = os.path.join(save_path,'{}_bounds.shp'.format('.'.join(fname.split('.')[:-1])))
        # if file not already saved in path
        if not os.path.isfile(pkl_file):
            fsize = ftp.size(fname)
            with open(os.path.join(save_path,fname),'wb') as f:
                # retrieve binary file from server
                ftp.retrbinary('RETR {}'.format(fname),f.write)
            print('{} - size {} - saved'.format(fname,fsize))
            # create shape from extents
            r = h5py.File(os.path.join(save_path,fname))
            poly = np.zeros_like(poslist).astype(np.float32)
            try:
                for i,item in enumerate(poslist):
                    poly[i] = [r['HDFEOS']['SWATHS']['VNP_375M_GEOLOCATION']\
                                   ['Geolocation Fields']['Longitude'][item[0],item[1]],
                                   r['HDFEOS']['SWATHS']['VNP_375M_GEOLOCATION']\
                                   ['Geolocation Fields']['Latitude'][item[0],item[1]]]
            except ValueError:
                dims = r['HDFEOS']['SWATHS']['VNP_375M_GEOLOCATION']['Geolocation Fields']['Longitude'].shape
                poslist2 = def_poslist(*dims)
                poly = np.zeros_like(poslist2).astype(np.float32)
                for i,item in enumerate(poslist2):
                    poly[i] = [r['HDFEOS']['SWATHS']['VNP_375M_GEOLOCATION']\
                                   ['Geolocation Fields']['Longitude'][item[0],item[1]],
                                   r['HDFEOS']['SWATHS']['VNP_375M_GEOLOCATION']\
                                   ['Geolocation Fields']['Latitude'][item[0],item[1]]]
            df = pd.DataFrame(poly,columns=['lon','lat'])
            df.to_pickle(pkl_file)
            df.to_csv(csv_file)
            df['geometry'] = df.apply(lambda x: Point((float(x.lon),float(x.lat))),axis=1)
            df = geopandas.GeoDataFrame(df,geometry='geometry')
            df.to_file(shp_file,driver='ESRI Shapefile')
            r.close()
            if os.path.isfile(os.path.join(save_path,fname)):
                os.remove(os.path.join(save_path,fname))
            print('File {} processed'.format(fname))
        else:
            print('{} skipped'.format(fname))
    # at end of day, go back to the year folder
    ftp.cwd('..')
    #break
# close ftp once done
ftp.close()
