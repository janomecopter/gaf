import gaf
import datetime
import numpy as np
import matplotlib.pyplot as plt
#  from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1 import AxesGrid

desert = (-23.062968, 135.069303)
coast = (-35.563905, 150.307933)
daylist = [gaf.Time((2016, d, 0, 0)) for d in np.arange(14)+212]

f = plt.figure(figsize=(14, 4))
grid = AxesGrid(f, 111, nrows_ncols=(1, 2), axes_pad=0.1, label_mode='1', cbar_location='right', cbar_mode='single')
f2, ax = plt.subplots(1, 2, figsize=(14, 8), sharey=True)
for j, obj in enumerate([desert, coast]):
    loc = gaf.AHILocation(obj)
    bt = np.zeros((14, 144))
    for i, day in enumerate(daylist):
        start_time = day.dt - datetime.timedelta(minutes=np.floor(loc.time_offset/10)*10)
        bt[i] = gaf.block_bt_stack(loc, start_time)['l{}_{}_bt'.format(loc.line, loc.pixel)].values
    im = grid[j].imshow(bt[:, ~np.all(np.isnan(bt), axis=0)], vmin=270, vmax=320, aspect=3)
    grid[j].set_title('Loc: {:6.3f} E lon, {:6.3f} S lat'.format(loc.longitude, loc.latitude))
    grid[j].set_yticks(np.arange(0, 14, 2))
    ax[j].plot(bt[5])
    ax[j].set_xlim(0, len(~np.isnan(bt[5])))
    ax[j].set_xlabel('Image of day')
    grid[j].set_xlabel('Image of day')
    if not j:
        grid[j].set_ylabel('Day of Series')
        ax[j].set_ylabel('Brightness Temperature (K)')
    else:
        cb = grid.cbar_axes[0].colorbar(im)
        cb.set_label_text('Brightness Temperature (K)', fontsize=10)
f2.tight_layout()