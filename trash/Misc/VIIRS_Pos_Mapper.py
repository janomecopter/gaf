# -*- coding: utf-8 -*-
"""
Created on Thu Nov 09 13:16:35 2017

@author: e76243
"""

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import matplotlib.path as mplPath
import numpy as np
from gaf.tools import Time,listdir_end,draw_poly
from shapely.geometry import LineString
from osgeo import gdal
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("product", help='sensor product used')
args = parser.parse_args()

if args.product:
    proc = args.product
else:
    proc = 'VIIRS'
    #proc = 'MODIS'

if proc == 'VIIRS':
    file_path = 'C:/Scratch/cache/VIIRS'
    file_list = listdir_end(file_path,'.nc')
elif proc == 'MODIS':
    file_path = 'C:/Scratch/cache/MODIS/2016MM'
    file_list = listdir_end(file_path,'hdf')

# define list of days for each month of 2016
year = 2016
days = np.arange(366) + 1
dm = np.array([Time((year,day,0,0)).month for day in days])

months = []
for month in np.arange(12)+1:
    months.append(days[dm==month])

# catch error in basemap 1.1 geos handling
try:
    mb = Basemap(projection='geos',lon_0=140.7,resolution='h')
except ValueError:
    mb = Basemap(projection='geos',rsphere=(6378137,6356752.3142),lon_0=140.7,
                 resolution='h')
    
# create great circle plots
dud = (170.198333,-45.928055)
mdl = (95.977928,21.702156)
#vte = (102.563256,17.988322)
#cei = (99.882928,19.952342)

ll = mb.drawgreatcircle(*[l for x in [mdl,dud] for l in x])
plt.close('all')

try:
    gclist = mb(ll.vertices[:,0],ll.vertices[:,1],inverse=True)
except AttributeError:
    gclist = mb(ll[0].get_xydata()[:,0],ll[0].get_xydata()[:,1],inverse=True)
gc = np.column_stack((gclist[0],gclist[1]))
line = LineString(gc)

latints = []
for lat in np.arange(-45.25,21,2.5):
#for lat in np.arange(-45.25,19.75,2.5):
    l2 = LineString([(90,lat),(180,lat)])
    p = line.intersection(l2)    
    latints.append([p.x, p.y])

p = []
for pos in latints:    
    x = [np.round(pos[0]*4)/4., pos[1]]
    pl = np.array([[x[0]-1.25, x[1]], [x[0]+1.25, x[1]], [x[0]+1.25, x[1]+0.25], [x[0]-1.25, x[1]+0.25]])
    p.append(pl)
p = np.array(p)

patch_count = np.zeros((len(p),12))

def plot_disk(parray,m):
    f,ax = plt.subplots(figsize=(8,8))
    if proc == 'VIIRS':
        mb.scatter(parray[:,0],parray[:,1],s=5,c=parray[:,2],vmax=100,
                   alpha=0.2,latlon=True,ax=ax)
    elif proc == 'MODIS':
        mb.scatter(parray[:,0],parray[:,1],s=5,vmax=100,alpha=0.2,
                   latlon=True,ax=ax)
    mb.drawcoastlines(ax=ax)
    for patch in p:
        a = draw_poly(patch[:,1],patch[:,0],mb,ec='r',lw=0.5,fc='none',zorder=2)  
        ax.add_patch(a)  
    mstr = Time((year,m+1,1,0,0)).dt.strftime("%b")
    ax.set_title('{} {}'.format(proc,mstr))
    f.savefig('H:/Python/temp/{} 2016{:02d}.png'.format(proc,m+1),format='png',dpi=300)
    plt.close(f)

if proc == 'VIIRS':
    from netCDF4 import Dataset
    for m,month in enumerate(months):
        point_lat = []
        point_lon = []
        point_pow = []
        for day in month:
            flist = [item for item in file_list if 'A{}{:03d}.'.format(year,day) in item]
            for f in flist:
                with Dataset(f) as r:
                    try:
                        point_lat.append(r.variables['FP_latitude'][:])
                        point_lon.append(r.variables['FP_longitude'][:])
                        point_pow.append(r.variables['FP_power'][:])
                    except IndexError:
                        continue
        parray = np.column_stack((np.concatenate(point_lon).ravel(),
                                  np.concatenate(point_lat).ravel(),
                                  np.concatenate(point_pow).ravel()))
        for i,patch in enumerate(p):    
            path = mplPath.Path(patch)    
            patch_count[i,m] = np.count_nonzero(path.contains_points(parray[:,:2]))
        plot_disk(parray,m)

elif proc == 'MODIS':
    import pyproj
    p_modis_grid = pyproj.Proj('+proj=sinu +R=6371007.181 +nadgrids=@null +wktext')
    e = []
    for f in file_list:
        if f[39:46] not in e:
            e.append(f[39:46])
    gd = gdal.Open(file_list[0])
    dset_path = gd.GetSubDatasets()[0][0]
    d = gdal.Open(dset_path)
    transform = d.GetGeoTransform()
    pixelWidth = transform[1]
    pixelHeight = transform[5]
    x = []
    for i in range(2400):
        x.append(i*pixelWidth)
        xx = np.repeat(np.array(x),2400,axis=0).reshape(2400,-1).transpose()
    y = []
    for i in range(2400):
        y.append(i*pixelHeight)
        yy = np.tile(np.array(y).transpose(),(1,2400)).reshape(2400,-1).transpose()
    for m,day in enumerate(e):
        flist = [item for item in file_list if 'A{}.'.format(day) in item]
        for k,f in enumerate(flist):
            gd = gdal.Open(f)
            dset_path = gd.GetSubDatasets()[0][0]
            d = gdal.Open(dset_path)
            transform = d.GetGeoTransform()
            xOrigin = transform[0]
            yOrigin = transform[3]
            lon, lat = p_modis_grid(xx+xOrigin, yy+yOrigin, inverse=True)
            da = d.ReadAsArray()
            if not k:
                parray = np.column_stack((lon[da>0],lat[da>0],da[da>0]))
            else:
                parray = np.vstack((parray,np.column_stack((lon[da>0],lat[da>0],da[da>0]))))

        for i,patch in enumerate(p):
            path = mplPath.Path(patch)    
            patch_count[i,m] = np.count_nonzero(path.contains_points(parray[:,:2]))  
        plot_disk(parray,m)

f,ax = plt.subplots(figsize=(6,8))
ax.imshow(np.flipud(patch_count),vmax=1000,cmap='OrRd')
h,w = patch_count.shape
for x in range(h):
    for y in range(w):
        ax.annotate(str(np.flipud(patch_count.astype(int))[x][y]),xy=(y, x),
                    horizontalalignment='center',verticalalignment='center',size=6)
ax.xaxis.set_ticks(range(w))
ax.yaxis.set_ticks(range(h))
mlist = ['J','F','M','A','M','J','J','A','S','O','N','D']
ax.xaxis.set_ticklabels(mlist)
ax.yaxis.set_ticklabels([item[0,1] for item in p][::-1])
ax.set_xlabel('Month')
ax.set_ylabel('Latitude')
ax.set_title('{} Fire Frequency per patch - 2016'.format(proc),fontsize=10)
f.savefig('H:/Python/temp/{} fire freq 2016.png'.format(proc),format='png')
plt.close(f)
