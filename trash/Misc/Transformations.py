# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 15:12:22 2018

@author: e76243
"""

import numpy as np
import csv

# File with control point info
fname = 'H:/Python/temp/MatureCoords.csv'
# File of points to be translated
f2name = 'H:/Python/temp/MatureControl.csv'

point_list = []
with open(fname, 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        point_list.append(row)

ids = []
for row in point_list:
    ids.append(row.pop(0))
try:
    ids = np.array(ids).astype(int)
except:
    ids[0] = 42
    ids = np.array(ids).astype(int)
pl = np.array(point_list).astype(float)

A = np.zeros((len(point_list) * 3, 6))
L = np.zeros((len(point_list) * 3, 1))
for i, row in enumerate(pl):
    A[i * 3] = [row[0], -row[1], 0, 1, 0, 0]
    A[i * 3 + 1] = [row[1], row[0], 0, 0, 1, 0]
    A[i * 3 + 2] = [0, 0, row[2], 0, 0, 1]
    L[i * 3] = row[3]
    L[i * 3 + 1] = row[4]
    L[i * 3 + 2] = row[5]

X = np.dot(np.linalg.inv(np.dot(A.T, A)), np.dot(A.T, L))
v = np.dot(A, X) - L

cntrl_list = []
with open(f2name, 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        cntrl_list.append(row)

cntrl_ids = []
for row in cntrl_list:
    cntrl_ids.append(row.pop(0))
cntrl_ids = np.array(cntrl_ids)

cl = np.array(cntrl_list).astype(float)
AC = np.zeros((len(cntrl_list) * 3, 6))
for i, row in enumerate(cl):
    AC[i * 3] = [row[0], -row[1], 0, 1, 0, 0]
    AC[i * 3 + 1] = [row[1], row[0], 0, 0, 1, 0]
    AC[i * 3 + 2] = [0, 0, row[2], 0, 0, 1]
R = np.dot(AC, X)

print('\nMature Plot')
print('Transformation Result (local -> global)')
print('=======================================\n')
print('Z-axis rotation (deg): {:16.2f}' \
      .format(np.degrees(np.mean((np.arccos(X[0][0]), np.arcsin(X[1][0]))))))
print('X translation: {:24.3f}'.format(X[3][0]))
print('Y translation: {:24.3f}'.format(X[4][0]))
print('Z translation: {:24.3f}\n'.format(X[5][0]))

print('Resdiuals')
print('=========')
print('id\tx\t\ty\t\tz')
for i, row in enumerate(v.reshape((6, 3))):
    print('{}\t{:8.5f}\t{:8.5f}\t{:8.5f}'.format(ids[i], row[0], row[1], row[2]))

print('\nExtra control from {}\n'.format(f2name))
print('id\tx\ty\tz\t  E\t\tN\t\tRL')
for i, row in enumerate(R.reshape((-1, 3))):
    if not i:
        print('{}\t{:7.3f}\t{:7.3f}\t{:7.3f}\t  {:10.3f}\t{:10.3f}\t{:6.3f}' \
              .format(cntrl_ids[i][3:], cl[i, 0], cl[i, 1], cl[i, 2],
                      row[0], row[1], row[2]))
    else:
        print('{}\t{:7.3f}\t{:7.3f}\t{:7.3f}\t  {:10.3f}\t{:10.3f}\t{:6.3f}' \
              .format(cntrl_ids[i], cl[i, 0], cl[i, 1], cl[i, 2],
                      row[0], row[1], row[2]))
