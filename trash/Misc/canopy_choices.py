import numpy as np
import pandas as pd
import os


# This function creates an array of combinations based upon the supplied arrays
# see https://stackoverflow.com/questions/11144513/numpy-cartesian-product-of-x-and-y
# -array-points-into-single-array-of-2d-points
def cartesian_product(*arrays):
    la = len(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[..., i] = a
    return arr.reshape(-1, la)


# file_dir - change where appropriate
file_dir = '/home/sfm-user/Downloads'

# choices - binary representation of the heights used
choices = cartesian_product(*[np.arange(2) for i in np.arange(24)])
# take away combos of less than 2 points, or all
choices = np.delete(choices, np.where(np.sum(choices, axis=1) < 2)[0], axis=0)
choices = np.delete(choices, np.where(np.sum(choices, axis=1) == 24)[0], axis=0)

# read heights in
canopy_height_list = pd.read_csv(os.path.join(file_dir, 'height2.5m_24scans.csv'))
# split vals, cols off
canopy_vals = canopy_height_list.values
canopy_cols = canopy_height_list.columns
# result is the product of vals by choices
result = canopy_vals * choices

# set up a dataframe with the choices first
df = pd.DataFrame(data=choices, columns=canopy_cols)
# use np.where to turn our zeros into nans (using np.isclose because type(result)==np.float)
df['mean'] = np.nanmean(np.where(np.isclose(result, 0), np.nan, result), axis=1)
df['std'] = np.nanstd(np.where(np.isclose(result, 0), np.nan, result), axis=1)
# noting the number of combinations for easy splitting
df['num_cmb'] = np.count_nonzero(choices, axis=1)

# save pickles of the different combo values
for num in np.arange(2, 24):
    n_df = df[df.num_cmb == num].reset_index(drop=True)
    sub_df = n_df.loc[:, :'D6']
    # creates list of point identifiers, probably using magic
    # see https://stackoverflow.com/questions/32768555/find-the-set-of-column-indices-for-non-zero-
    # values-in-each-row-in-pandas-data-f/32769767
    n_df['cmb_lbl'] = sub_df.apply(lambda x: x != 0, raw=True).apply(
        lambda x: list(sub_df.columns[x.values]), axis=1)
    n_df.to_pickle(os.path.join(file_dir, 'combo{:02d}.pkl'.format(num)))
    print('{} complete'.format(num))
