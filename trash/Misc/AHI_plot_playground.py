import numpy as np
import matplotlib.pyplot as plt
import os
import gaf
import cartopy

ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)


def redef_imshow(ax, data):
    img_extent = [-5500000, 5500000, -5500000, 5500000]
    ax.imshow(data, origin='upper', extent=img_extent, transform=ahi_crs)


f, axes = plt.subplots(1, 3, True, True, subplot_kw=dict(projection=ahi_crs))
# merge masks
land_mask = gaf.ahi.get_land_mask()
vang = gaf.ahi.get_viewangle()

is_land = np.logical_and(land_mask == 0, vang < 80)

for i, image in enumerate([land_mask, is_land, vang]):
    redef_imshow(axes[i], image)
    axes[i].gridlines()
    axes[i].coastlines()

f.tight_layout()
#a = gaf.AHICapture((2016, 9, 23, 1, 10))
