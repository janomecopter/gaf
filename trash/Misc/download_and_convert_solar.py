import sys, os
sys.path.append(os.path.realpath('../..'))
import gaf
import numpy as np
from multiprocessing import Pool, cpu_count
from netCDF4 import Dataset


def main():
    array = [[2016, i, j, k] for i in np.arange(367) + 1 for j in range(24) for k in np.arange(0, 60, 10)]
    p = Pool(cpu_count() * 2)
    p.map(convert, (i for i in array))


def convert(time):
    a = gaf.AHICapture(time)
    if not a.files_avail[[k for k in a.files_avail.keys() if 'AHIB07' in k][0]]:
        a.band7
    if not a.files_avail[[k for k in a.files_avail.keys() if 'AHIB13' in k][0]]:
        a.band13

    pathmid = os.path.join(a.sensor, 'SOL', 'ZA', '{:04d}'.format(a.time.year), '{:03d}'.format(a.time.jday))
    if not os.path.isdir(os.path.join(gaf.IMAGEDIR, pathmid)):
        os.makedirs(os.path.join(gaf.IMAGEDIR, pathmid))
    if os.path.isfile(os.path.join(gaf.IMAGEDIR, pathmid, '{}{:03d}-{:02d}{:02d}-AHISOLZA.nc'.format(
            a.time.year, a.time.jday, a.time.hour, a.time.minute))):
        return 0

    filename = '{}{:03d}-{:02d}{:02d}-AHISOL-2000.nc'.format(a.time.year, a.time.jday, a.time.hour,
                                                             a.time.minute)
    fullpath = os.path.join(gaf.IMAGEDIR, pathmid, filename)
    fpath = gaf.get_solar(filename, pathmid)
    if not fpath:
        return 0
    d = gaf.AHIImage(fpath).data
    rounded = np.round(d, 0).astype(int)

    f = Dataset(os.path.join(gaf.IMAGEDIR, pathmid, '{}{:03d}-{:02d}{:02d}-AHISOLZA.nc'.format(
        a.time.year, a.time.jday, a.time.hour, a.time.minute)), 'w')
    f.createDimension('x', d.shape[1])
    f.createDimension('y', d.shape[0])
    data = f.createVariable('solar_za', 'u1', ('y', 'x'), zlib=True)
    data[:] = rounded
    f.close()

    os.remove(fullpath)


if __name__ == '__main__':
    main()
