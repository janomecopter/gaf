# **********************************************************************************************************************
# Description:  1. Implements Section 5.2 of Xu et al. - 2010 - New GOES imager algorithms for cloud and active fire
#                  detection and fire radiative power assessment across North, Sout.
#               2. Processes one year.
#               3. To control processing edit -  time points: array 'timepoints', months: array 'months'.
#
# Input Data:   1. Himawari 8 .nc files.
#                  BT_TIR : Himawari 8 Band 13 OBS.
#                  BT_MIR: Himawari 8 Band 7 OBS.
#                  RAD_VIS: Band 3 OBS.
#                  RAD_MIR: Band 7 converted (by BH's script).
#                  ALBEDO_VIS: Himawari 8 Band 3 BRF.
#
#               2. Input data directory structure is by year, month, band, obs/brf, time point eg:
#                  C:\Fires\CloudMask\TestData\2016\03\Band03\BRF\0510000
#
# Output Data:  1. Geotiffs - one for each time point, for each month, for each year.
#               2. A pixel value of >=1 indicates cloud. NB. Tests are additive ie pixels classed as cloud are not
#                  masked out for subsequent tests.
#               3. Pixels not designated as cloud = 0.
#               4. Output data directory structure is by year with month suffix eg:
#                  C:\Fires\CloudMask\TestData\OutputCloudMask\2016_03
#
# Assumptions:  1. Number of rows and cols remain constant across all .nc files.
#               2. All input data at same resolution. Xu et al use data with different input resolutions. Have gone
#                  with the same resolution across all input datasets as BOM supply all bands at 500m, 1km, and 2km.
#               3. Only processing data for maximum of one year.
#
# Notes:
#       1. Day/Night solar zenith angle currently set to 80 degrees.
#       2. Input data dirs hard-coded.
#       3. Moving window code hardly ever called as all pixels failing  (bt_mir > 280) and (bt_mir - bt_dif < 5)
#       4. Python 3.6.3
#       5. If this runs too slowly, should be easy to multi proc it with each proc assigned n time points.
#
# TO DO:    1. Confirm band input data are correct for each test.
#
# **********************************************************************************************************************

import os
import gc
import gdal
import glob
import logging
import numpy as np
import sys
from calendar import monthrange
from time import gmtime, strftime

NO_DATA = 10000000000
INDIR_03BRF_a = "C:\\Fires\\CloudMask\\TestData\\2016"
INDIR_03BRF_b = "Band03\\BRF"
INDIR_03OBS_a = "C:\\Fires\\CloudMask\\TestData\\2016"
INDIR_03OBS_b = "Band03\\OBS"
INDIR_07BRF_a = "C:\\Fires\\CloudMask\\TestData\\2016"                            #calculated brf data..BH script.
INDIR_07BRF_b = "Band07\\BRF"
INDIR_07OBS_a = "C:\\Fires\\CloudMask\\TestData\\2016"
INDIR_07OBS_b = "Band07\\OBS"
INDIR_13OBS_a = "C:\\Fires\\CloudMask\\TestData\\2016"
INDIR_13OBS_b = "Band13\\OBS"
INDIR_SOLAR_a = "C:\\Fires\\CloudMask\\TestData\\2016"
INDIR_SOLAR_b = "Solar"
OUT_DIR = "C:\\Fires\\CloudMask\\TestData\\OutputCloudMask"
HIM_FNAME = "-P1S-ABOM_OBS_B13-PRJ_GEOS141_2000-HIMAWARI8-AHI"
NUM_COLS = 5500
NUM_ROWS = 5500
NIGHT_ANGLE = 80
LOG_FILE = 'MASK.txt'

outdataDict = {}                      # output datasets by day of the month (key)
loggerDict = {}
step = 1                              # increment by 1 day in range loops

# ********** edit these values for production run**********
yearDict = {'year':'2016'}
months = ['03']                       # add more to list
timepoints = ['051000', '052000']     # add more to list


def createoutputimages(mnth, tp, indir):
    '''
    Create copy of the .nc file as a GeoTiff..for the current time point.
    Cloud mask values are inserted into these geotiffs.
    :param mnth:
    :param tp:
    :param indir:
    :return: True if output images created ok. False if not.
    '''

    try:
        yr = yearDict['year']
        loga = loggerDict.get('logger')
        od = OUT_DIR
        od = os.path.join(od, yr + "_" + mnth)
        if not os.path.exists(od):
            os.makedirs(od)

        driver = gdal.GetDriverByName("GTiff")
        dys = monthrange(int(yr), int(mnth))
        startday = dys[0]
        endday = dys[1]

        for d in range(startday, endday + 1, step):

            dstr = str(d)                                       # make sure day is always 2 digits for file naming
            if len(dstr) == 1:
                dstr = '0' + dstr

            dt = yr + mnth + dstr + tp
            src = os.path.join(indir, dt + HIM_FNAME + '.nc')
            src_ds = gdal.Open(src)
            dst = os.path.join(od, dt + '_CLOUDMASK' + '.tiff')
            driver.CreateCopy(dst, src_ds, 0)

            # close the datasets to flush to disk
            src_ds = None
            ds = gdal.Open(dst, gdal.GA_Update)

            # add in-memory output dataset to dict
            outdataDict[d] = ds

        del driver
        messages("createoutputimages() - Output images created for time point " + tp + " "
                 + strftime("%Y-%m-%d %H:%M:%S", gmtime()), loga, 'info')
        del loga
        return True

    except:
        messages("createoutputimages() - Output images not created for " + yr + " " + mnth + " " + tp + " "
                 + strftime("%Y-%m-%d %H:%M:%S", gmtime()), loga, 'error')
        del loga
        return False


def insertarraysintoimages(insertArrayDict):

    '''
    Inserts pixel value arrays into output image datasets. Each array has a corresponding output dataset (by day
    and time point). Day is determined from the keys of the insert array.
    :param insertArrayDict: dictionary object.
    :return: None
    '''

    loga = loggerDict.get('logger')

    # process each output array
    try:
        for i in insertArrayDict.keys():                    # key is the day number

            # get insert array
            arr = insertArrayDict.get(i)

            # get dataset for the current array
            ds = outdataDict.get(i)

            # write array to dataset
            bnd = ds.GetRasterBand(1)
            bnd.WriteArray(arr)
            ds.FlushCache()                                 # save to disk

        messages("insertarraysintoimages() - Inserted arrays to images " + strftime("%Y-%m-%d %H:%M:%S",
                                                                                            gmtime()), loga, 'info')
        del loga

    except:
        messages("insertarraysintoimages() - Unable to insert arrays to images " + strftime("%Y-%m-%d %H:%M:%S",
                                                                                            gmtime()), loga, 'error')
        del loga

    return None


def sortimagesbycreationdate(files):
    '''
    Sorts a list of files (with path) by creation date. The creation date is in the file name.
    :param files: List of path + file name to be sorted.
    :return: List of file names + path sorted by creation date.
    '''

    import re

    numbers = re.compile(r'(\d+)')

    def numericalSort(value):
        parts = numbers.split(value)
        parts[1::2] = map(int, parts[1::2])
        return parts

    sortedfiles = sorted(files, key=numericalSort)

    print(str(sortedfiles))
    return sortedfiles


def makegdalarray(dir, tp, sbds, sbdsnme):
    '''
    Given a directory of nc files and an empty array, creates a gdal array from the contents of the directory. If
    a subdataset is indicated, will extract it from each nc file.
    :param dir:
    :param tp: time point.
    :param sbds: subdataset..999 when no subdataset.
    :param sbdsnme: name of the subdataset to extract.
    :return: garray: a gdal array.
    '''

    imgit = glob.iglob(dir + '\\*' + tp + '*.nc')

    # sort image files by creation date
    imgit = sortimagesbycreationdate(imgit)
    garray = []

    # process each input image dataset (.nc)
    for nc in imgit:
        f = os.path.join(dir, nc)
        ds = gdal.Open(f, gdal.GA_ReadOnly)

        # extract subdataset, if requested
        if sbds != 999:
            subDatasets = ds.GetSubDatasets()
            for subDataset in subDatasets:
                if sbdsnme in subDataset[0]:
                    ds = gdal.Open(subDataset[0])
                    garray.append(ds)
        else:
            garray.append(ds)

        del ds

    del imgit
    return garray


def compute_window_stats_strided(image, window_w, window_h, insums=False, inmeans=False, invars=False):
    '''
    Populates the pixel at the centre of moving window with stats from the surrounding window pixels.
    :param image:       Image/nd array to run the window over.
    :param window_w:    Window width.
    :param window_h:    Window height.
    :param insums:      Sum of the surrounding pixel values in the window.
    :param inmeans:     Mean of the surrounding pixel values in the window.
    :param invars:      Variance of the surrounding pixel values in the window.
    :return: either sums or, means and variances as nd arrays.
    '''

    w, h = image.shape
    strided_image = np.lib.stride_tricks.as_strided(image,
                                                    shape=[w - window_w + 1, h - window_h + 1, window_w, window_h],
                                                    strides=image.strides + image.strides)
    if insums:
        sums = strided_image.sum(axis=(2, 3))
        return sums

    if inmeans:
        means = strided_image.mean(axis=(2, 3))
        mean_squares = (strided_image ** 2).mean(axis=(2, 3))
        variances = mean_squares - means ** 2
        return means, variances


def messages(inMessage, inLogger, inMessageLevel):
    '''
    Handles logging messages, and print statements.
    :param inMessage: infor or error message to log/display in console.
    :param inLogger: module level logger.
    :param inMessageLevel: info, error.
    :return:
    '''

    msg = inMessage
    msgLevel = inMessageLevel

    # Print the message to the console
    print(msg)

    # Add the message to the log
    logger = inLogger
    if inLogger:
        if msgLevel == 'info':
            logger.info(msg)
        elif msgLevel == 'error':
            logger.error(msg)
        else:
            None

    return None


def createlogger():
    '''
    Create log file to hold info and error messages for each time point run. Log file stored in current working dir.
    :params:  none.
    :return:  a module level logger.
    '''

    cwd = os.getcwd()
    logger = logging.getLogger('CloudMaskSingleProc')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(os.path.join(cwd, LOG_FILE))
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    module_logger = logging.getLogger('CloudMaskSingleProc')

    return module_logger


def main(month, timepoint, dir3brf, dir3obs, dir7brf, dir7obs, dir13obs, dirsol):
    '''
    Controls the cloud mask test process.
    :param month:       the month to process.
    :param tp:          time point being being tested in the current month/year.
    :param dir3brf:     band 03 brf data directory.
    :param dir3obs:     band 03 obs data directory.
    :param dir7brf:     band 07 brf data directory.
    :param dir7obs:     band 07 obs data directory.
    :param dir13obs:    band 03 obs data directory.
    :param dirsol:      band solar data directory.
    :return: None
    '''

    loga = loggerDict.get('logger')
    year = yearDict['year']
    messages("main() - Cloud Mask tests started for year " + year + " month: " + month + " time point: " + timepoint +
             " " + strftime("%Y-%m-%d %H:%M:%S", gmtime()), loga, 'info')

    # create gdal arrays of all datasets in the selected directories
    gdalarray3brf, gdalarray3obs, gdalarray7brf, gdalarray7obs, gdalarray13obs, gdalarraysolar = None, None, None, None, None, None
    bnddict = {'3brf': dir3brf, '3obs': dir3obs, '7brf': dir7brf, '7obs': dir7obs, '13obs': dir13obs}
    for bnd in bnddict.keys():

        if os.listdir(bnddict[bnd]):
            try:
                ga = makegdalarray(bnddict[bnd], timepoint, 999, None)
                if bnd == '3brf':
                    gdalarray3brf = ga
                elif bnd == '3obs':
                    gdalarray3obs = ga
                elif bnd == '7brf':
                    gdalarray7brf = ga
                elif bnd == '7obs':
                    gdalarray7obs = ga
                elif bnd == '13obs':
                    gdalarray13obs = ga
                del ga
            except:
                 messages("main() GDAL array creation not completed - TERMINATING", loga, 'error')
                 sys.exit()
        else:
            messages("main() Empty directory for band " + str(bnddict[bnd]) + " - TERMINATING", loga, 'error')
            sys.exit()

    # for the solar gdal array specify the solar zenith angle sub-dataset
    try:
        gdalarraysolar = makegdalarray(dirsol, timepoint, 2, 'solar_zenith_angle')
    except:
        messages("main() Solar GDAL array creation not completed - TERMINATING", loga, 'error')
        sys.exit()


    # 1. test if each 2d array is less than the stats (as per section 5.2.1). Place the results for each day as a 2d
    # array in dict.
    # - load up 2d arrays for each band 13 obs gdalarray
    thirteenobsdict = None
    if gdalarray13obs:

        try:
            thirteenobsdict = {day: np.array(i.GetRasterBand(1).ReadAsArray())
                               for day, i in enumerate(gdalarray13obs, start=1)}
            del gdalarray13obs

            # Change NO_DATA to 0 so can use 2d arrays in stats calc
            arys = []
            for day in thirteenobsdict.keys():
                arr = thirteenobsdict[day]
                arr1 = np.copy(arr)
                arr1[arr1 >= NO_DATA] = 0
                arys.append(arr1)

            # create stats from all 2d arrays for the current month
            ms = np.mean(arys, axis=0)                      # get mean vert through all 2d arrays
            stds = np.std(arys, axis=0)                     # get std vert through all 2d arrays
            calc = np.subtract(ms, stds * 1.5)
            del ms, stds

            outdict = {}
            for day in thirteenobsdict.keys():
                arr = thirteenobsdict[day]
                results = np.less(arr, calc)
                results = results.astype(int)
                outdict[day] = results
            del calc

        except:
            messages("main() Temporal Contrast test error  - TERMINATING", loga,
                     'error')
            sys.exit()
    else:
        messages("main() Unable to test Temporal Contrast - Empty GDAL array for Band 13 OBS - TERMINATING", loga,
                 'error')
        sys.exit()


    # 2. TIR_BT Threshold test (section 5.2.2)
    try:
        for day in thirteenobsdict.keys():
            arr = thirteenobsdict[day]
            results = np.less(arr, 207)
            results = results.astype(int)
            prevresults = outdict[day]
            cmbndresults = np.add(results, prevresults)     # get pixel vals of 2 where both tests indicate cloud
            outdict[day] = cmbndresults

    except:
        messages("main() TIR_BT Threshold test error - TERMINATING", loga, 'error')
        sys.exit()


    # 3. Infrared Difference test (section 5.2.3)
    if gdalarray7obs and gdalarraysolar:

        try:
            solardict = {day: np.array(i.GetRasterBand(1).ReadAsArray())
                                for day, i in enumerate(gdalarraysolar, start=1)}

            sevenobsdict = {day: np.array(i.GetRasterBand(1).ReadAsArray())
                                for day, i in enumerate(gdalarray7obs, start=1)}
            del gdalarray7obs

            for day in sevenobsdict.keys():
                bt_mir = sevenobsdict[day]
                solzangarr = solardict[day]
                tir_bt = thirteenobsdict[day]
                bt_dif = np.subtract(bt_mir, tir_bt)

                # day: (bt_dif > 6 and bt_mir < 290) where solzangarr > 80    (section 5.2.3, 4a)
                bt_difA = (bt_dif > 6)
                bt_mirA = (bt_mir < 290)

                calcA = np.logical_and(bt_difA, bt_mirA)
                dayresults = np.logical_and(calcA, solzangarr > NIGHT_ANGLE)
                del calcA

                # night:  (bt_dif > 5 and bt_mir < 285)  where solzangarr < 80    (section 5.2.3, 4b)
                bt_difA = (bt_dif > 5)
                bt_mirA = (bt_mir < 285)
                del bt_dif
                del bt_mir

                calcA = np.logical_and(bt_difA, bt_mirA)
                nightresults = np.logical_and(calcA, solzangarr < NIGHT_ANGLE)
                del calcA

                # update out 2d array with results form these tests
                dayresults = dayresults.astype(int)
                nightresults = nightresults.astype(int)
                prevresults = outdict[day]
                cmbndresults = np.add(dayresults, prevresults)     # pixel vals of 1, 2 and 3 where multiple tests indicate cloud
                cmbndresults = np.add(nightresults, cmbndresults)
                outdict[day] = cmbndresults

        except:
            messages("main() Infrared Difference test error - TERMINATING", loga, 'error')
            sys.exit()
    else:
        messages("main() Unable to test Infrared Difference - Empty GDAL array for Band 07 OBS or SOLAR", loga,
                 'error')


    # 4. Albedo test: albedo > 0.15 (section 5.2.4)
    if gdalarray3brf:

        try:
            threebrfdict = {day: np.array(i.GetRasterBand(1).ReadAsArray())
                               for day, i in enumerate(gdalarray3brf, start=1)}
            del gdalarray3brf

            for day in threebrfdict.keys():
                alb1 = threebrfdict[day]
                alb2 = np.logical_and(alb1 > 0.15, alb1 < NO_DATA)
                solzangarr = solardict[day]
                alb2dayresults = np.logical_and(alb2, solzangarr < NIGHT_ANGLE)
                alb2dayresults = alb2dayresults.astype(int)
                prevresults = outdict[day]
                cmbndresults = np.add(alb2dayresults, prevresults)
                outdict[day] = cmbndresults

            del threebrfdict

        except:
            messages("main() Albedo test error - TERMINATING", loga, 'error')
            sys.exit()
    else:
        messages("main() Unable to test Albedo - Empty GDAL array for Band 03 BRF", loga,
                 'error')


    # 5. Daytime VIS to MIR radiance ratio (section 5.2.5)
    if gdalarray7brf and gdalarray3obs:

        try:
            threeobsdict = {day: np.array(i.GetRasterBand(1).ReadAsArray())
                               for day, i in enumerate(gdalarray3obs, start=1)}
            sevenbrfdict = {day: np.array(i.GetRasterBand(1).ReadAsArray())
                               for day, i in enumerate(gdalarray7brf, start=1)}
            del gdalarray3obs
            del gdalarray7brf

            for day in threeobsdict.keys():
                rad_vis = threeobsdict[day]
                rad_mir = sevenbrfdict[day]  # this is using brf
                vismirrad = (rad_vis / rad_mir) > 100
                solzangarr = solardict[day]
                vismirrad_dayresults = np.logical_and(vismirrad, solzangarr > NIGHT_ANGLE)
                vismirrad_dayresults = vismirrad_dayresults.astype(int)
                prevresults = outdict[day]
                cmbndresults = np.add(vismirrad_dayresults, prevresults)
                outdict[day] = cmbndresults

            del threeobsdict
            del sevenbrfdict

        except:
            messages("main() Daytime VIS to MIR radiance ratio test error - TERMINATING", loga, 'error')
            sys.exit()
    else:
        messages("main() Unable to test Daytime VIS to MIR radiance ratio - Empty GDAL array for"
                 " Band 03 OBS or Band 07 BRF", loga, 'error')


    # 6. Night-time ambient MIR BT contrast (section 5.2.6) -
    #    get all the pixels that meet criteria to become part of a spatial window
    try:
        for day in sevenobsdict.keys():

            # test for pixels meeting (bt_mir > 280) and (bt_mir - bt_dif < 5)   (section 5.2.6)
            bt_mir = sevenobsdict[day]
            solzangarr = solardict[day]
            tir_bt = thirteenobsdict[day]
            bt_dif = np.subtract(bt_mir, tir_bt)
            bt_mir[bt_mir >= NO_DATA] = 0                                   # because NO_DATA value is > 280
            candidatesa = (bt_mir > 280)
            candidatesb = (bt_mir - bt_dif < 5)
            candidatesc =  np.logical_and(candidatesa, candidatesb)

            # test if any candidates meet these criteria..otherwise continue to next day
            candidatescint = candidatesc.astype(int)
            cnt1 = np.count_nonzero(candidatescint == 1)

            if cnt1 >= 1:

                del cnt1

                # some pixels meet (bt_mir > 280) and (bt_mir - bt_dif < 5) so, get those that are night time pixels
                nighttime = (solzangarr > NIGHT_ANGLE)
                candidates = np.logical_and(candidatesc, nighttime)
                candidatesint = candidates.astype(int)                      # 1s and 0s
                del nighttime
                del candidatesa
                del candidatesb
                del candidatesc

                # any pixels left?
                cnt2 = np.count_nonzero(candidatesint == 1)
                if cnt2 == 0:

                    # no, so continue to next day
                    del cnt2
                    continue

                # yes, so do the window analysis
                else:

                    del cnt2
                    bt_mir_candidates = (bt_mir * candidatesint)            # leaves bt_mir values for windowing
                    bt_mir_candidates = np.float64(bt_mir_candidates)       # otherwise stats wont compute
                    out = np.zeros(5500, 5500)

                    # process all window sizes
                    szes = [5,7,9,11,13,15,17,19,21]
                    for sze in szes:

                        sums = compute_window_stats_strided(candidatesint, sze, sze, True, False, False)

                        if np.count_nonzero(sums >= 10) >= 1:
                            bt_mir_means, bt_mir_vrncs = compute_window_stats_strided(bt_mir_candidates, sze, sze, False,
                                                                                      True, True)
                            bt_mir_stdev = np.sqrt(bt_mir_vrncs)

                            # test BT_MIR value of pixel against mean/st dev of surrounding pixels
                            outb = bt_mir < (bt_mir_means - (2 * bt_mir_stdev) - 10)
                            addmask = np.logical_and(out == 0, out == 0)    # so only add in new hits
                            out = np.add(out, outb, where=addmask)

                    # insert results into out dict.
                    nightbtmircontrastresults = out.astype(int)
                    prevresults = outdict[day]
                    cmbndresults = np.add(nightbtmircontrastresults,
                                          prevresults)
                    outdict[day] = cmbndresults
                    del bt_mir_candidates
                    del nightbtmircontrastresults

            else:
                continue

        del sevenobsdict
        del thirteenobsdict
        del gdalarraysolar

    except:
        messages("main() Night-time ambient MIR BT contrast test error - TERMINATING", loga, 'error')
        sys.exit()


    # insert 2d cloud result arrays into geotiffs
    try:
        insertarraysintoimages(outdict)
        messages("main() - Cloud Mask run completed for year " + year + " month: " + month + " time point: " +
                 timepoint + " " + strftime("%Y-%m-%d %H:%M:%S", gmtime()), loga, 'info')
    except:
        messages("main() - Problem inserting arrays into output images for year " + year + " month: " + month +
                 " time point: " + timepoint + " " + strftime("%Y-%m-%d %H:%M:%S", gmtime()), loga, 'error')

    gc.collect()
    return None


if __name__ == "__main__":

    gc.enable()
    tmpnts = timepoints
    logger = createlogger()
    loggerDict['logger'] = logger

    for m in months:

        for tp in tmpnts:

            # create output images (INDIR_13OBS .nc files used as 'templates' for output Geotiffs)
            dir_13obs = os.path.join(INDIR_13OBS_a, m, INDIR_13OBS_b, tp)
            result = createoutputimages(m, tp, dir_13obs)
            if result is False:
                continue

            else:

                dir_03brf = os.path.join(INDIR_03BRF_a, m, INDIR_03BRF_b, tp)
                dir_03obs = os.path.join(INDIR_03OBS_a, m, INDIR_03OBS_b, tp)
                dir_07brf = os.path.join(INDIR_07BRF_a, m, INDIR_07BRF_b, tp)
                dir_07obs = os.path.join(INDIR_07OBS_a, m, INDIR_07OBS_b, tp)
                solar = os.path.join(INDIR_SOLAR_a, m, INDIR_SOLAR_b, tp)

                # run tests
                main(m, tp, dir_03brf, dir_03obs, dir_07brf, dir_07obs, dir_13obs, solar)
