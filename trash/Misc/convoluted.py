import gaf
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy import ndimage

a = gaf.AHICapture((2016, 9, 23, 12, 10))

vcslp = (np.logical_and(a.solar.data > 80, np.logical_and(a.band7.data > 280, a.band7.data - a.band13.data < 5)))
vcslp = np.ma.getdata(vcslp)
mask = np.load(os.path.join(gaf.DATADIR, 'AHIDisk_mask_5500.npy'))
vcslp[mask] = 0
# vcslp = (np.logical_and(a.solar.data > 80, a.band13.data > 280))
# vcslp = (a.band7.data - a.band13.data)
plt.imshow(vcslp)
