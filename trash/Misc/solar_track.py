import datetime
import calendar
import numpy as np
import pandas as pd
from pandas._libs.tslib import Timestamp
import pysolar
import matplotlib.pyplot as plt
from pathlib import Path
import os


save_path = str(Path.home())
print('Save Path: {}'.format(save_path))


class Time(object):
    '''
    Converts date and time to datetime object.
    Output: dt,jday,posix objects
    '''
    def __init__(self, raw_data):
        if isinstance(raw_data, (list, tuple, np.ndarray)):
            if len(raw_data) == 4:
                # assume [y,jday,hh,mm]
                self.dt = datetime.datetime.strptime('{}{:03d}{:02d}{:02d}'.format(*raw_data), '%Y%j%H%M')
            elif len(raw_data) == 5:
                self.dt = datetime.datetime(*raw_data)
            elif len(raw_data) < 4:
                raise Exception('Not enough information to decide how this time is structured.')
            else:  # len>5
                raise Exception('Too much information to decide how this time is structured.')
        elif isinstance(raw_data, datetime.datetime):
            self.dt = raw_data
        elif isinstance(raw_data, Timestamp):
            self.dt = raw_data.to_pydatetime()
        else:
            # assume floating point epoch
            self.dt = datetime.datetime.utcfromtimestamp(raw_data)
        self.jday = self.dt.timetuple().tm_yday
        # create posix time
        self.posix = calendar.timegm(self.dt.timetuple())
        [setattr(self, item, getattr(self.dt, item)) for item in ['year',
                                                                  'month', 'day', 'hour', 'minute']]


def create_solar_info(position, tlist):
    # pandas - nice
    df = pd.DataFrame(columns=['time', 'azimuth', 'altitude', 'power', 'jday', 'plt_azimuth'])
    df.time = tlist
    for i, item in enumerate(df.time):
        df.loc[i, ['azimuth', 'altitude']] = pysolar.solar.get_position(*position, df.loc[i, 'time'].dt)
        df.loc[i, 'power'] = pysolar.radiation.get_radiation_direct(df.loc[i, 'time'].dt, df.loc[i, 'altitude'])
    df['jday'] = [item.jday for item in df.time]
    # eliminate night time - altitude < 0
    df = df[df.altitude > 0]
    # optional (nice for plotting) - make azimuths greater than 180 negative
    df.plt_azimuth = df.azimuth
    df.plt_azimuth[df.azimuth > 180] -= 360
    return df


def plot_info(df, position):
    # plots - day of year and power
    f, ax = plt.subplots(1, 2, figsize=(12.5, 6.5))
    im0 = ax[0].scatter(df.plt_azimuth, df.altitude, c=df.jday)
    cb0 = plt.colorbar(im0, ax=ax[0])
    cb0.set_label('Julian day')
    im1 = ax[1].scatter(df.plt_azimuth, df.altitude, c=df.power)
    cb1 = plt.colorbar(im1, ax=ax[1])
    cb1.set_label('Incident power $(W/m^2)$')
    for i in np.arange(2):
        ax[i].set_xlabel('Azimuth')
        ax[i].set_ylabel('Altitude')
    f.suptitle('Location: {} for 2017'.format(position))
    save_name = os.path.join(save_path, 'plot.png')
    f.savefig(save_name, format='png', dpi=300)
    print('{} saved'.format(save_name))


if __name__ == "__main__":
    # proof of concept - 2017 for cnr LaTrobe and Swanston Sts
    position = (-37.809558, 144.963815)
    # create list of Time objects
    tlist = [Time((2017, jd, h, 0)) for jd in np.arange(365) + 1 for h in np.arange(24)]

    df = create_solar_info(position, tlist)
    plot_info(df, position)
