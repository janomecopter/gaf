import numpy as np
import matplotlib.pyplot as plt
import os
import gaf
from mpl_toolkits.basemap import Basemap
import cartopy

a = gaf.AHICapture((2016, 9, 23, 1, 10))
data = np.ma.array(a.band7.data, mask=np.load(os.path.join(gaf.DATADIR, 'AHIDisk_mask_5500.npy')))
lat = a.band7.latitude
lon = a.band7.longitude
lat[np.isinf(lat)] = 0
lon[np.isinf(lon)] = 0
# mb = Basemap(projection='tmerc', lon_0=145, lat_0=-23, height=200000, width=180000, resolution='h')
# f, ax = plt.subplots(figsize=(10, 8))
# im = mb.pcolormesh(a.band7.longitude, a.band7.latitude, data, vmin=290, vmax=320, latlon=True, ax=ax)
# mb.drawmeridians(np.arange(110, 160, 0.5), labels=[0, 0, 0, 1], ax=ax)
# mb.drawmeridians(np.arange(110, 160) + 0.5, ax=ax)
# mb.drawparallels(np.arange(-48, -8, 0.5), labels=[1, 0, 0, 0], ax=ax)
# mb.drawparallels(np.arange(-48, -8) + 0.5, ax=ax)
# mb.drawcoastlines(ax=ax)
# cb = plt.colorbar(im)
# cb.set_label('Brightness temperature')
# ax.set_title('AHI B07 {}-{:02d}-{:02d} {:02d}{:02d} UTC'.format(a.time.year, a.time.month, a.time.day,
#                                                                a.time.hour, a.time.minute))
ax = plt.axes(projection=cartopy.crs.Geostationary(140.7))
ax.coastlines()
ax.add_feature(cartopy.feature.OCEAN, zorder=100, edgecolor='0.5')
ax.set_global()
plot = ax.pcolormesh(lon, lat, data, transform=cartopy.crs.PlateCarree())
cb = plt.colorbar(plot)
plt.show()
