import numpy as np
import pandas as pd
import itertools

for num in np.arange(2, 24):
    pc_list = ['{}{}'.format(a, b) for a in ['A', 'B', 'C', 'D'] for b in np.arange(6) + 1]
    df = pd.DataFrame(columns=[*['pc{:02d}'.format(i) for i in np.arange(num)], 'result'])
    pc_combo = np.array([list(item) for item in itertools.combinations(pc_list, num)])
    pc_col_list = [col for col in df.columns if 'pc' in col]
    for col in pc_col_list:
        col_num = int(col[2:])
        df[col] = pc_combo[:, col_num]
    for i in df.index:
        for item in pc_col_list:
            # pull a bunch of item s out by number of cols
            df.loc[i, item]
        # do comparison
        # whatever_it_is = df.loc[i, result] = result
    df.to_pickle('filepath/combo{:02d}.pkl'.format(num))