import cartopy
import os
import matplotlib.pyplot as plt
import fiona
from shapely.geometry import MultiPolygon, shape
from matplotlib.collections import PatchCollection
from descartes import PolygonPatch
import pandas as pd
import scipy.io as sio
import numpy as np
import cartopy.io.shapereader as shpreader
from cartopy.feature import ShapelyFeature


proj_path = r'C:/Users/Public/Downloads'

cst = MultiPolygon([shape(pol['geometry']) for pol in fiona.open(
    os.path.join(proj_path, 'AntarcticCoast', 'all_coast_poly_2003.shp'))])
coast_patches = []
for p in cst:
    coast_patches.append(PolygonPatch(p, fc='None', ec='k', alpha=1., zorder=1))

'''
bergs = pd.read_pickle(os.path.join(proj_path, 'AntarcticCoast', 'iceberg.pkl'))
berg_patches = []
for p in bergs:
    berg_patches.append(PolygonPatch(p, fc='None', ec='b', alpha=1., lw=2, zorder=3))
'''

colors = ['#2EDB93', '#D1E615', '#DE9E1F']
runs = {}
for i in np.arange(3) + 1:
    path = pd.read_pickle(os.path.join(proj_path, 'AntarcticCoast', 'Path{}.pkl'.format(i)))
    if i == 1:
        ends = path.geometry.loc[[9, 64]]
        end_patches = []
        for p in ends:
            end_patches.append(PolygonPatch(p, fc='None', ec='r', alpha=1., lw=2, zorder=3))
    runs[i] = []
    for item in path.geometry.loc[10:63]:
        runs[i].append(PolygonPatch(item, fc='None', ec=colors[i-1], alpha=1., lw=1, zorder=2))

bathymetry_grid = sio.loadmat(os.path.join(proj_path, 'AntarcticCoast', 'BathymetryGrid.mat'))
depth = bathymetry_grid['A']
latitude = bathymetry_grid['latg']
longitude = bathymetry_grid['long']

berg = sio.loadmat(os.path.join(proj_path, 'AntarcticCoast', 'bergMask.mat'))
bmask = berg['mask'] > np.min(berg['mask'])
bmask = np.ma.array(bmask, mask=~bmask)

crs = cartopy.crs.PlateCarree()
f, ax = plt.subplots(figsize=(12, 5), subplot_kw=dict(projection=crs))
ax.contourf(longitude, latitude, depth, 25, cmap='gray', vmin=-2000, zorder=0)
ax.contourf(longitude, latitude, bmask, cmap='Blues', zorder=1)
ax.add_collection(PatchCollection(coast_patches, match_original=True))
for k in runs.keys():
    ax.add_collection(PatchCollection(runs[k], match_original=True))
ax.add_collection(PatchCollection(end_patches, match_original=True))
# ax.add_collection(PatchCollection(berg_patches, match_original=True))
xlims = (142.6, 145.8)
ylims = (-67.4, -66.2)
ax.set_xlim(xlims)
ax.set_ylim(ylims)
ax.set_xlabel('Longitude')
ax.set_ylabel('Latitude')
ax.gridlines(draw_labels=True, zorder=0)
ax.text(xlims[0] + (xlims[1] - xlims[0]) * 0.01, ylims[0] + (ylims[1] - ylims[0]) * 0.03,
        'Projection: PlateCarree\nCoast: AAT Coastline 2003 dataset', fontsize=6, zorder=1)

f.savefig(os.path.join(proj_path, 'Multipath.png'), format='png', dpi=300)
