# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 14:06:58 2018

@author: e76243
"""

from netCDF4 import Dataset
from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
from rasterio.control import GroundControlPoint

def str_parser(string):
    str_lst = string.split(' ')
    flt_lst = []
    for i,item in enumerate(str_lst):
        if 'TS' in item:
            continue
        elif ':' in item:
            continue
        else:
            try:
                flt_lst.append(float(item[:-1]))
            except ValueError:
                try:
                    flt_lst.append(float(item[:7]))
                except:
                    break
        
    return flt_lst

#r = Dataset('H:/Python/Misc/VNP14IMG.A2016002.1730.001.2017144021656.nc')
r = Dataset('H:/Python/Misc/VNP14IMG.A2016003.0418.001.2017144054909.nc')

ext_pts = []
for i in range(4):
    ext_pts.append([getattr(r,'GRingPointLongitude')[i],
                    getattr(r,'GRingPointLatitude')[i]])
ext_pts = np.array(ext_pts)

ctrl_pts = []
lat_str = getattr(r,'Unagg_GRingLatitude')
lon_str = getattr(r,'Unagg_GRingLongitude')

lat_figs = str_parser(lat_str)
lon_figs = str_parser(lon_str)

ud_instances = [0,1500,3000,4500,6000,6464]
lr_instances = [0,3200,6400]
cp_order = [2,3,4,10,11,12,18,19,20,26,27,28,34,35,36,32,39,38]
gcps = []
for i,item in enumerate(cp_order):
    gcps.append(GroundControlPoint(ud_instances[i//3],lr_instances[i%3],
                                   lon_figs[item],lat_figs[item],0))


mb = Basemap(projection='geos',rsphere=(6378137,6356752.3142),lon_0=140.7,
             resolution='i')

f,ax = plt.subplots()
mb.plot(ext_pts[:,0],ext_pts[:,1],latlon=True,ax=ax)
cols = 'rgbcm'
for i,item in enumerate(cols):
    mb.plot(lon_figs[i*8:i*8+8],lat_figs[i*8:i*8+8],c=item,latlon=True,ax=ax)
    #mb.scatter(lon_figs[i*8],lat_figs[i*8],c=item,latlon=True,ax=ax)
    mb.scatter(lon_figs[i*8:i*8+8],lat_figs[i*8:i*8+8],c=item,latlon=True,ax=ax)
mb.drawcoastlines(ax=ax)

f2,ax2 = plt.subplots()
ax2.imshow(r.variables['fire mask'][:])
