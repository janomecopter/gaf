# -*- coding: utf-8 -*-
"""
Created on Wed Nov 08 14:27:14 2017

@author: e76243

Routine for downloading VNP14IMG files

Step 1. Obtain list of relevant VIIRS granules from Earthdata 
(https://search.earthdata.nasa.gov/). Select your time period and polygon area,
and search for VIIRS products. I used VNP14 data to create lists.

Step 2. Select "Download Data", and expand the granule list until all files
are loaded. Cut and paste this list into a separate text document.

Step 3. Save this text document in a location, and then change the vnpfile
variable to the same location.

Step 4. Change the spath value to a safe location to download data to.
"""

from ftplib import FTP
import os

# server info - different products can be obtained by changing the dpath variable
server = 'ladsweb.modaps.eosdis.nasa.gov'
user = 'anonymous'
passwd = 'bryan.hally@rmit.edu.au'
dpath = 'allData/5000/VNP14IMG/2016'
# vnpfile is a list of VNP14 granules for a search area and time
vnpfile = 'C:/Scratch/cache/VIIRS/VIIRS_20160712.txt'
# change spath to save to a different location
spath = 'C:/Scratch/cache/VIIRS/'

# Readlines out of saved list of files
with open(vnpfile) as f:
    l = f.readlines()
# Grab the file names, which are listed every second line
vnplist = l[::2]
# split the julian date and time of granules into a separate list
dtlist = [[item.split('.')[1][-3:],item.split('.')[2]] for item in vnplist]
# create a list of unique julian days, as the server needs to be crawled by date
day_list = list(set([item[0] for item in dtlist]))
day_list.sort()

# open connection to ftp server
ftp = FTP(server,user=user,passwd=passwd)
# change working directory to the directory in dpath
ftp.cwd(dpath)

for day in day_list:
    # separate out files for the selected day
    tlist = [item for item in dtlist if day in item[0]]
    ftp.cwd(day)
    # grab full list of files on the server for that day
    flist = ftp.nlst()
    for time in tlist:
        # crawl flist for the relevant file
        fname = [item for item in flist if '{}.{}.'.format(day,time[1]) in item][0]
        # if file not already saved in path
        if not os.path.isfile(os.path.join(spath,fname)):
            fsize = ftp.size(fname)
            with open(os.path.join(spath,fname),'wb') as f:
                # retrieve binary file from server
                ftp.retrbinary('RETR {}'.format(fname),f.write)
            print('{} - size {} - saved'.format(fname,fsize))
        else:
            print('{} skipped'.format(fname))
    # at end of day, go back to the year folder
    ftp.cwd('..')
# close ftp once done
ftp.close()