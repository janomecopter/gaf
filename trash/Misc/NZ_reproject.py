import os
import gaf
import cartopy
import cartopy.crs as ccrs
import numpy as np
import fiona
from shapely.geometry import Polygon, MultiPolygon, MultiLineString, shape
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from descartes import PolygonPatch

extents = [4750, 4830, 3850, 3930]
# proj_path = '/home/sfm-user/Downloads/NZCoast'
proj_path = r'C:/Users/Public/Downloads/NZCoast'


def subselect_image(im):
    return im[extents[0]:extents[1], extents[2]:extents[3]]


def scale_bar(ax, length=None, location=(0.5, 0.05), linewidth=3):
    """
    ax is the axes to draw the scalebar on.
    length is the length of the scalebar in km.
    location is center of the scalebar in axis coordinates.
    (ie. 0.5 is the middle of the plot)
    linewidth is the thickness of the scalebar.
    """
    # Get the limits of the axis in lat long
    llx0, llx1, lly0, lly1 = ax.get_extent(ccrs.PlateCarree())
    # Make tmc horizontally centred on the middle of the map,
    # vertically at scale bar location
    sbllx = (llx1 + llx0) / 2
    sblly = lly0 + (lly1 - lly0) * location[1]
    tmc = ccrs.TransverseMercator(sbllx, sblly)
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(tmc)
    #Turn the specified scalebar location into coordinates in metres
    sbx = x0 + (x1 - x0) * location[0]
    sby = y0 + (y1 - y0) * location[1]

    #Calculate a scale bar length if none has been given
    #(Theres probably a more pythonic way of rounding the number but this works)
    if not length:
        length = (x1 - x0) / 5000 #in km
        ndim = int(np.floor(np.log10(length))) #number of digits in number
        length = round(length, -ndim) #round to 1sf
        #Returns numbers starting with the list
        def scale_number(x):
            if str(x)[0] in ['1', '2', '5']: return int(x)
            else: return scale_number(x - 10 ** ndim)
        length = scale_number(length)

    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbx - length * 500, sbx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sby, sby], transform=tmc, color='k', linewidth=linewidth)
    #Plot the scalebar label
    ax.text(sbx, sby, str(length) + ' km', transform=tmc,
            horizontalalignment='center', verticalalignment='bottom')



lats = gaf.ahi.get_latitude()
lons = gaf.ahi.get_longitude()
lats = subselect_image(lats)
lons = subselect_image(lons)

mp = MultiPolygon([shape(pol['geometry']) for pol in fiona.open(
    os.path.join(proj_path, 'nz-coastlines-and-islands-polygons-topo-150k.shp'))])
rds = MultiLineString([shape(line['geometry']) for line in fiona.open(
    os.path.join(proj_path, 'nz-road-centrelines-topo-150k.shp'))])

patches = []
for p in mp:
    patches.append(PolygonPatch(p, fc='None', ec='#555555', alpha=1., zorder=1))

f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=cartopy.crs.UTM(59, True)))
ax.scatter(lons, lats, transform=cartopy.crs.PlateCarree(), zorder=2)
ax.add_collection(PatchCollection(patches, match_original=True))
ax.add_geometries([rds], ccrs.UTM(59, True), edgecolor='r', facecolor='None', alpha=0.4, linewidth=1, zorder=0)
ax.set_xlim((616400, 650000))
ax.set_ylim((5156000, 5185000))
scale_bar(ax, 10, (0.21, 0.05))
ax.set_title('AHI Pixel Centres - MWIR 2km - Christchurch area, NZ\nProjection: UTM Zone 59')

f.savefig(os.path.join(proj_path, 'fig.png'), format='png', dpi=300)
print('Plot complete')