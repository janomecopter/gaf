from gaf.sts import *
import matplotlib.cm as mpcm
import platform
import matplotlib.pyplot as plt
import gaf
from mpl_toolkits.axes_grid1 import ImageGrid


cmrb = mpcm.RdBu_r
cmrb.set_bad(color='0.75')
cmvr = mpcm.viridis
cmvr.set_bad(color='0.75')
cmo = mpcm.Oranges
cmo.set_bad(color='0.75')

if platform.node() == 'smgs-seo-bryan':
    data_storage_folder = '/home/bryan/data/P4/run_3/summary'
elif platform.node() == 'smgs-seo-samurai':
    data_storage_folder = '/mnt/solid_store/data/sts_run3'


def summarise(area_key, data_storage_folder):
    flist = [f for f in os.listdir(data_storage_folder) if np.logical_and(area_key in f, '.df' in f)]
    flist.sort()
    for i, f in enumerate(flist):
        df = pd.read_pickle(os.path.join(data_storage_folder, f))
        # df.pred_time = df.pred_time.iloc[0].dt
        if not i:
            summary_df = df
        else:
            summary_df = pd.concat((summary_df, df))
        if not i % 100:
            print(i)
    summary_df['datetime'] = [getattr(summary_df.pred_time.iloc[i], 'dt') for i in np.arange(summary_df.shape[0])]
    return summary_df


def print_area_rms(area_key):
    flist = [f for f in os.listdir(data_storage_folder) if np.logical_and(area_key in f, '.df' in f)]
    flist.sort()
    for i, f in enumerate(flist):
        df = pd.read_pickle(os.path.join(data_storage_folder, f))
        # df.pred_time = df.pred_time.iloc[0].dt
        if not i:
            summary_df = df
        else:
            summary_df = pd.concat((summary_df, df))
    summary_df['datetime'] = [getattr(summary_df.pred_time.iloc[i], 'dt') for i in np.arange(summary_df.shape[0])]
    print('key\tDT\t\t\t\t\tSTS_RMS\tCont_filt\tSTS_filt\tSTS_full')
    rms_df = pd.DataFrame(columns=['datetime', 'sts_rms'])
    for i, key in enumerate(summary_df.groupby(summary_df.datetime).groups.keys()):
        g0 = summary_df.groupby(summary_df.datetime).get_group(key)
        print('{}\t{}\t{:.3f}\t{:.3f}\t{:.3f}\t{:.3f}'.format(
            i, key, np.nanmean(g0.sts_rms_mean[g0.sts_rms_mean != 10000]), np.nanstd(g0.cont_filt_diff), np.nanstd(g0.sts_filt_diff),
            np.nanstd(g0.sts_full_diff)
        ))
        rms_df.loc[i] = [key, np.nanmean(g0.sts_rms_mean[g0.sts_rms_mean != 10000])]
    return summary_df, rms_df


def plot_time(df, index_number=None):
    if index_number is None:
        g0 = df
    else:
        g0 = df.groupby('pred_time').get_group(df.pred_time.iloc[index_number*2500])
    # show temps
    f, ax = plt.subplots(2, 4, figsize=(12.5, 7.5))
    for i, k in enumerate(['sensor_bt7', 'cont_filt_mean', 'sts_filt_mean', 'sts_full_mean']):
        if ~i:
            t_min = np.nanmin(g0[k].values)
            t_max = np.nanmax(g0[k].values)
        else:
            t_min = np.min((t_min, np.nanmin(g0[k].values)))
            t_max = np.max((t_max, np.nanmax(g0[k].values)))
        t_min = (t_min//5) * 5
        t_max = (1 + t_max//5) * 5

    for i, k in enumerate(['sensor_bt7', 'cont_filt_mean', 'sts_filt_mean', 'sts_full_mean']):
        ax[0, i].imshow(g0[k].values.reshape(50, -1), cmap=cmvr, vmin=t_min, vmax=t_max)
        ax[0, i].set_title(k)
        ax[0, i].set_xticklabels([])
        ax[0, i].set_yticklabels([])
    # f.suptitle('Temps', fontsize=12)
    # f.set_tight_layout(True)

    # show diffs
    # f, ax = plt.subplots(1, 3, figsize=(8.5, 3.5))
    for i, k in enumerate(['cont_filt_diff', 'sts_filt_diff', 'sts_full_diff']):
        ax[1, i+1].imshow(g0[k].values.reshape(50, -1), cmap=cmrb, vmin=-5, vmax=5)
        ax[1, i+1].set_title(k)
        ax[1, i+1].set_xticklabels([])
        ax[1, i+1].set_yticklabels([])
    ax[1, 0].remove()
    f.suptitle('{}'.format(g0.pred_time.iloc[0].dt), fontsize=12)
    f.set_tight_layout(True)

    print('{}\t{:.3f}\t{:.3f}\t{:.3f}'.format(getattr(g0.pred_time.iloc[0], 'dt'), np.nanstd(g0.cont_filt_diff.values),
        np.nanstd(g0.sts_filt_diff.values), np.nanstd(g0.sts_full_diff.values)))


def stats_per_time_step(summary_df):
    temp_df = pd.DataFrame()
    for i, key in enumerate(summary_df.groupby(summary_df.datetime).groups.keys()):
        g0 = summary_df.groupby(summary_df.datetime).get_group(key)
        temp_df = temp_df.append({'time': key,
                                  'sts_rms': np.nanmean(g0.sts_rms_mean[g0.sts_rms_mean != 10000]),
                                  'cont_filt_std': np.nanstd(g0.cont_filt_diff),
                                  'sts_filt_std': np.nanstd(g0.sts_filt_diff),
                                  'sts_full_std': np.nanstd(g0.sts_full_diff)}, ignore_index=True)
    f, ax = plt.subplots()
    ax.scatter(temp_df.time, temp_df.sts_filt_std, c='g')
    ax.scatter(temp_df.time, temp_df.cont_filt_std, c='r')


def break_by_hour(df):
    df['hour'] = [getattr(df.pred_time.iloc[i], 'hour') for i in np.arange(df.shape[0])]
    for i in list(df.groupby(df.hour).groups.keys()):
        g0 = df.groupby(df.hour).get_group(i)
        print('Hour:      {}'.format(i))
        print('Cont_filt: {:.3f}'.format(np.nanstd(g0.cont_filt_diff)))
        print('STS_filt:  {:.3f}'.format(np.nanstd(g0.sts_filt_diff)))
        print('STS_full:  {:.3f}\n'.format(np.nanstd(g0.sts_full_diff)))


def analyse_decay(area_key, selection_time, prediction_time):
    #area_key = 'thl_q'
    #selection_time = Time([2016, 2, 28, 0, 50])
    selection_rms = Dataset(os.path.join('/mnt/solid_store/data/sts_run3', '{}_{}_{:03d}_{:02d}_{:02d}_rms.nc'.format(
                    area_key, *[getattr(selection_time, item) for item in ['year', 'jday', 'hour', 'minute']]
                                                 ))).variables['rms'][:]
    cs_areas = get_cs_areas()
    num_predictors = 24
    search_radius = 50
    sample_area = np.array(cs_areas[area_key]) + np.array([50, -50, 50, -50])
    make_prediction(prediction_time, sample_area, search_radius, selection_rms, area_key, num_predictors)

    #for d in range(25):
    #    prediction_time = Time(selection_time.dt + datetime.timedelta(hours=24 * d + 2))
    #    print(prediction_time.dt)
    #    make_prediction(prediction_time, sample_area, search_radius, selection_rms, area_key, num_predictors)


prediction_time = gaf.Time((2016, 4, 19, 23, 10))
t_array = [gaf.Time(prediction_time.dt + datetime.timedelta(minutes=10 * int(i))) for i in np.arange(144 * 7)]
k = 'sea_c'
[analyse_decay(k, gaf.Time((2016, 4, 19, 21, 10)), t) for t in t_array]

'''
def examine_process(input_data):
    area_key, pred_time = input_data
    cs_areas = get_cs_areas()
    cont_lv = load_cont_lv(os.path.join(IMAGEDIR, 'P4'))
    use_cldmask = True
    sample_area = np.array(cs_areas[area_key]) + np.array([50, -50, 50, -50])
    predictor_time = Time(pred_time.dt - datetime.timedelta(hours=2))
    selection_rms = Dataset(os.path.join(data_storage_folder, '{}_{}_{:03d}_{:02d}_{:02d}_rms.nc'.format(
        area_key, *[getattr(predictor_time, item) for item in ['year', 'jday', 'hour', 'minute']]))).variables['rms'][:]
    search_radius = 50
    num_predictors = 24

    if type(sample_area) is not np.array:
        sample_area = np.array(sample_area)
    image_area = sample_area + np.array([-1, 1, -1, 1]) * search_radius

    # get land/sea mask info for sample area
    sub_land = cont_lv[image_area[0]:image_area[1], image_area[2]:image_area[3]]
    bsub_land = cont_lv[sample_area[0]:sample_area[1], sample_area[2]:sample_area[3]]

    sub_k = area_key[:3]
    cld_tl = get_top_left_of_subset_cloud(sub_k)
    a = AHICapture(pred_time)
    try:
        bt7 = a.band7.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
        bt13 = a.band13.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
        solar_zenith = a.solarza.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
    except AttributeError:
        print('No file available for {}'.format(a.time.dt))
    bt7[~sub_land] = np.nan
    if use_cldmask:
        try:
            cld_path = os.path.join(IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                    '{:03d}'.format(a.time.jday), area_key[:3])
            cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                           a.time.minute)
            r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
        except IndexError:
            print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
        except FileNotFoundError:
            print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
        t_cld = r.variables['cloud_mask'][0, image_area[0] - cld_tl[0]: image_area[1] - cld_tl[0],
                image_area[2] - cld_tl[1]: image_area[3] - cld_tl[1]]
        bt7[t_cld == 1] = np.nan

    # apply tests as per MODIS fire product outlier exclusion (Giglio 2003)
    delta_t = bt7 - bt13
    bt7[np.logical_and.reduce((solar_zenith < 85, delta_t > 20, bt7 > 325))] = np.nan
    bt7[np.logical_and.reduce((solar_zenith >= 85, delta_t > 10, bt7 > 310))] = np.nan

    bt_pad = np.empty((bt7.shape[0] + 1, bt7.shape[1] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:-1, :-1] = bt7.astype(np.float32)
    bt_pad[bt_pad > 400] = np.nan

    arraywidth = 2 * search_radius + 1
    q = np.arange(arraywidth ** 2).reshape(arraywidth, -1)
    dist = np.sqrt((q // arraywidth - search_radius) ** 2 + (q % arraywidth - search_radius) ** 2)

    # create a meshed coordinate array based upon the sample area size
    y_vals = np.arange(sample_area[1] - sample_area[0])
    x_vals = np.arange(sample_area[3] - sample_area[2])
    coords = np.meshgrid(x_vals, y_vals)

    val_array = np.array([bt_pad[wy:wy + arraywidth, wx:wx + arraywidth] for wy in y_vals for wx in x_vals]).reshape(
        len(x_vals) * len(y_vals), -1)
    val_array[~np.ravel(bsub_land), :] = np.nan
    sensor_bt7 = np.copy(val_array[:, val_array.shape[1] // 2])

    selection_array = selection_rms.argsort(axis=1)
    large_pred_vec = np.take_along_axis(val_array, selection_array[:, 1:num_predictors * 40 + 1], 1)
    large_sts_rms = np.take_along_axis(selection_rms, selection_array[:, 1:num_predictors * 40 + 1], 1)
    large_pred_vec[large_sts_rms == 10000] = np.nan

    norm_pred_vec = large_pred_vec[:, :num_predictors]
    sts_mean = np.nanmean(norm_pred_vec, 1)
    sts_rms = large_sts_rms[:, :num_predictors]
    # sts_rms = np.ma.array(sts_rms, mask=sts_rms == 10000)
    sts_rms[sts_rms == 10000] = np.nan
    sts_rms_mean = np.nanmean(sts_rms, axis=1)
    sts_valid_count = np.count_nonzero(~np.isnan(norm_pred_vec), axis=1)
    sts_diff = sensor_bt7 - sts_mean

    filt_mask = np.logical_or(np.isnan(norm_pred_vec),
                              np.abs(norm_pred_vec - np.nanmean(norm_pred_vec, axis=1)[:, np.newaxis])
                              > 2 * np.nanstd(norm_pred_vec, axis=1)[:, np.newaxis])
    sts_filt_mean = np.ma.mean(np.ma.masked_array(norm_pred_vec, mask=filt_mask), axis=1)
    sts_filt_mean[sts_filt_mean < 200] = np.nan
    sts_filt_diff = sensor_bt7 - sts_filt_mean

    cont_inds = q[dist < 3].reshape(-1)
    cont_inds = np.delete(cont_inds, len(cont_inds) // 2)
    cont_vec = np.take_along_axis(val_array, cont_inds[np.newaxis, :], 1)
    cont_valid_count = np.count_nonzero(~np.isnan(cont_vec), axis=1)
    cont_mean = np.nanmean(cont_vec, 1)
    cont_diff = sensor_bt7 - cont_mean
    filt_mask = np.logical_or(np.isnan(cont_vec), np.abs(cont_vec - np.nanmean(cont_vec, axis=1)[:, np.newaxis])
                              > 2 * np.nanstd(cont_vec, axis=1)[:, np.newaxis])
    cont_filt_mean = np.ma.mean(np.ma.masked_array(cont_vec, mask=filt_mask), axis=1)
    cont_filt_mean[cont_filt_mean < 200] = np.nan
    cont_filt_diff = sensor_bt7 - cont_filt_mean
    cont_rms = np.take_along_axis(selection_rms, cont_inds[np.newaxis, :], 1)
    cont_rms[cont_rms == 10000] = np.nan
    cont_rms_mean = np.nanmean(cont_rms, axis=1)

    sts_full_pred = np.zeros(len(x_vals) * len(y_vals))
    sts_full_rms = np.zeros(len(x_vals) * len(y_vals))
    sts_full_count = np.zeros(len(x_vals) * len(y_vals))

    for i, vec in enumerate(large_pred_vec):
        fully_valid_vec = vec[~np.isnan(vec)][:num_predictors]
        mask = np.logical_or(np.isnan(fully_valid_vec),
                             np.abs(fully_valid_vec - np.nanmean(fully_valid_vec)) > 2 * np.nanstd(fully_valid_vec))
        full_pred_mean = np.ma.mean(np.ma.masked_array(fully_valid_vec, mask=mask))
        full_pred_valid = np.count_nonzero(~mask)

        fully_valid_rms = large_sts_rms[i, ~np.isnan(vec)][:num_predictors]
        # fully_valid_rms[fully_valid_rms > 50] = np.nan
        full_pred_rms = np.ma.mean(np.ma.masked_array(
            fully_valid_rms, mask=np.logical_or(mask, np.isnan(fully_valid_rms))))
        if full_pred_mean < 200:
            sts_full_pred[i] = np.nan
        else:
            sts_full_pred[i] = full_pred_mean
        sts_full_rms[i] = full_pred_rms
        sts_full_count[i] = full_pred_valid
    sts_full_diff = sensor_bt7 - sts_full_pred
'''