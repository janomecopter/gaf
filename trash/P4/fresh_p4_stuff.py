import gaf
import numpy as np
from netCDF4 import Dataset
import os
import sys
import pandas as pd
import cartopy
import datetime
import pickle
import matplotlib.pyplot as plt
import matplotlib.cm as mpcm
from mpl_toolkits.axes_grid1 import ImageGrid


# Functions
def load_cont_lv():
    va = gaf.get_viewangle()
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = gaf.get_land_mask()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = gaf.AHICapture((2016, 6, 1, 0))
        file_kw = 'B07-w05-counts_valid'
        temp = Dataset(os.path.join(savepath, '{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        array = np.copy(flt_land)
        array[np.isnan(temp)] = False
        np.save(os.path.join(savepath, 'cont_lv.npy'), array)
    else:
        array = np.load(os.path.join(savepath, 'cont_lv.npy'))
    array = np.ma.array(array, mask=va > 80)
    return array


ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
savepath = os.path.join(gaf.IMAGEDIR, 'P4')

cmrb = mpcm.RdBu_r.copy()
cmrb.set_bad(color='0.75')
cmvr = mpcm.viridis.copy()
cmvr.set_bad(color='0.75')
cmo = mpcm.Oranges.copy()
cmo.set_bad(color='0.75')

cont_lv = load_cont_lv()

# amended cs_areas for spatio-temporal work
cs_areas = {'sea': [4350, 4650, 3000, 3300],
            'nwa': [3550, 3850, 1950, 2250],
            'bor': [2550, 2850, 1350, 1650],
            'thl': [1750, 2050, 750, 1050],
            'chn': [950, 1250, 1550, 1850],
            'jpn': [850, 1150, 2450, 2750],
            'sib': [150, 450, 1950, 2250]}

with open(os.path.join(savepath, 'ncs_areas.pkl'), 'rb') as f:
    ncs_areas = pickle.load(f)
cs_areas.update(ncs_areas)

cld_tl = {'sea': [4350, 3000], 'nwa': [3550, 1950], 'bor': [2550, 1350], 'thl': [1750, 750],
          'chn': [950, 1550], 'jpn': [850, 2450], 'sib': [150, 1950]}

num_points = 24

# setup distance array
radius = 50
arraywidth = 2 * radius + 1
q = np.arange(arraywidth ** 2).reshape(arraywidth, -1)
dist = np.sqrt((q // arraywidth - radius) ** 2 + (q % arraywidth - radius) ** 2)
use_cldmask = True
# pred_time = gaf.Time((2016, 105, 2, 0))
# pred_time = gaf.Time((2016, 82, 2, 10))
pred_time = gaf.Time((2016, 117, 5, 0))

# freq = '1h'
num_times = 48


def process_time(pred_time, freq, k, plot=False):
    # grab base sub_land (for eliminating sea tiles)
    sub_land = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
    bsub_land = sub_land[radius:-radius, radius:-radius]
    # num_land_pixels = np.count_nonzero(bsub_land)

    if freq[-3:] == 'min':
        minute_gap = int(freq[:-3])
    elif freq[-1:] == 'h':
        minute_gap = int(freq[:-1]) * 60
    elif freq[-1:] == 'd':
        minute_gap = int(freq[:-1]) * 1440
    else:
        print('make freq sensible please')
        sys.exit()

    # create t_list - list of times for prediction step
    t_list = [pred_time.dt - datetime.timedelta(minutes=int(minute_gap * i)) for i in np.arange(num_times + 1)]
    t_list.sort()

    # create storage for brightness temperature information
    bt_array = np.empty((num_times + 1, cs_areas[k][1] - cs_areas[k][0], cs_areas[k][3] - cs_areas[k][2]))
    bt_array[:] = np.nan
    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        try:
            bt = a.band7.data[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
        except AttributeError:
            print('No file available for {}'.format(a.time.dt))
            continue
        bt[~sub_land] = np.nan
        if use_cldmask:
            sub_k = k[:3]
            try:
                cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                        '{:03d}'.format(a.time.jday), k[:3])
                cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                               a.time.minute)
                r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
            except IndexError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            except FileNotFoundError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            t_cld = r.variables['cloud_mask'][0, cs_areas[k][0] - cld_tl[sub_k][0]: cs_areas[k][1] - cld_tl[sub_k][0],
                                              cs_areas[k][2] - cld_tl[sub_k][1]: cs_areas[k][3] - cld_tl[sub_k][1]]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt.astype(np.float32)

    bt_pad = np.empty((bt_array.shape[0], bt_array.shape[1] + 1, bt_array.shape[2] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:, :-1, :-1] = bt_array.astype(np.float32)
    bt_pad[bt_pad > 400] = np.nan

    array_size = 50
    y_vals = x_vals = np.arange(array_size)
    coords = np.meshgrid(x_vals, y_vals)
    val_array = np.array([bt_pad[:, wy:wy + (radius * 2 + 1), wx:wx + (radius * 2 + 1)]
                          for wy in y_vals for wx in x_vals])
    va = val_array.swapaxes(0, 1).reshape(bt_array.shape[0], val_array.shape[0], -1)
    va[:, ~np.ravel(bsub_land), :] = np.nan
    cent_vals = va[:-1, :, va.shape[2] // 2]
    diffs = va[:-1] - cent_vals[:, :, np.newaxis]
    rms = np.nanmean(diffs ** 2, axis=0)
    amount_valid = np.count_nonzero(~np.isnan(diffs), axis=0)
    rms[:, np.ravel(dist > 50)] = 10000
    rms[amount_valid < 4] = 10000
    inds = rms.argsort()

    # TODO: total the rms[inds] and divide by 10000 - this will reveal how many are invalid
    # then use take to recover only the valid predictors somehow
    nan_inds = np.copy(inds).astype(np.float)
    nan_inds[~np.ravel(bsub_land)] = np.nan
    pred_vec = np.take_along_axis(va[-1], inds[:, 1:num_points + 1], 1)

    cont_inds = q[dist < 3].reshape(-1)
    cont_inds = np.delete(cont_inds, 12)
    cont_vec = np.take_along_axis(va[-1], cont_inds[np.newaxis, :], 1)

    pred_rms = np.take_along_axis(rms, inds[:, 1:num_points + 1], 1)
    pred_rms[pred_rms > 50] = np.nan

    cont_rms = np.take_along_axis(rms, cont_inds[np.newaxis, :], 1)
    cont_rms[cont_rms > 50] = np.nan

    ix = np.isin(inds, cont_inds)
    part_ix = ix[:, 1:num_points + 1]

    df = pd.DataFrame(columns=['time', 'freq'])
    df['x'] = np.ravel(coords[0]) + cs_areas[k][2] + radius
    df['y'] = np.ravel(coords[1]) + cs_areas[k][0] + radius
    df['bt'] = np.ravel(va[-1, :, va.shape[2]//2])
    df['sts_mean'] = np.nanmean(pred_vec, 1)
    # df['sts_valid'] = np.count_nonzero(~np.isnan(pred_vec), axis=1)
    df['sts_diff'] = df.bt - df.sts_mean
    df['sts_rms'] = np.nanmean(np.take_along_axis(rms, inds[:, 1:num_points + 1], 1), axis=1)
    df['select_dist'] = np.nanmean(np.sqrt(np.abs(nan_inds[:, 1:num_points + 1] // arraywidth - radius) ** 2 +
                                   np.abs(nan_inds[:, 1:num_points + 1] % arraywidth - radius) ** 2), axis=1)
    df['cont_mean'] = np.nanmean(cont_vec, 1)
    df['cont_valid'] = np.count_nonzero(~np.isnan(cont_vec), axis=1)
    df['cont_diff'] = df.bt - df.cont_mean
    df['cont_rms'] = np.nanmean(cont_rms, axis=1)
    filt_mask = np.logical_or(np.isnan(pred_vec), np.abs(pred_vec - np.nanmean(pred_vec, axis=1)[:, np.newaxis])
                              > 2 * np.nanstd(pred_vec, axis=1)[:, np.newaxis])
    df['sts_filt_mean'] = np.ma.mean(np.ma.masked_array(pred_vec, mask=filt_mask), axis=1)
    df['sts_filt_diff'] = df.bt - df.sts_filt_mean
    df['pred_valid'] = np.count_nonzero(~np.isnan(pred_vec), 1)
    df['wax'] = df.sts_filt_diff * (df.pred_valid / num_points) / np.nanmean(pred_rms, 1)
    df['pred_vec'] = pred_vec.tolist()
    df['pred_rms'] = pred_rms.tolist()
    df['cont_vec'] = cont_vec.tolist()
    df['select_inds'] = nan_inds[:, 1:num_points + 1].tolist()
    df['time'].iloc[0] = pred_time
    df['freq'].iloc[0] = freq
    #pklpath = os.path.join(savepath, 'take_two', '{}'.format(freq),
    #                       '{}_{}_{:03d}_{:02d}_{:02d}.df'.format(k, pred_time.year, pred_time.jday, pred_time.hour,
    #                                                              pred_time.minute))
    pklpath = '/mnt/spinny_store/BH/data/P4/cover/{}_{}_{:03d}_{:02d}_{:02d}.df'.format(k, pred_time.year,
                                                                                        pred_time.jday, pred_time.hour,
                                                                                        pred_time.minute)
    df.to_pickle(pklpath)


def plots_mk5():
    pklpath = '/mnt/spinny_store/BH/data/P4/cover'
    cont_pc = 0.65
    sts_pc = 0.25
    # anom_rate = 0.02
    fs = 14

    for pkl in gaf.listdir_end(pklpath, '.df'):

        df = pd.read_pickle(pkl)

        # produce plots
        tmin = np.max((np.floor(np.nanmin((np.nanpercentile(df.bt.values, 2),
                                           np.nanpercentile(df.sts_mean.values, 2),
                                           np.nanpercentile(df.cont_mean.values, 2))) / 5) * 5, 260))
        tmax = np.max((np.min((np.ceil(np.nanmax((np.nanpercentile(df.bt.values, 98),
                                                  np.nanpercentile(df.sts_mean.values, 98),
                                                  np.nanpercentile(df.cont_mean.values, 98))) / 5) * 5, 340)), 265))
        # set minimum candidates - 65%
        cont_mean = df.cont_mean.values
        cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
        try:
            sts_valid_vals = df.sts_valid.values
        except:
            sts_valid_vals = df.pred_valid.values
        sts_mean = df.sts_mean.values
        sts_mean[sts_valid_vals < sts_pc * 24] = np.nan
        sts_filt_mean = df.sts_filt_mean.values
        sts_filt_mean[sts_valid_vals < sts_pc * 24] = np.nan
        all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                       ~np.isnan(sts_filt_mean)))

        sts_filt_diff = df.sts_filt_diff.values
        sts_filt_diff[~all_4] = np.nan
        cont_diff = df.cont_diff.values
        cont_diff[~all_4] = np.nan

        f = plt.figure(figsize=(15, 3.2))
        grid = ImageGrid(f, (0.02, 0.05, 0.51, 0.92), (1, 3), axes_pad=0.1, cbar_mode='single', cbar_location='right',
                         label_mode='L')

        grid[0].imshow(df.bt.values.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
        grid[0].set_title('BT (4$\mu$m) - AHI B07', fontsize=fs)

        grid[1].imshow(sts_filt_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
        grid[1].set_title('STS estimate', fontsize=fs)

        im = grid[2].imshow(cont_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
        grid[2].set_title('Context (5x5) estimate', fontsize=fs)

        for val in np.arange(3):
            grid[val].set(xticklabels=[], yticklabels=[])

        cb = grid.cbar_axes[0].colorbar(im)
        cb.ax.yaxis.set_ticks(np.arange(tmin, tmax + 5, 5))
        cb.ax.yaxis.set_ticklabels(np.arange(tmin, tmax + 5, 5).astype(np.int), fontsize=fs)
        cb.ax.set_ylabel('Brightness Temp (K)', fontsize=fs)

        grid2 = ImageGrid(f, (0.6, 0.05, 0.35, 0.92), (1, 2), axes_pad=0.1, cbar_mode='single', cbar_location='right',
                          label_mode='L')

        im2 = grid2[0].imshow(sts_filt_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
        grid2[0].set_title('STS diff', fontsize=fs)
        grid2[1].imshow(cont_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
        grid2[1].set_title('Context (5x5) diff', fontsize=fs)

        for val in np.arange(2):
            grid2[val].set(xticklabels=[], yticklabels=[])

        cb = grid2.cbar_axes[0].colorbar(im2, extend='both')
        cb.ax.set_yticklabels(np.arange(-6, 6, 2), fontsize=fs)
        cb.ax.set_ylabel('$\Delta$T (K)', fontsize=fs)
        cb.ax.spines['top'].set_color(cmrb(255))
        cb.ax.spines['bottom'].set_color(cmrb(0))

        fsavepath = os.path.join(pklpath, pkl.split('/')[-1].split('.')[0] + '.png')
        f.savefig(fsavepath, dpi=600, format='png')
        plt.close(f)


pklpath = '/mnt/spinny_store/BH/data/P4/cover'


def plot_full(k, pred_time):
    pkllist = gaf.listdir_start(pklpath, '{}'.format(k))
    df_list = []
    for i, pkl in enumerate([item for item in pkllist if '{:03d}_{:02d}_{:02d}'.format(pred_time.jday, pred_time.hour,
                                                                                       pred_time.minute) in item]):
        df = pd.read_pickle(pkl)
        df_list.append(df[['x', 'y', 'bt', 'cont_mean', 'sts_filt_mean', 'sts_filt_diff', 'cont_diff']])
    sts_filt_mean = pd.concat(df_list, ignore_index=True)

    sort_df = sts_filt_mean.sort_values(by=['y', 'x'])
    bt = sort_df.bt.values.reshape(200, -1)
    contx = sort_df.cont_mean.values.reshape(200, -1)
    means = sort_df.sts_filt_mean.values.reshape(200, -1)
    sdiff = sort_df.sts_filt_diff.values.reshape(200, -1)
    cdiff = sort_df.cont_diff.values.reshape(200, -1)
    means[means == 0] = np.nan
    sdiff[means == 0] = np.nan
    cdiff[means == 0] = np.nan

    d = {'bt': bt, 'contx': contx, 'means': means, 'sdiff': sdiff, 'cdiff': cdiff}
    diff_params = dict(cmap=cmrb, vmin=-5, vmax=5)
    mean_params = dict(cmap=cmvr, vmin=280, vmax=310)

    for var in d.keys():
        if 'diff' in var:
            plot_var(k, pred_time, d[var], var, **diff_params)
        else:
            plot_var(k, pred_time, d[var], var, **mean_params)


def plot_var(k, pred_time, var, varname, **kwargs):
    f, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(var, **kwargs)
    ax.set_xticks([])
    ax.set_yticks([])
    f.set_tight_layout(True)
    for spine in ax.spines.values():
        spine.set_edgecolor('white')
    fname = os.path.join(pklpath, 'full', '{}_{:04d}_{:03d}_{:02d}{:02d}_{}.png'.format(
        k, pred_time.year, pred_time.jday, pred_time.hour, pred_time.minute, varname))
    f.savefig(os.path.join(pklpath, 'full', fname), dpi=600)
    plt.close(f)


key = 'sea'
for k in [item for item in cs_areas.keys() if '{}_'.format(key) in item]:
    process_time(pred_time, '2h', k)
plot_full(pred_time, key)


'''    f, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(bt, cmap=cmvr, vmin=300, vmax=320)
    ax.set_xticks([])
    ax.set_yticks([])
    f.set_tight_layout(True)
    for spine in ax.spines.values():
        spine.set_edgecolor('white')
    f.savefig(os.path.join(pklpath, 'full', '{}_bt.png'.format(k)), dpi=600)
    plt.close(f)

    f, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(contx, cmap=cmvr, vmin=300, vmax=320)
    ax.set_xticks([])
    ax.set_yticks([])
    f.set_tight_layout(True)
    for spine in ax.spines.values():
        spine.set_edgecolor('white')
    f.savefig(os.path.join(pklpath, 'full', '{}_{:04d}_{:03d}_{:02d}{:02d}_contm.png'.format(
        pred_time.year, pred_time.jday, pred_time.hour, pred_time.minute, k)), dpi=600)
    plt.close(f)

    f, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(means, cmap=cmvr, vmin=300, vmax=320)
    ax.set_xticks([])
    ax.set_yticks([])
    f.set_tight_layout(True)
    for spine in ax.spines.values():
        spine.set_edgecolor('white')
    f.savefig(os.path.join(pklpath, 'full', '{}_stsm.png'.format(k)), dpi=600)
    plt.close(f)

    f, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(sdiff, cmap=cmrb, vmin=-5, vmax=5)
    ax.set_xticks([])
    ax.set_yticks([])
    f.set_tight_layout(True)
    for spine in ax.spines.values():
        spine.set_edgecolor('white')
    f.savefig(os.path.join(pklpath, 'full', '{}_stsd.png'.format(k)), dpi=600)
    plt.close(f)

    f, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(cdiff, cmap=cmrb, vmin=-5, vmax=5)
    ax.set_xticks([])
    ax.set_yticks([])
    f.set_tight_layout(True)
    for spine in ax.spines.values():
        spine.set_edgecolor('white')
    f.savefig(os.path.join(pklpath, 'full', '{}_contd.png'.format(k)), dpi=600)
    plt.close(f)'''