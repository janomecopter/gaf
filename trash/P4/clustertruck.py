import gaf
import pandas as pd
import datetime
import matplotlib.path as path
import numpy as np
import matplotlib.pyplot as plt

loc = (-20.3184, 132.5475)
t = gaf.tools.Time((2016,6,19,0,0))

lat, lon = [np.round(x*4)/4 for x in loc]
verts = np.array([[lon-1, lat+1], [lon+1, lat+1], [lon+1, lat-1], [lon-1, lat-1]])
pp = path.Path(verts)

lon, lat, bt = gaf.mask_AHI_for_plot(gaf.EXAMPLEFILE)
vals = pp.contains_points(np.column_stack((np.ravel(lon), np.ravel(lat)))).reshape(lon.shape)
locs = np.column_stack((array for array in np.where(vals)))

img_index = pd.date_range(t.dt, t.dt + datetime.timedelta(minutes=1430),freq='10min')
img_times = img_index.to_pydatetime()
bt = np.zeros((len(img_times),locs.shape[0]))

for i, t in enumerate(img_times):
    try:
        a = gaf.AHICapture(t)
        b7 = a.band7
        bt[i] = b7.data[vals]
        print('{} processed'.format(b7.fname))
    except AttributeError:
        bt[i] = np.nan

bt_df = pd.DataFrame(index=img_index, data=bt)
bt_df = bt_df.dropna()
m_df = bt_df - bt_df.mean()

loc_ind = 2445
vec = m_df[[loc_ind]]

rms = np.array([np.sqrt(((m_df.values[:, i] - np.ravel(vec.values))**2).mean()) for i in np.arange(m_df.shape[1])])

mmp = np.zeros((5500, 5500))
mmp[:] = np.nan
for i, loc in enumerate(locs):
    mmp[loc[0], loc[1]] = rms[i]
f, ax = plt.subplots(1, 2)
ax[0].imshow(np.ma.masked_invalid(mmp), cmap='gray')
ax[0].set_xlim((np.nanmin(locs[:, 1])-20, np.nanmax(locs[:, 1])+20))
ax[0].set_ylim((np.nanmin(locs[:, 0])-20, np.nanmax(locs[:, 0])+20))
#gaf.tools.format_plot_time(ax[0])

low_ind = rms.argsort()[:30]
[ax[0].scatter(loc[1], loc[0]) for loc in locs[low_ind]]
[ax[1].plot(bt_df[[ind]]) for ind in low_ind]
#  f.tight_layout()
f.savefig('H:/Python/temp/clusterfig.png', format='png')