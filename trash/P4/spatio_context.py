import gaf
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import time
from multiprocessing import Pool
import platform
import pandas as pd
import cartopy
from shapely.geometry import Polygon, Point


# Functions
def load_cont_lv():
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = gaf.get_land_mask()
        va = gaf.get_viewangle()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = gaf.AHICapture((2016, 6, 1, 0))
        file_kw = ['B07-w{:02d}-counts_valid'.format(n) for n in np.arange(11) * 2 + 5]
        temp = Dataset(os.path.join(savepath, '{0}/{1:03d}/{2:02d}/{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw[0]))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        cont_lv = np.copy(flt_land)
        cont_lv[np.isnan(temp)] = False
        cont_lv = np.ma.array(cont_lv, mask=va > 80)
        np.save(os.path.join(savepath, 'cont_lv.npy'), cont_lv)
        return cont_lv
    else:
        return np.load(os.path.join(savepath, 'cont_lv.npy'))


def default_setup(k, use_cldmask=True, save_files=False):
    int_bounds = np.array([45, -45, 45, -45]) + np.array(cs_areas[k])
    ext_bounds = np.array([-50, 50, -50, 50]) + int_bounds
    area_inds = np.ravel(np.stack(np.meshgrid(np.arange(int_bounds[1] - int_bounds[0]) + 50,
                                              np.arange(int_bounds[3] - int_bounds[2]) + 50),
                                  axis=2)).reshape(-1, 2)
    search_bounds = np.arange(5) * 10 + 10
    t_list = [[2016, d, h, m] for d in np.arange(cs_jdays[k][0], cs_jdays[k][1] + 1)
              for h in [int(cs_times[k].split(':')[0])]
              for m in [int(cs_times[k].split(':')[1])]]
    bt_array = np.empty((len(t_list), ext_bounds[1]-ext_bounds[0], ext_bounds[3]-ext_bounds[2]))
    bt_array[:] = np.nan

    sub_land = cont_lv[ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]
    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        try:
            bt = a.band7.data[ext_bounds[0]:ext_bounds[1], ext_bounds[2]:ext_bounds[3]]
        except AttributeError:
            continue
        #continue
        bt[~sub_land] = np.nan
        if use_cldmask:
            try:
                r = Dataset(os.path.join(pt_clmask_loc, k,
                                         [f for f in os.listdir(os.path.join(pt_clmask_loc, k)) if
                                          '2016{:02d}{:02d}'.format(a.time.month, a.time.day) in f][0]))
            except FileNotFoundError:
                print('No cloud data for {:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                raise
            t_cld = r.variables['cloud_mask'][0, :210, :210]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt

    num_selections = 100
    df = pd.DataFrame(columns=['pos_y', 'pos_x', 'cent_value', 'cont_mean'])
    df = df.astype(dtype={col: np.int for col in df.columns[:2]})
    df = df.astype(dtype={col: np.float for col in df.columns[2:]})
    df['pos_y'] = area_inds[:, 1] + ext_bounds[0]
    df['pos_x'] = area_inds[:, 0] + ext_bounds[2]
    for i, pos in enumerate(area_inds):
        df.loc[i, 'cent_value'] = bt_array[-1, pos[0], pos[1]]
        cont_vals = bt_array[-1, pos[0] - 2:pos[0] + 3, pos[1] - 2:pos[1] + 3]
        cont_vals = np.delete(np.ravel(cont_vals), 12)
        df.loc[i, 'cont_mean'] = np.nanmean(cont_vals)
    df['cont_diff'] = df.cont_mean - df.cent_value
    if save_files:
        df.to_pickle(r'C:\Scratch\cache\P4\prelim\{}-30d-1pd-cont.pkl'.format(k))

    for sr_num, sr in enumerate(search_bounds):
        df = pd.DataFrame(columns=['pos_y', 'pos_x', 'cent_value', 'pred_mean', 'pred_weighted'])
        df['pos_y'] = area_inds[:, 1] + ext_bounds[0]
        df['pos_x'] = area_inds[:, 0] + ext_bounds[2]
        df = df.astype(dtype={col: np.int for col in df.columns[:2]})
        df = df.astype(dtype={col: np.float for col in df.columns[2:]})
        for i, pos in enumerate(area_inds):
            cent_vec = bt_array[:-1, pos[0], pos[1]]
            area_set = bt_array[:-1, pos[0] - sr:pos[0] + sr + 1, pos[1] - sr:pos[1] + sr + 1]
            diffs = area_set - cent_vec[:, np.newaxis, np.newaxis]
            # TODO: bring in unavailability in prediction image
            # diffs[np.isnan(bt_array[-1, pos[0] - sr:pos[0] + sr + 1, pos[1] - sr:pos[1] + sr + 1]) = np.nan
            rms = np.sqrt(np.nanmean(diffs ** 2, axis=0))
            ind = np.unravel_index(np.argsort(rms, axis=None), rms.shape)
            choices = np.array([ind[0][1:num_selections + 1], ind[1][1:num_selections + 1]])
            prediction_vec = bt_array[-1, choices[0] + pos[0] - sr, choices[1] + pos[1] - sr]
            df.loc[i, 'cent_value'] = bt_array[-1, pos[0], pos[1]]
            df.loc[i, 'pred_mean'] = np.nanmean(prediction_vec)
            weights = 1 / rms[choices[0], choices[1]]
            df.loc[i, 'pred_weighted'] = np.nansum(prediction_vec * weights) / np.sum(weights[~np.isnan(prediction_vec)])
        df['cs_diff'] = df.pred_mean - df.cent_value
        df['csw_diff'] = df.pred_weighted - df.cent_value
        if save_files:
            df.to_pickle(r'C:\Scratch\cache\P4\prelim\{}-30d-1pd-sr{}.pkl'.format(k, sr))


def format_results(k='all'):
    search_bounds = np.arange(5) * 10 + 10
    if k != 'all':
        k_list = [k]
    else:
        k_list = list(cs_areas.keys())
    for k in k_list:
        print('\n\nCase Study Area: {}'.format(k))
        # print context
        df = pd.read_pickle(r'C:\Scratch\cache\P4\prelim\{}-30d-1pd-cont.pkl'.format(k))
        print('Total pixels eval\'d: {}'.format(np.count_nonzero(~np.isnan(df.cent_value.values))))
        print('\\hline')
        print('\\textbf{Method} & \\textbf{Number} & \\textbf{Search Radius} & \\textbf{Mean} & '
              '\\textbf{Standard Deviation} \\\\')
        print('\\hline')
        count = np.count_nonzero(np.logical_and(~np.isnan(df.cont_mean.values), ~np.isnan(df.cent_value.values)))
        print('Context & {} & 5 $\\times$ 5 & {:.3f} & {:.3f}\\\\'.format(count, (df.cont_mean - df.cent_value).mean(),
                                                                          (df.cont_mean - df.cent_value).std()))
        print('\\hline')
        # get unweighted
        for sr_num, sr in enumerate(search_bounds):
            df = pd.read_pickle(r'C:\Scratch\cache\P4\prelim\{}-30d-1pd-sr{}.pkl'.format(k, sr))
            count = np.count_nonzero(np.logical_and(~np.isnan(df.pred_mean.values), ~np.isnan(df.cent_value.values)))
            if sr_num == 0:
                print('Spatio-Context & {} & {} & {:.3f} & {:.3f} \\\\'.format(count, sr,
                                                                               (df.pred_mean - df.cent_value).mean(),
                                                                               (df.pred_mean - df.cent_value).std()))
            elif sr_num == 1:
                print('(best 24 values) & & {} & {:.3f} & {:.3f} \\\\'.format(sr, (df.pred_mean - df.cent_value).mean(),
                                                                              (df.pred_mean - df.cent_value).std()))
            else:
                print(' & & {} & {:.3f} & {:.3f} \\\\'.format(sr, (df.pred_mean - df.cent_value).mean(),
                                                              (df.pred_mean - df.cent_value).std()))
        print('\\hline')
        # get weighted
        for sr_num, sr in enumerate(search_bounds):
            df = pd.read_pickle(r'C:\Scratch\cache\P4\prelim\{}-30d-1pd-sr{}.pkl'.format(k, sr))
            if sr_num == 0:
                print('Weighted S.C. & {} & {} & {:.3f} & {:.3f} \\\\'.format(count, sr,
                                                                              (df.pred_weighted - df.cent_value).mean(),
                                                                              (df.pred_weighted - df.cent_value).std()))
            elif sr_num == 1:
                print('(best 24 values & & {} & {:.3f} & {:.3f} \\\\'.format(sr, (df.pred_weighted - df.cent_value).mean(),
                                                                             (df.pred_weighted - df.cent_value).std()))
            elif sr_num == 2:
                print('by RMS) & & {} & {:.3f} & {:.3f} \\\\'.format(sr, (df.pred_weighted - df.cent_value).mean(),
                                                                     (df.pred_weighted - df.cent_value).std()))
            else:
                print(' & & {} & {:.3f} & {:.3f} \\\\'.format(sr, (df.pred_weighted - df.cent_value).mean(),
                                                              (df.pred_weighted - df.cent_value).std()))
        print('\\hline')


def ahi_convert_poly(y0, y1, x0, x1):
    pixs = 2000
    imgs = 5500000
    conv_coords = [x0*pixs-imgs, x1*pixs-imgs, imgs-y1*pixs, imgs-y0*pixs]
    locs = {'nw': [conv_coords[i] for i in [0, 3]], 'ne': [conv_coords[i] for i in [1, 3]],
            'se': [conv_coords[i] for i in [1, 2]], 'sw': [conv_coords[i] for i in [0, 2]]}
    p = Polygon([locs[key] for key in locs.keys()])
    return p


def plot_extents(k, bt):
    int_bounds = np.array([45, -45, 45, -45]) + np.array(cs_areas[k])
    ext_bounds = np.array([-50, 50, -50, 50]) + int_bounds
    f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=cartopy.crs.PlateCarree()))
    img_extent = [ext_bounds[2]*2000-5500000, ext_bounds[3]*2000-5500000, 5500000-ext_bounds[1]*2000,
                  5500000-ext_bounds[0]*2000]
    ax.imshow(bt, origin='upper', extent=img_extent, transform=ahi_crs)
    col_list = ['r', 'y']
    for i, extents in enumerate([int_bounds, cs_areas[k]]):
        p = ahi_convert_poly(*extents)
        x, y = p.exterior.xy
        ax.plot(x, y, c=col_list[i], transform=ahi_crs)
    ax.gridlines()


ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
if platform.node() == 'smgs-seo-sfmbox':
    savepath = '/media/sfm-user/Sunflower Bucket/P4'
elif platform.node() == 'sfmboxdos':
    savepath = '/mnt/storage/active-fire/P4'
else:
    savepath = r'C:/Scratch/cache/P4'
pt_clmask_loc = os.path.join(savepath, 'PTCH_CLD')
cont_lv = load_cont_lv()

cs_areas = {
    'sea': [4400, 4600, 3050, 3250],
    'nwa': [3600, 3800, 2000, 2200],
    'bor': [2600, 2800, 1400, 1600],
    'thl': [1800, 2000, 800, 1000],
    'chn': [1000, 1200, 1600, 1800],
    'jpn': [900, 1100, 2500, 2700],
    'sib': [200, 400, 2000, 2200]
}
cs_jdays = {'sea': [90, 120], 'nwa': [297, 327], 'bor': [45, 75], 'thl': [59, 89], 'chn': [240, 270],
            'jpn': [124, 154], 'sib': [131, 161]}
cs_times = {'sea': '03:50', 'nwa': '05:00', 'bor': '05:40', 'thl': '06:30', 'chn': '05:10',
            'jpn': '03:50', 'sib': '05:00'}

k = 'thl'





