import gaf
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import sys
import time
from multiprocessing import Pool
import platform
import pandas as pd
import cartopy
from shapely.geometry import Polygon, Point
import datetime
import pickle
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.cm as mpcm
from matplotlib import colors, gridspec
from mpl_toolkits.axes_grid1 import ImageGrid

cmrb = mpcm.RdBu_r.copy()
cmrb.set_bad(color='0.75')
cmvr = mpcm.viridis.copy()
cmvr.set_bad(color='0.75')
cmo = mpcm.Oranges.copy()
cmo.set_bad(color='0.75')


# Functions
def load_cont_lv():
    va = gaf.get_viewangle()
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = gaf.get_land_mask()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = gaf.AHICapture((2016, 6, 1, 0))
        file_kw = 'B07-w05-counts_valid'
        temp = Dataset(os.path.join(savepath, '{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        array = np.copy(flt_land)
        array[np.isnan(temp)] = False
        np.save(os.path.join(savepath, 'cont_lv.npy'), array)
    else:
        array = np.load(os.path.join(savepath, 'cont_lv.npy'))
    array = np.ma.array(array, mask=va > 80)
    return array


# constants
ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
if platform.node() == 'smgs-seo-sfmbox':
    savepath = '/media/sfm-user/Sunflower/P4'
    klist = ['thl_q', 'chn_m', 'nwa_p', 'sea_e', 'bor_g',
             'sib_r', 'thl_m', 'nwa_b', 'nwa_g', 'thl_c']
elif platform.node() == 'smgs-seo-sfmdos':
    savepath = '/mnt/Storage/active-fire/P4'
    klist = ['chn_g', 'thl_p', 'sea_h', 'sib_n', 'nwa_f',
             'thl_j', 'sea_b', 'thl_a', 'bor_a', 'chn_q']
elif platform.node() == 'smgs-seo-bryan':
    savepath = '/home/bryan/data/P4/'
elif platform.node() == 'bryan-metabox':
    savepath = '/run/media/bryan/Sunflower/P4'
else:
    savepath = r'C:/Scratch/cache/P4'

cont_lv = load_cont_lv()

# amended cs_areas for spatio-temporal work
cs_areas = {
    'sea': [4350, 4650, 3000, 3300],
    'nwa': [3550, 3850, 1950, 2250],
    'bor': [2550, 2850, 1350, 1650],
    'thl': [1750, 2050, 750, 1050],
    'chn': [950, 1250, 1550, 1850],
    'jpn': [850, 1150, 2450, 2750],
    'sib': [150, 450, 1950, 2250]
}
cs_area_toff = dict(
    [[k, gaf.AHILocation([np.mean(cs_areas[k][:2]),
                          np.mean(cs_areas[k][2:])]).time_offset / 60] for k in cs_areas.keys()])
# 2016 central julian day of weighted maximum fire activity cf. VIIRS
cs_jday = {'sea': 105, 'nwa': 312, 'bor': 60, 'thl': 74, 'chn': 255, 'jpn': 139, 'sib': 146}
cld_tl = {'sea': [4350, 3000], 'nwa': [3550, 1950], 'bor': [2550, 1350], 'thl': [1750, 750],
          'chn': [950, 1550], 'jpn': [850, 2450], 'sib': [150, 1950]}

with open(os.path.join(savepath, 'ncs_areas.pkl'), 'rb') as f:
    ncs_areas = pickle.load(f)
cs_areas.update(ncs_areas)

# cs_areas['sea_a'] = [4400, 4550, 3100, 3250]
# cs_areas['sea_b'] = [4350, 4500, 3100, 3250]
# cs_areas['chn_a'] = [1100, 1250, 1550, 1700]
# cs_areas['thl_a'] = [1800, 1950, 850, 1000]
cs_areas['nwa_s'] = [3600, 3750, 2025, 2175]

num_points = 24

# setup distance array
radius = 50
arraywidth = 2 * radius + 1
q = np.arange(arraywidth ** 2).reshape(arraywidth, -1)
dist = np.sqrt((q // arraywidth - radius) ** 2 + (q % arraywidth - radius) ** 2)

#k = 'sea_a'
use_cldmask = True
# pred_time = gaf.Time((2016, 105, 2, 0))

# freq = '1h'
num_times = 48

def plots_mk3(pkl):
    cont_pc = 0.65
    sts_pc = 0.25
    #anom_rate = 0.02

    df = pd.read_pickle(pkl)

    # produce plots
    tmin = np.max((np.floor(np.nanmin((np.nanpercentile(df.bt.values, 2),
                                       np.nanpercentile(df.sts_mean.values, 2),
                                       np.nanpercentile(df.cont_mean.values, 2))) / 5) * 5, 260))
    tmax = np.max((np.min((np.ceil(np.nanmax((np.nanpercentile(df.bt.values, 98),
                                              np.nanpercentile(df.sts_mean.values, 98),
                                              np.nanpercentile(df.cont_mean.values, 98))) / 5) * 5, 340)), 265))
    # set minimum candidates - 65%
    cont_mean = df.cont_mean.values
    cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
    sts_mean = df.sts_mean.values
    sts_mean[df.sts_valid.values < sts_pc * 24] = np.nan
    sts_filt_mean = df.sts_filt_mean.values
    sts_filt_mean[df.sts_valid.values < sts_pc * 24] = np.nan
    all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                   ~np.isnan(sts_filt_mean)))

    f, axs = plt.subplots(2, 4, figsize=(16.83, 7.35))
    im1 = f.axes[0].imshow(df.bt.values.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[0].set_title('BT (4$\mu$m) - AHI B07', fontsize=10)
    f.axes[5].imshow(cont_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[5].set_title('Context (5x5) estimate', fontsize=10)
    f.axes[1].imshow(sts_filt_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[1].set_title('STS estimate', fontsize=10)

    pred_rms_mean = np.nanmean(np.concatenate(df.pred_rms).reshape(2500, -1), 1)
    pred_rms_mean[~all_4] = np.nan
    im2 = f.axes[3].imshow(pred_rms_mean.reshape(50, -1), vmin=0, vmax=10, cmap=cmo)
    f.axes[3].set_title('RMSE of predictor pixels', fontsize=10)

    # sts_diff = df.sts_diff.values
    # sts_diff[~all_4] = np.nan
    cont_diff = df.cont_diff.values
    cont_diff[~all_4] = np.nan
    f.axes[6].imshow(cont_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[6].set_title('Context (5x5) diff', fontsize=10)

    sts_filt_diff = df.sts_filt_diff.values
    sts_filt_diff[~all_4] = np.nan
    im3 = f.axes[2].imshow(sts_filt_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[2].set_title('STS diff', fontsize=10)

    cont_rms = df.cont_rms
    count_invalid = (cont_rms // 416.666666).values.astype(int)
    cont_rms[count_invalid == 24] = np.nan
    cont_rms = cont_rms - 416.666666 * count_invalid
    cont_rms[~all_4] = np.nan

    f.axes[7].imshow(cont_rms.values.reshape(50, -1), vmin=0, vmax=10, cmap=cmo)
    f.axes[7].set_title('RMSE of 5x5 surround pixels', fontsize=10)

    for ax in f.axes:
        try:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        except AttributeError:
            pass

    for i in [0, 1, 5]:
        divider = make_axes_locatable(f.axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im1, cax=cax)
        cb.set_label('Brightness Temp (K)')
        try:
            cb.set_ticks(np.arange(tmin, tmax + 5, 5))
        except ValueError:
            pass

    for i in [2, 6]:
        divider = make_axes_locatable(f.axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im3, cax=cax, extend='both')
        cb.set_label('$\Delta$T (K)')

    for i in [3, 7]:
        divider = make_axes_locatable(f.axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im2, cax=cax, extend='max')
        cb.set_label('RMSE (K)')

    f.axes[4].remove()
    f.set_tight_layout(True)

    pred_time = df.time.iloc[0]
    freq = df.freq.iloc[0]
    k = pkl.split('/')[-1][:5]
    num_times = 48

    # outlier rate == 2.0\%
    outlier_pc = 98
    sts_mean_sans_out = np.nanmean(sts_filt_diff[np.abs(sts_filt_diff) < np.nanpercentile(np.abs(sts_filt_diff),
                                                                                          outlier_pc)])
    sts_std_sans_out = np.nanstd(sts_filt_diff[np.abs(sts_filt_diff) < np.nanpercentile(np.abs(sts_filt_diff),
                                                                                        outlier_pc)])
    cont_mean_sans_out = np.nanmean(cont_diff[np.abs(cont_diff) < np.nanpercentile(np.abs(cont_diff), outlier_pc)])
    cont_std_sans_out = np.nanstd(cont_diff[np.abs(cont_diff) < np.nanpercentile(np.abs(cont_diff), outlier_pc)])

    f.text(0.02, 0.45,
           'CS Area:   {}\n'
           'AHI loc:   {}\n'
           'Pred time: {}j{:03d} {:02d}:{:02d} UTC\n'
           'Specs:     {} images x {} freq\n\n'
           'STS filt   mean:  {:>6.3f}  stdev: {:.3f}\n'
           '<98th pc   mean:  {:>6.3f}  stdev: {:.3f}\n'
           'STS 98th pc temp: {:>6.3f}\n\n'
           'Cont 5x5   mean:  {:>6.3f}  stdev: {:.3f}\n'
           '<98th pc   mean:  {:>6.3f}  stdev: {:.3f}\n'
           'Cont 98th pc temp:{:>6.3f}\n\n'
           'n:                {:>5d}'.format(k, np.array(cs_areas[k]) + np.array([50, -50, 50, -50]),
                                             pred_time.year, pred_time.jday, pred_time.hour, pred_time.minute,
                                             num_times, freq,
                                             np.nanmean(sts_filt_diff), np.nanstd(sts_filt_diff),
                                             sts_mean_sans_out, sts_std_sans_out,
                                             np.nanpercentile(np.abs(sts_filt_diff), outlier_pc),
                                             np.nanmean(cont_diff), np.nanstd(cont_diff),
                                             cont_mean_sans_out, cont_std_sans_out,
                                             np.nanpercentile(np.abs(cont_diff), outlier_pc),
                                             np.count_nonzero(all_4)),
           family='monospace', va='top')

    fsavepath = pkl.split('.')[0] + '.png'
    f.savefig(fsavepath, dpi=600, format='png')
    # return f, axs
    plt.close(f)


def plots_mk4(pkl):
    fsavepath = os.path.join('/'.join(pkl.split('/')[:-1]), 'figs', pkl.split('/')[-1].split('.')[0] + '.png')
    if os.path.isfile(fsavepath):
        return 0

    cont_pc = 0.65
    sts_pc = 0.25
    # anom_rate = 0.02

    df = pd.read_pickle(pkl)

    # produce plots
    tmin = np.max((np.floor(np.nanmin((np.nanpercentile(df.bt.values, 2),
                                       np.nanpercentile(df.sts_mean.values, 2),
                                       np.nanpercentile(df.cont_mean.values, 2))) / 5) * 5, 260))
    tmax = np.max((np.min((np.ceil(np.nanmax((np.nanpercentile(df.bt.values, 98),
                                              np.nanpercentile(df.sts_mean.values, 98),
                                              np.nanpercentile(df.cont_mean.values, 98))) / 5) * 5, 340)), 265))
    # set minimum candidates - 65%
    cont_mean = df.cont_mean.values
    cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
    try:
        sts_valid_vals = df.sts_valid.values
    except:
        sts_valid_vals = df.pred_valid.values
    sts_mean = df.sts_mean.values
    sts_mean[sts_valid_vals < sts_pc * 24] = np.nan
    sts_filt_mean = df.sts_filt_mean.values
    sts_filt_mean[sts_valid_vals < sts_pc * 24] = np.nan
    all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                   ~np.isnan(sts_filt_mean)))
    f, axs = plt.subplots(1, 6, figsize=(18.59, 2.78))
    im1 = f.axes[0].imshow(df.bt.values.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[0].set_title('BT (4$\mu$m) - AHI B07', fontsize=10)
    f.axes[1].imshow(sts_filt_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[1].set_title('STS estimate', fontsize=10)
    f.axes[2].imshow(cont_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[2].set_title('Context (5x5) estimate', fontsize=10)

    sts_filt_diff = df.sts_filt_diff.values
    sts_filt_diff[~all_4] = np.nan
    im2 = f.axes[3].imshow(sts_filt_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[3].set_title('STS diff', fontsize=10)

    cont_diff = df.cont_diff.values
    cont_diff[~all_4] = np.nan
    f.axes[4].imshow(cont_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[4].set_title('Context (5x5) diff', fontsize=10)

    for ax in f.axes:
        try:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        except AttributeError:
            pass

    divider = make_axes_locatable(f.axes[2])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(im1, cax=cax)
    cb.set_label('Brightness Temp (K)')
    try:
        cb.set_ticks(np.arange(tmin, tmax + 5, 5))
    except ValueError:
        pass

    divider = make_axes_locatable(f.axes[4])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(im2, cax=cax, extend='both')
    cb.set_label('$\Delta$T (K)')

    f.axes[5].remove()
    f.set_tight_layout(True)

    pred_time = df.time.iloc[0]
    freq = df.freq.iloc[0]
    k = pkl.split('/')[-1][:5]
    num_times = 48

    # outlier rate == 2.0\%
    outlier_pc = 98
    sts_mean_sans_out = np.nanmean(sts_filt_diff[np.abs(sts_filt_diff) < np.nanpercentile(np.abs(sts_filt_diff),
                                                                                          outlier_pc)])
    sts_std_sans_out = np.nanstd(sts_filt_diff[np.abs(sts_filt_diff) < np.nanpercentile(np.abs(sts_filt_diff),
                                                                                        outlier_pc)])
    cont_mean_sans_out = np.nanmean(cont_diff[np.abs(cont_diff) < np.nanpercentile(np.abs(cont_diff), outlier_pc)])
    cont_std_sans_out = np.nanstd(cont_diff[np.abs(cont_diff) < np.nanpercentile(np.abs(cont_diff), outlier_pc)])

    f.text(0.855, 0.92,
           'CS Area:   {}\n'
           'AHI loc:   {}\n'
           'Pred time: {}j{:03d} {:02d}:{:02d} UTC\n'
           'Specs:     {} images x {} freq\n\n'
           'STS filt   mean:  {:>6.3f}  stdev: {:.3f}\n'
           '<98th pc   mean:  {:>6.3f}  stdev: {:.3f}\n'
           'STS 98th pc temp: {:>6.3f}\n\n'
           'Cont 5x5   mean:  {:>6.3f}  stdev: {:.3f}\n'
           '<98th pc   mean:  {:>6.3f}  stdev: {:.3f}\n'
           'Cont 98th pc temp:{:>6.3f}\n\n'
           'n:                {:>5d}'.format(k, np.array(cs_areas[k]) + np.array([50, -50, 50, -50]),
                                             pred_time.year, pred_time.jday, pred_time.hour, pred_time.minute,
                                             num_times, freq,
                                             np.nanmean(sts_filt_diff), np.nanstd(sts_filt_diff),
                                             sts_mean_sans_out, sts_std_sans_out,
                                             np.nanpercentile(np.abs(sts_filt_diff), outlier_pc),
                                             np.nanmean(cont_diff), np.nanstd(cont_diff),
                                             cont_mean_sans_out, cont_std_sans_out,
                                             np.nanpercentile(np.abs(cont_diff), outlier_pc),
                                             np.count_nonzero(all_4)),
           family='monospace', va='top', fontsize=8)

    fsavepath = os.path.join('/'.join(pkl.split('/')[:-1]), 'figs', pkl.split('/')[-1].split('.')[0] + '.png')
    f.savefig(fsavepath, dpi=600, format='png')
    plt.close(f)


def plots_mk5(pkl):
    # fsavepath = os.path.join('/'.join(pkl.split('/')[:-1]), 'figs', pkl.split('/')[-1].split('.')[0] + '.png')
    # if os.path.isfile(fsavepath):
    #    return 0

    cont_pc = 0.65
    sts_pc = 0.25
    # anom_rate = 0.02
    fs = 14

    df = pd.read_pickle(pkl)

    # produce plots
    tmin = np.max((np.floor(np.nanmin((np.nanpercentile(df.bt.values, 2),
                                       np.nanpercentile(df.sts_mean.values, 2),
                                       np.nanpercentile(df.cont_mean.values, 2))) / 5) * 5, 260))
    tmax = np.max((np.min((np.ceil(np.nanmax((np.nanpercentile(df.bt.values, 98),
                                              np.nanpercentile(df.sts_mean.values, 98),
                                              np.nanpercentile(df.cont_mean.values, 98))) / 5) * 5, 340)), 265))
    # set minimum candidates - 65%
    cont_mean = df.cont_mean.values
    cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
    try:
        sts_valid_vals = df.sts_valid.values
    except:
        sts_valid_vals = df.pred_valid.values
    sts_mean = df.sts_mean.values
    sts_mean[sts_valid_vals < sts_pc * 24] = np.nan
    sts_filt_mean = df.sts_filt_mean.values
    sts_filt_mean[sts_valid_vals < sts_pc * 24] = np.nan
    all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                   ~np.isnan(sts_filt_mean)))

    sts_filt_diff = df.sts_filt_diff.values
    sts_filt_diff[~all_4] = np.nan
    cont_diff = df.cont_diff.values
    cont_diff[~all_4] = np.nan

    f = plt.figure(figsize=(15, 3.2))
    grid = ImageGrid(f, (0.02, 0.05, 0.51, 0.92), (1, 3), axes_pad=0.1, cbar_mode='single', cbar_location='right',
                     label_mode='L')

    grid[0].imshow(df.bt.values.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    grid[0].set_title('BT (4$\mu$m) - AHI B07', fontsize=fs)

    grid[1].imshow(sts_filt_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    grid[1].set_title('STS estimate', fontsize=fs)

    im = grid[2].imshow(cont_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    grid[2].set_title('Context (5x5) estimate', fontsize=fs)

    for val in np.arange(3):
        grid[val].set(xticks=[], yticks=[])

    cb = grid.cbar_axes[0].colorbar(im)
    cb.ax.yaxis.set_ticks(np.arange(tmin, tmax + 5, 5))
    cb.ax.yaxis.set_ticklabels(np.arange(tmin, tmax + 5, 5).astype(int), fontsize=fs)
    cb.ax.set_ylabel('Brightness Temp (K)', fontsize=fs)

    grid2 = ImageGrid(f, (0.6, 0.05, 0.35, 0.92), (1, 2), axes_pad=0.1, cbar_mode='single', cbar_location='right',
                      label_mode='L')

    im2 = grid2[0].imshow(sts_filt_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    grid2[0].set_title('STS diff', fontsize=fs)
    grid2[1].imshow(cont_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    grid2[1].set_title('Context (5x5) diff', fontsize=fs)

    for val in np.arange(2):
        grid2[val].set(xticks=[], yticks=[])

    cb = grid2.cbar_axes[0].colorbar(im2, extend='both')
    cb.set_ticks([-4, -2, 0, 2, 4])
    cb.ax.set_yticklabels(['$-$4', '$-$2', '0', '2', '4'], fontsize=fs)
    cb.set_label('$\Delta$T (K)', fontsize=fs)
    cb.ax.spines['top'].set_color(cmrb(255))
    cb.ax.spines['bottom'].set_color(cmrb(0))

    fsavepath = os.path.join('/run/media/bryan/Sunflower/P4/paper_figures', pkl.split('/')[-1].split('.')[0] + '.png')
    f.savefig(fsavepath, dpi=300, format='png')
    plt.close(f)


if __name__ == '__main__':
    flist = gaf.listdir_end('/home/sfm-user/data/AHI/P4/take_two/2h', '.df')
    p = Pool(6)
    p.map(plots_mk4, (i for i in flist))
