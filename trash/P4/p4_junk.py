import gaf
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import time
from multiprocessing import Pool
import platform
import pandas as pd
import cartopy
from shapely.geometry import Polygon, Point
import datetime
import shutil


# Functions
def load_cont_lv():
    va = gaf.get_viewangle()
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = gaf.get_land_mask()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = gaf.AHICapture((2016, 6, 1, 0))
        file_kw = 'B07-w05-counts_valid'
        temp = Dataset(os.path.join(savepath, '{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        array = np.copy(flt_land)
        array[np.isnan(temp)] = False
        np.save(os.path.join(savepath, 'cont_lv.npy'), array)
    else:
        array = np.load(os.path.join(savepath, 'cont_lv.npy'))
    array = np.ma.array(array, mask=va > 80)
    return array


def predict_old(k, pred_time, num_points=24, num_times=30, freq='3h', radius=50, use_cldmask=True):
    a = gaf.AHICapture(pred_time)
    save_fname = os.path.join(savepath, 'process', '{}-{}{:03d}{:02d}{:02d}-n{}-t{}-fq{}-r{}-st_res.pkl'.format(
        k, a.time.year, a.time.jday, a.time.hour, a.time.minute, num_points, num_times, freq, radius
    ))
    bt_use_range = np.array([radius, -radius, radius, -radius]) + cs_areas[k]
    area_inds = np.ravel(np.stack(np.meshgrid(np.arange(bt_use_range[1] - bt_use_range[0]) + radius,
                                              np.arange(bt_use_range[3] - bt_use_range[2]) + radius),
                                  axis=2)).reshape(-1, 2)
    if not isinstance(pred_time, gaf.Time):
        pred_time = gaf.Time(pred_time)
    # parse freq info
    if freq[-3:] == 'min':
        minute_gap = int(freq[:-3])
    elif freq[-1:] == 'h':
        minute_gap = int(freq[:-1]) * 60
    elif freq[-1:] == 'd':
        minute_gap = int(freq[:-1]) * 1440
    else:
        raise ValueError('Invalid time frequency selected')

    t_list = [pred_time.dt - datetime.timedelta(minutes=int(minute_gap * i)) for i in np.arange(num_times + 1)]
    t_list.sort()
    bt_array = np.empty((num_times + 1, cs_areas[k][1] - cs_areas[k][0], cs_areas[k][3] - cs_areas[k][2]))
    bt_array[:] = np.nan
    sub_land = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]

    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        try:
            bt = a.band7.data[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
        except AttributeError:
            print('No file available for {}'.format(a.time.dt))
            continue
        bt[~sub_land] = np.nan
        if use_cldmask:
            try:
                cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                        '{:03d}'.format(a.time.jday), k)
                cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                               a.time.minute)
                r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
            except IndexError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            except FileNotFoundError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            t_cld = r.variables['cloud_mask'][0, :-1, :-1]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt.astype(np.float32)

    bt_pad = np.empty((bt_array.shape[0], bt_array.shape[1] + 1, bt_array.shape[2] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:, :-1, :-1] = bt_array.astype(np.float32)

    st_df = pd.DataFrame(columns=['pos_y', 'pos_x', 'cent_value', 'pred_mean', 'pred_weighted'])
    st_df['pos_y'] = area_inds[:, 1] + cs_areas[k][0]
    st_df['pos_x'] = area_inds[:, 0] + cs_areas[k][2]

    for i, pos in enumerate(area_inds):
        cent_vec = bt_array[:-1, pos[0], pos[1]]
        area_set = bt_array[:-1, pos[0] - radius:pos[0] + radius + 1, pos[1] - radius:pos[1] + radius + 1]
        diffs = area_set - cent_vec[:, np.newaxis, np.newaxis]
        rms = np.sqrt(np.nanmean(diffs ** 2, axis=0))
        ind = np.unravel_index(np.argsort(rms, axis=None), rms.shape)
        choices = np.array([ind[0][1:num_points + 1], ind[1][1:num_points + 1]])
        prediction_vec = bt_array[-1, choices[0] + pos[0] - radius, choices[1] + pos[1] - radius]
        st_df.loc[i, 'cent_value'] = bt_array[-1, pos[0], pos[1]]
        st_df.loc[i, 'pred_mean'] = np.nanmean(prediction_vec)
        weights = 1 / rms[choices[0], choices[1]]
        st_df.loc[i, 'pred_weighted'] = np.nansum(prediction_vec * weights) / np.sum(weights[~np.isnan(prediction_vec)])

    st_df['cs_diff'] = st_df.pred_mean - st_df.cent_value
    st_df['csw_diff'] = st_df.pred_weighted - st_df.cent_value
    print('cs_diff_std: {:.4f}'.format(st_df.cs_diff.std()))
    print('csw_diff_std: {:.4f}'.format(st_df.csw_diff.std()))

    if True:
        # a = gaf.AHICapture(t_list[-1])
        st_df.to_pickle(save_fname)
        print('{} saved'.format(os.path.basename(save_fname).split('.')[0]))
    return 0


# two step process - 1. determine prediction locations
def find_prediction_points(k, pred_time, num_points=24, num_times=30, freq='3h', radius=50, use_cldmask=True):
    a = gaf.AHICapture(pred_time)
    save_fname = os.path.join(savepath, 'process', '{}-{}{:03d}{:02d}{:02d}-n{}-t{}-fq{}-r{}-tt_res.pkl'.format(
        k, a.time.year, a.time.jday, a.time.hour, a.time.minute, num_points, num_times, freq, radius
    ))
    # if os.path.isfile(save_fname):
    #     return -1

    bt_use_range = np.array([radius, -radius, radius, -radius]) + cs_areas[k]
    area_inds = np.ravel(np.stack(np.meshgrid(np.arange(bt_use_range[1] - bt_use_range[0]) + radius,
                                              np.arange(bt_use_range[3] - bt_use_range[2]) + radius),
                                  axis=2)).reshape(-1, 2)
    if not isinstance(pred_time, gaf.Time):
        pred_time = gaf.Time(pred_time)
    # parse freq info
    if freq[-3:] == 'min':
        minute_gap = int(freq[:-3])
    elif freq[-1:] == 'h':
        minute_gap = int(freq[:-1]) * 60
    elif freq[-1:] == 'd':
        minute_gap = int(freq[:-1]) * 1440
    else:
        raise ValueError('Invalid time frequency selected')
    t_list = [pred_time.dt - datetime.timedelta(minutes=int(minute_gap * i)) for i in np.arange(num_times + 1)]
    t_list.sort()
    bt_array = np.empty((num_times + 1, cs_areas[k][1] - cs_areas[k][0], cs_areas[k][3] - cs_areas[k][2]))
    bt_array[:] = np.nan
    sub_land = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]

    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        try:
            bt = a.band7.data[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
        except AttributeError:
            print('No file available for {}'.format(a.time.dt))
            continue
        bt[~sub_land] = np.nan
        if use_cldmask:
            try:
                cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year), 
                                        '{:03d}'.format(a.time.jday), k[:3])
                cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                               a.time.minute)
                r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
            except IndexError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            except FileNotFoundError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            t_cld = r.variables['cloud_mask'][0, :cs_areas[k][1] - cs_areas[k][0], :cs_areas[k][3] - cs_areas[k][2]]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt.astype(np.float32)
    # return bt_array


    # def matrix_shuffle(bt_array, radius, num_points):
    # create a stacked array of temperature vectors for selection
    # axis0: radius**2 + 2*radius
    # axis1: num_times
    # axis2, 3: grid size (to be determined based on fall over rate)
    bt_pad = np.empty((bt_array.shape[0], bt_array.shape[1] + 1, bt_array.shape[2] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:, :-1, :-1] = bt_array.astype(np.float32)
    
    mdf = []
    # subset into 200/n x 200/n arrays and then process
    sub_arrays = 4
    for n in np.arange(sub_arrays**2):
        array_size = int(200/sub_arrays)
        x_off = n % sub_arrays
        y_off = n // sub_arrays
        y_vals = np.arange(array_size) + y_off * array_size
        x_vals = np.arange(array_size) + x_off * array_size
        coords = np.meshgrid(x_vals, y_vals)

        val_array = np.array([bt_pad[:, wy:wy + (radius * 2 + 1), wx:wx + (radius * 2 + 1)]
                              for wy in y_vals for wx in x_vals])

        va = val_array.swapaxes(0, 1).reshape(bt_array.shape[0], val_array.shape[0], -1)
        cent_vals = va[:-1, :, va.shape[2] // 2]
        diffs = va[:-1] - cent_vals[:, :, np.newaxis]
        rms = np.nanmean(diffs ** 2, axis=0)
        ai = np.argsort(rms, 1)
        weights = 1 / np.take_along_axis(rms, ai[:, 1:num_points + 1], 1)
        pred_vec = np.take_along_axis(va[-1], ai[:, 1:num_points + 1], 1)

        df = pd.DataFrame(columns=['x', 'y', 'bt', 'mean', 'wmean'])
        df['x'] = np.ravel(coords[0]) + cs_areas[k][2] + array_size
        df['y'] = np.ravel(coords[1]) + cs_areas[k][0] + array_size
        df['bt'] = np.ravel(bt_pad[-1, coords[1] + array_size, coords[0] + array_size])
        df.bt[df.bt < 250] = np.nan
        df['mean'] = np.nanmean(pred_vec, 1)
        df['wmean'] = (np.nansum(pred_vec * weights, axis=1) /
                       np.array([np.sum(weights[i][~np.isnan(pred_vec[i])])
                                 for i in np.arange(weights.shape[0])]))
        if not n:
            mdf = df
        else:
            mdf = mdf.append(df, ignore_index=True)
        print('Loop {} complete'.format(n))
    mdf['dmean'] = mdf['mean'] - mdf.bt
    mdf['dwmean'] = mdf['wmean'] - mdf.bt

    if True:
        # a = gaf.AHICapture(t_list[-1])
        mdf.to_pickle(save_fname)
        print('{} saved'.format(os.path.basename(save_fname).split('.')[0]))
    return save_fname


def get_context(k, pred_time, radius, use_cldmask=True):
    bt_use_range = np.array([radius, -radius, radius, -radius]) + cs_areas[k]
    area_inds = np.ravel(np.stack(np.meshgrid(np.arange(bt_use_range[1] - bt_use_range[0]) + radius,
                                              np.arange(bt_use_range[3] - bt_use_range[2]) + radius),
                                  axis=2)).reshape(-1, 2)

    if not isinstance(pred_time, gaf.Time):
        pred_time = gaf.Time(pred_time)
    a = gaf.AHICapture(pred_time)
    try:
        bt = a.band7.data[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
    except AttributeError:
        print('No file available for {}'.format(a.time.dt))
        return 1
    sub_land = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]

    bt[~sub_land] = np.nan
    if use_cldmask:
        try:
            cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                    '{:03d}'.format(a.time.jday), k)
            cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                           a.time.minute)
            r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
        except IndexError:
            print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
            return 1
        except FileNotFoundError:
            print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
            return 1
        t_cld = r.variables['cloud_mask'][0, :-1, :-1]
        bt[t_cld == 1] = np.nan

    cont_df = pd.DataFrame()
    cont_df['y'] = area_inds[:, 1] + cs_areas[k][0]
    cont_df['x'] = area_inds[:, 0] + cs_areas[k][2]
    cont_df['bt'] = bt[area_inds[:, 1], area_inds[:, 0]]
    val_array = np.array([bt[wy + 48:wy - 52, wx + 48:wx - 52] for wy in np.arange(5) for wx in np.arange(5)])
    val_array = np.delete(val_array, 12, 0)
    cont_df['mean'] = np.nanmean(val_array, 0)[area_inds[:, 1] - 50, area_inds[:, 0] - 50]
    cont_df['counts'] = np.count_nonzero(~np.isnan(val_array), axis=0)[area_inds[:, 1] - 50, area_inds[:, 0] - 50]
    cont_df['diff'] = cont_df['mean'] - cont_df['bt']
    cont_df.to_pickle(os.path.join(savepath, 'process', '{}-{}{:03d}{:02d}{:02d}-cont_res.pkl'.format(
        k, a.time.year, a.time.jday, a.time.hour, a.time.minute)))


def predict_temp(locs, method='mean'):
    pass


def show_cloud_with_b7(k, im_time):
    a = gaf.AHICapture(im_time)
    f, ax = plt.subplots(1, 3, figsize=(13, 5))
    try:
        ax[0].imshow(a.band7.data)
        ax[0].set_ylim(cs_areas[k][1], cs_areas[k][0])
        ax[0].set_xlim(cs_areas[k][2], cs_areas[k][3])
        ax[1].imshow(a.band13.data)
        ax[1].set_ylim(cs_areas[k][1], cs_areas[k][0])
        ax[1].set_xlim(cs_areas[k][2], cs_areas[k][3])
        d_offset = 0
        cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', str(a.time.year), str(a.time.jday + d_offset), k)
        cld_file = os.path.join(cld_path, [l for l in os.listdir(cld_path) if
                                           '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month,
                                                                               a.time.day + d_offset,
                                                                               a.time.hour, a.time.minute) in l][0])
        r = Dataset(cld_file)
        lm = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
        ax[2].imshow(np.ma.array(r.variables['cloud_mask'][0, :-1, :-1], mask=~lm))
        figtitle = '{}_{}{:02d}{:02d}{:02d}{:02d}'.format(k, a.time.year, a.time.month, a.time.day, a.time.hour,
                                                          a.time.minute)
        f.suptitle(figtitle)
        f.set_tight_layout(True)
        f.savefig(os.path.join(savepath, 'temp', '{}.png'.format(figtitle)))
    except AttributeError:
        pass
    plt.close(f)


# constants
ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
if platform.node() == 'smgs-seo-sfmbox':
    savepath = '/media/sfm-user/Sunflower/P4'
elif platform.node() == 'sfmboxdos':
    savepath = '/mnt/storage/active-fire/P4'
elif platform.node() == 'smgs-seo-bryan':
    savepath = '/home/bryan/data/P4/'
else:
    savepath = r'C:/Scratch/cache/P4'

cont_lv = load_cont_lv()

# amended cs_areas for spatio-temporal work
cs_areas = {
    'sea': [4350, 4650, 3000, 3300],
    'nwa': [3550, 3850, 1950, 2250],
    'bor': [2550, 2850, 1350, 1650],
    'thl': [1750, 2050, 750, 1050],
    'chn': [950, 1250, 1550, 1850],
    'jpn': [850, 1150, 2450, 2750],
    'sib': [150, 450, 1950, 2250]
}
cs_area_toff = dict(
    [(k, gaf.AHILocation([np.mean(cs_areas[k][:2]), np.mean(cs_areas[k][2:])]).time_offset/60)
     for k in cs_areas.keys()])
# 2016 central julian day of weighted maximum fire activity cf. VIIRS
cs_jday = {'sea': 105, 'nwa': 312, 'bor': 60, 'thl': 74, 'chn': 255, 'jpn': 139, 'sib': 146}


def run_code():
    opt_list = []
    for k in ['nwa', 'chn']:
        #if k == 'nwa':
        #    continue
        #elif k == 'chn':
        #    continue
        #else:
        l = [[k, (2016, d, h, 10)] for d in np.arange(7) + cs_jday[k] - 3 for h in np.arange(12) * 2]
        opt_list.append(l)
    opt_list = gaf.flatten_list(opt_list)
    for option in opt_list:
        print(option)
        get_context(option[0], option[1], 50)
        find_prediction_points(option[0], option[1], num_times=72, freq='20min', num_points=24, radius=50)


def rehouse_cloud():
    write_path = '/media/sfm-user/Sunflower/AHI/CLD/2016'
    read_path = '/home/sfm-user/data/AHI/CLDtemp'
    for month in [k for k in os.listdir(read_path) if 'zip' not in k]:
        k_list = os.listdir(os.path.join(read_path, month))
        for k in k_list:
            for dirpath, dirnames, files in os.walk(os.path.join(read_path, month, k)):
                for name in files:
                    mnth = int(name[4:6])
                    day = int(name[6:8])
                    t = gaf.Time((2016, mnth, day, 0, 0))
                    if not os.path.isdir(os.path.join(write_path, '{:03d}'.format(t.jday), k)):
                        os.makedirs(os.path.join(write_path, '{:03d}'.format(t.jday), k))
                    shutil.move(os.path.join(dirpath, name), os.path.join(write_path, '{:03d}'.format(t.jday), k, name))


if __name__ == "__main__":
    #time.sleep(5400)
    run_code()
'''
    num_points = [24, 50, 100]
    freq = ['10min', '30min', '1h', '3h', '6h']
    num_times = [6, 12, 30]
    # radius = [25, 50]
    days = np.arange(5) + 220
    hours = [1, 5, 9, 13, 17, 21]

    params = [[a, b, c, d, e] for a in days for b in hours for c in num_points for d in num_times for e in freq]
    
    #p = Pool(6)
    #p.map(run_code, (i for i in params))
'''

def df_per_cs_area():
    process_path = os.path.join(savepath, 'process')
    for k in cs_areas.keys():
        flist = gaf.listdir_start(process_path, k)
        tlist = list(set([f.split('-')[1] for f in flist]))
        tlist.sort()
        df = pd.DataFrame(columns=['tstr', 'cmean', 'cstd', 'smean', 'sstd', 'count<16',
                                   'cmean<16', 'cstd<16', 'smean<16', 'sstd<16'])
        for i, t in enumerate(tlist):
            flist = gaf.listdir_start(process_path, '{}-{}'.format(k, t))
            try:
                cdf = pd.read_pickle([f for f in flist if 'cont' in f][0])
                sdf = pd.read_pickle([f for f in flist if 'tt' in f][0])
            except:
                continue
            df.loc[i] = [t, cdf['diff'].mean(), cdf['diff'].std(), sdf['dmean'].mean(), sdf['dmean'].std(),
                         np.count_nonzero(cdf['counts'] < 16), cdf['diff'][cdf['counts'] < 16].mean(),
                         cdf['diff'][cdf['counts'] < 16].std(), sdf['dmean'][cdf['counts'] < 16].mean(),
                         sdf['dmean'][cdf['counts'] < 16].std()]
        df.to_pickle(os.path.join(process_path, '{}-summary.pkl'.format(k)))

    for k in cs_areas.keys():
        df = pd.read_pickle(os.path.join(process_path, '{}-summary.pkl'.format(k)))
        df['hour'] = [int(i[7:9]) for i in df['tstr']]
        gdf = df.groupby('hour').mean()
        f, axes = plt.subplots(1, 2, figsize=(12,6))
        plt_type = ['mean', 'std']
        for i, ax in enumerate(axes):
            cols = [c for c in gdf.columns if plt_type[i] in c]
            [ax.plot(gdf.index, gdf[c], label='{}'.format(c)) for c in cols]
            ax.legend()
            ax.set_xlabel('Hour (UTC)')
            ax.set_ylabel('Temperature (K)')
            ax.set_title('{} by hour - {}'.format(plt_type[i], k))
            if i == 1:
                ax.set_ylim(0, ax.get_ylim()[1])
        f.savefig(os.path.join(process_path, '{}-hourly.png'.format(k)), format='png', dpi=600)
