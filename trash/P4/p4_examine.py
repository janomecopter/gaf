import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import gaf
import os
import platform
from netCDF4 import Dataset
import datetime


if platform.node() == 'smgs-seo-bryan':
    savepath = '/home/bryan/data/P4'
else:
    raise ImportError('fix data folder before continuing')


# Functions
def load_cont_lv():
    va = gaf.get_viewangle()
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = gaf.get_land_mask()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = gaf.AHICapture((2016, 6, 1, 0))
        file_kw = 'B07-w05-counts_valid'
        temp = Dataset(os.path.join(savepath, '{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        array = np.copy(flt_land)
        array[np.isnan(temp)] = False
        np.save(os.path.join(savepath, 'cont_lv.npy'), array)
    else:
        array = np.load(os.path.join(savepath, 'cont_lv.npy'))
    array = np.ma.array(array, mask=va > 80)
    return array


cont_lv = load_cont_lv()

df = pd.DataFrame(columns=['k', 'day', 'time', 'std_context', 'std_spatio-temp'])

flist = gaf.listdir_end(savepath, 'cont_res.pkl')

for i, f in enumerate(flist):
    fex = os.path.basename(f)
    fst = gaf.listdir_start(savepath, fex[:16]+'n')[0]
    df.loc[i, 'k'] = fex[:3]
    df.loc[i, 'day'] = int(fex[8:11])
    df.loc[i, 'time'] = int(fex[11:15])
    df.loc[i, 'std_context'] = np.nanstd(pd.read_pickle(f)['diff'].values)
    df.loc[i, 'std_spatio-temp'] = np.nanstd(pd.read_pickle(fst)['dmean'].values)


for k in df['k'].unique():
    a = df.groupby('k').get_group(k).sort_values(['day', 'time']).reset_index()
    f, ax = plt.subplots()
    ax.plot(a['std_context'], label='context')
    ax.plot(a['std_spatio-temp'], label='spatio-temp')
    ax.set_title(k)
    ax.legend()

for i, f in enumerate(flist):
    fex = os.path.basename(f)
    fst = gaf.listdir_start(savepath, fex[:16] + 'n')[0]
    s = pd.read_pickle(f)
    d = pd.read_pickle(fst)
    q = s[['x', 'y', 'counts', 'diff']].merge(d[['x', 'y', 'dmean']], 'outer', on=['x', 'y'])
    q = q.rename(columns={'diff': 'cont_diff', 'dmean': 'st_diff'})

    fig, ax = plt.subplots(1, 3, figsize=(14.5, 4), sharex=True, sharey=True)
    for i, item in enumerate(['bt', 'cont_diff', 'st_diff']):
        if not i:
            im = ax[i].imshow(q[item].values.reshape(200, -1))
        else:
            im = ax[i].imshow(q[item].values.reshape(200, -1), vmin=-12, vmax=12)
        plt.colorbar(im, ax=ax[i])
        ax[i].set_title(item)
    fig.set_tight_layout(True)

# amended cs_areas for spatio-temporal work
cs_areas = {
    'sea': [4350, 4650, 3000, 3300],
    'nwa': [3550, 3850, 1950, 2250],
    'bor': [2550, 2850, 1350, 1650],
    'thl': [1750, 2050, 750, 1050],
    'chn': [950, 1250, 1550, 1850],
    'jpn': [850, 1150, 2450, 2750],
    'sib': [150, 450, 1950, 2250]
}


def single_pixel_examine(k=None, t=None, location=None):
    if k is None:
        k = 'sea'
    if t is None:
        t = (2016, 229, 0, 10)
    bt_stack = np.empty((73, 200, 200))
    bt_stack[:] = np.nan
    for i in range(73):
        tobj = gaf.Time(gaf.Time(t).dt + datetime.timedelta(minutes=20 * i))
        a = gaf.AHICapture(tobj).band7.data[cs_areas[k][0] + 50:cs_areas[k][1] - 50,
                                            cs_areas[k][2] + 50:cs_areas[k][3] - 50]
        sub_land = cont_lv[cs_areas[k][0] + 50:cs_areas[k][1] - 50, cs_areas[k][2] + 50:cs_areas[k][3] - 50]
        a[~sub_land] = np.nan
        cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(tobj.year), '{:03d}'.format(tobj.jday), k)
        cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(tobj.year, tobj.month, tobj.day, tobj.hour, tobj.minute)
        try:
            cld = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0])).variables[
                      'cloud_mask'][0, :-1, :-1]
            a[cld[50:-50, 50:-50] == 1] = np.nan
            bt_stack[i] = a
        except:
            pass
    if location is None:
        location = [61, 137]
    n_bt_stack = bt_stack[:] - bt_stack[:, location[0], location[1]][:, np.newaxis, np.newaxis]
    cut_stack = n_bt_stack[:, location[0] - 50:location[0] + 51, location[1] - 50:location[1] + 51]
    rms = np.nanmean(cut_stack[:-1] ** 2, axis=0)

    fig, axes = plt.subplots(1, 3, figsize=(16, 4.5), sharex=True, sharey=True)
    a = gaf.AHICapture(tobj)
    image = np.stack((a.band3.data, a.band2.data, a.band1.data), axis=-1)
    image[np.any(image == 1e20, axis=-1)] = [0, 0, 0]
    image = image * 6
    image[image > 1] = 1
    image[image < 0] = 0
    im_bnds = [location[0] + cs_areas[k][0], location[0] + cs_areas[k][0] + 101,
               location[1] + cs_areas[k][2], location[1] + cs_areas[k][2] + 101]
    axes[0].imshow(image[im_bnds[0]:im_bnds[1], im_bnds[2]:im_bnds[3]])
    ims = dict()
    ims[0] = axes[1].imshow(rms, vmax=10)
    ims[1] = axes[2].imshow(bt_stack[-1, location[0] - 50:location[0] + 51, location[1] - 50:location[1] + 51],
                            vmin=270, vmax=310)
    coord_list = np.array([[item // 101, item % 101] for item in np.argsort(np.ravel(rms))[1:25]])
    for i, ax in enumerate(axes[1:]):
        ax.scatter(50, 50, s=4, c='r')
        ax.scatter(coord_list[:, 1], coord_list[:, 0], s=4, c='m')
        cb = plt.colorbar(ims[i], ax=ax)
        if not i:
            cb.set_label('RMSE (K)')
        else:
            cb.set_label('Band 7 BT (K)')
    axes[0].set_yticklabels([])
    labels = ['a', 'b', 'c']
    for i, ax in enumerate(axes):
        ax.set_xticklabels([])
        ax.text(-2, 0, '{})'.format(labels[i]), va='top', ha='right', fontsize=14)

    b = cut_stack[-1]
    axes[1].text(98, 98, 'cont RMS: {:.3f}K\ns-t RMS: {:.3f}K'.format(
        np.nanmean(np.delete(np.ravel(rms[48:53, 48:53]), 12)),
        np.nanmean(np.ravel(rms[coord_list[:, 0], coord_list[:, 1]]))), ha='right', va='bottom')
    axes[2].text(98, 98, 'cont mean: {:.3f}K\ns-t mean: {:.3f}K'.format(
        b[50, 50] - np.nanmean(np.delete(np.ravel(b[48:53, 48:53]), 12)),
        b[50, 50] - np.nanmean(np.ravel(b[coord_list[:, 0], coord_list[:, 1]]))), ha='right', va='bottom')
    fig.set_tight_layout(True)
    fig.savefig(os.path.join(savepath, 'poster_fig.png'), format='png', dpi=300)
