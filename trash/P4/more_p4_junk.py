import gaf
import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import sys
import time
from multiprocessing import Pool
import platform
import pandas as pd
import cartopy
from shapely.geometry import Polygon, Point
import datetime
import pickle
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.cm as mpcm
from matplotlib import colors, gridspec
from matplotlib.lines import Line2D

cmrb = mpcm.RdBu_r.copy()
cmrb.set_bad(color='0.75')
cmvr = mpcm.viridis.copy()
cmvr.set_bad(color='0.75')
cmo = mpcm.Oranges.copy()
cmo.set_bad(color='0.75')
cmp = mpcm.Purples.copy()
cmp.set_bad(color='0.75')


# Functions
def load_cont_lv():
    va = gaf.get_viewangle()
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = gaf.get_land_mask()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = gaf.AHICapture((2016, 6, 1, 0))
        file_kw = 'B07-w05-counts_valid'
        temp = Dataset(os.path.join(savepath, '{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        array = np.copy(flt_land)
        array[np.isnan(temp)] = False
        np.save(os.path.join(savepath, 'cont_lv.npy'), array)
    else:
        array = np.load(os.path.join(savepath, 'cont_lv.npy'))
    array = np.ma.array(array, mask=va > 80)
    return array


# constants
ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
if platform.node() == 'smgs-seo-sfmbox':
    savepath = '/home/sfm-user/data/AHI/P4'
    klist = ['thl_q', 'chn_m', 'nwa_p', 'sea_e', 'bor_g',
             'sib_r', 'thl_m', 'nwa_b', 'nwa_g', 'thl_c']
elif platform.node() == 'smgs-seo-sfmdos':
    savepath = '/mnt/Storage/active-fire/P4'
    klist = ['chn_g', 'thl_p', 'sea_h', 'sib_n', 'nwa_f',
             'thl_j', 'sea_b', 'thl_a', 'bor_a', 'chn_q']
elif platform.node() == 'smgs-seo-bryan':
    savepath = '/home/bryan/data/P4/'
elif platform.node() == 'bryan-metabox':
    savepath = '/run/media/bryan/Sunflower/P4'
else:
    savepath = r'C:/Scratch/cache/P4'

cont_lv = load_cont_lv()

# amended cs_areas for spatio-temporal work
cs_areas = {
    'sea': [4350, 4650, 3000, 3300],
    'nwa': [3550, 3850, 1950, 2250],
    'bor': [2550, 2850, 1350, 1650],
    'thl': [1750, 2050, 750, 1050],
    'chn': [950, 1250, 1550, 1850],
    'jpn': [850, 1150, 2450, 2750],
    'sib': [150, 450, 1950, 2250]
}
cs_area_toff = dict(
    [[k, gaf.AHILocation([np.mean(cs_areas[k][:2]),
                          np.mean(cs_areas[k][2:])]).time_offset / 60] for k in cs_areas.keys()])
# 2016 central julian day of weighted maximum fire activity cf. VIIRS
cs_jday = {'sea': 105, 'nwa': 312, 'bor': 60, 'thl': 74, 'chn': 255, 'jpn': 139, 'sib': 146}
cld_tl = {'sea': [4350, 3000], 'nwa': [3550, 1950], 'bor': [2550, 1350], 'thl': [1750, 750],
          'chn': [950, 1550], 'jpn': [850, 2450], 'sib': [150, 1950]}

with open(os.path.join(savepath, 'ncs_areas.pkl'), 'rb') as f:
    ncs_areas = pickle.load(f)
cs_areas.update(ncs_areas)

# cs_areas['sea_a'] = [4400, 4550, 3100, 3250]
# cs_areas['sea_b'] = [4350, 4500, 3100, 3250]
# cs_areas['chn_a'] = [1100, 1250, 1550, 1700]
# cs_areas['thl_a'] = [1800, 1950, 850, 1000]
# cs_areas['nwa_s'] = [3600, 3750, 2025, 2175]

num_points = 24

# setup distance array
radius = 50
arraywidth = 2 * radius + 1
q = np.arange(arraywidth ** 2).reshape(arraywidth, -1)
dist = np.sqrt((q // arraywidth - radius) ** 2 + (q % arraywidth - radius) ** 2)

#k = 'sea_a'
use_cldmask = True
# pred_time = gaf.Time((2016, 105, 2, 0))

# freq = '1h'
num_times = 48


def set_times(k):
    # set up time array - array hardcoded by np.random.choice(np.arange(6)*10, 4*31)
    days = np.arange(31) + cs_jday[k[:3]] - 15
    hrs = np.array([0, 9, 12, 15])
    # no 40's here because of the stationkeeping times, 30's due to missing cloud masks
    mins = np.array(
        [0, 50, 0, 0, 20, 50, 50, 10, 50, 20, 50, 50, 10, 10, 20, 0, 20, 20, 50, 10, 0, 0, 50, 10, 50, 50, 10, 50, 0, 20,
         20, 50, 20, 50, 50, 20, 10, 50, 0, 10, 10, 10, 20, 10, 20, 50, 50, 0, 0, 50, 50, 50, 50, 0, 0, 0, 10, 0, 20, 10,
         20, 0, 0, 0, 50, 10, 10, 20, 20, 20, 10, 10, 20, 50, 20, 50, 0, 50, 20, 20, 20, 20, 20, 0, 0, 10, 20, 20, 50, 10,
         20, 20, 10, 10, 20, 10, 10, 50, 20, 20, 10, 0, 50, 50, 50, 0, 0, 10, 20, 50, 0, 0, 20, 20, 10, 0, 10, 0, 50, 20,
         0, 10, 50, 10])
    t_offset = int(np.floor(gaf.AHILocation((np.mean(cs_areas[k][:2]), np.mean(cs_areas[k][2:]))).time_offset/60 + 0.5))
    dh_list = np.array([[2016, d, h] for d in days for h in hrs])
    local_t_list = np.append(dh_list, mins[:, np.newaxis], axis=1)
    utc_t_list = np.array([gaf.Time(gaf.Time(i).dt - datetime.timedelta(hours=t_offset)) for i in local_t_list])
    return utc_t_list


def process_time(pred_time, freq, k, plot=False):
    # grab base sub_land (for eliminating sea tiles)
    sub_land = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
    bsub_land = sub_land[radius:-radius, radius:-radius]
    # num_land_pixels = np.count_nonzero(bsub_land)

    if freq[-3:] == 'min':
        minute_gap = int(freq[:-3])
    elif freq[-1:] == 'h':
        minute_gap = int(freq[:-1]) * 60
    elif freq[-1:] == 'd':
        minute_gap = int(freq[:-1]) * 1440
    else:
        print('make freq sensible please')
        sys.exit()

    # create t_list - list of times for prediction step
    t_list = [pred_time.dt - datetime.timedelta(minutes=int(minute_gap * i)) for i in np.arange(num_times + 1)]
    t_list.sort()

    # create storage for brightness temperature information
    bt_array = np.empty((num_times + 1, cs_areas[k][1] - cs_areas[k][0], cs_areas[k][3] - cs_areas[k][2]))
    bt_array[:] = np.nan
    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        try:
            bt = a.band7.data[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
        except AttributeError:
            print('No file available for {}'.format(a.time.dt))
            continue
        bt[~sub_land] = np.nan
        if use_cldmask:
            sub_k = k[:3]
            try:
                cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                        '{:03d}'.format(a.time.jday), k[:3])
                cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                               a.time.minute)
                r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
            except IndexError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            except FileNotFoundError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            t_cld = r.variables['cloud_mask'][0, cs_areas[k][0] - cld_tl[sub_k][0]: cs_areas[k][1] - cld_tl[sub_k][0],
                                              cs_areas[k][2] - cld_tl[sub_k][1]: cs_areas[k][3] - cld_tl[sub_k][1]]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt.astype(np.float32)

    bt_pad = np.empty((bt_array.shape[0], bt_array.shape[1] + 1, bt_array.shape[2] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:, :-1, :-1] = bt_array.astype(np.float32)
    bt_pad[bt_pad > 400] = np.nan

    array_size = 50
    y_vals = x_vals = np.arange(array_size)
    coords = np.meshgrid(x_vals, y_vals)
    val_array = np.array([bt_pad[:, wy:wy + (radius * 2 + 1), wx:wx + (radius * 2 + 1)]
                          for wy in y_vals for wx in x_vals])
    va = val_array.swapaxes(0, 1).reshape(bt_array.shape[0], val_array.shape[0], -1)
    va[:, ~np.ravel(bsub_land), :] = np.nan
    cent_vals = va[:-1, :, va.shape[2] // 2]
    diffs = va[:-1] - cent_vals[:, :, np.newaxis]
    rms = np.nanmean(diffs ** 2, axis=0)
    amount_valid = np.count_nonzero(~np.isnan(diffs), axis=0)
    rms[:, np.ravel(dist > 50)] = 10000
    rms[amount_valid < 4] = 10000
    inds = rms.argsort()

    # TODO: total the rms[inds] and divide by 10000 - this will reveal how many are invalid
    # then use take to recover only the valid predictors somehow
    nan_inds = np.copy(inds).astype(np.float)
    nan_inds[~np.ravel(bsub_land)] = np.nan
    pred_vec = np.take_along_axis(va[-1], inds[:, 1:num_points + 1], 1)

    cont_inds = q[dist < 3].reshape(-1)
    cont_inds = np.delete(cont_inds, 12)
    cont_vec = np.take_along_axis(va[-1], cont_inds[np.newaxis, :], 1)

    pred_rms = np.take_along_axis(rms, inds[:, 1:num_points + 1], 1)
    pred_rms[pred_rms > 50] = np.nan

    cont_rms = np.take_along_axis(rms, cont_inds[np.newaxis, :], 1)
    cont_rms[cont_rms > 50] = np.nan

    ix = np.isin(inds, cont_inds)
    part_ix = ix[:, 1:num_points + 1]

    df = pd.DataFrame(columns=['time', 'freq'])
    df['x'] = np.ravel(coords[0]) + cs_areas[k][2] + radius
    df['y'] = np.ravel(coords[1]) + cs_areas[k][0] + radius
    df['bt'] = np.ravel(va[-1, :, va.shape[2]//2])
    df['sts_mean'] = np.nanmean(pred_vec, 1)
    # df['sts_valid'] = np.count_nonzero(~np.isnan(pred_vec), axis=1)
    df['sts_diff'] = df.bt - df.sts_mean
    df['sts_rms'] = np.nanmean(np.take_along_axis(rms, inds[:, 1:num_points + 1], 1), axis=1)
    df['select_dist'] = np.nanmean(np.sqrt(np.abs(nan_inds[:, 1:num_points + 1] // arraywidth - radius) ** 2 +
                                   np.abs(nan_inds[:, 1:num_points + 1] % arraywidth - radius) ** 2), axis=1)
    df['cont_mean'] = np.nanmean(cont_vec, 1)
    df['cont_valid'] = np.count_nonzero(~np.isnan(cont_vec), axis=1)
    df['cont_diff'] = df.bt - df.cont_mean
    df['cont_rms'] = np.nanmean(cont_rms, axis=1)
    filt_mask = np.logical_or(np.isnan(pred_vec), np.abs(pred_vec - np.nanmean(pred_vec, axis=1)[:, np.newaxis])
                              > 2 * np.nanstd(pred_vec, axis=1)[:, np.newaxis])
    df['sts_filt_mean'] = np.ma.mean(np.ma.masked_array(pred_vec, mask=filt_mask), axis=1)
    df['sts_filt_diff'] = df.bt - df.sts_filt_mean
    df['pred_valid'] = np.count_nonzero(~np.isnan(pred_vec), 1)
    df['wax'] = df.sts_filt_diff * (df.pred_valid / num_points) / np.nanmean(pred_rms, 1)
    df['pred_vec'] = pred_vec.tolist()
    df['pred_rms'] = pred_rms.tolist()
    df['cont_vec'] = cont_vec.tolist()
    df['select_inds'] = nan_inds[:, 1:num_points + 1].tolist()
    df['time'].iloc[0] = pred_time
    df['freq'].iloc[0] = freq
    #pklpath = os.path.join(savepath, 'take_two', '{}'.format(freq),
    #                       '{}_{}_{:03d}_{:02d}_{:02d}.df'.format(k, pred_time.year, pred_time.jday, pred_time.hour,
    #                                                              pred_time.minute))
    pklpath = '/home/sfm-user/data/AHI/P4/2h/{}_{}_{:03d}_{:02d}_{:02d}.df'.format(k, pred_time.year,
                                                                                   pred_time.jday, pred_time.hour,
                                                                                   pred_time.minute)
    df.to_pickle(pklpath)

    '''
    if plot:
        f, ax = plt.subplots(1, 3, figsize=(15, 4))
        ax[0].hist(np.ravel(np.sqrt(np.abs(nan_inds[:, 1:num_points + 1] // arraywidth - radius) ** 2 +
                                    np.abs(nan_inds[:, 1:num_points + 1] % arraywidth - radius) ** 2)),
                   bins=np.arange(51), cumulative=True, density=True, histtype='step', zorder=1)
        ax[0].grid(which='major', axis='both', alpha=0.5)
        ax[0].set_xlim(0, 50)
        ax[0].set_ylim(0, 1)
        ax[0].set_title('Distance of selected candidates from centre - freq {}'.format(freq), fontsize=10)

        im = ax[1].imshow(np.mean(np.sqrt(np.abs(nan_inds[:, 1:num_points + 1] // arraywidth - radius) ** 2 +
                                          np.abs(nan_inds[:, 1:num_points + 1] % arraywidth - radius) ** 2),
                                  axis=1).reshape(50, -1), vmax=50)
        ax[1].set_title('Distance of selected candidates from centre - freq {}'.format(freq), fontsize=10)
        cb = plt.colorbar(im, ax=ax[1])
        cb.set_label('Distance (pixels)')

        im = ax[2].imshow(
            np.ma.masked_array(100 * (np.ones(part_ix.shape[0]) - np.count_nonzero(part_ix, axis=1) / 24).reshape(50, -1),
                               mask=~bsub_land), vmin=0)
        cb = plt.colorbar(im, ax=ax[2])
        ax[2].set_title('Proportion of candidates from outside 5x5 region - freq {}'.format(freq), fontsize=10)
        cb.set_label('Percentage')
        f.subplots_adjust(top=0.935, bottom=0.071, left=0.027, right=0.953, hspace=0.11, wspace=0.1)

    # print(
    #     'Day {}, portion context most similar: {:.4f}'.format(pred_time.jday,
    #                                                           np.count_nonzero(part_ix)/(num_points * num_land_pixels)))
    # return np.count_nonzero(part_ix)/(num_points * num_land_pixels)
    '''
    return pklpath


def numbers_and_plots(pkl):
    cont_pc = 0.65
    sts_pc = 0.25
    df = pd.read_pickle(pkl)

    # produce plots
    tmin = np.max(np.floor(np.nanmin((np.nanmin(df.bt.values), np.nanmin(df.sts_mean.values),
                                      np.nanmin(df.cont_mean.values))) / 5) * 5, 260)
    tmax = np.min(np.ceil(np.nanmax((np.nanmax(df.bt.values), np.nanmax(df.sts_mean.values),
                                     np.nanmax(df.cont_mean.values))) / 5) * 5, 340)
    # set minimum candidates - 65%
    cont_mean = df.cont_mean.values
    cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
    sts_mean = df.sts_mean.values
    sts_mean[df.sts_valid.values < sts_pc * 24] = np.nan
    sts_filt_mean = df.sts_filt_mean.values
    sts_filt_mean[df.sts_valid.values < sts_pc * 24] = np.nan
    all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                   ~np.isnan(sts_filt_mean)))

    f, axs = plt.subplots(2, 4, figsize=(16.83, 7.35))
    f.axes[0].imshow(df.bt.values.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[0].set_title('BT (4$\mu$m)', fontsize=10)
    f.axes[1].imshow(cont_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[1].set_title('Context estimate', fontsize=10)
    im1 = f.axes[2].imshow(sts_filt_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[2].set_title('STS filtered estimate', fontsize=10)
    pred_rms_mean = np.nanmean(np.concatenate(df.pred_rms).reshape(2500, -1), 1)
    pred_rms_mean[~all_4] = np.nan
    im2 = f.axes[3].imshow(pred_rms_mean.reshape(50, -1), vmin=0, vmax=10, cmap=cmvr)
    f.axes[3].set_title('RMSE of predictors', fontsize=10)

    # sts_diff = df.sts_diff.values
    # sts_diff[~all_4] = np.nan
    cont_diff = df.cont_diff.values
    cont_diff[~all_4] = np.nan
    f.axes[5].imshow(cont_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[5].set_title('Cont diff', fontsize=10)

    sts_filt_diff = df.sts_filt_diff.values
    sts_filt_diff[~all_4] = np.nan
    im3 = f.axes[6].imshow(sts_filt_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[6].set_title('STS filtered diff', fontsize=10)
    try:
        df.pred_valid
        wax = df.wax.values
    except AttributeError:
        pred_valid = np.count_nonzero(~np.isnan(np.concatenate(df.pred_vec).reshape(2500, -1)), 1)
        wax = df.wax.values * pred_valid / 24
    wax[~all_4] = np.nan
    im4 = f.axes[7].imshow(wax.reshape(50, -1), vmin=-10, vmax=10, cmap=cmrb)
    f.axes[7].set_title('WAX', fontsize=10)

    for ax in f.axes:
        try:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        except AttributeError:
            pass

    divider = make_axes_locatable(f.axes[2])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(im1, cax=cax)
    cb.set_label('Temperature (K)')
    try:
        cb.set_ticks(np.arange(tmin, tmax + 5, 5))
    except ValueError:
        pass

    divider = make_axes_locatable(f.axes[3])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(im2, cax=cax, extend='max')
    cb.set_label('RMS Error (K)')

    divider = make_axes_locatable(f.axes[6])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(im3, cax=cax, extend='both')
    cb.set_label('Temp diff (K)')

    divider = make_axes_locatable(f.axes[7])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(im4, cax=cax, extend='both')
    cb.set_label('Value')
    cb.set_ticks(np.arange(5) * 5 - 10)

    f.axes[4].remove()
    f.set_tight_layout(True)

    pred_time = df.time.iloc[0]
    freq = df.freq.iloc[0]
    k = pkl.split('/')[-1][:5]
    num_times = 48

    f.text(0.02, 0.45,
           'CS Area:   {}\n'
           'AHI loc:   {}\n'
           'Pred time: {}j{:03d} {:02d}:{:02d} UTC\n'
           'Specs:     {} images x {} freq\n'
           'Cont 5x5   mean: {:>6.3f}  stdev: {:.3f}\n'
           'STS filt   mean: {:>6.3f}  stdev: {:.3f}\n'
           'Pixels:    {}'.format(k, np.array(cs_areas[k]) + np.array([50, -50, 50, -50]),
                                  pred_time.year, pred_time.jday, pred_time.hour, pred_time.minute,
                                  num_times, freq,
                                  np.nanmean(cont_diff), np.nanstd(cont_diff),
                                  np.nanmean(sts_filt_diff), np.nanstd(sts_filt_diff),
                                  np.count_nonzero(all_4)),
           family='monospace', va='top')

    fsavepath = pkl.split('.')[0] + '.png'
    f.savefig(fsavepath, dpi=600, format='png')
    # return f, axs
    plt.close(f)


def plots_mk3(pkl):
    cont_pc = 0.65
    sts_pc = 0.25
    #anom_rate = 0.02

    df = pd.read_pickle(pkl)

    # produce plots
    tmin = np.max((np.floor(np.nanmin((np.nanpercentile(df.bt.values, 2),
                                       np.nanpercentile(df.sts_mean.values, 2),
                                       np.nanpercentile(df.cont_mean.values, 2))) / 5) * 5, 260))
    tmax = np.max((np.min((np.ceil(np.nanmax((np.nanpercentile(df.bt.values, 98),
                                              np.nanpercentile(df.sts_mean.values, 98),
                                              np.nanpercentile(df.cont_mean.values, 98))) / 5) * 5, 340)), 265))
    # set minimum candidates - 65%
    cont_mean = df.cont_mean.values
    cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
    sts_mean = df.sts_mean.values
    sts_mean[df.sts_valid.values < sts_pc * 24] = np.nan
    sts_filt_mean = df.sts_filt_mean.values
    sts_filt_mean[df.sts_valid.values < sts_pc * 24] = np.nan
    all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                   ~np.isnan(sts_filt_mean)))

    f, axs = plt.subplots(2, 4, figsize=(16.83, 7.35))
    im1 = f.axes[0].imshow(df.bt.values.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[0].set_title('BT (4$\mu$m) - AHI B07', fontsize=10)
    f.axes[5].imshow(cont_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[5].set_title('Context (5x5) estimate', fontsize=10)
    f.axes[1].imshow(sts_filt_mean.reshape(50, -1), vmin=tmin, vmax=tmax, cmap=cmvr)
    f.axes[1].set_title('STS estimate', fontsize=10)

    pred_rms_mean = np.nanmean(np.concatenate(df.pred_rms).reshape(2500, -1), 1)
    pred_rms_mean[~all_4] = np.nan
    im2 = f.axes[3].imshow(pred_rms_mean.reshape(50, -1), vmin=0, vmax=10, cmap=cmo)
    f.axes[3].set_title('RMSE of predictor pixels', fontsize=10)

    # sts_diff = df.sts_diff.values
    # sts_diff[~all_4] = np.nan
    cont_diff = df.cont_diff.values
    cont_diff[~all_4] = np.nan
    f.axes[6].imshow(cont_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[6].set_title('Context (5x5) diff', fontsize=10)

    sts_filt_diff = df.sts_filt_diff.values
    sts_filt_diff[~all_4] = np.nan
    im3 = f.axes[2].imshow(sts_filt_diff.reshape(50, -1), vmin=-5, vmax=5, cmap=cmrb)
    f.axes[2].set_title('STS diff', fontsize=10)

    cont_rms = df.cont_rms
    count_invalid = (cont_rms // 416.666666).values.astype(int)
    cont_rms[count_invalid == 24] = np.nan
    cont_rms = cont_rms - 416.666666 * count_invalid
    cont_rms[~all_4] = np.nan

    f.axes[7].imshow(cont_rms.values.reshape(50, -1), vmin=0, vmax=10, cmap=cmo)
    f.axes[7].set_title('RMSE of 5x5 surround pixels', fontsize=10)

    for ax in f.axes:
        try:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        except AttributeError:
            pass

    for i in [0, 1, 5]:
        divider = make_axes_locatable(f.axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im1, cax=cax)
        cb.set_label('Brightness Temp (K)')
        try:
            cb.set_ticks(np.arange(tmin, tmax + 5, 5))
        except ValueError:
            pass

    for i in [2, 6]:
        divider = make_axes_locatable(f.axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im3, cax=cax, extend='both')
        cb.set_label('$\Delta$T (K)')

    for i in [3, 7]:
        divider = make_axes_locatable(f.axes[i])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(im2, cax=cax, extend='max')
        cb.set_label('RMSE (K)')

    f.axes[4].remove()
    f.set_tight_layout(True)

    pred_time = df.time.iloc[0]
    freq = df.freq.iloc[0]
    k = pkl.split('/')[-1][:5]
    num_times = 48

    # outlier rate == 2.0\%
    outlier_pc = 98
    sts_mean_sans_out = np.nanmean(sts_filt_diff[np.abs(sts_filt_diff) < np.nanpercentile(np.abs(sts_filt_diff),
                                                                                          outlier_pc)])
    sts_std_sans_out = np.nanstd(sts_filt_diff[np.abs(sts_filt_diff) < np.nanpercentile(np.abs(sts_filt_diff),
                                                                                        outlier_pc)])
    cont_mean_sans_out = np.nanmean(cont_diff[np.abs(cont_diff) < np.nanpercentile(np.abs(cont_diff), outlier_pc)])
    cont_std_sans_out = np.nanstd(cont_diff[np.abs(cont_diff) < np.nanpercentile(np.abs(cont_diff), outlier_pc)])

    f.text(0.02, 0.45,
           'CS Area:   {}\n'
           'AHI loc:   {}\n'
           'Pred time: {}j{:03d} {:02d}:{:02d} UTC\n'
           'Specs:     {} images x {} freq\n\n'
           'STS filt   mean:  {:>6.3f}  stdev: {:.3f}\n'
           '<98th pc   mean:  {:>6.3f}  stdev: {:.3f}\n'
           'STS 98th pc temp: {:>6.3f}\n\n'
           'Cont 5x5   mean:  {:>6.3f}  stdev: {:.3f}\n'
           '<98th pc   mean:  {:>6.3f}  stdev: {:.3f}\n'
           'Cont 98th pc temp:{:>6.3f}\n\n'
           'n:                {:>5d}'.format(k, np.array(cs_areas[k]) + np.array([50, -50, 50, -50]),
                                             pred_time.year, pred_time.jday, pred_time.hour, pred_time.minute,
                                             num_times, freq,
                                             np.nanmean(sts_filt_diff), np.nanstd(sts_filt_diff),
                                             sts_mean_sans_out, sts_std_sans_out,
                                             np.nanpercentile(np.abs(sts_filt_diff), outlier_pc),
                                             np.nanmean(cont_diff), np.nanstd(cont_diff),
                                             cont_mean_sans_out, cont_std_sans_out,
                                             np.nanpercentile(np.abs(cont_diff), outlier_pc),
                                             np.count_nonzero(all_4)),
           family='monospace', va='top')

    fsavepath = pkl.split('.')[0] + '.png'
    f.savefig(fsavepath, dpi=600, format='png')
    # return f, axs
    plt.close(f)


def proc_sets(keys=None):
    if keys is None:
        keys = klist
    for k in keys:
        utc_t_list = set_times(k)
        freq = '2h'
        for t in utc_t_list:
            pkl = os.path.join(savepath, '{}'.format(freq),
                               '{}_{}_{:03d}_{:02d}_{:02d}.df'.format(k, t.year, t.jday, t.hour, t.minute))
            if not os.path.isfile(pkl):
                process_time(t, '2h', k)
                print('Time {} complete'.format(t.dt))


def summarise_global_metrics(cs_key, save=True):
    sub_land = cont_lv[cs_areas[cs_key][0]:cs_areas[cs_key][1], cs_areas[cs_key][2]:cs_areas[cs_key][3]]
    bsub_land = sub_land[radius:-radius, radius:-radius]
    tot_land = np.count_nonzero(bsub_land.data)

    pklpath = os.path.join(savepath, 'take_two', '2h')
    flist = gaf.listdir_contains(pklpath, cs_key)
    sflist = [x for x in flist if '.df' in x]
    # d = {}

    mdf = pd.DataFrame(columns=['cont_diff_mean', 'cont_diff_std', 'cont_mean_rms', 'sts_diff_mean', 'sts_diff_std',
                                'sts_pred_rms', 'cont_98_mean', 'cont_98_std', 'cont_98_temp', 'sts_98_mean',
                                'sts_98_std', 'sts_98_temp', 'bt_n', 'sts_n', 'cont_n', 'n', 'hour', 'tot_land'])

    for f in sflist:
        cont_pc = 0.65
        sts_pc = 0.25
        outlier_pc = 98

        df = pd.read_pickle(f)
        cont_mean = df.cont_mean.values
        cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
        try:
            sts_valid_vals = df.sts_valid.values
        except:
            sts_valid_vals = df.pred_valid.values
        sts_mean = df.sts_mean.values
        sts_mean[sts_valid_vals < sts_pc * 24] = np.nan
        sts_filt_mean = df.sts_filt_mean.values
        sts_filt_mean[sts_valid_vals < sts_pc * 24] = np.nan
        all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                       ~np.isnan(sts_filt_mean)))
        df.sts_filt_diff[~all_4] = np.nan
        df.cont_diff[~all_4] = np.nan
        pred_rms_mean = np.nanmean(np.concatenate(df.pred_rms).reshape(2500, -1), 1)
        pred_rms_mean[~all_4] = np.nan

        sts_mean_sans_out = np.nanmean(
            df.sts_filt_diff[np.abs(df.sts_filt_diff) < np.nanpercentile(np.abs(df.sts_filt_diff),
                                                                         outlier_pc)])
        sts_std_sans_out = np.nanstd(
            df.sts_filt_diff[np.abs(df.sts_filt_diff) < np.nanpercentile(np.abs(df.sts_filt_diff),
                                                                         outlier_pc)])
        sts_out_temp = np.nanpercentile(np.abs(df.sts_filt_diff), outlier_pc)
        cont_mean_sans_out = np.nanmean(
            df.cont_diff[np.abs(df.cont_diff) < np.nanpercentile(np.abs(df.cont_diff), outlier_pc)])
        cont_std_sans_out = np.nanstd(
            df.cont_diff[np.abs(df.cont_diff) < np.nanpercentile(np.abs(df.cont_diff), outlier_pc)])
        cont_out_temp = np.nanpercentile(np.abs(df.cont_diff), outlier_pc)


        # mdf['sts_filt_diff'] = df.sts_filt_diff
        # mdf['cont_diff'] = df.cont_diff
        # mdf['pred_rms_mean'] = pred_rms_mean
        cont_rms = df.cont_rms
        count_invalid = (cont_rms // 416.666666).values.astype(int)
        cont_rms[count_invalid == 24] = np.nan
        cont_rms = cont_rms - 416.666666 * count_invalid
        d_ind = '{:03d}_{:02d}:{:02d}'.format(*[getattr(df.time.iloc[0], x) for x in ['jday', 'hour', 'minute']])
        hour = df.time.iloc[0].hour

        mdf.loc[d_ind] = [df.cont_diff.mean(), df.cont_diff.std(), df.cont_rms.mean(),
                          df.sts_filt_diff.mean(), df.sts_filt_diff.std(), np.nanmean(pred_rms_mean),
                          cont_mean_sans_out, cont_std_sans_out, cont_out_temp,
                          sts_mean_sans_out, sts_std_sans_out, sts_out_temp,
                          np.count_nonzero(~np.isnan(df.bt.values)),
                          np.count_nonzero(~np.isnan(sts_filt_mean)),
                          np.count_nonzero(~np.isnan(cont_mean)),
                          np.count_nonzero(all_4), hour, tot_land]

    if save:
        mdf.to_pickle(os.path.join(savepath, 'take_two', '2h', '{}_summary.pkl'.format(cs_key)))


def global_metric_format():
    flist = gaf.listdir_end(os.path.join(savepath, 'take_two', '2h'), '.pkl')
    key_list = [f.split('/')[-1][:5] for f in flist]
    key_list.sort()
    print('& \\multicolumn{4}{|l}{\\textbf{Outliers retained}} & \\multicolumn{4}{|l}{\\textbf{Outliers removed}}\\\\'
          '\\hline')
    print('\\textbf{Site} & \\textbf{STS $\\mu$ (K)} & \\textbf{STS $\\sigma$ (K)} & \\textbf{Cont $\\mu$ (K)} & '
          '\\textbf{Cont $\\sigma$ (K)} & \\textbf{STS $\\mu$ (K)} & \\textbf{STS $\\sigma$ (K)} & '
          '\\textbf{Cont $\\mu$ (K)} & \\textbf{Cont $\\sigma$ (K)}\\\\\\midrule')
    for k in key_list:
        mdf = pd.read_pickle(os.path.join(savepath, 'take_two', '2h', '{}_summary.pkl'.format(k)))
        #print('{} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} \\\\'.format(
        #    '\_'.join(k.split('_')), np.nansum(mdf.sts_diff_mean.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.sts_diff_std.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.cont_diff_mean.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.cont_diff_std.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.sts_98_mean.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.sts_98_std.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.cont_98_mean.values * mdf.n.values / np.nansum(mdf.n)),
        #    np.nansum(mdf.cont_98_std.values * mdf.n.values / np.nansum(mdf.n))
        #))
        print('{} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:4.1f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} '
              '& {:4.1f} \\\\'.format(
            '\_'.join(k.split('_')),
            np.nansum(mdf.cont_diff_mean.values * mdf.n.values / np.nansum(mdf.n)),
            np.nansum(mdf.cont_diff_std.values * mdf.n.values / np.nansum(mdf.n)),
            np.nansum(mdf.sts_diff_mean.values * mdf.n.values / np.nansum(mdf.n)),
            np.nansum(mdf.sts_diff_std.values * mdf.n.values / np.nansum(mdf.n)),
            -100 * (1 - np.nansum(mdf.sts_diff_std.values * mdf.n.values / np.nansum(mdf.n)) / np.nansum(
                mdf.cont_diff_std.values * mdf.n.values / np.nansum(mdf.n))),
            np.nansum(mdf.cont_98_mean.values * mdf.n.values / np.nansum(mdf.n)),
            np.nansum(mdf.cont_98_std.values * mdf.n.values / np.nansum(mdf.n)),
            np.nansum(mdf.sts_98_mean.values * mdf.n.values / np.nansum(mdf.n)),
            np.nansum(mdf.sts_98_std.values * mdf.n.values / np.nansum(mdf.n)),
            -100 * (1 - np.nansum(mdf.sts_98_std.values * mdf.n.values / np.nansum(mdf.n)) / np.nansum(
                mdf.cont_98_std.values * mdf.n.values / np.nansum(mdf.n)))
        ))


def aggregated_cs_stats():
    print(' & \\multicolumn{5}{c|}{Outliers retained & \\multicolumn{5}{c}{Outliers removed} \\\\')
    print('& \\multicolumn{2}{c|}{Context} & \\multicolumn{2}{c|}{STS} & & \\multicolumn{2}{c|}{Context} & '
          '\\multicolumn{2}{c|}{STS} & \\\\')
    print('Site & $\\mu$ (K) & $\\sigma$ (K) & $\\mu$ (K) & $\\sigma$ (K) & $\\Delta\\sigma$\\% & $\\mu$ (K) & '
          '$\\sigma$ (K) & $\\mu$ (K) & $\\sigma$ (K) & $\\Delta\\sigma$\\% \\\\\\midrule')
    flist = gaf.listdir_end(os.path.join(savepath, 'take_two', '2h'), '.pkl')
    for k in ['sea', 'nwa', 'bor', 'thl', 'chn', 'jpn', 'sib']:
        key_list = [f.split('/')[-1][:5] for f in flist if k in f]
        for i, a in enumerate(key_list):
            if not i:
                df = pd.read_pickle(os.path.join(savepath, 'take_two', '2h', '{}_summary.pkl'.format(a)))
            else:
                df = pd.concat((df,
                                pd.read_pickle(os.path.join(savepath, 'take_two', '2h', '{}_summary.pkl'.format(a)))))
        print('{} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & {:4.1f} & {:.3f} & {:.3f} & {:.3f} & {:.3f} '
              '& {:4.1f} \\\\'.format(
            k,
            np.nansum(df.cont_diff_mean.values * df.n.values / np.nansum(df.n)),
            np.nansum(df.cont_diff_std.values * df.n.values / np.nansum(df.n)),
            np.nansum(df.sts_diff_mean.values * df.n.values / np.nansum(df.n)),
            np.nansum(df.sts_diff_std.values * df.n.values / np.nansum(df.n)),
            -100 * (1 - np.nansum(df.sts_diff_std.values * df.n.values / np.nansum(df.n)) / np.nansum(
                df.cont_diff_std.values * df.n.values / np.nansum(df.n))),
            np.nansum(df.cont_98_mean.values * df.n.values / np.nansum(df.n)),
            np.nansum(df.cont_98_std.values * df.n.values / np.nansum(df.n)),
            np.nansum(df.sts_98_mean.values * df.n.values / np.nansum(df.n)),
            np.nansum(df.sts_98_std.values * df.n.values / np.nansum(df.n)),
            -100 * (1 - np.nansum(df.sts_98_std.values * df.n.values / np.nansum(df.n)) / np.nansum(
                df.cont_98_std.values * df.n.values / np.nansum(df.n)))
        ))

    print('\nSite & total land & number bt & pc cont-bt & pc sts-bt & pc sts-cont\\\\')
    for k in ['sea', 'nwa', 'bor', 'thl', 'chn', 'jpn', 'sib']:
        key_list = [f.split('/')[-1][:5] for f in flist if k in f]
        for i, a in enumerate(key_list):
            if not i:
                df = pd.read_pickle(os.path.join(savepath, 'take_two', '2h', '{}_summary.pkl'.format(a)))
            else:
                df = pd.concat((df,
                                pd.read_pickle(os.path.join(savepath, 'take_two', '2h', '{}_summary.pkl'.format(a)))))
        print('{} & {:.0f} & {:.0f} & {:.2f} & {:.2f} & {:.2f}\\\\'.format(
            k, np.sum(df.tot_land), np.nansum(df.bt_n), 100 * np.nansum(df.cont_n)/np.nansum(df.bt_n),
            100 * np.nansum(df.sts_n) / np.nansum(df.bt_n), 100 * np.nansum(df.sts_n) / np.nansum(df.cont_n)
        ))

''' 
# hour stuff
    for val in mdf.hour.unique():
        gdf = mdf.groupby('hour').get_group(val)
        print('Group: {} STS_std: {:.3f} Cont_std: {:.3f} STS_98: {:.3f} Cont_98: {:.3f}'.format(
            val,
            (gdf.sts_diff_std.values * gdf.n.values / gdf.n.sum()).sum(),
            (gdf.cont_diff_std.values * gdf.n.values / gdf.n.sum()).sum(),
            (gdf.sts_98_std.values * gdf.n.values / gdf.n.sum()).sum(),
            (gdf.cont_98_std.values * gdf.n.values / gdf.n.sum()).sum()))
'''


def print_indiv_metrics(cs_key, plot=False):
    pklpath = os.path.join(savepath, 'take_two', '2h')
    flist = gaf.listdir_contains(pklpath, cs_key)
    sflist = [x for x in flist if '.df' in x]
    d = {}
    for i, f in enumerate(sflist):
        cont_pc = 0.65
        sts_pc = 0.25
        mdf = pd.DataFrame()
        df = pd.read_pickle(f)
        cont_mean = df.cont_mean.values
        cont_mean[df.cont_valid.values < cont_pc * 24] = np.nan
        sts_mean = df.sts_mean.values
        sts_mean[df.sts_valid.values < sts_pc * 24] = np.nan
        sts_filt_mean = df.sts_filt_mean.values
        sts_filt_mean[df.sts_valid.values < sts_pc * 24] = np.nan
        all_4 = np.logical_and.reduce((~np.isnan(df.bt.values), ~np.isnan(cont_mean),
                                       ~np.isnan(sts_filt_mean)))
        df.sts_filt_diff[~all_4] = np.nan
        df.cont_diff[~all_4] = np.nan
        pred_rms_mean = np.nanmean(np.concatenate(df.pred_rms).reshape(2500, -1), 1)
        pred_rms_mean[~all_4] = np.nan
        mdf['bt'] = df.bt
        mdf['sts_filt_diff'] = df.sts_filt_diff
        mdf['cont_diff'] = df.cont_diff
        mdf['pred_rms_mean'] = pred_rms_mean
        mdf['pred_valid'] = df.pred_valid
        cont_rms = df.cont_rms
        count_invalid = (cont_rms // 416.666666).values.astype(int)
        cont_rms[count_invalid == 24] = np.nan
        mdf['cont_rms'] = cont_rms - 416.666666 * count_invalid
        mdf['hour'] = df.time.iloc[0].hour
        d_key = '{:03d}_{:02d}:{:02d}'.format(*[getattr(df.time.iloc[0], x) for x in ['jday', 'hour', 'minute']])
        d[d_key] = mdf

    df = pd.concat([d[k] for k in d.keys()], keys=[k for k in d.keys()])

    valids = np.logical_and.reduce((~np.isnan(df.pred_rms_mean.groupby(level=1).mean().values),
                                    ~np.isnan(df.sts_filt_diff.groupby(level=1).mean().values),
                                    ~np.isnan(df.cont_rms.groupby(level=1).mean().values),
                                    ~np.isnan(df.cont_diff.groupby(level=1).mean().values)))
    cnt_rsq = gaf.rsquare(df.cont_rms.groupby(level=1).mean().values[valids],
                          df.cont_diff.groupby(level=1).std().values[valids])
    sts_rsq = gaf.rsquare(df.pred_rms_mean.groupby(level=1).mean().values[valids],
                          df.sts_filt_diff.groupby(level=1).std().values[valids])
    if plot:
        f, axes = plt.subplots(1, 2, figsize=(7.88, 3.53))
        axes[0].imshow(df.pred_rms_mean.groupby(level=1).mean().values.reshape(50, -1), vmin=0, vmax=20)
        axes[0].set_title('STS Pred RMS', fontsize=10)
        im = axes[1].imshow(df.cont_rms.groupby(level=1).mean().values.reshape(50, -1), vmin=0, vmax=20)
        axes[1].set_title('Cont RMS', fontsize=10)
        for ax in f.axes:
            try:
                ax.set_xticklabels([])
                ax.set_yticklabels([])
            except AttributeError:
                pass
        cb = plt.colorbar(im, ax=axes[1])
        cb.set_ticks(np.arange(5) * 5)
        f.set_tight_layout(True)

    print('Site: {}\n'
          'Context  mean: {:.3f}  stdev: {:.3f}  mean_rms: {:.3f}\n'
          'STS      mean: {:.3f}  stdev: {:.3f}  mean_rms: {:.3f}\n'
          'Context correlation: {:.4f}\n'
          'STS correlation: {:.4f}'.format(
        cs_key, df.cont_diff.mean(), df.cont_diff.std(), df.cont_rms.mean(),
        df.sts_filt_diff.mean(), df.sts_filt_diff.std(), df.pred_rms_mean.mean(),
        cnt_rsq, sts_rsq))
    return df


def plot_areal_diffs(df):
    f, ax = plt.subplots(1, 2, figsize=(8.8, 4))
    ax[0].imshow(df['cont_diff'].groupby(level=1).mean().values.reshape(50, -1), cmap=cmrb, vmin=-5, vmax=5)
    ax[0].set_title('cont_diff', fontsize=10)
    im = ax[1].imshow(df['sts_filt_diff'].groupby(level=1).mean().values.reshape(50, -1), cmap=cmrb, vmin=-5, vmax=5)
    ax[1].set_title('sts_diff', fontsize=10)
    cb = plt.colorbar(im, ax=ax[1], extend='both')
    cb.set_label('Temp diff (K)')
    f.set_tight_layout(True)


def plot_hour_diffs(df):
    val_list = df.hour.unique()
    val_list.sort()
    for val in val_list:
        gdf = df.groupby('hour').get_group(val)
        print('Hour: {:2d} Cont_mean: {:>6.3f} cont_std: {:>6.3f} sts_mean: {:>6.3f} sts_std: {:>6.3f}'.format(
            val, gdf['cont_diff'].groupby(level=1).mean().mean(), gdf['cont_diff'].groupby(level=1).mean().std(),
            gdf['sts_filt_diff'].groupby(level=1).mean().mean(), gdf['sts_filt_diff'].groupby(level=1).mean().std()
        ))
        gdf = df.groupby('hour').get_group(val)
        f, ax = plt.subplots(1, 2, figsize=(8.8, 4))
        ax[0].imshow(gdf['cont_diff'].groupby(level=1).mean().values.reshape(50, -1), cmap=cmrb, vmin=-5, vmax=5)
        ax[0].set_title('cont_diff', fontsize=10)
        im = ax[1].imshow(gdf['sts_filt_diff'].groupby(level=1).mean().values.reshape(50, -1), cmap=cmrb, vmin=-5,
                          vmax=5)
        ax[1].set_title('sts_diff', fontsize=10)
        cb = plt.colorbar(im, ax=ax[1], extend='both')
        cb.set_label('Temp diff (K)')
        f.suptitle(val)
        f.set_tight_layout(True)

'''
    gdf = df.groupby('hour').get_group(8)
    f, ax = plt.subplots(1, 2, figsize=(8.8, 4))
    ax[0].imshow(
        (gdf['cont_diff'].groupby(level=1).max().values - gdf['cont_diff'].groupby(level=1).min().values).reshape(50,
                                                                                                                  -1),
        cmap=cmo, vmin=0, vmax=30)
    ax[0].set_title('cont_diff', fontsize=10)
    im = ax[1].imshow((gdf['sts_filt_diff'].groupby(level=1).max().values - gdf['sts_filt_diff'].groupby(
        level=1).min().values).reshape(50, -1), cmap=cmo, vmin=0, vmax=30)
    ax[1].set_title('sts_diff', fontsize=10)
    cb = plt.colorbar(im, ax=ax[1], extend='both')
    cb.set_label('Temp diff (K)')
    f.set_tight_layout(True)
    
'''


def plot_cs_areas(cs_areas):
    ocean = '#4286f4'
    earth = '#db8553'
    cmap_disk = colors.ListedColormap([ocean, earth])
    bounds = [0, 0.5, 1]
    norm = colors.BoundaryNorm(bounds, cmap_disk.N)

    f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))
    for k in list(cs_areas.keys()):
        cs_extent = cs_areas[k] + np.array([50, -50, 50, -50])
        locs = {
            'nw': [cs_extent[i] for i in [0, 2]],
            'ne': [cs_extent[i] for i in [0, 3]],
            'se': [cs_extent[i] for i in [1, 3]],
            'sw': [cs_extent[i] for i in [1, 2]]
        }
        grid = np.empty((5, 2))
        for i, l in enumerate(locs.keys()):
            grid[i] = [getattr(gaf.AHILocation(locs[l]), x) for x in ['longitude', 'latitude']]
        grid[-1] = grid[0]
        ax.plot(grid[:, 0], grid[:, 1], transform=cartopy.crs.PlateCarree(), color='k')
        ax.text((locs['se'][1]-2750)*2000 - 70000, (-locs['se'][0]+2750)*2000 + 40000, k, transform=ahi_crs,
                fontsize=14, color='k')
    img_extent = [-5500000, 5500000, -5500000, 5500000]
    ax.imshow(cont_lv, cmap=cmap_disk, norm=norm, origin='upper', extent=img_extent, transform=ahi_crs)
    ax.gridlines(xlocs=np.arange(-180, 195, 15), ylocs=np.arange(-75, 85, 15))


def plot_indiv_training_pixels(pred_time, k, px_num):
    freq = '2h'
    df = pd.read_pickle(f'{gaf.IMAGEDIR}/P4/take_two/{freq}/{k}_{pred_time.year}_{pred_time.jday:03d}_'
                        f'{pred_time.hour:02d}_{pred_time.minute:02d}.df')
    # grab base sub_land (for eliminating sea tiles)
    sub_land = cont_lv[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
    bsub_land = sub_land[radius:-radius, radius:-radius]
    # num_land_pixels = np.count_nonzero(bsub_land)

    if freq[-3:] == 'min':
        minute_gap = int(freq[:-3])
    elif freq[-1:] == 'h':
        minute_gap = int(freq[:-1]) * 60
    elif freq[-1:] == 'd':
        minute_gap = int(freq[:-1]) * 1440
    else:
        print('make freq sensible please')
        sys.exit()

    # create t_list - list of times for prediction step
    t_list = [pred_time.dt - datetime.timedelta(minutes=int(minute_gap * i)) for i in np.arange(num_times + 1)]
    t_list.sort()

    # create storage for brightness temperature information
    bt_array = np.empty((num_times + 1, cs_areas[k][1] - cs_areas[k][0], cs_areas[k][3] - cs_areas[k][2]))
    bt_array[:] = np.nan
    for i, t in enumerate(t_list):
        a = gaf.AHICapture(t)
        try:
            bt = a.band7.data[cs_areas[k][0]:cs_areas[k][1], cs_areas[k][2]:cs_areas[k][3]]
        except AttributeError:
            print('No file available for {}'.format(a.time.dt))
            continue
        bt[~sub_land] = np.nan
        if use_cldmask:
            sub_k = k[:3]
            try:
                cld_path = os.path.join(gaf.IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                        '{:03d}'.format(a.time.jday), k[:3])
                cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                               a.time.minute)
                r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
            except IndexError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            except FileNotFoundError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            t_cld = r.variables['cloud_mask'][0, cs_areas[k][0] - cld_tl[sub_k][0]: cs_areas[k][1] - cld_tl[sub_k][0],
                                              cs_areas[k][2] - cld_tl[sub_k][1]: cs_areas[k][3] - cld_tl[sub_k][1]]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt.astype(np.float32)

    bt_pad = np.empty((bt_array.shape[0], bt_array.shape[1] + 1, bt_array.shape[2] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:, :-1, :-1] = bt_array.astype(np.float32)
    bt_pad[bt_pad > 400] = np.nan

    array_size = 50
    y_vals = x_vals = np.arange(array_size)
    coords = np.meshgrid(x_vals, y_vals)
    val_array = np.array([bt_pad[:, wy:wy + (radius * 2 + 1), wx:wx + (radius * 2 + 1)]
                          for wy in y_vals for wx in x_vals])
    va = val_array.swapaxes(0, 1).reshape(bt_array.shape[0], val_array.shape[0], -1)
    va[:, ~np.ravel(bsub_land), :] = np.nan
    cent_vals = va[:-1, :, va.shape[2] // 2]
    diffs = va[:-1] - cent_vals[:, :, np.newaxis]
    rms = np.nanmean(diffs ** 2, axis=0)
    amount_valid = np.count_nonzero(~np.isnan(diffs), axis=0)
    rms[:, np.ravel(dist > 50)] = 10000
    rms[amount_valid < 4] = 10000
    inds = rms.argsort()

    # TODO: total the rms[inds] and divide by 10000 - this will reveal how many are invalid
    # then use take to recover only the valid predictors somehow
    nan_inds = np.copy(inds).astype(float)
    nan_inds[~np.ravel(bsub_land)] = np.nan
    pred_vec = np.take_along_axis(va[-1], inds[:, 1:num_points + 1], 1)

    cont_inds = q[dist < 3].reshape(-1)
    cont_inds = np.delete(cont_inds, 12)
    cont_vec = np.take_along_axis(va[-1], cont_inds[np.newaxis, :], 1)

    pred_rms = np.take_along_axis(rms, inds[:, 1:num_points + 1], 1)
    pred_rms[pred_rms > 50] = np.nan

    cont_rms = np.take_along_axis(rms, cont_inds[np.newaxis, :], 1)
    cont_rms[cont_rms > 50] = np.nan

    ix = np.isin(inds, cont_inds)
    part_ix = ix[:, 1:num_points + 1]

    f = plt.figure(figsize=(18.6, 4.77))
    ax = plt.subplot2grid((1, 4), (0, 0))
    # loc = [df.x[px_num], df.y[px_num]]
    rms_px = rms[px_num]
    rms_px[rms_px > 200] = np.nan
    im = ax.imshow(rms_px.reshape(101, -1), vmax=50, cmap=cmo)
    ax.scatter(50, 50, s=8, c='r', zorder=2)
    ax.scatter(
        *[np.array([[(x % 101), (x // 101)] for x in df.loc[px_num].select_inds])[:, i] for i
          in np.arange(2)], s=8, c='b', zorder=1)
    ax.plot([47.5, 47.5, 52.5, 52.5, 47.5], [47.5, 52.5, 52.5, 47.5, 47.5], lw=1, c='g', zorder=2)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("bottom", size="3%", pad=0.07)
    cb = f.colorbar(im, cax=cax, orientation='horizontal')
    for l in cb.ax.xaxis.get_ticklabels():
        l.set_size(16)
    cb.set_label('Training RMSE (K)', fontsize=16)

    ax = plt.subplot2grid((1, 4), (0, 1), colspan=3)
    ax.plot(np.arange(48), va[:-1, px_num, 5100], zorder=2, lw=2, c='r')
    for val in np.arange(24):
        ax.plot(np.arange(48), va[:-1, px_num, inds[px_num, val + 1]], c='b', alpha=0.2, zorder=1)
    for val in cont_inds:
        ax.plot(np.arange(48), va[:-1, px_num, val], c='g', alpha=0.2, zorder=1)
    ax.scatter(48, va[-1, px_num, 5100], s=16, c='r', zorder=2)
    ax.scatter(np.zeros(24) + 48.3, va[-1, px_num, inds[px_num, 1:25]], s=6, c='b', alpha=0.2)
    ax.scatter(48, np.nanmean(va[-1, px_num, inds[px_num, 1:25]]), s=24, c='b', marker='x', zorder=1)
    ax.scatter(np.zeros(24) + 47.7, va[-1, px_num, cont_inds], s=6, c='g', alpha=0.2)
    ax.scatter(48, np.nanmean(va[-1, px_num, cont_inds]), s=24, c='g', marker='x', zorder=1)
    ax.set_xlabel('Time', fontsize=16)
    ax.set_ylabel('Temperature (K)', fontsize=16)
    custom_lines = [Line2D([0], [0], c='r'), Line2D([0], [0], c='b'), Line2D([0], [0], c='g')]
    ax.legend(handles=custom_lines, labels=['Target BT', 'STS Training BT', 'Context BT'], fontsize=14)
    ax.set_xticks(np.arange(5)*12)
    ax.set_xlim(-1, 49)
    ax.set_xticklabels(['$-$96h', '$-$72h', '$-$48h', '$-$24h', 't'], fontsize=16)
    ax.yaxis.set_ticks(
        np.arange(np.ceil(ax.get_ybound()[0] / 10) * 10, 10 + np.floor(ax.get_ybound()[1] / 10) * 10, 10))
    for l in ax.yaxis.get_ticklabels():
        l.set_size(16)
    ax.grid()
    f.set_tight_layout(True)
    f.savefig(f'{gaf.IMAGEDIR}/P4/paper_figures/{k}_{pred_time.year}_{pred_time.jday:03d}_{pred_time.hour:02d}_'
              f'{pred_time.minute:02d}_{px_num:04d}_traj.png',
              format='png', dpi=600)
    plt.close(f)



def more_proc():
    proc_list = [
        'jpn_b', 'jpn_j', 'jpn_n', 'jpn_f', 'jpn_e',
        'sib_b', 'sib_m', 'sib_d', 'sib_l', 'sib_j',
        'chn_k', 'chn_a', 'chn_e', 'chn_b',
        'bor_l', 'bor_h', 'bor_f', 'bor_j', 'bor_p',
        'nwa_e', 'nwa_q', 'nwa_c', 'sea_j', 'sea_c'
    ]
    proc_sets(proc_list)


if __name__ == '__main__':
    more_proc()


