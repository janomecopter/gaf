import gaf
import numpy as np
from netCDF4 import Dataset
import os
import matplotlib.pyplot as plt


def nc_write(fpath, data):
    if not os.path.isdir(os.path.dirname(fpath)):
        os.makedirs(os.path.dirname(fpath))
    r = Dataset(fpath, 'w')
    r.createDimension('x', data.shape[1])
    r.createDimension('y', data.shape[0])
    var = r.createVariable('data', 'f4', ('x', 'y',), zlib=True)
    var[:] = data
    var.comment = 'context statistics for background temperature - {}'.format(os.path.basename(fpath))
    r.instrument = 'AHI-8'
    r.creator_name = 'RMIT Geospatial Science'
    r.creator_email = 'bryan.hally@rmit.edu.au'
    r.close()
    print('{} saved'.format(os.path.basename(fpath)))


def ind_picker(lvl):
    top = np.arange(lvl) - lvl // 2 * window - lvl // 2
    bot = np.arange(lvl) + lvl // 2 * window - lvl // 2
    number_sides = lvl - 2
    for i, item in enumerate(np.arange(number_sides) - (number_sides // 2)):
        if not i:
            side = np.array([item * window - lvl // 2, item * window + lvl // 2])
        else:
            side = np.concatenate((side, np.array([item * window - lvl // 2, item * window + lvl // 2])))
    return np.concatenate((top, side, bot)) + (window ** 2 // 2)


savepath = r'C:/Scratch/cache/P3'
band7 = True
window = w = 15
chunk_size = cs = 500
no_chunks = 5500 // cs
land = gaf.get_land_mask()
va = gaf.get_viewangle()
flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(5500, -1)
# break into 500 pixel subsets by land > 5
blks = np.column_stack(np.where(np.array([np.count_nonzero(flt_land[y - cs:y, x - cs:x]) > 5
                                          for y in np.arange(0, 5500, cs) + cs
                                          for x in np.arange(0, 5500, cs) + cs]).reshape(no_chunks, -1)))
d = [[2016, 71, 2, 0], blks[40]]
a = gaf.AHICapture(d[0])
y, x = d[1]
if y == 0:
    y_range = [0, cs + w // 2]
elif y == 10:
    y_range = [5500 - cs - (w // 2), 5500]
else:
    y_range = [y * cs - (w // 2), (y + 1) * cs + (w // 2)]
if x == 0:
    x_range = [0, cs + w // 2]
elif x == 10:
    x_range = [5500 - cs - (w // 2), 5500]
else:
    x_range = [x * cs - (w // 2), (x + 1) * cs + (w // 2)]

if band7:
    band = a.band7
else:
    band = a.band13
bt = band.data[y_range[0]:y_range[1], x_range[0]:x_range[1]]
# mask land and va>80
sub_land = flt_land[y_range[0]:y_range[1], x_range[0]:x_range[1]]
bt[~sub_land] = np.nan
# import cloud product
try:
    r = Dataset(os.path.join(gaf.IMAGEDIR_AHI, 'CLD', '{}'.format(a.time.year), '{:03d}'.format(a.time.jday),
                             '{}{:02d}{:02d}{:02d}0000-P1S-ABOM_OBS_CLOUDMASK-PRJ_GEOS141_2000-'
                             'HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month, a.time.day, a.time.hour)))
    cld = (r['cloud_mask'][0, y_range[0]:y_range[1], x_range[0]:x_range[1]]) > 0
    bt[cld] = np.nan
except:
    print('No cloud data available for 2016{:02d}{:02d}'.format(a.time.month, a.time.day))
    raise

# pad zeros to get rid of indexing issue
bt_pad = np.empty((bt.shape[0] + 1, bt.shape[1] + 1,))
bt_pad[:] = np.nan
bt_pad[:-1, :-1] = bt

val_array = np.array([bt_pad[wx:wx - window, wy:wy - window]
                      for wx in np.arange(w) for wy in np.arange(w)])
val_array = np.swapaxes(val_array, 0, 2)
val_array = np.swapaxes(val_array, 0, 1)

vals = {}
for value in np.arange(5, window + 2, 2):
    # merge lvls 3 and 5
    if value == 5:
        inds = np.concatenate((ind_picker(value - 2), ind_picker(value)))
    else:
        inds = ind_picker(value)
    vals[value] = np.take(val_array, inds, axis=2)
# separate out grid for analysis
bt_focus = bt[w // 2:-(w // 2), w // 2:-(w // 2)]


value = 9
max_window_items = 24
pc = 0.15
fpref = '{}{:03d}-{:02d}00-B{:02d}-[{},{}]'.format(a.time.year, a.time.jday, a.time.hour, band.band,
                                                           y, x)
window_diffs = vals[value] - bt_focus[:, :, np.newaxis]
window_diffs_abs = np.abs(window_diffs)

mn = int(pc * max_window_items)
num_nan = np.sum(np.isnan(window_diffs), axis=2)
wind_diff_sort = np.sort(window_diffs, axis=2)
wind_abs_sort = np.sort(window_diffs_abs, axis=2)

neg_mean = np.mean(wind_diff_sort[:, :, :mn], axis=2)
best_mean = np.mean(wind_abs_sort[:, :, :mn], axis=2)
row_idx = np.repeat(np.arange(cs**2)[np.newaxis, :], mn).reshape(cs**2, -1)
X = -np.arange(mn) + max_window_items - 1
bad_picks = (np.broadcast_to(X, (cs, cs, mn)) - num_nan[:, :, np.newaxis]).reshape(cs**2, -1)
pos_mean = np.nanmean(wind_diff_sort.reshape(cs**2, -1)[row_idx, bad_picks], axis=1).reshape(cs, -1)
worst_mean = np.nanmean(wind_abs_sort.reshape(cs**2, -1)[row_idx, bad_picks], axis=1).reshape(cs, -1)

nc_write(os.path.join(savepath, fpref + '-w{:02d}-neg_mean-{:.2f}.nc'.format(value, pc)), neg_mean)
nc_write(os.path.join(savepath, fpref + '-w{:02d}-pos_mean-{:.2f}.nc'.format(value, pc)), pos_mean)
nc_write(os.path.join(savepath, fpref + '-w{:02d}-best_mean-{:.2f}.nc'.format(value, pc)), best_mean)
nc_write(os.path.join(savepath, fpref + '-w{:02d}-worst_mean-{:.2f}.nc'.format(value, pc)), worst_mean)


# growing window analysis
if value != 5:
    for pc in np.arange(7) * 0.10 + 0.15:
        #bad_list = np.array([np.newaxis,np.newaxis])
        #good_list = np.array(())
        bad_list = 0
        good_list = 0
        for lw in np.arange(5, value + 2, 2):
            #  need no. of values less than trigger in each window size
            if lw == 5:
                lw_window_items = 24
            else:
                lw_window_items = (lw // 2) * 8 # number of items in lw iterable window
            tpc = (lw**2) - 1   # total pixels inside lw window
            vpc = int((pc * lw_window_items)-0.005) # highest number of pixels to trigger invalid response at lw window
            ttc = int(tpc * pc)
            lw_diffs = vals[lw] - bt_focus[:, :, np.newaxis] # diffs for this lw window
            lw_diffs_abs = np.sort(np.abs(lw_diffs)) # abs diffs for lw window

            num_nan = np.sum(np.isnan(lw_diffs), axis=2) # work out how many nans live in each pixel list
            # create the selection index arrays - firstly propagate the row indices
            row_idx = np.repeat(np.arange(cs ** 2)[np.newaxis, :], vpc).reshape(cs ** 2, -1)
            # create aranges for the values to be separated from the lw array
            X = -np.arange(vpc) + lw_window_items - 1
            # broadcast to cs * cs * vpc, take away the nan values, reshape to cs**2
            bad_picks = (np.broadcast_to(X, (cs, cs, vpc)) - num_nan[:, :, np.newaxis]).reshape(cs ** 2, -1)

            # nans will not be eliminated at this time - we have to try to get rid of them
            if not isinstance(bad_list, np.ndarray):
                bad_list = lw_diffs_abs.reshape(cs**2, -1)[row_idx, bad_picks]
            else:
                bad_list = np.concatenate((bad_list, lw_diffs_abs.reshape(cs**2, -1)[row_idx, bad_picks]), axis=1)
            if not isinstance(good_list, np.ndarray):
                good_list = lw_diffs_abs.reshape(cs**2, -1)[:, :vpc]
            else:
                good_list = np.concatenate((good_list, lw_diffs_abs.reshape(cs**2, -1)[:, :vpc]), axis=1)
            if lw != value:
                bad_list = np.sort(bad_list, axis=1)[:, -(ttc+1):]
                good_list = np.sort(good_list, axis=1)[:, :(ttc-1)]
            else:
                bad_list = np.sort(bad_list, axis=1)[:, -ttc:]
                good_list = np.sort(good_list, axis=1)[:, :ttc]
        best_mean_sw = np.mean(good_list, axis=1).reshape(500, -1)
        worst_mean_sw = np.mean(bad_list, axis=1).reshape(500, -1)
        nc_write(os.path.join(savepath, fpref + '-lw{:02d}-{:.2f}-worst_mean.nc'.format(lw, pc)), worst_mean_sw)
        nc_write(os.path.join(savepath, fpref + '-lw{:02d}-{:.2f}-best_mean.nc'.format(lw, pc)), best_mean_sw)
