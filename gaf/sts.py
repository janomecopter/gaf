import numpy as np
from netCDF4 import Dataset
import os
import sys
import pandas as pd
import cartopy
import datetime
import pickle
import platform
from multiprocessing import Pool

# gaf imports
from gaf import IMAGEDIR, Time
from gaf.ahi import get_viewangle, get_land_mask, AHICapture, AHILocation

ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)


# Functions
def load_cont_lv(savepath):
    va = get_viewangle()
    if not os.path.isfile(os.path.join(savepath, 'cont_lv.npy')):
        array_size = ars = 5500
        land = get_land_mask()
        flt_land = np.all((np.ravel(land == 0), np.ravel(va < 80)), axis=0).reshape(ars, -1)
        a = AHICapture((2016, 6, 1, 0))
        file_kw = 'B07-w05-counts_valid'
        temp = Dataset(os.path.join(savepath, '{0}{1:03d}-{2:02d}00-{3}.nc'.format(
            a.time.year, a.time.jday, a.time.hour, file_kw))).variables['data'][:, :]
        # nan out values in counts_valid that aren't land
        temp[~flt_land] = np.nan
        # copy the land mask into a fresh array
        array = np.copy(flt_land)
        array[np.isnan(temp)] = False
        np.save(os.path.join(savepath, 'cont_lv.npy'), array)
    else:
        array = np.load(os.path.join(savepath, 'cont_lv.npy'))
    array = np.ma.array(array, mask=va > 80)
    return array


def get_top_left_of_subset_cloud(key):
    cld_tl = {'sea': [4350, 3000], 'nwa': [3550, 1950], 'bor': [2550, 1350], 'thl': [1750, 750],
              'chn': [950, 1550], 'jpn': [850, 2450], 'sib': [150, 1950]}
    return cld_tl[key]


def get_cs_areas():
    cs_areas = {
        'sea': [4350, 4650, 3000, 3300],
        'nwa': [3550, 3850, 1950, 2250],
        'bor': [2550, 2850, 1350, 1650],
        'thl': [1750, 2050, 750, 1050],
        'chn': [950, 1250, 1550, 1850],
        'jpn': [850, 1150, 2450, 2750],
        'sib': [150, 450, 1950, 2250]
    }
    with open(os.path.join(os.path.join(IMAGEDIR, 'P4'), 'ncs_areas.pkl'), 'rb') as f:
        ncs_areas = pickle.load(f)
    cs_areas.update(ncs_areas)
    return cs_areas


def gather_selection_rms(end_time_prediction, number_of_selections, frequency, sample_area, search_radius, area_key,
                         use_cldmask=True, cont_lv=load_cont_lv(os.path.join(IMAGEDIR, 'P4'))):
    """This function returns a list of selected pixels for STS estimation at a point after end_time_prediction

    :parameter
    ----------
    end_time_prediction : gaf.Time
        the time of the last image in the prediction set
    number_of_selections : n (int)
        the number of images the search goes back for
    frequency : mins (int)
        the time spacing of the selection images
    sample_area : [min_y (int), max_y (int), min_x (int), max_x (int)]
        the bounds of the area being sampled in image coordinates
    search_radius : int
        the search area for gathering selections

    :returns
    np.array (sample_area_y x sample_area_x x number_of_selections x 2)
        relative coordinate array of selection pixels
    """
    # create image area for selection array
    if type(sample_area) is not np.array:
        sample_area = np.array(sample_area)
    image_area = sample_area + np.array([-1, 1, -1, 1]) * search_radius

    # get land/sea mask info for sample area
    sub_land = cont_lv[image_area[0]:image_area[1], image_area[2]:image_area[3]]
    bsub_land = cont_lv[sample_area[0]:sample_area[1], sample_area[2]:sample_area[3]]

    # create storage for brightness temperature information
    bt_array = np.empty((number_of_selections, image_area[1] - image_area[0], image_area[3] - image_area[2]))
    bt_array[:] = np.nan

    image_time_list = [end_time_prediction.dt - datetime.timedelta(minutes=int(frequency * i)) for i in
                       np.arange(number_of_selections)]
    image_time_list.sort()

    # stack brightness temperatures
    sub_k = area_key[:3]
    cld_tl = get_top_left_of_subset_cloud(sub_k)
    for i, t in enumerate(image_time_list):
        a = AHICapture(t)
        try:
            bt = a.band7.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
        except AttributeError:
            print('No file available for {}_{:03d}_{:02d}_{:02d}'.format(
                *[getattr(a.time, i) for i in ['year', 'jday', 'hour', 'minute']]))
            continue
        bt[~sub_land] = np.nan

        if use_cldmask:
            try:
                cld_path = os.path.join(IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                        '{:03d}'.format(a.time.jday), area_key[:3])
                cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                               a.time.minute)
                r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
            except IndexError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            except FileNotFoundError:
                print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
                continue
            t_cld = r.variables['cloud_mask'][0, image_area[0] - cld_tl[0]: image_area[1] - cld_tl[0],
                                              image_area[2] - cld_tl[1]: image_area[3] - cld_tl[1]]
            bt[t_cld == 1] = np.nan
        bt_array[i] = bt.astype(np.float32)

    # pad bt_array to help the slicing work properly
    bt_pad = np.empty((bt_array.shape[0], bt_array.shape[1] + 1, bt_array.shape[2] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:, :-1, :-1] = bt_array.astype(np.float32)
    bt_pad[bt_pad > 400] = np.nan

    arraywidth = 2 * search_radius + 1
    q = np.arange(arraywidth ** 2).reshape(arraywidth, -1)
    dist = np.sqrt((q // arraywidth - search_radius) ** 2 + (q % arraywidth - search_radius) ** 2)

    # create a meshed coordinate array based upon the sample area size
    y_vals = np.arange(sample_area[1] - sample_area[0])
    x_vals = np.arange(sample_area[3] - sample_area[2])
    coords = np.meshgrid(x_vals, y_vals)
    val_array = np.array([bt_pad[:, wy:wy + arraywidth, wx:wx + arraywidth] for wy in y_vals for wx in x_vals])
    va = val_array.swapaxes(0, 1).reshape(bt_array.shape[0], val_array.shape[0], -1)
    va[:, ~np.ravel(bsub_land), :] = np.nan
    cent_vals = va[:, :, va.shape[2] // 2]
    diffs = va[:] - cent_vals[:, :, np.newaxis]
    rms = np.nanmean(diffs ** 2, axis=0)
    amount_valid = np.count_nonzero(~np.isnan(diffs), axis=0)
    rms[:, np.ravel(dist > 50)] = 10000
    rms[amount_valid < 4] = 10000
    # inds = rms.argsort()
    # return inds
    return rms


def make_prediction(prediction_time, sample_area, search_radius, selection_rms, area_key, num_predictors,
                    use_cldmask=True, cont_lv=load_cont_lv(os.path.join(IMAGEDIR, 'P4'))):
    """This function makes a prediction of the brightness temperature based upon an input array of selection locations

    :parameter
    pred_time : gaf.Time
        time of image prediction
    sample_area : [min_y (int), max_y (int), min_x (int), max_x (int)]
        the bounds of the area being sampled in image coordinates
    search_radius : int
        the search area for gathering selections
    selection_array :

    """
    if type(sample_area) is not np.array:
        sample_area = np.array(sample_area)
    image_area = sample_area + np.array([-1, 1, -1, 1]) * search_radius

    # get land/sea mask info for sample area
    sub_land = cont_lv[image_area[0]:image_area[1], image_area[2]:image_area[3]]
    bsub_land = cont_lv[sample_area[0]:sample_area[1], sample_area[2]:sample_area[3]]

    sub_k = area_key[:3]
    cld_tl = get_top_left_of_subset_cloud(sub_k)
    a = AHICapture(prediction_time)
    try:
        bt7 = a.band7.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
        bt13 = a.band13.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
        solar_zenith = a.solarza.data[image_area[0]:image_area[1], image_area[2]:image_area[3]]
    except AttributeError:
        print('No file available for {}'.format(a.time.dt))
        return 0
    bt7[~sub_land] = np.nan
    if use_cldmask:
        try:
            cld_path = os.path.join(IMAGEDIR, 'AHI', 'CLD', '{}'.format(a.time.year),
                                    '{:03d}'.format(a.time.jday), area_key[:3])
            cld_name = '{}{:02d}{:02d}{:02d}{:02d}'.format(a.time.year, a.time.month, a.time.day, a.time.hour,
                                                           a.time.minute)
            r = Dataset(os.path.join(cld_path, [f for f in os.listdir(cld_path) if cld_name in f][0]))
        except IndexError:
            print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
            return 0
        except FileNotFoundError:
            print('No cloud data for 2016{:03d} - {:02d}:{:02d}'.format(a.time.jday, a.time.hour, a.time.minute))
            return 0
        t_cld = r.variables['cloud_mask'][0, image_area[0] - cld_tl[0]: image_area[1] - cld_tl[0],
                                          image_area[2] - cld_tl[1]: image_area[3] - cld_tl[1]]
        bt7[t_cld == 1] = np.nan

    # apply tests as per MODIS fire product outlier exclusion (Giglio 2003)
    delta_t = bt7 - bt13
    bt7[np.logical_and.reduce((solar_zenith < 85, delta_t > 20, bt7 > 325))] = np.nan
    bt7[np.logical_and.reduce((solar_zenith >= 85, delta_t > 10, bt7 > 310))] = np.nan

    bt_pad = np.empty((bt7.shape[0] + 1, bt7.shape[1] + 1,))
    bt_pad[:] = np.nan
    bt_pad[:-1, :-1] = bt7.astype(np.float32)
    bt_pad[bt_pad > 400] = np.nan

    arraywidth = 2 * search_radius + 1
    q = np.arange(arraywidth ** 2).reshape(arraywidth, -1)
    dist = np.sqrt((q // arraywidth - search_radius) ** 2 + (q % arraywidth - search_radius) ** 2)

    # create a meshed coordinate array based upon the sample area size
    y_vals = np.arange(sample_area[1] - sample_area[0])
    x_vals = np.arange(sample_area[3] - sample_area[2])
    coords = np.meshgrid(x_vals, y_vals)

    val_array = np.array([bt_pad[wy:wy + arraywidth, wx:wx + arraywidth] for wy in y_vals for wx in x_vals]).reshape(
        len(x_vals) * len(y_vals), -1)
    val_array[~np.ravel(bsub_land), :] = np.nan
    sensor_bt7 = np.copy(val_array[:, val_array.shape[1] // 2])

    selection_array = selection_rms.argsort(axis=1)
    large_pred_vec = np.take_along_axis(val_array, selection_array[:, 1:num_predictors * 40 + 1], 1)
    large_sts_rms = np.take_along_axis(selection_rms, selection_array[:, 1:num_predictors * 40 + 1], 1)
    large_sts_dist = np.take_along_axis(np.tile(np.ravel(dist), (selection_array.shape[0], 1)),
                                        selection_array[:, 1:num_predictors * 40 + 1], 1)
    large_pred_vec[large_sts_rms == 10000] = np.nan
    large_sts_dist[large_sts_rms == 10000] = np.nan

    norm_pred_vec = large_pred_vec[:, :num_predictors]
    sts_mean = np.nanmean(norm_pred_vec, 1)
    sts_rms = large_sts_rms[:, :num_predictors]
    # sts_rms = np.ma.array(sts_rms, mask=sts_rms == 10000)
    # sts_rms[sts_rms == 10000] = np.nan
    sts_rms_mean = np.nanmean(sts_rms, axis=1)
    sts_raw_dist = np.nanmean(large_sts_dist[:, :num_predictors], axis=1)
    sts_valid_count = np.count_nonzero(~np.isnan(norm_pred_vec), axis=1)
    sts_diff = sensor_bt7 - sts_mean

    filt_mask = np.logical_or(np.isnan(norm_pred_vec),
                              np.abs(norm_pred_vec - np.nanmean(norm_pred_vec, axis=1)[:, np.newaxis])
                              > 2 * np.nanstd(norm_pred_vec, axis=1)[:, np.newaxis])
    sts_filt_mean = np.ma.mean(np.ma.masked_array(norm_pred_vec, mask=filt_mask), axis=1)
    sts_filt_mean[sts_filt_mean < 200] = np.nan
    sts_filt_diff = sensor_bt7 - sts_filt_mean

    cont_inds = q[dist < 3].reshape(-1)
    cont_inds = np.delete(cont_inds, len(cont_inds) // 2)
    cont_vec = np.take_along_axis(val_array, cont_inds[np.newaxis, :], 1)
    cont_valid_count = np.count_nonzero(~np.isnan(cont_vec), axis=1)
    cont_mean = np.nanmean(cont_vec, 1)
    cont_diff = sensor_bt7 - cont_mean
    filt_mask = np.logical_or(np.isnan(cont_vec), np.abs(cont_vec - np.nanmean(cont_vec, axis=1)[:, np.newaxis])
                              > 2 * np.nanstd(cont_vec, axis=1)[:, np.newaxis])
    cont_filt_mean = np.ma.mean(np.ma.masked_array(cont_vec, mask=filt_mask), axis=1)
    cont_filt_mean[cont_filt_mean < 200] = np.nan
    cont_filt_diff = sensor_bt7 - cont_filt_mean
    cont_rms = np.take_along_axis(selection_rms, cont_inds[np.newaxis, :], 1)
    cont_rms[cont_rms == 10000] = np.nan
    cont_rms_mean = np.nanmean(cont_rms, axis=1)

    sts_full_pred = np.zeros(len(x_vals) * len(y_vals))
    sts_full_rms = np.zeros(len(x_vals) * len(y_vals))
    sts_full_count = np.zeros(len(x_vals) * len(y_vals))
    sts_full_dist = np.zeros(len(x_vals) * len(y_vals))

    for i, vec in enumerate(large_pred_vec):
        fully_valid_vec = vec[~np.isnan(vec)][:num_predictors]
        mask = np.logical_or(np.isnan(fully_valid_vec),
                             np.abs(fully_valid_vec - np.nanmean(fully_valid_vec)) > 2 * np.nanstd(fully_valid_vec))
        full_pred_mean = np.ma.mean(np.ma.masked_array(fully_valid_vec, mask=mask))
        sts_full_count[i] = np.count_nonzero(~mask)

        fully_valid_rms = large_sts_rms[i, ~np.isnan(vec)][:num_predictors]
        # fully_valid_rms[fully_valid_rms > 50] = np.nan
        sts_full_rms[i] = np.ma.mean(np.ma.masked_array(
            fully_valid_rms, mask=np.logical_or(mask, np.isnan(fully_valid_rms))))
        full_valid_dist = large_sts_dist[i, ~np.isnan(vec)][:num_predictors]
        sts_full_dist[i] = np.ma.mean(np.ma.masked_array(
            full_valid_dist, mask=np.logical_or(mask, np.isnan(fully_valid_rms))))
        if full_pred_mean < 200:
            sts_full_pred[i] = np.nan
        else:
            sts_full_pred[i] = full_pred_mean
    sts_full_diff = sensor_bt7 - sts_full_pred

    df = pd.DataFrame(columns=['pred_time'])
    df['x'] = np.tile(x_vals + sample_area[2], y_vals.shape[0])
    df['y'] = np.repeat(y_vals + sample_area[0], x_vals.shape[0])
    df['sensor_bt7'] = sensor_bt7
    df['sts_raw_mean'] = sts_mean
    df['sts_raw_diff'] = sts_diff
    df['sts_raw_dist'] = sts_raw_dist
    df['sts_rms_mean'] = sts_rms_mean
    df['sts_filt_mean'] = sts_filt_mean
    df['sts_filt_diff'] = sts_filt_diff
    df['sts_valid_count'] = sts_valid_count
    df['cont_raw_mean'] = cont_mean
    df['cont_raw_diff'] = cont_diff
    df['cont_rms_mean'] = cont_rms_mean
    df['cont_filt_mean'] = cont_filt_mean
    df['cont_filt_diff'] = cont_filt_diff
    df['cont_valid_count'] = cont_valid_count
    df['sts_full_mean'] = sts_full_pred
    df['sts_full_diff'] = sts_full_diff
    df['sts_full_rms'] = sts_full_rms
    df['sts_full_count'] = sts_full_count
    df['sts_full_dist'] = sts_full_dist
    df['pred_time'].iloc[:] = prediction_time
    pkl_name = '{}_{}_{:03d}_{:02d}_{:02d}.df'.format(area_key, a.time.year, a.time.jday, a.time.hour, a.time.minute)
    df.to_pickle(os.path.join('/mnt/solid_store/data/sts_xtras', pkl_name))
    print('Prediction {} complete'.format(pkl_name))


def set_times(area_key):
    # set up time array - array hardcoded by np.random.choice(np.arange(6)*10, 4*31)
    days = np.arange(31) + cs_jday[area_key[:3]] - 15
    hrs = np.array([0, 9, 12, 15])
    # no 40's here because of the stationkeeping times, 30's due to missing cloud masks
    mins = np.array(
        [0, 50, 0, 0, 20, 50, 50, 10, 50, 20, 50, 50, 10, 10, 20, 0, 20, 20, 50, 10, 0, 0, 50, 10, 50, 50, 10, 50, 0,
         20, 20, 50, 20, 50, 50, 20, 10, 50, 0, 10, 10, 10, 20, 10, 20, 50, 50, 0, 0, 50, 50, 50, 50, 0, 0, 0, 10, 0,
         20, 10, 20, 0, 0, 0, 50, 10, 10, 20, 20, 20, 10, 10, 20, 50, 20, 50, 0, 50, 20, 20, 20, 20, 20, 0, 0, 10, 20,
         20, 50, 10, 20, 20, 10, 10, 20, 10, 10, 50, 20, 20, 10, 0, 50, 50, 50, 0, 0, 10, 20, 50, 0, 0, 20, 20, 10, 0,
         10, 0, 50, 20, 0, 10, 50, 10])
    t_offset = int(np.floor(AHILocation((np.mean(cs_areas[area_key][:2]),
                                         np.mean(cs_areas[area_key][2:]))).time_offset/60 + 0.5))
    dh_list = np.array([[2016, d, h] for d in days for h in hrs])
    local_t_list = np.append(dh_list, mins[:, np.newaxis], axis=1)
    utc_t_list = np.array([Time(Time(i).dt - datetime.timedelta(hours=t_offset)) for i in local_t_list])
    return utc_t_list


def save_rms_to_nc(filename, array):
    r = Dataset(filename, 'w')
    r.createDimension('cells', array.shape[0])
    r.createDimension('predictors', array.shape[1])
    data = r.createVariable('rms', 'f4', ('cells', 'predictors',), zlib=True)
    data[:] = array
    data.comment = 'RMS data based upon 48 x 2hr analysis of differences up to the time in the filename'
    r.instrument = 'AHI'
    r.creator_name = 'RMIT Geospatial Science'
    r.creator_email = 'bryan.hally@rmit.edu.au'
    r.close()


def multiprocess_main_loop(input_data):
    area_key, t = input_data
    sample_area = np.array(cs_areas[area_key]) + np.array([50, -50, 50, -50])
    selection_time = Time(t.dt - datetime.timedelta(hours=2))
    try:
        selection_rms = Dataset(os.path.join(
            '/mnt/solid_store/data/sts_run3', '{}_{}_{:03d}_{:02d}_{:02d}_rms.nc'.format(
                area_key, *[getattr(selection_time, item) for item in ['year', 'jday', 'hour', 'minute']]
                                             ))).variables['rms'][:]
    except:
        return 0
    search_radius = 50
    num_predictors = 24
    make_prediction(t, sample_area, search_radius, selection_rms, area_key, num_predictors)


if __name__ == '__main__':
    #cont_lv = load_cont_lv(os.path.join(IMAGEDIR, 'P4'))
    #use_cldmask = True
    klist = ['sea_e', 'bor_g', 'sib_r', 'thl_m', 'nwa_b', 'nwa_g', 'thl_c', 'thl_q', 'chn_m', 'nwa_p', 'chn_g', 'thl_p',
             'sea_h', 'sib_n', 'nwa_f', 'thl_j', 'sea_b', 'thl_a', 'bor_a', 'chn_q', 'jpn_b', 'jpn_j', 'jpn_n', 'jpn_f',
             'jpn_e', 'sib_b', 'sib_m', 'sib_d', 'sib_l', 'sib_j', 'chn_k', 'chn_a', 'chn_e', 'chn_b', 'bor_l', 'bor_h',
             'bor_f', 'bor_j', 'bor_p', 'nwa_e', 'nwa_q', 'nwa_c', 'sea_j', 'sea_c', 'thl_k', 'sea_f', 'sea_k', 'jpn_k',
             'jpn_p'
             ]
    cs_jday = {'sea': 105, 'nwa': 312, 'bor': 60, 'thl': 74, 'chn': 255, 'jpn': 139, 'sib': 146}
    cs_areas = get_cs_areas()
    number_of_selections = 48
    frequency = 120
    # search_radius = 50
    # num_predictors = 24
    # for k in klist:
    #     times = set_times(k[:3])
    #     sample_area = np.array(cs_areas[k]) + np.array([50, -50, 50, -50])
    #     for t in times:
    #         predictor_time = Time(t.dt - datetime.timedelta(hours=2))
    #         if np.logical_or(os.path.isfile(os.path.join(
    #                 IMAGEDIR, 'P4', 'run_3', '{}_{}_{:03d}_{:02d}_{:02d}.df'.format(
    #                     k, *[getattr(t, i) for i in ['year', 'jday', 'hour', 'minute']]))),
    #             os.path.isfile(os.path.join(
    #                 IMAGEDIR, 'P4', 'run_3', '{}_{}_{:03d}_{:02d}_{:02d}_rms.nc'.format(
    #                     k, *[getattr(predictor_time, i) for i in ['year', 'jday', 'hour', 'minute']])))):
    #             continue
    #         selection_rms = gather_selection_rms(predictor_time, number_of_selections, frequency, sample_area,
    #                                              search_radius, k)
    #         selection_rms = Dataset(os.path.join(IMAGEDIR, 'P4', 'run_3', '{}_{}_{:03d}_{:02d}_{:02d}_rms.nc'.format(
    #             k, *[getattr(predictor_time, i) for i in ['year', 'jday', 'hour', 'minute']]))).variables['rms'][:]
    #         save_rms_to_nc(os.path.join(IMAGEDIR, 'P4', 'run_3', '{}_{}_{:03d}_{:02d}_{:02d}_rms.nc'.format(
    #             k, *[getattr(predictor_time, i) for i in ['year', 'jday', 'hour', 'minute']])), selection_rms)
    #         make_prediction(t, sample_area, search_radius, selection_rms, k, num_predictors)

    process_list = []
    for i, k in enumerate(klist):
        times = set_times(k[:3])
        if not i:
            process_list = [[k, t] for t in times]
        else:
            process_list.extend([[k, t] for t in times])

    p = Pool(8)
    p.map(multiprocess_main_loop, (i for i in process_list))
