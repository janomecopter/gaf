# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 18:59:48 2017

@author: e76243
"""
import platform
import os

DATADIR = os.path.join(os.path.normpath(os.path.join(__file__, os.pardir, os.pardir)), 'data')
if not os.path.isdir(DATADIR):
    os.makedirs(DATADIR)

if 'Windows' in platform.platform():
    # platform.node()=='W8404669':
    # defpath = r'C:\Scratch\temp'
    # coordpath = r'H:\Python\AHI'
    from .tools import win_disk_labeller

    drive_label, path = win_disk_labeller()
    if drive_label != '':
        IMAGEDIR = r'{}'.format(path)
    else:
        IMAGEDIR = r'C:\Scratch\cache'
        print('No hard drive - IMAGEDIR @ {}\n'.format(IMAGEDIR))
    # outpath = r'C:\Scratch\temp'
elif platform.node() == 'bryan-X555LJ':
    # defpath = '/home/bryan/Research/Satfire/temp'
    # coordpath = '/home/bryan/Research/Satfire/AHI'
    if os.path.exists('/media/bryan/Sunflower Bucket'):
        IMAGEDIR = '/media/bryan/Sunflower Bucket'
        # drive_label = 'Sunflower Bucket'
    elif os.path.exists('/media/bryan/Sunflower Vase'):
        IMAGEDIR = '/media/bryan/Sunflower Vase'
        # drive_label = 'Sunflower Vase'
    else:
        IMAGEDIR = '/mnt/storage/Himawari'
        print('Warning - Fix pathing to external drives before continuing.')
        # raise ImportError('Fix pathing to external drives before continuing.')
    # outpath = '/home/bryan/Research/Satfire/temp'
elif platform.node() == 'smgs-seo-sfmbox':
    # defpath = '/mnt/storage/Himawari/temp'
    # coordpath = '/mnt/storage/Himawari'
    if os.path.exists('/media/sfm-user/Sunflower'):
        IMAGEDIR = '/media/sfm-user/Sunflower'
        # drive_label = 'Sunflower Bucket'
    # elif os.path.exists('/media/sfm-user/Sunflower Vase'):
    # IMAGEDIR = '/media/sfm-user/Sunflower Vase'
    # drive_label = 'Sunflower Vase'
    else:
        IMAGEDIR = '/mnt/storage/Himawari'
        print('Warning - Fix pathing to external drives before continuing.')
        # raise ImportError('Fix pathing to external drives before continuing.')
    # outpath = '/mnt/storage/Himawari/temp'
elif platform.node() == 'smgs-seo-sfmdos':
    if os.path.exists('/media/sfm-user/Sunflower'):
        IMAGEDIR = '/media/sfm-user/Sunflower'
    else:
        IMAGEDIR = '/mnt/Storage/active-fire'
elif '.'.join(platform.node().split('.')[1:]) == 'eres.rmit.edu.au':
    IMAGEDIR = '/active-fire'
elif platform.node() == 'smgs-seo-bryan':
    if os.path.exists('/media/sfm-user/Sunflower'):
        IMAGEDIR = '/media/sfm-user/Sunflower'
    else:
        IMAGEDIR = '/home/bryan/data'
elif platform.node() == 'smgs-seo-samurai':
    if os.path.exists('/media/e76243/Sunflower'):
        IMAGEDIR = '/media/e76243/Sunflower'
    else:
        IMAGEDIR = '/mnt/spinny_store/BH/data'
elif platform.node() == 'bryan-metabox':
    IMAGEDIR = '/run/media/bryan/Sunflower'
else:
    raise ImportError('Define folders in the gaf/config.py file')

if not os.path.isdir(IMAGEDIR):
    os.makedirs(IMAGEDIR)

'''
EXAMPLEFILE = os.path.join(DATADIR, 'AHI-Example.nc')
if not os.path.isfile(EXAMPLEFILE):
    stdout.write('Initial config - downloading example AHI 2km\n')
    stdout.flush()
    url = 'http://dapds00.nci.org.au/thredds/fileServer/rr5/satellite/obs'\
          '/himawari8/FLDK/2016/09/01/0200/20160901020000-P1S-ABOM_OBS_B07'\
          '-PRJ_GEOS141_2000-HIMAWARI8-AHI.nc'
    from .tools import file_downloader
    file_downloader(url, EXAMPLEFILE)
# ahi coords 2km is superseded by BOM Ancillary Data

AHICOORDS2KM = os.path.join(DATADIR, 'AHI-Coords2km.npz')
if not os.path.isfile(AHICOORDS2KM):
    stdout.write('Initial config - creating AHI 2km coordinates\n')
    stdout.flush()
    lon, lat = coords_from_nc(EXAMPLEFILE)
    with open(AHICOORDS2KM, 'wb') as f:
'''
