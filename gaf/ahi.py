# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 16:58:13 2017

@author: Bryan Hally
"""

import numpy as np
from netCDF4 import Dataset
# import pyproj
# from mpl_toolkits.basemap import Basemap, maskoceans
import os.path
from sys import stdout
# from thredds_crawler.crawl import Crawl
import matplotlib.pyplot as plt
import datetime
import cartopy

from .tools import Time, file_downloader, FilenameParser, cloud_mask_write
from . import IMAGEDIR

ahi7TtoI_params = [132850.7746477298, 3698.0788552409813, -6.937464229518511]
ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)
IMAGEDIR_AHI = os.path.join(IMAGEDIR, 'AHI')


#  AHI navigation parameters
#  count from zero error - central coordinates shifted from
#  standard documentation to allow for this


class AHILocation(object):
    '''
    Takes a set of two coordinates (AHI pixel location or lat/lon) and 
    provides an attributed object, in 2km space.
    
    Input: data - list object consisting of [line,pixel] or [lat,lon]. 
    Lines numbering 100 or less will be treated as [lat,lon] - this will
    affect few cases (latitudes>69N).
    
    Output: Location object with attributes {.latitude, .longitude, 
                                             .line, .pixel}
    '''

    def __init__(self, data, res=2000):
        if isinstance(data, (list, tuple, np.ndarray)):
            if len(data) != 2:
                raise Exception('Too many coordinates in list')
        else:
            raise Exception('Location not defined as coordinates in list')
        if res not in [500, 1000, 2000]:
            raise Exception('Invalid resolution given.')
        self.params = {'sub_lon': 140.70000, 'Rs': 42164.00000, 'Rpol': 6356.7523, 'Req': 6378.1370,
                       'coff': (2750 * 2000 / res) - 0.5, 'loff': (2750 * 2000 / res) - 0.5,
                       'sclunit': 2 ** -16, 'cfac': 20466275 * 2000 / res, 'lfac': 20466275 * 2000 / res}
        if data[0] < 90:  # lat,lon
            self.latitude = np.float32(data[0])
            self.longitude = np.float32(data[1])
            self.latlon = list((self.latitude, self.longitude))
            self.PixelLocator()
        else:  # line,pixel
            self.line = data[0]
            self.pixel = data[1]
            self.AHICoords()
        self.time_offset = np.floor(self.longitude * 4)

    def AHICoords(self):
        # input: values of pix, lin
        # output: values of lat, lon
        c = float(self.pixel)
        l = float(self.line)
        x = np.radians((c - self.params['coff']) / (self.params['sclunit'] * self.params['cfac']))
        y = np.radians((l - self.params['loff']) / (self.params['sclunit'] * self.params['lfac']))

        Param3 = self.params['Req'] ** 2 / self.params['Rpol'] ** 2
        ParamSd = self.params['Rs'] ** 2 - self.params['Req'] ** 2
        try:
            Sd = np.sqrt((self.params['Rs'] * np.cos(x) * np.cos(y)) ** 2 - (np.cos(y) ** 2
                                                                             + Param3 * np.sin(y) ** 2) * ParamSd)
        except ValueError:
            return np.nan, np.nan
        else:
            Sn = (self.params['Rs'] * np.cos(x) * np.cos(y) - Sd) / (np.cos(y) ** 2 + Param3 * np.sin(y) ** 2)
            S1 = self.params['Rs'] - (Sn * np.cos(x) * np.cos(y))
            S2 = Sn * np.sin(x) * np.cos(y)
            S3 = -Sn * np.sin(y)
            Sxy = np.sqrt(S1 ** 2 + S2 ** 2)

            self.longitude = (np.degrees(np.arctan2(S2, S1)) + self.params['sub_lon']).astype(np.float32)
            if self.longitude > 180:
                self.longitude = self.longitude - 360
            self.latitude = np.degrees(np.arctan(Param3 * S3 / Sxy)).astype(np.float32)
            self.latlon = list((self.latitude, self.longitude))

    def PixelLocator(self):
        # input: values of lat, lon
        # output: values of pix,lin
        param1 = (self.params['Req'] ** 2 - self.params['Rpol'] ** 2) / self.params['Req'] ** 2
        param2 = self.params['Rpol'] ** 2 / self.params['Req'] ** 2

        lon = np.radians(self.longitude)
        lat = np.radians(self.latitude)

        phi = np.arctan(param2 * np.tan(lat))
        Re = self.params['Rpol'] / np.sqrt(1 - param1 * np.cos(phi) ** 2)
        r1 = self.params['Rs'] - Re * np.cos(phi) * np.cos(lon - np.radians(self.params['sub_lon']))
        r2 = -Re * np.cos(phi) * np.sin(lon - np.radians(self.params['sub_lon']))
        r3 = Re * np.sin(phi)

        rn = np.sqrt(r1 ** 2 + r2 ** 2 + r3 ** 2)
        x = np.degrees(np.arctan2(-r2, r1))
        y = np.degrees(np.arcsin(-r3 / rn))

        self.pixel = self.params['coff'] + x * self.params['sclunit'] * self.params['cfac']
        self.line = self.params['loff'] + y * self.params['sclunit'] * self.params['lfac']

        self.pixel = np.floor(self.pixel + 0.5).astype(int)
        self.line = np.floor(self.line + 0.5).astype(int)


class AHIImage(FilenameParser):
    '''
    Contains image attributes such as lat,lon as well as image data
    '''

    def __init__(self, fpath, mask=False):
        FilenameParser.__init__(self, fpath)
        self.mask = mask
        if self.obs_type == 'BRF':
            self.keyword = 'brf'
        elif self.obs_type == 'SOL':
            self.keyword = 'zenith'
        elif self.obs_type == 'SOLZA':
            self.keyword = 'solar'
        elif self.band > 6:
            self.keyword = 'brightness_temperature'
        else:
            self.keyword = 'radiance'

    def test_file(self):
        try:
            r = Dataset(self.fpath)
            r.close()
        except IOError:
            os.remove(self.fpath)
            get_nc_file(self.fname)

    def get_single_pixel_value(self, location):
        if not isinstance(location, AHILocation):
            location = AHILocation(location, res=self.res)
        self.test_file()
        with Dataset(self.fpath) as r:
            data_k = [k for k in r.variables.keys() if self.keyword in k][0]
            return r.variables[data_k][0, location.line, location.pixel]

    def get_list_of_pixel_vals(self, locs):
        with Dataset(self.fpath) as r:
            data_k = [k for k in r.variables.keys() if self.keyword in k][0]
            rl = np.zeros((len(locs)))
            for i, loc in enumerate(locs):
                if not isinstance(loc, AHILocation):
                    location = AHILocation(loc, res=self.res)
                else:
                    location = loc
                rl[i] = r.variables[data_k][0, location.line, location.pixel]
        return rl

    def get_all_data(self, verbose=True):
        self.test_file()
        geom_file = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-{}.nc'.format(self.res))
        if not os.path.isfile(geom_file):
            get_ancillary('PRJ', self.res)
        with Dataset(geom_file) as g:
            lon = g.variables['lon'][0, :, :]
            lat = g.variables['lat'][0, :, :]
        self.lon = lon.astype(np.float32)
        self.lat = lat.astype(np.float32)
        with Dataset(self.fpath) as r:
            '''
            wgs84 = pyproj.Proj(proj='latlong', datum='WGS84')
            ahi = pyproj.Proj(str(r.variables['geostationary'].proj4))
            x, y = np.meshgrid(r.variables['x'][:], r.variables['y'][:])
            lon, lat = pyproj.transform(ahi, wgs84, x, y)
            self.lon = lon.astype(np.float32)
            self.lat = lat.astype(np.float32)
            '''
            data_k = [k for k in r.variables.keys() if self.keyword in k][0]
            image_data = r.variables[data_k][:]
            if len(image_data.shape) < 3:
                self._data = np.ma.array(image_data, mask=lat.mask)
            elif self.mask:
                self._data = np.ma.array(image_data[0, :, :], mask=lat.mask)
            else:
                self._data = image_data[0, :, :]
        if verbose:
            stdout.write('lat, lon, data produced for {}\n'.format(self.fname))
            stdout.flush()

    @property
    def latitude(self):
        try:
            return self.lat
        except AttributeError:
            self.get_all_data(verbose=False)
            return self.lat

    @property
    def longitude(self):
        try:
            return self.lon
        except AttributeError:
            self.get_all_data(verbose=False)
            return self.lon

    @property
    def data(self):
        try:
            return self._data
        except AttributeError:
            if self.mask:
                self.get_all_data(verbose=False)
            else:
                self.test_file()
                with Dataset(self.fpath) as r:
                    data_k = [k for k in r.variables.keys() if self.keyword in k][0]
                    image_data = r.variables[data_k][:]
                    if len(image_data.shape) < 3:
                        self._data = image_data
                    else:
                        self._data = image_data[0, :, :]
                geom_file = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-{}.nc'.format(self.res))
                if not os.path.isfile(geom_file):
                    get_ancillary('PRJ', self.res)
                with Dataset(geom_file) as g:
                    mask = g.variables['lat'][0, :, :]
                self._data = np.ma.array(self._data, mask=mask.mask)
            return self._data

    def plot_fulldisk(self, cbar=True):
        try:
            self._data
        except AttributeError:
            self.get_all_data(verbose=False)
        f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))  # creates cartopy ax instance
        img_extent = [-5500000, 5500000, -5500000, 5500000]  # extents of geostationary image
        ax.gridlines(xlocs=np.arange(-180, 195, 15))  # extend across dateline because gridlines hates you
        ax.coastlines('10m', '0.2')  # adjust second value to control color
        plot_kw = dict(cmap='gray')
        if self.band == 7:
            plot_kw.update(vmin=270, vmax=330)  # limits influence of high temp outliers
        # plot data using exploded plot_kw dict (double the splat)
        ax.imshow(self.data, origin='upper', extent=img_extent, transform=ahi_crs, **plot_kw)
        if cbar:
            try:  # if they exist
                vmin = plot_kw['vmin']
                vmax = plot_kw['vmax']
            except KeyError:  # then we interrogate the data
                vmin = np.nanmin(self.data)
                vmax = np.nanmax(self.data)
            # creates a scalar mappable of the mapped data
            sm = plt.cm.ScalarMappable(cmap=plot_kw['cmap'], norm=plt.Normalize(vmin, vmax))
            sm._A = []  # not sure what this does, but why break things
            cb = plt.colorbar(sm, ax=ax)  # creates colorbar (not attached to ax vextents - be careful resizing
            cb.set_label(self.keyword)  # Clean up to properly label
        f.savefig(r'C:\Scratch\temp\test.png', format='png')


class AHICloud_BOM(FilenameParser):
    def __init__(self, fpath):
        FilenameParser.__init__(self, fpath)
        self.data_k = 'clear_sky_probability'

    def get_single_pixel_value(self, location):
        if not isinstance(location, AHILocation):
            location = AHILocation(location)
        with Dataset(self.fpath) as r:
            val = r.variables[self.data_k][0, location.line, location.pixel]
            return np.round(val * 3).astype(int)

    def get_list_of_pixel_vals(self, locs):
        rl = np.zeros((len(locs)))
        with Dataset(self.fpath) as r:
            for i, loc in enumerate(locs):
                location = AHILocation(loc)
                rl[i] = r.variables[self.data_k][0, location.line, location.pixel]
        return np.round(rl * 3).astype(int)

    def get_all_data(self):
        geom_file = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-{}.nc'.format(2000))
        if not os.path.isfile(geom_file):
            get_ancillary('PRJ', self.res)
        with Dataset(geom_file) as g:
            lon = g.variables['lon'][0, :, :]
            lat = g.variables['lat'][0, :, :]
        self.lon = lon.astype(np.float32)
        self.lat = lat.astype(np.float32)
        with Dataset(self.fpath) as r:
            self._data = r.variables[self.data_k][0, :, :]

    @property
    def latitude(self):
        return self.lat

    @property
    def longitude(self):
        return self.lon

    @property
    def csp(self):
        return np.round(self._data * 3).astype(int)


class AHICapture(object):
    '''
    Allows access to AHI files and their variables per image time. Uses
    IMAGEDIR as defined in config.py
    '''

    def __init__(self, time):
        # super(AHICapture,self).__init__(time)
        if not isinstance(time, Time):
            self.time = Time(time)
        else:
            self.time = time
        # create file naming convention
        self.sensor = 'AHI'
        self.bands = np.arange(16) + 1
        res = [2000, 1000, 500]
        images = []
        images.append([[i, j] for i in self.bands[:2] for j in res[:2]])
        images.append([[3, j] for j in res])
        images.append([[4, j] for j in res[:2]])
        images.append([[i, 2000] for i in self.bands[4:]])
        images = [item for sublist in images for item in sublist]
        _obs_images = [item for item in images if item[1] == 2000]
        _brf_images = [item for item in images if item[0] < 7]
        self.obs_images = ['{}{:03d}-{:02d}{:02d}-AHIB{:02d}-{:04d}.nc'.format(
            self.time.year, self.time.jday, self.time.hour,
            self.time.minute, *i) for i in _obs_images]
        self.brf_images = ['{}{:03d}-{:02d}{:02d}-AHIR{:02d}-{:04d}.nc'.format(
            self.time.year, self.time.jday, self.time.hour,
            self.time.minute, *i) for i in _brf_images]
        self.cld_images = ['{}{:03d}-{:02d}{:02d}-AHICLD-{}.nc'.format(
            self.time.year, self.time.jday, self.time.hour,
            self.time.minute, i) for i in ['BOM', 'WU']]
        self.sol_images = ['{}{:03d}-{:02d}{:02d}-AHISOL{}.nc'.format(
            self.time.year, self.time.jday, self.time.hour,
            self.time.minute, i) for i in ['-2000', 'ZA']]
        # check availability local
        # TODO: fix pathing - bands in paths
        self.files_avail = {}
        for flist in [self.obs_images, self.brf_images, self.cld_images, self.sol_images]:
            for f in flist:
                if ~np.logical_or('CLD' in f, 'SOL' in f):
                    folder = ['B{:02d}'.format(int(f[17:19]))]
                elif 'BOM' in f:
                    folder = ['CLD_BOM']
                elif 'WU' in f:
                    folder = ['CLD_WU']
                elif 'SOL' in f:
                    if not 'ZA' in f:
                        folder = ['SOL']
                    else:
                        folder = ['SOL', 'ZA']
                pathmid = os.path.join(self.sensor, *folder, '{:04d}'.format(self.time.year),
                                       '{:03d}'.format(self.time.jday))
                if os.path.isfile(os.path.join(IMAGEDIR, pathmid, f)):
                    self.files_avail[f] = True
                else:
                    self.files_avail[f] = False

    def __getattr__(self, bandname):
        isband = isbomcld = iswucld = issolar = issolarza = False
        head, sep, tail = bandname.lower().partition('band')
        try:
            band = tail.upper()
            if head == '':
                try:
                    band, img_type, res = BandnameParser(bandname)
                    isband = True
                except TypeError:
                    raise
            elif head == 'bomcld':
                isbomcld = True
            elif head == 'wucld':
                iswucld = True
            elif head == 'solar':
                issolar = True
            elif head == 'solarza':
                issolarza = True
        except ValueError:
            stdout.write('Band {} is not an AHI band.'.format(band))
            stdout.flush()
            raise
        if isband:
            if img_type == 'r':
                keys = [s for s in self.brf_images if 'R{:02d}'.format(int(band)) in s]
                key = [s for s in keys if '{}'.format(res) in s][0]
            elif img_type == 'b':
                key = [s for s in self.obs_images if 'B{:02d}'.format(int(band)) in s][0]
            pathmid = "/".join((self.sensor, 'B{:02d}'.format(int(band)),
                                '{:04d}'.format(self.time.year),
                                '{:03d}'.format(self.time.jday)))
            fpath = os.path.join(IMAGEDIR, pathmid, key)
            if not self.files_avail[key]:
                # fname = self.gather_file(key,fpath)
                fname = get_nc_file(key)
                if not fname:
                    # print 'File not available on NCI server'
                    return 0
                else:
                    self.files_avail[key] = True
            return AHIImage(fpath)
        elif isbomcld:
            key = [s for s in self.cld_images if 'BOM' in s][0]
            if not self.files_avail[key]:
                return 0
            pathmid = os.path.join(self.sensor, 'CLD_BOM',
                                   '{:04d}'.format(self.time.year),
                                   '{:03d}'.format(self.time.jday))
            fpath = os.path.join(IMAGEDIR, pathmid, key)
            return AHICloud_BOM(fpath)
        elif iswucld:
            key = [s for s in self.cld_images if 'WU' in s][0]
            pathmid = os.path.join(self.sensor, 'CLD_WU',
                                   '{:04d}'.format(self.time.year),
                                   '{:03d}'.format(self.time.jday))
            fpath = os.path.join(IMAGEDIR, pathmid, key)
            if not self.files_avail[key]:
                cld = self.create_cloud_mask_wu(False)
                if type(cld) != np.ndarray:
                    print('No cloud product available')
                    return 0
                if not os.path.isdir(os.path.join(IMAGEDIR, pathmid)):
                    os.makedirs(os.path.join(IMAGEDIR, pathmid))
                cloud_mask_write(fpath, cld)
                # r = Dataset(fpath, 'w')
                # r.createDimension('x', 5500)
                # r.createDimension('y', 5500)
                # data = r.createVariable('cld_data', 'i1', ('x', 'y',), zlib=True)
                # data[:] = cld
                # r.close()
                self.files_avail[key] = True
                print('Cloud product produced for {}'.format(key))
            else:
                cld = Dataset(fpath).variables['cld_data'][:]
                print('Cloud retrieved for {}'.format(key))
            return cld
        elif issolar:
            pathmid = os.path.join(self.sensor, 'SOL',
                                   '{:04d}'.format(self.time.year),
                                   '{:03d}'.format(self.time.jday))
            fpath = os.path.join(IMAGEDIR, pathmid, self.sol_images[0])
            if not self.files_avail[self.sol_images[0]]:
                fname = get_solar(self.sol_images[0], pathmid)
                if not fname:
                    # print 'File not available on NCI server'
                    return 0
                else:
                    self.files_avail[self.sol_images[0]] = True
            return AHIImage(fpath)
        elif issolarza:
            pathmid = os.path.join(self.sensor, 'SOL', 'ZA',
                                   '{:04d}'.format(self.time.year),
                                   '{:03d}'.format(self.time.jday))
            fpath = os.path.join(IMAGEDIR, pathmid, self.sol_images[1])
            if not self.files_avail[self.sol_images[1]]:
                print('File not available - check pathing')
                return 0
            return AHIImage(fpath)
        else:
            pass


def BandnameParser(bandname):
    h, s, t = bandname.lower().partition('band')
    if t == '':
        print('Non-standard bandname supplied')
        return None
    try:
        band = int(t[:2])
        if band < 17:
            img_type = 'b'
            res = 2000
        else:
            print('Band {} is not an AHI band'.format(band))
            return None
    except ValueError or IndexError:
        band = int(t[0])
        if band > 6 and band < 17:
            img_type = 'b'
            res = 2000
        elif t[1] == 'b':
            img_type = 'b'
            res = 2000
        elif t[1] == 'r':
            img_type = 'r'
            try:
                res_str = t[2:]
                if band == 3:
                    if int(res_str) in [500, 1000, 2000]:
                        res = int(res_str)
                    else:
                        raise Exception('Invalid resolution selected')
                elif band < 5:
                    if int(res_str) in [1000, 2000]:
                        res = int(res_str)
                    else:
                        raise Exception('Invalid resolution selected')
                else:
                    res = 2000
            except IndexError:
                res = 2000
    return band, img_type, res


# Random toolery
def coords_from_geom(resolution=2000):
    geom_file = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-{}.nc'.format(resolution))
    if not os.path.isfile(geom_file):
        get_ancillary('PRJ', resolution)
    with Dataset(geom_file) as g:
        lat = g.variables['lat'][0, :, :]
        lon = g.variables['lon'][0, :, :]
    return lon, lat


def get_nc_file(filename):
    a = FilenameParser(filename)
    # url = 'http://dapds00.nci.org.au/thredds/fileServer/rr5/satellite/obs' \
    #       '/himawari8/FLDK/{0}/{1:02d}/{2:02d}/{3:02d}{4:02d}/{0}{1:02d}' \
    #       '{2:02d}{3:02d}{4:02d}00-P1S-ABOM_{5}_B{6:02d}-PRJ_GEOS141_' \
    #       '{7}-HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month,
    #                                     a.time.day, a.time.hour,
    #                                     a.time.minute, a.obs_type,
    #                                     a.band, a.res)
    url = 'http://dapds00.nci.org.au/thredds/fileServer/ra22/satellite-products/arc/obs/himawari-ahi/fldk/v1-0/' \
          '{0}/{1:02d}/{2:02d}/{3:02d}{4:02d}/{0}{1:02d}' \
          '{2:02d}{3:02d}{4:02d}00-P1S-ABOM_{5}_B{6:02d}-PRJ_GEOS141_' \
          '{7}-HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month,
                                        a.time.day, a.time.hour,
                                        a.time.minute, a.obs_type,
                                        a.band, a.res)
    pathmid = "/".join((a.sensor, 'B{:02d}'.format(int(a.band)),
                        '{:04d}'.format(a.time.year),
                        '{:03d}'.format(a.time.jday)))
    fpath = os.path.join(IMAGEDIR, pathmid, filename)
    return file_downloader(url, fpath, verbose=False)


def get_solar(filename, pathmid):
    a = FilenameParser(filename)
    url = 'http://dapds00.nci.org.au/thredds/fileServer/rr5/satellite/obs' \
          '/himawari8/FLDK/{0}/{1:02d}/{2:02d}/{3:02d}{4:02d}/{0}{1:02d}' \
          '{2:02d}{3:02d}{4:02d}00-P1S-ABOM_GEOM_SOLAR-PRJ_GEOS141_' \
          '2000-HIMAWARI8-AHI.nc'.format(a.time.year, a.time.month,
                                         a.time.day, a.time.hour,
                                         a.time.minute)
    fpath = os.path.join(IMAGEDIR, pathmid, filename)
    return file_downloader(url, fpath, verbose=False)


def get_ancillary(img_type='PRJ', resolution=2000):
    kws = dict(DEM='AUSDEM', LAND='LAND', PRJ='SENSOR')
    try:
        url = 'http://dapds00.nci.org.au/thredds/fileServer/ra22/satellite-products/arc/obs/himawari-ahi/fldk/v1-0/' \
              'ancillary/00000000000000-P1S-ABOM_GEOM_{}-PRJ_' \
              'GEOS141_{}-HIMAWARI8-AHI.nc'.format(kws[img_type], resolution)
    except KeyError:
        print('Specify what product you want to download (DEM, LAND, PRJ)')
        raise
    fpath = os.path.join(IMAGEDIR_AHI, 'AHI-{}-{}.nc'.format(img_type, resolution))
    return file_downloader(url, fpath, verbose=False)


def create_cloud_mask():
    pass


def get_land_mask():
    f = os.path.join(IMAGEDIR_AHI, 'AHI-LAND-2000.nc')
    if not os.path.isfile(f):
        get_ancillary('LAND', 2000)
    with Dataset(f) as r:
        mask = r.variables['Band1'][:, :].data
    return mask


def get_latitude():
    f = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-2000.nc')
    if not os.path.isfile(f):
        get_ancillary('PRJ', 2000)
    with Dataset(f) as r:
        return r.variables['lat'][0, :, :]


def get_longitude():
    f = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-2000.nc')
    if not os.path.isfile(f):
        get_ancillary('PRJ', 2000)
    with Dataset(f) as r:
        return r.variables['lon'][0, :, :]


def get_viewangle():
    f = os.path.join(IMAGEDIR_AHI, 'AHI-PRJ-2000.nc')
    if not os.path.isfile(f):
        get_ancillary('PRJ', 2000)
    with Dataset(f) as r:
        return r.variables['sensor_zenith_angle'][0, :, :]


def plot_geostationary(ax, data, resolution=2000, cbar=False, cmap='viridis', vmin=None, vmax=None, coast=True,
                       **kwargs):
    img_size = data.shape
    img_extent = [-img_size[0] * resolution / 2, img_size[0] * resolution / 2,
                  -img_size[0] * resolution / 2, img_size[0] * resolution / 2]
    # f, ax = plt.subplots(figsize=(10, 8), subplot_kw=dict(projection=ahi_crs))
    if vmin is None:
        vmin = np.nanmin(data)
    if vmax is None:
        vmax = np.nanmax(data)
    ax.imshow(data, cmap=cmap, origin='upper', extent=img_extent, transform=ahi_crs, vmin=vmin,
              vmax=vmax, **kwargs)
    ax.gridlines(xlocs=np.arange(-180, 195, 15))  # extend across dateline because gridlines hates you
    if coast:
        ax.coastlines('10m', '1.0')
    if cbar:
        # creates a scalar mappable of the mapped data
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin, vmax))
        sm._A = []  # not sure what this does, but why break things
        plt.colorbar(sm, ax=ax)  # creates colorbar (not attached to ax vextents - be careful resizing
