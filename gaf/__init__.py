# coding: utf-8
"""
geostationary-active-fire

Module of tools for reading and processing geostationary imagery, and 
for the production of working outputs from various fire and fire-related
algorithms.

@author: Bryan Hally, RMIT Unversity/University of Twente
Contact: bryan.hally@rmit.edu.au
"""
from .config import DATADIR, IMAGEDIR
from .tools import *
from .ahi import *
from .bat import *
