# -*- coding: utf-8 -*-
"""
GAF-Tools
Created on Tue Jul 25 17:52:47 2017

@author: bryan.hally

Spiritual replacement for satfiretools (synthfire repo).  
"""
# print 'Working!'

from netCDF4 import Dataset
import datetime
import calendar
import numpy as np
import os
import requests
from sys import stdout
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
from scipy.optimize import minimize
from scipy.stats import linregress
from pandas._libs.tslib import Timestamp
from pandas import HDFStore
from matplotlib.patches import Polygon
import cartopy

ahi_crs = cartopy.crs.Geostationary(140.7, satellite_height=35785863)


def win_disk_labeller():
    import win32com.client
    try:
        strComputer = "."
        objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
        objSWbemServices = objWMIService.ConnectServer(strComputer, "root\cimv2")
        colItems = objSWbemServices.ExecQuery("SELECT * FROM Win32_DiskDrive WHERE InterfaceType = \"USB\"")
        DiskDrive_DeviceID = colItems[0].DeviceID.replace('\\', '').replace('.', '')
        colItems = objSWbemServices.ExecQuery("SELECT * from Win32_DiskDriveToDiskPartition")
        for objItem in colItems:
            if DiskDrive_DeviceID in str(objItem.Antecedent):
                DiskPartition_DeviceID = objItem.Dependent.split('=')[1].replace('"', '')
        colItems = objSWbemServices.ExecQuery("SELECT * from Win32_LogicalDiskToPartition")
        for objItem in colItems:
            if DiskPartition_DeviceID in str(objItem.Antecedent):
                LogicalDisk_DeviceID = objItem.Dependent.split('=')[1].replace('"', '')
        colItems = objSWbemServices.ExecQuery(
            "SELECT * from Win32_LogicalDisk WHERE DeviceID=\"" + LogicalDisk_DeviceID + "\"")
        volname = colItems[0].VolumeName
        if volname == 'Sunflower Bucket' or volname == 'Sunflower Vase':
            return volname, LogicalDisk_DeviceID
        else:
            return '', ''
    except IndexError:
        # print 'No hard drive in computer. Replace and reimport.'
        return '', ''


class Time(object):
    def __init__(self, raw_data):
        '''
        Converts date and time to datetime object.
        Output: dt,jday,posix objects
        '''
        if isinstance(raw_data, (list, tuple, np.ndarray)):
            if len(raw_data) == 4:
                # assume [y,jday,hh,mm]
                self.dt = datetime.datetime.strptime('{}{:03d}{:02d}{:02d}'.format(*raw_data), '%Y%j%H%M')
            elif len(raw_data) == 5:
                self.dt = datetime.datetime(*raw_data)
            elif len(raw_data) < 4:
                raise Exception('Not enough information to decide how this time is structured.')
            else:  # len>5
                raise Exception('Too much information to decide how this time is structured.')
        elif isinstance(raw_data, datetime.datetime):
            self.dt = raw_data
        elif isinstance(raw_data, Timestamp):
            self.dt = raw_data.to_pydatetime()
        else:
            # assume floating point epoch
            self.dt = datetime.datetime.utcfromtimestamp(raw_data)
        self.jday = self.dt.timetuple().tm_yday
        # create posix time
        self.posix = calendar.timegm(self.dt.timetuple())
        [setattr(self, item, getattr(self.dt, item)) for item in ['year',
                                                                  'month', 'day', 'hour', 'minute']]


# Directory functions - start for pickles, end for nc files


def listdir_start(d, search):
    return [os.path.join(d, f) for f in os.listdir(d) if f.startswith(search)]


def listdir_end(d, search):
    return [os.path.join(d, f) for f in os.listdir(d) if f.endswith(search)]


def listdir_contains(d, search):
    return [os.path.join(d, f) for f in os.listdir(d) if search in f]


def flatten_list(l):
    return [item for s in l for item in s]


# usable for fileservers with no authentication


def file_downloader(url, outfile, verbose=True):
    '''
    Grabs files from any server and saves them at the location 'outfile'
    '''
    if not os.path.isdir(os.path.dirname(outfile)):
        os.makedirs(os.path.dirname(outfile))
    # TODO: replace with system proxy retrieval
    proxies = {'http': 'http://bproxy.rmit.edu.au:8080'}
    try:
        if requests.head('http://www.google.com'):
            proxy = False
    except requests.packages.urllib3.exceptions.ProxySchemeUnknown:
        if requests.head('http://www.google.com', proxies=proxies):
            proxy = True
    if np.isin(requests.head(url).status_code, [200, 301]):
        stdout.write('Retrieving {} from server\n'.format(url.split('/')[-1]))
        stdout.flush()
        chunksize = 40960
        attempts = 0
        while attempts < 2:
            try:
                if proxy:
                    res = requests.get(url, proxies=proxies)
                    attempts = 99
                else:
                    res = requests.get(url)
                attempts = 99
            except:
                attempts += 1
        if attempts == 2:
            stdout.write('{} - save failed'.format(os.path.basename(outfile)))
            stdout.flush()
        else:
            with open(outfile, 'wb') as wFile:
                for chunk in res.iter_content(chunksize):
                    wFile.write(chunk)
        return outfile
    else:
        if verbose:
            stdout.write('{} is not on server'.format(os.path.basename(outfile)))
            stdout.flush()
        return 0


def format_plot_time(ax, dhrs=3):
    major_ticks = mdates.HourLocator(byhour=np.arange(0, 24, dhrs))
    minor_ticks = mdates.MinuteLocator(byminute=[0, 30])
    hfmt = mdates.DateFormatter('%H:%M')
    ax.xaxis.set_major_locator(major_ticks)
    ax.xaxis.set_minor_locator(minor_ticks)
    ax.xaxis.set_major_formatter(hfmt)


class FilenameParser(object):
    '''
    Parses filename for image details - quicker than reading from file
    '''

    def __init__(self, fpath):
        # determine filetype
        self.fpath = fpath
        self.fname = os.path.basename(fpath)
        timetup = (int(self.fname[:4]), int(self.fname[4:7]),
                   int(self.fname[8:10]), int(self.fname[10:12]))
        if '.nc' in fpath:
            self.sensor = self.fname[13:16]
            if 'CLD' in self.fname:
                self.obs_type = 'CLD'
                self.res = 2000
            elif 'SOL' in self.fname:
                if 'ZA' in self.fname:
                    self.obs_type = 'SOLZA'
                else:
                    self.obs_type = 'SOL'
                self.res = 2000
            else:
                self.band = int(self.fname[17:19])
                self.res = int(self.fname[20:24])
                self.obs_type = 'BRF'
                if self.fname[16] == 'B':
                    self.obs_type = 'OBS'
        elif '.h5' in fpath:
            self.band = int(self.fname[-5:-3])
        self.time = Time(timetup)


# Butterworth low pass filters for the daily and extended datasets


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    # use filtfilt because of double ended goodness
    y = filtfilt(b, a, data)
    return y


# svd tools for producing fittings


def svd_minimiser(s, obs, U, ci_init=[], fun_res=[], verbose=False):
    if ci_init == []:
        if verbose:
            stdout.write('\nNo ci given - assumed first run - calc from obs, U')
            stdout.flush()
        ci_init = np.dot(obs, U)

    estar = np.dot(U, ci_init)
    ommission = []
    for i in range(len(obs)):
        res = np.abs(obs[i] - estar[i])
        if res >= s / np.sqrt(3):
            ommission.append(i)
    if verbose:
        stdout.write('\nS-value: {:.02f} Number of outliers: {}'.format(s, len(ommission)))
        stdout.flush()
    c_res = minimize(error_function, ci_init, args=(obs, U, s))
    fun_res.append(c_res.fun)
    return c_res.x, obs, U, fun_res, ommission


def error_function(ci, *args):
    # supply obs, U and s as constants
    obs, U, s = args
    # vector of predictions - turns out a reverse dot product works well here
    estar = np.dot(U, ci)
    # calculate residuals
    x = obs - estar
    # divide errors of returns below the line by two, to raise the temperature
    # and minimise the influence of cloud
    x[x < 0] /= 2
    # eliminate evaluation of temperatures above 335K
    x[obs > 335] = 0
    # rho is the 'error' function - s is an arbitrary scale value
    rho = x ** 2 / (s + x ** 2)
    E = 0
    # this loop applies m, which is zero when the residual is greater than s/sqrt(3)
    for i in range(len(obs)):
        res = np.abs(obs[i] - estar[i])
        if res < s / np.sqrt(3):
            E += rho[i]
    return E


# Random stats


def rms(observations, predictions):
    return np.sqrt(((observations - predictions) ** 2).mean())


def rsquare(observations, predictions):
    __, __, r_value, __, __ = linregress(observations, predictions)
    return r_value ** 2


# pandas datastore helpers


def h5store(filename, df, **kwargs):
    store = HDFStore(filename)
    store.put('df', df)
    store.get_storer('df').attrs.metadata = kwargs
    store.close()


def h5load(filename):
    with HDFStore(filename) as store:
        data = store['df']
        metadata = store.get_storer('df').attrs.metadata
    return data, metadata


def plt_std_df(d, md):
    f, ax = plt.subplots()
    ax.plot(d.time_loc, d.bt, label='Band 7 BT')
    ax.plot(d.time_loc, d.fit, label='Fitted temp')
    ax2 = ax.twinx()
    ax2.plot(d.time_loc, d.cld, 'g--', lw=0.4, label='CSP')


def draw_poly(lats, lons, m, **kwargs):
    x, y = m(lons, lats)
    xy = list(zip(x, y))
    poly = Polygon(xy, **kwargs)
    return poly


def cloud_mask_write(fpath, cld):
    if not os.path.isdir(os.path.dirname(fpath)):
        os.makedirs(os.path.dirname(fpath))
    r = Dataset(fpath, 'w')
    r.createDimension('x', cld.shape[1])
    r.createDimension('y', cld.shape[0])
    data = r.createVariable('cld_data', 'i1', ('y', 'x',), zlib=True)
    data[:] = cld
    data.bitmask = '0 = no cloud, 1 = 30 day thermal contrast, 2 = low TIR, 4 = infrared diff (day), ' \
                   '8 = infrared diff (night), 16 = VIS albedo, 32 = VIS / MIR radiance, ' \
                   '64 = nighttime ambient contrast'
    data.comment = 'Cloud mask based on Wu et al 2010. Remote Sensing of Environment 114 (1876-1895)'
    r.instrument = 'AHI'
    r.creator_name = 'RMIT Geospatial Science'
    r.creator_email = 'bryan.hally@rmit.edu.au'
    r.close()
    print('{} saved'.format(fpath))


def crt_subplots(**kwargs):
    return plt.subplots(subplot_kw=dict(projection=ahi_crs), **kwargs)
