# -*- coding: utf-8 -*-
"""
Created on Thu Aug 10 15:47:48 2017

@author: e76243
"""

import numpy as np
import pandas as pd
from .ahi import AHIImage, AHILocation, AHICapture, coords_from_geom
from . import IMAGEDIR
import gaf.tools as tools
import os
from sys import stdout
import datetime

pd.options.mode.chained_assignment = None


def find_block_indices(location):
    lon, lat = coords_from_geom(2000)
    df = pd.DataFrame({'lat': np.ravel(np.ma.masked_invalid(lat)),
                       'y_ind': np.repeat(np.arange(5500), 5500),
                       'x_ind': np.ravel(np.repeat(np.arange(5500), 5500).reshape(5500, 5500).T),
                       'lon': np.ravel(np.ma.masked_invalid(lon))})
    df['rnd_lat'] = np.floor(df['lat']*4)/4.
    df['rnd_lon'] = np.floor(df['lon']*4)/4.
    df = df[['x_ind', 'y_ind', 'rnd_lat', 'rnd_lon']]
    
    gb = df.groupby(['rnd_lat', 'rnd_lon'])
    
    if not isinstance(location, AHILocation):
        location = AHILocation(location)
    train_lat = np.floor(location.latitude * 4)/4.
    train_lon = np.floor(location.longitude * 4)/4.
    # return AHI coords to every non-sea pixel in the required block
    return np.fliplr(gb.get_group(tuple([train_lat, train_lon]))\
                     [['x_ind', 'y_ind']].values)


def make_medians(fpath):
    a = AHIImage(fpath, mask=True)
    # check to see if valid AHIB07 image
    if a.band != 7:
        stdout.write('Not a Band 7 image')
        stdout.flush()
        return 0
    # make filename for storage frame
    df_fname = os.path.join(IMAGEDIR, 'BAT', '{}'.format(a.time.year),
                            '{:03d}'.format(a.time.jday),
                            '{}{:03d}-{:02d}{:02d}-B{:02d}.h5'.format(a.time.year,a.time.jday, a.time.hour,
                                                                      a.time.minute, a.band))
    # if file exists, return saved frame
    if os.path.isfile(df_fname):
        return pd.read_hdf(df_fname, 'df')
    # if storage folder doesn't exist create it
    elif not os.path.isdir(os.path.dirname(df_fname)):
        os.makedirs(os.path.dirname(df_fname))
    # get lat,lon,data
    a.get_all_data()
    # create dataFrame with flattened arrays
    df = pd.DataFrame({'lat': np.ravel(np.ma.masked_invalid(a.latitude)),
                       'lon': np.ravel(np.ma.masked_invalid(a.longitude)),
                       'data': np.ravel(a.data)})
    
    # round coords for blocks
    df['rnd_lat'] = np.floor(df['lat']*4)/4.
    df['rnd_lon'] = np.floor(df['lon']*4)/4.
    
    # eliminate clouded returns < 270K
    group_df = df[df['data'] > 270].groupby(['rnd_lat', 'rnd_lon'])['data']
    # create medians
    median = group_df.median()
    # count valid pixels
    count = group_df.count()
    
    group_df = median.reset_index()
    # eliminate blocks with less than 20 valid entries
    group_df = group_df[count.reset_index()['data'] > 20]
    # create bins for indexes
    # lat_bins = np.arange(-90,90,0.25)
    # latf = pd.DataFrame({'lat_ind': np.arange(lat_bins.shape[0]),
    #                     'rnd_lat': lat_bins})
    lon_bins = np.arange(-180, 180, 0.25)
    lonf = pd.DataFrame({'lon_ind': np.arange(lon_bins.shape[0]),
                         'rnd_lon': lon_bins})
    # merge to create index_cols
    # group_df = pd.merge(group_df,latf,on='rnd_lat')
    group_df = pd.merge(group_df, lonf, on='rnd_lon')
    
    # blow away dateline issues caused by maskoceans
    group_df = group_df.loc[np.logical_and(group_df['rnd_lon'] > -179.8,
                                           group_df['rnd_lon'] < 179.6), :]
    # save file to IMAGEDIR
    group_df.to_hdf(df_fname, 'df')
    if os.path.isfile(df_fname):
        stdout.write('File saved as {}\n'.format(os.path.basename(df_fname)))
        stdout.flush()
    else:
        stdout.write('File {} unavailable\n'.format(os.path.basename(df_fname)))
        stdout.flush()
    return group_df


def produce_std_data(location, start_time, window=14, verbose=False):

    # Produces data standardised over a 24 hour period per training pixel
    # window defines number of day into the past to produce training data for

    # convert latitude to training coordinates
    if not isinstance(location, AHILocation):
        location = AHILocation(location)
    train_lat = np.floor(location.latitude * 4)/4.
    train_lon = np.floor(location.longitude * 4)/4.
        
    # some function to separate out files according to final_time and window
    final_t = tools.Time(start_time)
    list_d = []
    # create list of days of training data to explore - year agnostic
    for i in range(window + 2):
        list_d.append([getattr(tools.Time(final_t.dt - datetime.timedelta(days=i)), item) for item in ['year', 'jday']])
    list_d.sort()
    # print list_d
    
    flist = tools.flatten_list([tools.listdir_end(os.path.join(IMAGEDIR, 'BAT',
                                                  str(item[0]), '{:03d}'.format(item[1])),
                                                  'h5') for item in list_d])
    
    if verbose:
        stdout.write('Stacking dataframes')
        stdout.flush()
    el = np.zeros((len(flist), 2))
    for i, f in enumerate(flist):
        st = datetime.datetime.now()
        # read pd file in
        df = pd.read_hdf(f, 'df')
        t1 = datetime.datetime.now()
        try:
            h = df.groupby('rnd_lat').get_group(train_lat)
        except KeyError:
            continue
        # h = df[df['rnd_lat']==train_lat].sort_values('rnd_lon')
        h['time_loc'] = pd.to_datetime(tools.FilenameParser(f).time.posix + h['rnd_lon']*240, unit='s')
        if not i:
            mdf = h[['lon_ind', 'data', 'time_loc']]
        else:
            mdf = pd.concat((mdf, h[['lon_ind', 'data', 'time_loc']]))
        el[i] = [(t1-st).total_seconds(), (datetime.datetime.now() - t1).total_seconds()]
    
    if verbose:
        stdout.write('Stacking complete')
        stdout.flush()
    
    # h = mdf[['lon_ind','data','time_loc']]
    avg = mdf.groupby('lon_ind')[['data', 'time_loc']].rolling('86400s', on='time_loc').mean().reset_index()
    std = mdf.groupby('lon_ind')[['data', 'time_loc']].rolling('86400s', on='time_loc').std().reset_index()
    avg = avg.drop('level_1', axis=1)
    std = std.drop('level_1', axis=1)
    
    df = pd.merge(mdf, avg, how='left', left_on=['lon_ind', 'time_loc'],
                  right_on=['lon_ind', 'time_loc'])
    
    df = pd.merge(df, std, how='left', left_on=['lon_ind', 'time_loc'],
                  right_on=['lon_ind', 'time_loc'])
    
    df = df.rename(columns={'data_x': 'median', 'data_y': 'mean', 'data': 'std'})
    
    df['std_med'] = (df['median']-df['mean'])/df['std']
    
    adf = df.groupby('time_loc')['std_med'].median().reset_index()
    
    training_finish = final_t.dt + datetime.timedelta(hours=train_lon/15)
    filt_start = training_finish - datetime.timedelta(hours=window * 24 + 2)
    filt_end = training_finish + datetime.timedelta(hours=2)
    
    fmdf = adf[np.logical_and(adf['time_loc'] >= filt_start, adf['time_loc'] < filt_end)]
    
    order = 5
    fs = 1/60.         # sample rate, Hz, dependent on gs
    cutoff = 1/10800.  # desired cutoff frequency of the filter, Hz
    
    fmdf = fmdf.join(pd.DataFrame({'filt': tools.butter_lowpass_filter(fmdf['std_med'].values, cutoff, fs, order)},
                                  index=fmdf.index))
    
    training_start = training_finish - datetime.timedelta(hours=window * 24)
    fmdf = fmdf[np.logical_and(fmdf['time_loc'] >= training_start,
                               fmdf['time_loc'] < training_finish)]

    # create new time index for interpolation
    t_df = pd.DataFrame({'time_loc': pd.date_range(training_start,
                                                   training_finish,
                                                   freq='1min').delete(-1).to_pydatetime()})
    ff = pd.merge(t_df, fmdf, how='left', left_on='time_loc', right_on='time_loc')
    # messy interpolation step when there are missing values
    ff['filt'].interpolate(inplace=True)
    # create date independent time column and groupby
    ff['time'] = ff.time_loc.dt.time
    gb = ff.groupby('time').groups
    
    # obtain image times for one location
    bt_df = make_std_diurnal(location, start_time)
    
    # create numpy array of filtered medians by time
    array = []
    for item in bt_df['time']:
        mlist = []
        for key in gb[item]:
            mlist.append(ff['filt'][key])
        array.append(mlist)
    array = np.array(array)
    
    df = pd.DataFrame(array, columns=['day{}'.format(d) for d in range(window)],
                      index=bt_df['time'])
    
    return df
    # TODO: split here for other fitting methods


def SVD_fit(bt_df, t_df):    
    # perform svd
    U, sing, _ = np.linalg.svd(t_df.values, full_matrices=False)
    # grab the 90% vectors
    for i, item in enumerate(sing):
        if np.sum(sing[:i+1])/np.sum(sing) > 0.9:
            U = U[:, :i+1]
            break
     
    s_vals = [1.724, 1.718, 1.703, 1.687, 1.656, 1.626, 1.577, 1.487, 1.413, 1.306]
    c_res, obs, U, fun_result, omit = tools.svd_minimiser(s_vals[0], bt_df['s_temp'].values, U)
    if tools.rms(obs, np.dot(U, c_res)) * bt_df['bt'].std() > 0.75:
        for val in s_vals[1:]:
            c_res, obs, U, fun_result, _ = tools.svd_minimiser(val, obs, U, c_res, fun_result)
            if tools.rms(obs, np.dot(U, c_res))*bt_df['bt'].std() < 0.75:
                break
            
    fit_res = np.dot(U, c_res)
    bt_df['fit'] = fit_res*bt_df['bt'].std() + bt_df['bt'].mean()
    return bt_df


def make_std_diurnal(location, start_time, cloud=False):
    '''
    Produce standardised diurnal data for a specified location and finish time
    bt_df returns a pandas DataFrame with columns bt, time_loc, time_utc,
    s_temp(standardised), and time (time_loc sans date)
    '''
    if not isinstance(location, AHILocation):
        location = AHILocation(location)
    final_t = tools.Time(start_time)
    train_lon = np.floor(location.longitude * 4)/4.
    # produce list of available bt values for the location specified 
    img_index = pd.date_range(final_t.dt, final_t.dt + datetime.timedelta(minutes=1430),
                              freq='10min')
    img_times = img_index.to_pydatetime()
    
    # iterate by files and append data where available
    bt_data = []

    for t in img_times:
        try:
            a = AHICapture(t)
            b7 = a.band7
            bt = b7.get_single_pixel_value(location)
            bt_data.append(bt)
        except:
            bt_data.append(np.nan)
    
    bt_df = pd.DataFrame({'time_utc': img_times,
                          'time_loc': (img_index + datetime.timedelta(hours=train_lon/15)).to_pydatetime(),
                          'bt': bt_data})
    bt_df = bt_df.dropna()

    if cloud:
        cld_data = []
        for t in bt_df.time_utc:
            a = AHICapture(t)
            cld = a.cld
            if cld:
                csp = cld.get_single_pixel_value(location)
                cld_data.append(csp)
            else:
                cld_data.append(np.nan)
        bt_df['cld'] = cld_data

    bt_df['s_temp'] = (bt_df['bt'] - bt_df['bt'].mean())/bt_df['bt'].std()
    bt_df['time'] = bt_df['time_loc'].dt.time
    return bt_df


def block_bt_stack(locs, start_time, end_time=None, cloud=False, verbose=False):
    """
    Takes a stack of locations and times, and returns a DataFrame with
    AHI brightness temperature information by time.
    """
    try:
        len(locs)
        if isinstance(locs[0], AHILocation):
            train_lon = np.floor(locs[0].longitude * 4) / 4
            ahi_locs = [loc if isinstance(loc, AHILocation) else AHILocation(loc) for loc in locs]
        else:
            train_lon = np.floor(AHILocation(locs[0]).longitude * 4) / 4.
            ahi_locs = [AHILocation(loc) for loc in locs]
    except TypeError:
        if isinstance(locs, AHILocation):
            train_lon = np.floor(locs.longitude * 4) / 4
            ahi_locs = [locs]

    if not isinstance(start_time, tools.Time):
        start_time = tools.Time(start_time)
    if end_time is None:  # default to 24hr period
        end_time = start_time

    img_times = pd.date_range(start_time.dt, end_time.dt + datetime.timedelta(minutes=1430),
                              freq='10min').to_pydatetime()
    bt_df = pd.DataFrame({'time_loc': img_times + datetime.timedelta(hours=train_lon/15)},
                          index=img_times)
    bt_df.index.names = ['time_utc']

    for loc in ahi_locs:
        bt_df['l{}_{}_bt'.format(loc.line, loc.pixel)] = np.nan
        #  bt_df['l{}_{}_cld'.format(loc.line, loc.pixel)] = np.nan
    for t in img_times:
        try:
            a = AHICapture(t)
            b7 = a.band7
            rl = b7.get_list_of_pixel_vals(ahi_locs)
            for i, loc in enumerate(ahi_locs):
                bt_df.at[t, 'l{}_{}_bt'.format(loc.line, loc.pixel)] = rl[i]
            if verbose:
                stdout.write('data temp at {}'.format(t))
                stdout.flush()
        except AttributeError:
            pass
        '''
        try:
            a = AHICapture(t)
            cld = a.cld
            if cld:
                csp = cld.get_list_of_pixel_vals(ahi_locs)
                for i, loc in enumerate(ahi_locs):
                    bt_df.set_value(t, 'l{}_{}_cld'.format(loc.line, loc.pixel), csp[i])
        except:
            pass
        '''
    return bt_df